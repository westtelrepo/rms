# Report Management Service (RMS)

Also known as MIS or MIS 2.0. RMS is apparently already an overloaded term for 9-1-1 so beware.

You will need WUS setup and running to get this to work.

# Development Setup

This uses vagrant. Use `vagrant up` to bring up the development VM. (See `Vagrantfile` for more details. It's a somewhat complex environment so read through it if you have issues.)

# Visual Studio Code Remote

See the same section in `wus`

# Setting up everything

Do everything here inside the vagrant VM (either VS Code remote or in `vagrant ssh`).

Compile `legacy_converter`: `cd ~/legacy_converter && ninja`

Install JS dependencies: `cd /vagrant && yarn install`

Build the frontend: `cd /vagrant/rms_web && yarn build`

Build backend schemas: `cd /vagrant/rms && yarn pre-build` (you'll only need to run this if you change stuff under `rms/src/schema`)

Build backend: `cd /vagrant/rms && yarn build`

Run backend (in non-TLS setup):

```bash
export RMS_ROOT=http://192.168.56.5:8082
export RMS_CLIENT_ID=rms2_insecure
export OPENID_ISS=http://192.168.56.5:8081/oidc/.well-known/openid-configuration
export APP_PROXY=1
cd /vagrant/rms
node .
```

Go to http://192.168.56.5:8082 in your browser, login to WUS if needed.

If you get a "WestTel Portal OIDC Error Page" with "Error: invalid_client" make sure you upserted rms_insecure from the WUS README.

If it is successful the MIS Tenant List should be empty.

# Notable admin endpoints

`/admin` is the main one (`http://192.168.56.5:8082/admin` in full, with the default)

Make sure you go to `/init` and hit `Initialize DB` before doing anything.

`/ssh` lets you add credentials so that RMS can access log files on an SSH server. RMS can also access log files locally, by default it looks in `/vagrant/tmp_logs/${CONTROLLER_ID}` (aka `/vagrant/tmp_logs/WT-001-1`)

`/monitor` is a monitoring endpoint, you can use this to (kinda) see what RMS is doing.

`/db` lets you see info about the DBs managed by RMS, some properties, and also allows you to upgrade their schemas (if needed).

On `/admin` you will want to put a name in the bottom textbox and hit "Create Tenant" to create a tenant/DB. Once created (which may take a little bit of time) you can click the name to go to an admin page.

Most configs are fairly basic, nothing if you are using the default directory. Enterprise setups are more significant (TODO).

`/tenant/$TENANT/ali_editor` is somewhat useful if you are figuring out new ALI formats.

# TLS notes

export RMS_ROOT=https://rms.test.westtel.com
export RMS_CLIENT_ID=rms2
export OPENID_ISS=https://wus.test.westtel.com/oidc/.well-known/openid-configuration
export APP_PROXY=1
