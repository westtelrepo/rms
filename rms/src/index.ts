import koa_router from "@koa/router";
import Ajv from "ajv";
import node_assert from "assert";
import crypto from "crypto";
import escape_html from "escape-html";
import fs from "fs";
import Immutable from "immutable";
import { JWKS } from "jose";
import koa, { ParameterizedContext } from "koa";
import koa_bodyparser from "koa-bodyparser";
import helmet from "koa-helmet";
import { DateTime } from "luxon";
import { Client, custom, Issuer } from "openid-client";
import QueryStream from "pg-query-stream";
import zlib from "zlib";

import { INDEXJS_PATH } from "./ci_const";
import * as data from "./db/data_migrations";
import * as meta from "./db/meta_migrations";
import { get_tenant_db } from "./db/pgp_tenant";
import { get_schema_version_t } from "./db/schema";
import * as tenant from "./db/tenant";
import {
    IKoaState,
    OPENID_ISS,
    OPENID_ISS_ORIGIN,
    pgp,
    RMS_CLIENT_ID,
    RMS_OPENID_CB,
    RMS_ROOT,
    RMS_SECURE,
} from "./globals";
import { legacy_convert } from "./legacy_converter";
import {
    assert_auth,
    csrf_route,
    decrypt_rms,
    logout_route,
    openid_cb,
    openid_refresh,
    redirect_auth,
} from "./openid";
import { keyscan } from "./ssh";
import { TaskRunner } from "./task";
import { tenant_router } from "./tenant_router";

const assert: (x: unknown) => asserts x = node_assert;

const openid_jwks = new JWKS.KeyStore();
openid_jwks.generateSync("OKP", "Ed25519", { use: "sig" });

custom.setHttpOptionsDefaults({
    timeout: 5000,
    headers: { "User-Agent": undefined },
});

let check_session_iframe: string | undefined;
let client: Client | null = null;
(async function () {
    while (true) {
        try {
            const issuer = await Issuer.discover(OPENID_ISS);
            check_session_iframe = issuer.metadata.check_session_iframe as
                | string
                | undefined;
            client = new issuer.Client(
                {
                    client_id: RMS_CLIENT_ID,
                    redirect_uris: [RMS_OPENID_CB],
                    token_endpoint_auth_method: "private_key_jwt",
                },
                openid_jwks.toJWKS(true),
            );
            client[custom.clock_tolerance] = 3;
            return;
        } catch (e) {
            console.log("got error making openid client", e);
            await new Promise(resolve => setTimeout(resolve, 5000));
        }
    }
})().catch(e => {
    console.error("Major issue trying to get openid info: ", e);
});

const db = pgp<unknown>({});

import schema from "./schema/json/log.schema.json";

async function check_all() {
    console.log("running check_all");
    const ajv = new Ajv();
    const validate = ajv.compile(schema);
    const tenants = (await tenant.list_tenants(db))
        .map(x => ({ x, r: Math.random() }))
        .sort((a, b) => a.r - b.r)
        .map(x => x.x);

    for (const t of tenants) {
        console.log("checking", t);
        const tenant_db = await get_tenant_db(db, t);

        const qs = new QueryStream("SELECT entry FROM private.logs");
        const result = await tenant_db.stream(qs, stream => {
            (async () => {
                for await (const chunk of stream) {
                    const result = validate((chunk as any).entry);
                    assert(typeof result === "boolean");
                    if (!result) {
                        console.error(
                            "not valid!",
                            validate.errors,
                            (chunk as any).entry,
                        );
                    }
                }
            })().catch(e => console.error("Error in check_all", e));
        });
        console.log("checked: ", t, result);
    }
}
// check_all().catch(e => console.log("error in check_all", e));

const app = new koa<IKoaState>();
if (process.env.APP_PROXY === "1") {
    app.proxy = true;
}
const router = new koa_router<IKoaState, koa.DefaultContext>();
const task_runner = new TaskRunner();

router.use(
    koa_bodyparser({
        enableTypes: ["json", "text"],
    }),
);

// load and fingerprint file
const indexjs_file = fs.readFileSync(INDEXJS_PATH);
const indexjs_sha256 = crypto
    .createHash("sha256")
    .update(indexjs_file)
    .digest()
    .toString("hex");

// compress the index.js file
// do it asynchronously because it takes a couple seconds (especially for brotli)
let indexjs_gzip: Buffer | null = null;
let indexjs_br: Buffer | null = null;

// only compress in production
if (process.env.NODE_ENV === "production") {
    zlib.gzip(indexjs_file, (e, result) => {
        if (!e) {
            indexjs_gzip = result;
        } else {
            console.log("Could not gzip index.js", e);
        }
    });

    zlib.brotliCompress(indexjs_file, (e, result) => {
        if (!e) {
            indexjs_br = result;
        } else {
            console.log("Could not brotli compress index.js", e);
        }
    });
}

router.get(
    [
        "/",
        "/admin",
        "/admin/(.*)",
        "/tenant/(.*)",
        "/old_view",
        "/init",
        "/ssh",
        "/db",
        "/monitor",
        "/monitor/(.*)",
    ],
    redirect_auth,
    async (ctx: ParameterizedContext<IKoaState>) => {
        // TODO: technically redundant
        const tokenset = await decrypt_rms(ctx);
        if (tokenset === null) {
            ctx.throw(403);
        }

        ctx.set("Content-Type", "text/html; charset=utf8");

        const openid = JSON.stringify({
            sub: tokenset.sub,
            check_session_iframe,
            session_state: tokenset.session_state,
            preferred_username: tokenset.preferred_username,
            client_id: RMS_CLIENT_ID,
            openid_iss_origin: OPENID_ISS_ORIGIN,
        });

        // only use fingerprinted file in production
        const filename =
            process.env.NODE_ENV === "production"
                ? `/js/index.${indexjs_sha256}.js`
                : "/js/index.js";
        ctx.body = `<!DOCTYPE html>
<html>
    <head>
        <title>WestTel – Metrics &amp; Reporting</title>
        <style nonce="${ctx.state.style_nonce}">#openid {display: none};</style>
        <script nonce="${ctx.state.nonce}">window.__webpack_nonce__ = "${
            ctx.state.style_nonce
        }"</script>
        <script type="module" src="${filename}" nonce="${
            ctx.state.nonce
        }"></script>
    </head>
    <body>
        <div id="root"></div>
        <div id="openid">${escape_html(openid)}</div>
    </body>
</html>`;
    },
);

router.get("/js/index.js", async ctx => {
    ctx.set("Content-Type", "application/javascript; charset=utf-8");

    // make things a little faster in development, use Last-Modified header
    ctx.status = 200;
    ctx.lastModified = fs.statSync(INDEXJS_PATH).mtime;
    if (ctx.fresh) {
        ctx.status = 304;
        ctx.body = null;
    } else {
        ctx.body = fs.createReadStream(INDEXJS_PATH);
    }
});

router.get("/svg/WestTel.svg", async ctx => {
    ctx.set("Content-Type", "image/svg+xml");
    // TODO: set an appropriate CSP, this image has inline styles
    // TODO: caching
    ctx.remove("content-security-policy");
    const LOGO = "./WestTel_Logo.svg";
    ctx.status = 200;
    ctx.lastModified = fs.statSync(LOGO).mtime;
    if (ctx.fresh) {
        ctx.status = 304;
        ctx.body = null;
    } else {
        ctx.body = fs.createReadStream(LOGO);
    }
});

if (process.env.NODE_ENV !== "production") {
    router.get("/js/index.js.map", async ctx => {
        ctx.body = fs.createReadStream(INDEXJS_PATH + ".map");
    });

    // IIFE for testing only (because Edge does not report syntax errors
    // for modules!)
    router.get("/js/index.iife.js", async ctx => {
        ctx.set("Content-Type", "application/javascript; charset=utf-8");
        ctx.body = fs.createReadStream("../rms_web/index.iife.js");
    });

    router.get("/js/index.iife.js.map", async ctx => {
        ctx.body = fs.createReadStream("../rms_web/index.iife.js.map");
    });
}

function str_or_assert(x: any): string {
    assert(typeof x === "string");
    return x;
}

router.get("/js/:fingerprint", async ctx => {
    const fingerprint = str_or_assert(ctx.params.fingerprint);

    if (fingerprint === `index.${indexjs_sha256}.js`) {
        ctx.set("Content-Type", "application/javascript; charset=utf-8");
        // cache for a year. If file changes, filename changes
        ctx.set("Cache-Control", "max-age=31536000, immutable");

        // immutable is usually only supported on secure origins
        // use an ETag to help non-secure origins
        ctx.status = 200;
        ctx.etag = indexjs_sha256;
        if (ctx.fresh) {
            ctx.status = 304;
            ctx.body = null;
        } else {
            // brotli is smaller, so try it first
            if (indexjs_br && ctx.acceptsEncodings("br") === "br") {
                ctx.set("Content-Encoding", "br");
                ctx.body = indexjs_br;
            } else if (
                indexjs_gzip &&
                ctx.acceptsEncodings("gzip") === "gzip"
            ) {
                ctx.set("Content-Encoding", "gzip");
                ctx.body = indexjs_gzip;
            } else if (ctx.acceptsEncodings("identity") === "identity") {
                ctx.body = indexjs_file;
            } else {
                ctx.throw(406); // 406 Not Acceptable
            }
        }
    } else {
        ctx.assert(false, 404);
    }
});

router.use(
    "/api/tenant",
    tenant_router.routes(),
    tenant_router.allowedMethods(),
);

async function all_tenant_refresh(forced: boolean) {
    const monitor_promise = get_monitoring();

    const tenants = await tenant.list_tenants(db);
    const controller_list = [];
    for (const t of tenants) {
        const tenant_db = await get_tenant_db(db, t);
        const c = await data.list_controllers(tenant_db);
        for (const e of c) {
            controller_list.push({
                tenant: t,
                tenant_db,
                ...e,
            });
        }
    }

    const monitor = await monitor_promise;

    // we run the oldest tenant/controller data first
    // if it has never been updated, we do it first
    const clist = Immutable.List(controller_list)
        .map(e => {
            const legacy = monitor.find(
                x => x.controller === e.controller && x.tenant === e.tenant,
            );
            return {
                ...e,
                legacy_last_successful_import: legacy
                    ? legacy.legacy_last_successful_import ?? 0
                    : 0,
            };
        })
        .sortBy(x => x.legacy_last_successful_import);

    const all_tasks = task_runner.all_tasks;
    for (const c of clist) {
        if (
            !all_tasks.some(
                e => e.tenant === c.tenant && e.controller === c.controller,
            )
        ) {
            task_runner.push({
                tenant: c.tenant,
                controller: c.controller,
                forced: false,
                run: async () =>
                    legacy_convert({
                        controller: c.controller,
                        db: c.tenant_db,
                        forced,
                        metadb: db,
                    }),
            });
        }
    }
}

if (process.env.RMS_AUTOREFRESH !== undefined) {
    setInterval(() => {
        console.log("start autorefresh");
        all_tenant_refresh(false).catch(e =>
            console.error("Major issue during autorefresh: ", e),
        );
    }, 60 * 60 * 1000);
}

router.post("/api/all_tenant_refresh", assert_auth(false), async ctx => {
    await all_tenant_refresh(true);
    ctx.body = null;
});

// THIS IS A MAJOR DEBUG ROUTE
router.post("/api/recreate_all_tenants", assert_auth(false), async ctx => {
    // eslint-disable-next-line @typescript-eslint/require-array-sort-compare
    const tenants = (await tenant.list_tenants(db)).sort();

    for (const t of tenants) {
        console.log("Recreating", t);
        console.time("recreate " + t);
        const tenant_db = await get_tenant_db(db, t);
        const controller_list = await data.list_controllers(tenant_db);

        await tenant.create_tenant(db, "tmp");
        const new_db = await get_tenant_db(db, "tmp");
        for (const c of controller_list) {
            await data.upsert_controller(new_db, c.controller, c.config);
        }

        await tenant.drop_tenant(db, t);
        await tenant.rename_tenant(db, "tmp", t);

        console.timeEnd("recreate " + t);
    }
    console.log("DONE!");
    ctx.body = null;
});

async function corona_report() {
    // eslint-disable-next-line @typescript-eslint/require-array-sort-compare
    const tenants = (await tenant.list_tenants(db)).sort();

    const yesterday = DateTime.utc().startOf("day");
    const y1 = yesterday.minus({ days: 30 });
    const y2 = y1.minus({ days: 30 });
    const y3 = y2.minus({ days: 30 });

    const periods: [string, string][] = [
        [y3.toISO(), y2.toISO()],
        [y2.toISO(), y1.toISO()],
        [y1.toISO(), yesterday.toISO()],
    ];

    const ret: any[] = [];

    for (const t of tenants) {
        const tenant_db = await get_tenant_db(db, t);
        const controller_list = await data.list_controllers(tenant_db);

        for (const c of controller_list) {
            const r = [];
            r.push(t);
            r.push(c.controller);
            for (const p of periods) {
                r.push(
                    (
                        await tenant_db.one<{ count: string }>(
                            "SELECT COUNT(*) FROM private.xfer_info WHERE controller = $(c) AND utc >= $(p1) AND utc < $(p2) AND is_911 AND transfer_type = 'initial' AND incoming",
                            {
                                c: c.controller,
                                p1: p[0],
                                p2: p[1],
                            },
                        )
                    ).count,
                );
            }
            ret.push(r);
        }
    }

    return { ret, periods };
}

router.get("/api/corona_report", assert_auth(false), async ctx => {
    ctx.body = await corona_report();
});

router.post("/api/vacuum_all", assert_auth(false), async ctx => {
    const tenants = await tenant.list_tenants(db);
    for (const t of tenants) {
        console.log("Vacuum tenant", t);
        const tenant_db = await get_tenant_db(db, t);
        await tenant_db.none("VACUUM");
    }
    ctx.body = null;
});

router.post("/api/init_meta_db", assert_auth(false), async ctx => {
    await meta.migrate({ db });
    ctx.body = null;
});

router.get("/api/tasks", assert_auth(true), async ctx => {
    ctx.body = {
        running: task_runner.running_tasks,
        queued_tasks: task_runner.queued_tasks,
    };
});

router.post("/api/ssh/keyscan", assert_auth(false), async ctx => {
    assert(ctx.is("text/plain"));
    assert(ctx.accepts("json"));
    const host = str_or_assert(ctx.request.body);
    ctx.assert(typeof host === "string", 400);

    const keys = await keyscan(host, 22);
    const res: { kt: string; hash: string }[] = [];
    for (const e of keys) {
        const str = e[1].toString("base64");
        res.push({
            kt: e[0],
            hash: str.endsWith("=") ? str.substr(0, str.length - 1) : str,
        });
    }

    ctx.body = res;
});

router.get("/api/ssh/creds", assert_auth(false), async ctx => {
    ctx.body = await meta.get_ssh_credentials(db);
});

router.post("/api/ssh/cred", assert_auth(false), async ctx => {
    assert(ctx.is("json"));
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const body = ctx.request.body;
    await meta.set_ssh_credential(
        db,
        body.host,
        body.port,
        body.username,
        body.cred,
    );
    ctx.body = null;
});

router.get("/api/ssh/servers", assert_auth(false), async ctx => {
    assert(ctx.accepts("json"));
    const result = await meta.get_ssh_servers(db);
    const res: {
        host: string;
        port: number;
        hashes: { kt: string; hash: string }[];
    }[] = [];
    for (const e of result) {
        let add_to = res.find(
            elem => e.host === elem.host && e.port === elem.port,
        );
        if (add_to === undefined) {
            add_to = { host: e.host, port: e.port, hashes: [] };
            res.push(add_to);
        }
        const str = e.hash.toString("base64");
        add_to.hashes.push({
            kt: e.kt,
            hash: str.endsWith("=") ? str.substr(0, str.length - 1) : str,
        });
    }
    ctx.body = res;
});

router.post("/api/ssh/servers", assert_auth(false), async ctx => {
    assert(ctx.is("json"));
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const server = ctx.request.body;

    await meta.set_ssh_server(
        db,
        server.host,
        server.port,
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        server.hashes.map((e: any) => ({
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            kt: e.kt,
            hash: Buffer.from(e.hash, "base64"),
        })),
    );

    ctx.body = null;
});

router.get("/api/db", assert_auth(false), async ctx => {
    const tenants = await tenant.list_tenants_info(db);
    const ret: any[] = [];
    for (const e of tenants) {
        const d = await get_tenant_db(db, e.name);
        const xfer_size = await d.one<{ size: string; internal_size: string }>(
            "SELECT pg_size_pretty(pg_total_relation_size(relid)) AS size, pg_size_pretty(pg_relation_size(relid)) AS internal_size FROM pg_catalog.pg_statio_user_tables WHERE schemaname = 'private' AND relname = 'xfer_info'",
        );
        ret.push({
            ...e,
            schema_version: await d.task(async t => get_schema_version_t(t)),
            xfer_info_total_relation_size: xfer_size.size,
            xfer_info_relation_size: xfer_size.internal_size,
        });
    }
    ctx.body = ret;
});

router.post("/api/db/migrate_all_tenants", assert_auth(false), async ctx => {
    const tenants = await tenant.list_tenants(db);
    for (const t of tenants) {
        console.log("migrating", t);
        const d = await get_tenant_db(db, t);
        await data.migrate({ db: d });
    }
    ctx.body = null;
});

async function get_monitoring(): Promise<
    {
        tenant: string;
        controller: string;
        legacy_last_call: number | null;
        legacy_last_log: number | null;
        legacy_last_successful_import: number | null;
        legacy_interval: number | null;
    }[]
> {
    const tenants = await tenant.list_tenants(db);
    const ret = [];
    for (const t of tenants) {
        const d = await get_tenant_db(db, t);
        const arr = await data.list_monitoring(d);
        for (const e of arr) {
            ret.push({ tenant: t, ...e });
        }
    }
    return ret;
}

router.get("/api/monitor", assert_auth(false), async ctx => {
    ctx.body = await get_monitoring();
});

router.get("/api/monitor_ali", assert_auth(false), async ctx => {
    const tenants = await tenant.list_tenants(db);
    const ret: any[] = [];
    for (const t of tenants) {
        const d = await get_tenant_db(db, t);
        ret.push({
            tenant: t,
            ali_unparsed_count: (
                await d.one<{ count: string }>(
                    "SELECT count(*) FROM private.ali_info WHERE ali_parsed IS NULL",
                )
            ).count,
        });
    }
    ctx.body = ret;
});

router.get("/api/csrf", csrf_route);

// it all starts here
// TODO: error page
app.use(async (ctx, next) => {
    const begin = process.hrtime();
    ctx.set("Cache-Control", "no-cache, max-age=0"); // default cache header

    await next();
    const interval = process.hrtime(begin);
    ctx.set(
        "X-Response-Time",
        String(interval[0] * 1e3 + interval[1] * 1e-6) + " ms",
    );
});

app.use(async (ctx, next) => {
    if (ctx.URL.origin !== new URL(RMS_ROOT).origin) {
        ctx.throw(400, "wrong origin");
    }

    // set state
    ctx.state.nonce = crypto.randomBytes(16).toString("base64");
    ctx.state.style_nonce = crypto.randomBytes(16).toString("base64");
    if (client) {
        ctx.state.client = client;
    } else {
        ctx.throw("OpenID not setup yet");
    }
    ctx.state.db = db;
    ctx.state.task_runner = task_runner;

    const rms = await decrypt_rms(ctx);
    if (rms) {
        ctx.state.access_token = rms.access_token;
        try {
            // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
            ctx.state.is_superadmin = !!(
                await ctx.state.client.userinfo(rms.access_token)
            )["http://westtel.com/superadmin"];
        } catch (e) {
            console.log("error setting superadmin: ", e);
        }
    } else {
        ctx.state.is_superadmin = false;
        ctx.state.access_token = undefined;
    }
    await next();
});

function make_helmet(allow_frames: boolean) {
    return async function allow_helmet(
        ctx: koa.ParameterizedContext<IKoaState>,
        next: () => Promise<any>,
    ) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return helmet({
            permittedCrossDomainPolicies: { permittedPolicies: "none" },
            dnsPrefetchControl: { allow: false },
            frameguard: { action: allow_frames ? "sameorigin" : "deny" },
            referrerPolicy: { policy: "no-referrer" },
            contentSecurityPolicy: {
                directives: {
                    baseUri: ["'none'"],
                    blockAllMixedContent: [],
                    connectSrc: ["'self'"],
                    defaultSrc: ["'none'"],
                    fontSrc: ["'none'"],
                    formAction: ["'none'"],
                    frameAncestors: allow_frames ? ["'self'"] : ["'none'"],
                    frameSrc: ["'self'", OPENID_ISS_ORIGIN],
                    imgSrc: ["'self'"], // because favicon
                    manifestSrc: ["'none'"],
                    mediaSrc: ["'none'"],
                    objectSrc: ["'none'"],
                    pluginTypes: ["'none'"],
                    prefetchSrc: ["'self'", OPENID_ISS_ORIGIN],
                    // browsers that recognize strict-dynamic will disable self and unsafe-inline
                    // those that recognize nonce will disable unsafe-inline
                    scriptSrc: [
                        "'self'",
                        "'strict-dynamic'",
                        `'nonce-${ctx.state.nonce}'`,
                        "'unsafe-inline'",
                    ],
                    // emotion.js also needs this nonce
                    styleSrc: [`'nonce-${ctx.state.style_nonce}'`],
                    workerSrc: ["'none'"],
                    sandbox: [
                        "allow-modals",
                        "allow-scripts",
                        "allow-same-origin",
                        "allow-downloads",
                    ],
                    // destroy DOM XSS in browsers that support it (Chrome)
                    requireTrustedTypesFor: ["'script'"],
                    trustedTypes: [],
                },
            },
            hsts: RMS_SECURE
                ? {
                      maxAge: 31536000,
                      includeSubDomains: true,
                  }
                : false,
            expectCt: RMS_SECURE ? { enforce: true, maxAge: 31536000 } : false,
        })(ctx, next);
    };
}

app.use(make_helmet(false));

router.get("/openid/cb", make_helmet(true), openid_cb);
router.get("/openid/refresh", make_helmet(true), openid_refresh);
router.get("/logout", logout_route);

router.get("/openid/jwks", async ctx => {
    ctx.body = openid_jwks.toJWKS();
});

app.use(router.routes()).use(router.allowedMethods());

const PORT = process.env.PORT !== undefined ? Number(process.env.PORT) : 8080;

app.listen(PORT);
