DROP SCHEMA public;

CREATE SCHEMA private; -- only rms has access to this
CREATE SCHEMA ro; -- RO roles have RO access to this

CREATE TABLE logs (
    id UUID PRIMARY KEY,
    controller TEXT NOT NULL,
    entry JSONB NOT NULL,
    legacy BOOLEAN NOT NULL,
    utc TIMESTAMPTZ NOT NULL,
    callid UUID,
    CHECK(id = (entry->>'id')::uuid),
    CHECK(callid IS NOT DISTINCT FROM (entry->'call'->>'id')::uuid)
);

ALTER TABLE logs ALTER COLUMN callid SET (n_distinct = -0.11); -- TODO: better estimate

CREATE INDEX ON logs(controller, utc);
CREATE INDEX ON logs(callid);

CREATE TABLE call_info (
    callid UUID NOT NULL,
    xferid UUID NOT NULL,
    incoming boolean NOT NULL,
    PRIMARY KEY(callid, xferid, incoming),
    legacy BOOLEAN NOT NULL
    -- data here
);

CREATE TABLE config (
    id TEXT PRIMARY KEY,
    value JSONB NOT NULL
);
-- reimport legacy on change:
-- "legacy" config as last imported

-- reimport individual file on size change
CREATE TABLE log_sizes (
    filename TEXT PRIMARY KEY,
    size BIGINT NOT NULL,
    legacy BOOLEAN NOT NULL
);

CREATE TABLE views (
    name TEXT PRIMARY KEY,
    config JSONB NOT NULL
);

CREATE TABLE ro_roles (
    name TEXT PRIMARY KEY
);

-- function to_cdr_uri(callid UUID) -> TEXT

-- database rms_metadata

CREATE USER rms WITH PASSWORD 'rms' CREATEDB CREATEROLE;

CREATE TABLE managed_database (
    name TEXT PRIMARY KEY
);

CREATE TABLE controller (
    name TEXT PRIMARY KEY,
    config JSONB NOT NULL -- includes legacy config
);

CREATE TABLE database_with_controller (
    database TEXT NOT NULL REFERENCES managed_database,
    controller TEXT NOT NULL REFERENCES controller,
    PRIMARY KEY(database, controller)
);
