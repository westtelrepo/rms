import node_assert from "assert";
import { IDatabase } from "pg-promise";

import { parse_ali } from "./ali";
import { ascii_plus_to_base64 } from "./legacy_converter";
import { assert_is_log, cmp_log, LogJSON, to_utc } from "./schema/log";

const assert: (cond: any, msg?: string) => asserts cond = node_assert;

function redact_log(log: LogJSON) {
    if (log.ali?.record?.base64 !== undefined) {
        log.ali.record.base64 = ascii_plus_to_base64("<REDACTED>");
    }

    if (log.txt_msg !== undefined) {
        log.txt_msg = "<REDACTED>";
    }
}

interface WithCallID {
    call: {
        id: string;
    };
}

function assert_is_log_with_callid_array(
    x: unknown[],
): asserts x is (WithCallID & LogJSON)[] {
    x.forEach(e => {
        assert_is_log(e);
        assert(e.call && typeof e.call.id === "string");
    });
}

export async function get_cdr(
    db: IDatabase<unknown>,
    callid: string,
    redacted: boolean = false,
) {
    const logs = (
        await db.any<{ x: unknown }>(
            "SELECT entry AS x FROM private.logs WHERE callid = $(callid)",
            { callid },
        )
    ).map(x => x.x);

    assert_is_log_with_callid_array(logs);

    if (logs.length === 0) {
        // TODO: probably better way of doing this
        const e: any = new Error("CDR not found");
        e.expose = true;
        e.status = 404;
        throw e;
    }

    const controller = logs[0].controller;

    if (redacted) {
        for (const e of logs) {
            redact_log(e);
        }
    }

    // TODO: better sort function
    logs.sort((a, b) => cmp_log(a, b));

    let controller_callid: string | undefined;
    let ani: string | undefined;
    let pani: string | undefined;
    let sip: string | undefined;
    let trunk: number | undefined;
    let call_initiation: number | undefined;
    let call_answered: number | undefined;
    let call_disconnect: number | undefined;
    let call_abandoned: number | undefined;
    let abandoned = false;
    let initial_participant: undefined | { num?: number; calltaker?: string };
    for (const e of logs) {
        if (typeof e.oldlog.callid === "string") {
            controller_callid = e.oldlog.callid;
        }

        if (typeof e.call.ani === "string") {
            ani = e.call.ani;
        }

        if (typeof e.call.pani === "string") {
            pani = e.call.pani;
        }

        if (typeof e.call.trunk === "number") {
            trunk = e.call.trunk;
        }

        if (typeof e.call.sip === "string") {
            sip = e.call.sip;
        }

        if (
            e.transfer &&
            e.transfer.type === "initial" &&
            e.transfer.state === "dial"
        ) {
            call_initiation = to_utc(e.t.ut);
        }

        if (
            e.transfer &&
            e.transfer.type === "initial" &&
            e.transfer.state === "join"
        ) {
            call_answered = to_utc(e.t.ut);
            initial_participant = {
                num: e.user?.position,
                calltaker: e.user?.name,
            };
        }

        if (
            e.transfer &&
            e.transfer.type === "initial" &&
            e.transfer.state === "abandon"
        ) {
            abandoned = true;
            call_abandoned = to_utc(e.t.ut);
        }

        if (e.code.startsWith("call/disconnect/")) {
            call_disconnect = to_utc(e.t.ut);
        }
    }

    let utc_begin: number | undefined;
    if (logs.length > 0) {
        utc_begin = to_utc(logs[0].t.ut);
    }

    if (typeof ani === "string" && typeof pani === "string" && ani === pani) {
        // if the ANI and pANI are equal than the pANI is not a real pANI
        pani = undefined;
    }

    function create_message(
        x: LogJSON & WithCallID,
    ): { desc: string; desc_extended?: any } {
        if (typeof x.code !== "string") {
            throw new Error("log must have code");
        }

        let desc_extended: any;

        const base64 = x.ali?.record?.base64;
        if (typeof base64 === "string") {
            desc_extended = {
                base64,
                line_split: (parse_ali({ base64 }) ?? {}).line_split!,
            };
        } else if (x.tdd && typeof x.tdd.conversation_record === "string") {
            desc_extended = x.tdd.conversation_record;
        }

        let desc = x.d.msg;

        const name = x.user?.name;
        const position = x.user?.position;
        const destination = x.call.destination;

        // message rewriting
        switch (x.code) {
            case "call/ringing/":
                const trunk = x.call.trunk;
                const pani = x.call.pani;
                const sip = x.call.sip;
                desc = `Call ringing${
                    typeof trunk === "number" ? ` on trunk ${trunk}` : ""
                }${typeof pani === "string" ? ` from pANI ${pani}` : ""}${
                    typeof x.call.ani === "string"
                        ? ` from ANI ${x.call.ani}`
                        : ""
                }${typeof sip === "string" ? ` from SIP ${sip}` : ""}`;
                break;
            case "abandoned/takeover/":
                desc = `Abandoned call taken control by ${
                    name !== undefined ? `user ${name}` : "unknown user"
                } on ${
                    position !== undefined
                        ? `position ${position}`
                        : "unknown position"
                }`;
                break;
            case "call/connect/":
                desc = `Call answered${
                    name !== undefined ? ` by user ${name}` : ""
                }${position !== undefined ? ` at position ${position}` : ""}`;
                break;
            case "call/leave/":
                desc = `Conference Leave${
                    name !== undefined ? ` by user ${name}` : ""
                }${position !== undefined ? ` at position ${position}` : ""}`;
                break;
            case "call/disconnect/":
                desc = `Disconnect${
                    name !== undefined ? ` by user ${name}` : ""
                }${position !== undefined ? ` at position ${position}` : ""}`;
                break;
            case "call/outbound/":
                desc = `Call dialed${
                    name !== undefined ? ` by user ${name}` : ""
                }${position !== undefined ? ` at position ${position}` : ""}${
                    destination !== undefined ? ` to ${destination}` : ""
                }`;
                break;
            case "call/join/":
                desc = `Conference Join${
                    name !== undefined ? ` by user ${name}` : ""
                }${position !== undefined ? ` at position ${position}` : ""}`;
                break;
            case "transfer/dial/tandem/gui/":
                desc = `Tandem Transfer from GUI${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/dial/ng911/gui/":
                desc = `NG911 SIP Transfer from GUI${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/join/ng911/gui/":
                desc = `NG911 SIP Transfer from GUI Connected${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/dial/conference/gui/":
                desc = `Conference Transfer from GUI${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/join/conference/gui/":
                desc = `Conference Transfer from GUI Connected${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/dial/attended/phone/":
                desc = `Attended Transfer from Phone Dialed${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/dial/blind/phone/":
                desc = `Blind Transfer from Phone Dialed${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/join/attended/unattended/":
                desc = `Unattended Transfer from Phone Connected${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
            case "transfer/join/blind/phone/":
                desc = `Blind Transfer from Phone Connected${
                    name !== undefined ? ` by user ${name}` : ""
                }${
                    position !== undefined ? ` at position ${position}` : ""
                } to ${x.transfer?.destination ?? "unknown destination"}`;
                break;
        }

        return {
            desc,
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            desc_extended,
        };
    }

    return {
        ani,
        pani,
        sip,
        trunk,
        callid,
        controller_callid,
        redacted,
        controller,
        psaps_mentioned: [
            ...new Set(
                logs
                    .map(e => e.user?.psap)
                    .filter((e): e is string => typeof e === "string"),
            ),
        ],
        is_911: logs.some(e => e.call.is_911),
        events: logs.map(e => ({
            utc: to_utc(e.t.ut),
            t: to_utc(e.t.ut) - utc_begin!,
            id: e.id,
            desc: create_message(e),
            code: e.code,
            log: e.x,
            call_initiation:
                e.transfer &&
                e.transfer.type === "initial" &&
                e.transfer.state === "dial"
                    ? true
                    : undefined,
            call_answered:
                e.transfer &&
                e.transfer.type === "initial" &&
                e.transfer.state === "join"
                    ? true
                    : undefined,
            call_disconnect: e.code.startsWith("call/disconnect/")
                ? true
                : undefined,
            call_abandoned:
                e.transfer &&
                e.transfer.type === "initial" &&
                e.transfer.state === "abandon"
                    ? true
                    : undefined,
        })),
        call_initiation,
        call_answered,
        call_disconnect,
        abandoned,
        call_abandoned,
        initial_participant,
    };
}
