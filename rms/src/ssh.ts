import ssh2 from "ssh2";
import uuid from "uuid-1345";

export const key_types = [
    "ssh-rsa",
    "ecdsa-sha2-nistp256",
    "ecdsa-sha2-nistp384",
    "ecdsa-sha2-nistp521",
    "ssh-ed25519",
];

export async function keyscan(host: string, port: number) {
    const hashes = new Map<string, Buffer>();

    for (const kt of key_types) {
        const client = new ssh2.Client();
        await new Promise(resolve => {
            client.on("error", () => {
                // ignore the error, we will always get one
                resolve();
            });
            client.connect({
                host,
                port,
                algorithms: {
                    serverHostKey: [kt],
                },
                // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                hostHash: "sha256" as any,
                hostVerifier: hash => {
                    hashes.set(kt, Buffer.from(hash, "hex"));
                    resolve();
                    return false;
                },
                username: uuid.v4(),
                password: uuid.v4(),
            });
        });
    }

    return hashes;
}
