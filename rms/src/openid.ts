/* eslint-disable @typescript-eslint/ban-types */

import crypto from "crypto";
import escape_html from "escape-html";
import koa, { ParameterizedContext } from "koa";
import { errors, generators } from "openid-client";
import {
    ConsumeOptions,
    errors as pasetoerros,
    ProduceOptions,
    V2,
} from "paseto";

import {
    IKoaState,
    RMS_OPENID_CB,
    RMS_ROOT,
    RMS_SECURE,
    RMS_SUPERADMIN_TOKEN,
} from "./globals";

/**
 * Encapsulates a key and a type and allows encrypting/decrypting objects of that type.
 */
class Encryptor<T extends object> {
    constructor(
        private readonly key: crypto.KeyObject | Buffer = crypto.randomBytes(
            32,
        ),
    ) {}

    public async decrypt(
        str: string | null | undefined,
        options?: ConsumeOptions<false>,
    ): Promise<T | null> {
        if (typeof str !== "string") {
            return null;
        }

        try {
            return (await V2.decrypt(str, this.key, options)) as T;
        } catch (e) {
            if (e instanceof pasetoerros.PasetoError) {
                return null;
            } else {
                throw e;
            }
        }
    }

    public async encrypt(obj: T, options?: ProduceOptions): Promise<string> {
        return V2.encrypt(obj, this.key, options);
    }
}

// invalidating these does not log out users, though a client side OpenID refresh will be performed
// however, logins during a restart will fail
const login_state_key = new Encryptor<ILoginState>();
const openid_state_key = new Encryptor<IOpenIDState>();
const rms_key = new Encryptor<IRMSState>();
const csrf_key = new Encryptor<ICSRFKey>();

const RMS_COOKIE = RMS_SECURE ? "__Host-rms" : "rms";
const CSRF_COOKIE = RMS_SECURE ? "__Host-csrf" : "csrf";
const LOGIN_STATE_COOKIE_PREFIX = RMS_SECURE ? "__Host-openid-" : "openid-";

export async function decrypt_rms(ctx: koa.ParameterizedContext<IKoaState>) {
    return rms_key.decrypt(ctx.cookies.get(RMS_COOKIE));
}

export async function logout_route(ctx: koa.ParameterizedContext<IKoaState>) {
    const rms = await decrypt_rms(ctx);
    ctx.cookies.set(RMS_COOKIE, undefined as any, { sameSite: "lax" });
    const id_token =
        rms && rms.sub === ctx.query.sub ? rms.id_token : undefined;
    ctx.redirect(ctx.state.client.endSessionUrl({ id_token_hint: id_token }));
}

interface ILoginState {
    code_verifier: string;
    nonce: string;
    state: string;
}

interface IOpenIDState {
    which: string;
    url?: string;
    state: string;
}

interface IRMSState {
    access_token: string;
    sub: string;
    session_state?: string;
    preferred_username?: string;
    id_token: string;
}

interface ICSRFKey {
    csrf: string;
    sub: string;
    session_state?: string;
}

/** does not check for CSRF */
export async function redirect_auth(
    ctx: koa.ParameterizedContext<IKoaState, unknown>,
    next: () => Promise<any>,
) {
    const tokenset = await rms_key.decrypt(ctx.cookies.get(RMS_COOKIE));

    const c = ctx.state.client;
    const intro =
        tokenset !== null
            ? await c.introspect(tokenset.access_token, "access_token")
            : null;

    if (intro === null || !intro.active) {
        const which = generators.random(16);
        const state = await openid_state_key.encrypt({
            url: ctx.url,
            state: generators.state(),
            which,
        });
        const login_state = {
            code_verifier: generators.codeVerifier(),
            nonce: generators.nonce(),
            state,
        };

        ctx.cookies.set(
            LOGIN_STATE_COOKIE_PREFIX + which,
            await login_state_key.encrypt(login_state, {
                expiresIn: "1 hour",
            }),
            { maxAge: 30 * 60 * 1000 },
        );
        ctx.redirect(
            c.authorizationUrl({
                scope: "openid profile",
                code_challenge: generators.codeChallenge(
                    login_state.code_verifier,
                ),
                code_challenge_method: "S256",
                nonce: login_state.nonce,
                state,
            }),
        );
    } else {
        await next();
    }
}

function set_refresh_403(ctx: koa.Context): void {
    ctx.set("X-403-Reason", "refresh");
    ctx.body = "requires openid refresh";
    ctx.status = 403;
}

/** also checks for CSRF */
export function assert_auth(allow_non_superadmin: boolean) {
    return async function (
        ctx: koa.ParameterizedContext<IKoaState, unknown>,
        next: () => Promise<any>,
    ) {
        if (typeof RMS_SUPERADMIN_TOKEN === "string") {
            const [type, token] = ctx.get("Authorization").split(" ");
            if (type.toLowerCase() === "bearer") {
                if (token === RMS_SUPERADMIN_TOKEN) {
                    ctx.state.is_superadmin = true;
                    await next();
                    return;
                } else {
                    ctx.throw(403, "Invalid bearer token");
                }
            }
        }

        const tokenset = await rms_key.decrypt(ctx.cookies.get(RMS_COOKIE));

        if (tokenset === null) {
            set_refresh_403(ctx);
            return;
        } else {
            const c = ctx.state.client;
            const ret = await c.introspect(
                tokenset.access_token,
                "access_token",
            );
            const sub = ctx.get("x-openid-sub");
            if (!ret.active || sub !== tokenset.sub) {
                console.log("no longer active");
                set_refresh_403(ctx);
                return;
            } else {
                // CSRF checking
                // yes we check it also for GET/etc, it's easier to just put it on every request

                // check the origin header (quick check, defense in depth)
                // the Origin header, if present, must be correct
                if (
                    ctx.get("Origin") !== new URL(RMS_ROOT).origin &&
                    ctx.get("Origin") !== ""
                ) {
                    console.log(
                        "origin header is wrong",
                        ctx.get("Origin"),
                        "and",
                        new URL(RMS_ROOT).origin,
                    );
                    ctx.throw(403, "origin header is wrong");
                }

                const csrf_token = ctx.get("x-csrf");
                if (typeof csrf_token !== "string") {
                    console.log("no x-csrf");
                    set_refresh_403(ctx);
                    return;
                }
                const csrf = await csrf_key.decrypt(
                    ctx.cookies.get(CSRF_COOKIE),
                );
                if (csrf === null) {
                    console.log("invalid or missing csrf cookie");
                    set_refresh_403(ctx);
                    return;
                }

                if (csrf.csrf !== csrf_token) {
                    console.log("invalid csrf token");
                    set_refresh_403(ctx);
                    return;
                }
                if (csrf.sub !== tokenset.sub) {
                    console.log("csrf has wrong sub");
                    set_refresh_403(ctx);
                    return;
                }
                if (csrf.session_state !== tokenset.session_state) {
                    console.log("csrf has wrong session_state");
                    set_refresh_403(ctx);
                    return;
                }

                const userinfo = await ctx.state.client.userinfo(
                    tokenset.access_token,
                );
                // always allow superadmin
                // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
                if (userinfo["http://westtel.com/superadmin"]) {
                    await next();
                } else if (allow_non_superadmin) {
                    await next();
                } else {
                    ctx.throw(403, "user does not have access");
                }
            }
        }
    };
}

export async function csrf_route(ctx: ParameterizedContext<IKoaState>) {
    const rms = await rms_key.decrypt(ctx.cookies.get(RMS_COOKIE));
    if (rms === null) {
        set_refresh_403(ctx);
        return;
    }
    const csrf = await csrf_key.decrypt(ctx.cookies.get(CSRF_COOKIE));
    if (
        csrf === null ||
        csrf.sub !== rms.sub ||
        csrf.session_state !== rms.session_state
    ) {
        // tie CSRF to the subject and the session_state
        const new_csrf = generators.random(32);
        const new_c = await csrf_key.encrypt(
            { csrf: new_csrf, sub: rms.sub, session_state: rms.session_state },
            { expiresIn: "24 hours" },
        );
        ctx.cookies.set(CSRF_COOKIE, new_c, {
            sameSite: "strict",
            maxAge: 12 * 60 * 60 * 1000,
        });
        ctx.body = { csrf: new_csrf };
    } else {
        ctx.body = { csrf: csrf.csrf };
    }
}

function post_parent_body(obj: object, nonce: string) {
    return `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>RMS OpenID Callback</title>
</head>
<body>
    <div id="callback">${escape_html(JSON.stringify(obj))}</div>
    <script nonce="${nonce}">
        window.parent.postMessage(document.getElementById("callback").textContent, window.location.origin);
    </script>
</body>
</html>
`;
}

export async function openid_cb(ctx: ParameterizedContext<IKoaState>) {
    const c = ctx.state.client;
    const params = c.callbackParams(ctx.url);

    if (params.state === undefined) {
        ctx.throw(400, "openid state is required");
    }

    const state = await openid_state_key.decrypt(params.state);
    if (state === null) {
        ctx.throw(400);
    }
    const which: string = state.which;

    const login_state_cookie = ctx.cookies.get(
        LOGIN_STATE_COOKIE_PREFIX + which,
    );
    // clear login state
    ctx.cookies.set(LOGIN_STATE_COOKIE_PREFIX + which, undefined as any);

    const login_state = await login_state_key.decrypt(login_state_cookie);
    if (login_state === null) {
        ctx.throw(400);
    }
    try {
        const tokenset = await c.callback(
            RMS_OPENID_CB,
            c.callbackParams(ctx.url),
            {
                code_verifier: login_state.code_verifier,
                nonce: login_state.nonce,
                state: login_state.state,
            },
        );
        if (
            tokenset.access_token === undefined ||
            tokenset.id_token === undefined
        ) {
            ctx.throw(
                400,
                "invalid OpenID setup, requires access token and id token",
            );
        }
        const userinfo = await c.userinfo(tokenset);
        const rms = await rms_key.encrypt(
            {
                access_token: tokenset.access_token,
                sub: tokenset.claims().sub,
                preferred_username: userinfo.preferred_username,
                id_token: tokenset.id_token,
                session_state: tokenset.session_state,
            },
            { expiresIn: "1 hour" },
        );
        ctx.cookies.set(RMS_COOKIE, rms, {
            sameSite: "lax",
            maxAge: 30 * 60 * 1000,
        });
        if (state.url !== undefined) {
            ctx.redirect(state.url);
        } else {
            const callback = {
                sub: tokenset.claims().sub,
                session_state: tokenset.session_state,
                state: "success",
            };
            ctx.body = post_parent_body(callback, ctx.state.nonce);
        }
    } catch (e) {
        if (e instanceof errors.OPError) {
            if (
                e.error === "interaction_required" ||
                e.error === "login_required" ||
                e.error === "account_selection_required" ||
                e.error === "consent_required"
            ) {
                console.log("prompt none failed");
                ctx.body = post_parent_body(
                    { session_state: e.session_state, state: "error" },
                    ctx.state.nonce,
                );
            } else {
                throw e;
            }
        } else {
            throw e;
        }
    }
}

export async function openid_refresh(ctx: koa.ParameterizedContext<IKoaState>) {
    try {
        const current_session = await rms_key.decrypt(
            ctx.cookies.get(RMS_COOKIE),
        );
        // assert that we are refreshing the correct user, if there is a session
        ctx.assert(!current_session || current_session.sub === ctx.query.sub);
        const c = ctx.state.client;
        const which = generators.random(16);
        const state = await openid_state_key.encrypt({
            which,
            state: generators.state(),
        });
        const login_state = {
            code_verifier: generators.codeVerifier(),
            nonce: generators.nonce(),
            state,
        };
        const id_token = current_session ? current_session.id_token : undefined;

        ctx.cookies.set(
            LOGIN_STATE_COOKIE_PREFIX + which,
            await login_state_key.encrypt(login_state, {
                expiresIn: "1 hour",
            }),
            { maxAge: 30 * 60 * 1000 },
        );
        ctx.redirect(
            c.authorizationUrl({
                scope: "openid profile",
                code_challenge: generators.codeChallenge(
                    login_state.code_verifier,
                ),
                code_challenge_method: "S256",
                nonce: login_state.nonce,
                state,
                prompt: "none",
                id_token_hint: id_token,
            }),
        );
    } catch (e) {
        console.log(e);
        ctx.body = post_parent_body({ state: "error" }, ctx.state.nonce);
    }
}
