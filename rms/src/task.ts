export interface Task {
    tenant: string;
    controller: string;
    forced: boolean;
    run: () => Promise<any>;
}

export class TaskRunner {
    private is_running = false;
    private readonly queue: Task[] = [];
    private readonly executing: Task[] = [];
    private refresh?: () => void;

    public push(task: Task) {
        this.queue.push(task);
        this.run().catch(e =>
            console.error("Major issue running TaskRunner.run: ", e),
        );
    }

    private async run() {
        if (this.is_running) {
            if (this.refresh) {
                this.refresh();
            }
            return;
        }

        this.is_running = true;

        // the main purpose of this function is to make sure tasks never "reject"
        // and we will output an error message instead
        async function run_task(t: Task) {
            console.log("Running task", t.tenant, t.controller);
            try {
                await t.run();
            } catch (e) {
                console.error("Task failed", t.tenant, t.controller, e.message);
            } finally {
                await new Promise(resolve => setTimeout(resolve, 1000));
            }

            console.log("Finished task", t.tenant, t.controller);
            return t;
        }

        const tasks = new Map<Task, Promise<Task | undefined>>();

        while (this.queue.length > 0 || tasks.size > 0) {
            const refresh_promise = new Promise<undefined>(resolve => {
                this.refresh = resolve;
            });
            const SIMUL_TASKS = /^\d+$/.test(process.env.SIMUL_TASKS ?? "")
                ? Number(process.env.SIMUL_TASKS)
                : 1;
            // we execute at most SIMUL_TASKS tasks, and can only execute a task if there are tasks to be executed
            while (
                this.queue.length > 0 &&
                this.executing.length < SIMUL_TASKS
            ) {
                // TODO: only run task if from a different tenant
                const new_task = this.queue.shift()!;
                this.executing.push(new_task);
                tasks.set(new_task, run_task(new_task));
            }

            const finished = await Promise.race([
                refresh_promise,
                ...tasks.values(),
            ]);
            if (finished) {
                tasks.delete(finished);
                this.executing.splice(this.executing.indexOf(finished), 1);
            }
            this.refresh = undefined;
        }

        this.is_running = false;
    }

    public get running_tasks() {
        return this.executing;
    }

    public get queued_tasks() {
        return this.queue;
    }

    public get all_tasks() {
        return [...this.executing, ...this.queue];
    }
}
