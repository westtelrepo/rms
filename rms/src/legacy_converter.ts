/* eslint-disable @typescript-eslint/strict-boolean-expressions, @typescript-eslint/no-unsafe-assignment */

import assert from "assert";
import { ChildProcessWithoutNullStreams, spawn } from "child_process";
import crypto from "crypto";
import fs from "fs-extra";
import Immutable from "immutable";
import ms from "ms";
import path from "path";
import { IDatabase, ITask } from "pg-promise";
import { IConnectionParameters } from "pg-promise/typescript/pg-subset";
import ssh2 from "ssh2";
import stream, { Transform, Writable } from "stream";
import { StringDecoder } from "string_decoder";
import util from "util";
import uuid from "uuid-1345";
import { isMainThread, parentPort, Worker, workerData } from "worker_threads";

import { ali_formats, IParsedAli, parse_ali } from "./ali";
import { LEGACY_CONVERTER_PATH } from "./ci_const";
import {
    list_controllers,
    update_monitoring,
    update_monitoring_legacy_run,
} from "./db/data_migrations";
import { get_ssh_credential, get_ssh_server } from "./db/meta_migrations";
import { get_tenant_db } from "./db/pgp_tenant";
import { list_tenants } from "./db/tenant";
import { pgp } from "./globals";
import { assert_is_controller_config } from "./schema/controller_config";
import { cmp_log, LogJSON, to_utc } from "./schema/log";
import { key_types } from "./ssh";

const pipeline = util.promisify(stream.pipeline);

interface XferInfo {
    controller: string;
    callid: string;
    xferid: string;
    incoming: boolean;
    legacy_callid: string | null;
    psap: string;
    other_psap: string | null;
    utc: string;
    utc_connect: string | null;
    answer: string | null;
    call_length: string | null;
    abandoned: boolean;
    pos: number | null;
    calltaker: string | null;
    is_911: boolean;
    trunk: number | null;
    sip: string | null;
    destination: string | null;
    transfer_reaction: string;
    transfer_reaction_destination: string | null;
    transfer_reaction_xferid: string | null;
    transfer_reaction_time: string | null;
    transfer_type: string;
    cos: string | null;
    ani: string | null;
    pani: string | null;
    ali_raw: any;
    ali_parsed: any;
    legacy: boolean;
    version: number;
    max_entry_num: bigint;
}

interface AliInfo {
    controller: string;
    callid: string;
    subid: number;
    utc: string;
    ali_record: any;
    ali_parsed: any;
    config_version: string;
    legacy: boolean;
    entry_num: bigint;
}

function process_call(
    logs: Set<LogJSON & { __entry_num: bigint; call: { id: string } }>,
): { xfers: XferInfo[]; alis: AliInfo[] } {
    const version: number = 0;

    const ret: XferInfo[] = [];
    const ret_ali: AliInfo[] = [];

    // xferid -> dial
    const dial = new Map<string, any>();
    const second = new Map<string, any>();

    let is_911 = false;
    let trunk: null | number = null;
    let ali_raw: any = null;
    let ali_parsed: null | IParsedAli = null;
    let has_ali = false;
    let ani: string | null = null;
    let pani: string | null = null;
    let sip: string | null = null;

    // ALI subid
    let subid = 0;

    const callid = logs.values().next().value.call.id;
    const controller = logs.values().next().value.controller;
    let legacy_callid: string | null = null;
    let max_entry_num = BigInt(0);

    for (const x of Array.from(logs).sort(cmp_log)) {
        if (typeof x.oldlog.callid === "string") {
            legacy_callid = x.oldlog.callid;
        }

        if (x.transfer?.id && x.transfer.state === "dial") {
            if (!dial.has(x.transfer.id)) {
                dial.set(x.transfer.id, x);
            }
        } else if (
            x.transfer?.id &&
            (x.transfer.state === "join" || x.transfer.state === "abandon")
        ) {
            if (!second.has(x.transfer.id)) {
                second.set(x.transfer.id, x);
            }
        }

        if (x.__entry_num > max_entry_num) {
            max_entry_num = x.__entry_num;
        }

        if (x.call.is_911 === true) {
            is_911 = true;
        }

        if (typeof x.call.trunk === "number") {
            trunk = x.call.trunk;
        }

        if (typeof x.call.ani === "string") {
            ani = x.call.ani;
        }

        if (typeof x.call.pani === "string") {
            pani = x.call.pani;
        }

        if (typeof x.call.sip === "string") {
            if (typeof sip === "string" && x.call.sip !== sip) {
                assert(false, `${sip} and ${x.call.sip} differ`);
            }
            sip = x.call.sip;
        }

        if (x.ali?.record?.raw) {
            ali_raw = x.ali.record;
            ali_parsed = parse_ali(x.ali.record as any);
            has_ali = true;

            ret_ali.push({
                ali_parsed: parse_ali(x.ali.record as any),
                ali_record: ali_raw,
                callid,
                controller,
                config_version: "c101bd15-d1c7-4eeb-a4dc-9bde02ced573", // TODO: not constant
                utc: new Date(1000 * to_utc(x.t.ut)).toUTCString(),
                subid,
                legacy: true,
                entry_num: x.__entry_num,
            });
            subid++;
        }
    }

    const cos = ali_parsed?.cos ?? (has_ali ? "Unknown" : null);

    for (const x of dial) {
        const sec = second.get(x[0]);

        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        const xferid = uuid.check(x[0])
            ? x[0]
            : uuid.v5({
                  namespace: "7c8924b4-a5f9-4804-a849-78687754a332",
                  name: x[0],
              });

        const caller_psap = x[1].user ? x[1].user.psap : undefined;
        const callee_psap = sec?.user ? sec.user.psap : undefined;

        let abandoned = false;
        if (sec) {
            abandoned = sec.transfer ? sec.transfer.state === "abandon" : false;
        } else if (x[1].transfer?.no_callee_is_abandoned) {
            abandoned = true;
        } else {
            abandoned = false;
        }

        const transfer_type = x[1].transfer.type;
        const utc = new Date(1000 * to_utc(x[1].t.ut));
        const answer_utc =
            sec && !abandoned ? new Date(1000 * to_utc(sec.t.ut)) : null;

        const answer = answer_utc
            ? String(Number(answer_utc) - Number(utc)) + " ms"
            : null;

        let call_length: string | null = null;
        if (transfer_type === "initial") {
            let disconnect_valid = true;
            let disconnect: any = null;
            for (const log of logs) {
                if (log.code.startsWith("call/warning/forced_disconnect/")) {
                    disconnect_valid = false;
                }

                if (log.code.startsWith("call/disconnect/")) {
                    disconnect = log;
                }
            }

            if (disconnect_valid && disconnect && answer_utc) {
                call_length =
                    String(
                        to_utc(disconnect.t.ut) - Number(answer_utc) / 1000,
                    ) + " s";
            }
        }

        let destination: string | null = null;
        if (x[1].transfer.destination) {
            destination = x[1].transfer.destination;
        }
        if (sec?.transfer.destination) {
            // second destination wins
            // they are usually the same, the sometimes the first is something like "SharedLineOne"
            destination = sec.transfer.destination;
        }

        if (typeof caller_psap === "string") {
            ret.push({
                controller,
                callid,
                xferid,
                incoming: false,
                legacy_callid,
                psap: caller_psap,
                other_psap: callee_psap ?? null,
                utc: utc.toUTCString(),
                utc_connect: answer_utc ? answer_utc.toUTCString() : null,
                answer,
                call_length,
                abandoned,
                pos: x[1].user ? x[1].user.position : null,
                calltaker: x[1].user?.name ? x[1].user.name : null,
                is_911,
                trunk,
                sip,
                transfer_reaction: "none",
                transfer_type,
                cos,
                ani,
                pani,
                ali_raw,
                ali_parsed,
                legacy: true,
                version,
                destination,
                transfer_reaction_destination: null,
                transfer_reaction_xferid: null,
                transfer_reaction_time: null,
                max_entry_num,
            });
        }

        if (typeof callee_psap === "string") {
            ret.push({
                controller,
                callid,
                xferid,
                incoming: true,
                legacy_callid,
                psap: callee_psap,
                other_psap: caller_psap ?? null,
                utc: utc.toUTCString(),
                utc_connect: answer_utc ? answer_utc.toUTCString() : null,
                answer,
                call_length,
                abandoned,
                pos: sec?.user ? sec.user.position : null,
                calltaker: sec?.user?.name ? sec.user.name : null,
                is_911,
                trunk,
                sip,
                transfer_reaction: "none",
                transfer_type,
                cos,
                ani,
                pani,
                ali_raw,
                ali_parsed,
                legacy: true,
                version,
                destination,
                transfer_reaction_destination: null,
                transfer_reaction_xferid: null,
                transfer_reaction_time: null,
                max_entry_num,
            });
        }
    }

    // populate transfer_reaction
    for (let i = 0; i < ret.length; i++) {
        // only incoming non-abandoned calls have transfer reactions
        if (ret[i].incoming === true && !ret[i].abandoned) {
            // look to the calls to the future of this one
            for (let j = i + 1; j < ret.length; j++) {
                // the transfer_reaction to an incoming call must be outgoing
                if (
                    ret[j].incoming === false &&
                    // must not be abandoned
                    !ret[j].abandoned &&
                    // must come from the same psap
                    ret[j].psap === ret[i].psap &&
                    // can not be an intra-psap call (aka pos 1 to pos 2 at the same PSAP)
                    ret[j].other_psap !== ret[i].psap
                ) {
                    ret[i].transfer_reaction = ret[j].transfer_type;
                    ret[i].transfer_reaction_destination = ret[j].destination;
                    ret[i].transfer_reaction_xferid = ret[j].xferid;
                    ret[i].transfer_reaction_time =
                        String(
                            Number(new Date(ret[j].utc)) / 1000 -
                                Number(
                                    new Date(
                                        ret[i].utc_connect
                                            ? ret[i].utc_connect!
                                            : ret[i].utc,
                                    ),
                                ) /
                                    1000,
                        ) + " s";
                    // once we find it it is known, don't go any farther
                    break;
                }
            }
        }
    }

    return { xfers: ret, alis: ret_ali };
}

function newline_deliminated_json(): Transform {
    let str: string = "";
    const dec = new StringDecoder("utf8");
    return new Transform({
        transform(chunk: Buffer, encoding, cb) {
            try {
                str += dec.write(chunk);

                const to_send = str.split("\n");
                for (let i = 0; i < to_send.length - 1; i++) {
                    if (to_send[i] === "") {
                        continue;
                    }
                    this.push(JSON.parse(to_send[i]));
                }

                str = to_send[to_send.length - 1];
                cb();
            } catch (e) {
                cb(e);
            }
        },
        flush(cb) {
            try {
                str += dec.end();
                const to_send = str.split("\n");
                for (const s of to_send) {
                    if (s === "") {
                        continue;
                    }
                    this.push(JSON.parse(s));
                }
                cb();
            } catch (e) {
                cb(e);
            }
        },
        readableObjectMode: true,
    });
}

export async function do_all_legacy_convert(db: IDatabase<unknown>) {
    const tenants = await list_tenants(db);

    for (const t of tenants) {
        const tenant_db = await get_tenant_db(db, t);

        const controller_list = await list_controllers(tenant_db);

        for (const c of controller_list) {
            await legacy_convert({
                controller: c.controller,
                db: tenant_db,
                forced: true,
                metadb: db,
            });
        }
    }
}

export function ascii_plus_to_base64(str: string): string {
    const ret: number[] = [];
    for (const c of str) {
        const ch = c.charCodeAt(0);
        if (ch >= 0 && ch < 256) {
            ret.push(ch);
        } else if (ch >= 0xe000 && ch <= 0xe0ff) {
            ret.push(ch - 0xe000);
        } else {
            assert(false, `ascii_plus_to_base64 false assertion: ${ch}`);
        }
    }

    for (const i of ret) {
        assert(
            i >= 0 && i < 256,
            `ascii_plus_to_base64 assertion: ${i} ${JSON.stringify(str)}`,
        );
    }

    return Buffer.from(ret).toString("base64");
}

export function base64_to_ascii_plus(str: string): string {
    const buf = Buffer.from(str, "base64");
    let ret = "";
    for (const c of buf.values()) {
        if (c === 0 || c > 127) {
            ret += String.fromCharCode(0xe000 + c);
        } else {
            ret += String.fromCharCode(c);
        }
    }
    return ret;
}

if (!isMainThread) {
    (async () => {
        await legacy_convert_internal(workerData);
        parentPort!.postMessage("done");
        process.exit(0);
    })().catch(e => {
        console.error("error in legacy_convert thread: ", e);
        parentPort!.postMessage(e.message);
        process.exit(2);
    });
}

export async function legacy_convert({
    controller,
    db,
    forced,
    metadb,
}: {
    controller: string;
    db: IDatabase<unknown>;
    forced?: boolean;
    metadb: IDatabase<unknown>;
}): Promise<void> {
    const worker = new Worker(__filename, {
        workerData: { controller, db: db.$cn, forced, metadb: metadb.$cn },
    });

    const hasExited = new Promise<number>(resolve =>
        worker.once("exit", resolve),
    );

    let timeout: NodeJS.Timeout | undefined;
    try {
        await new Promise((resolve, reject) => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            timeout = setTimeout(() => {
                console.error(
                    "<3>FATAL: legacy_convert child took too long, killing self to prevent zombies/do major cleanup!",
                );
                // we kill ourselves because it is much easier to let systemd reclaim
                // the resources created by the worker (child process and mount)
                // than to do it ourselves
                process.exit(1);
            }, ms(process.env.RMS_LEGACY_CONVERT_TIMEOUT ?? "4h"));

            worker.on("message", e => {
                if (e === "done") {
                    resolve();
                } else {
                    reject(new Error(e));
                }
            });
            worker.on("error", e => {
                reject(e);
            });
        });
    } finally {
        await hasExited;
        console.log(`${controller} worker has exited`);
        if (timeout) {
            clearTimeout(timeout);
        }
    }
}

async function legacy_convert_internal({
    controller,
    db: dbc,
    forced,
    metadb: mc,
}: {
    controller: string;
    db: string | IConnectionParameters;
    forced?: boolean;
    metadb: string | IConnectionParameters;
}): Promise<void> {
    const db = pgp<unknown>(dbc);
    const metadb = pgp<unknown>(mc);

    const start_time = process.hrtime();

    const cs = new db.$config.pgp.helpers.ColumnSet([
        "id",
        "controller",
        "entry",
        "legacy",
        "utc",
        "callid",
        "legacy_entry_num",
    ]);

    console.time(controller + " import");

    let array: any[] = [];

    let has_succeeded = false;

    let skipping = false;

    let has_run_analyze = false;

    let running_hash = crypto.createHash("sha256");
    // a random UUID, acts as versioning info for this process
    // change this and everything shall be redone
    running_hash.update("35a71809-3e50-4be9-a7b6-05c107a64cb1");
    // TODO: don't include this, for now it is necessary
    running_hash.update(JSON.stringify(ali_formats));
    let entry_num = BigInt(0);
    let entry_num_known_good = BigInt(0);
    const digests = new Map<bigint, Buffer>();
    let has_run_delete = false;

    async function push_array(t: ITask<unknown>) {
        console.time(controller + " push_array");
        if (has_succeeded) {
            throw new Error("Got success message early");
        }

        if (array.length > 0 && array[array.length - 1].__success) {
            has_succeeded = true;
            array.pop();
        }

        for (const a of array) {
            assert.strictEqual(
                a.controller,
                controller,
                "legacy converter output has wrong controller",
            );

            a.id = uuid.v5({
                namespace: "4e34b1b8-9d56-4dbe-af71-d3f8d96cb043",
                name: `${controller}+${a.id}`,
            });

            if (a.call?.id) {
                a.call.id = uuid.v5({
                    namespace: "063b8523-d954-4375-91eb-bfe0908b2610",
                    name: `${controller}+${a.call.id}`,
                });
            }

            if (typeof a.d?.proc?.id === "string") {
                a.d.proc.id = uuid.v5({
                    namespace: "e0f03aaf-be3c-4349-a082-34f1991c1417",
                    name: `${controller}+${a.d.proc.id}`,
                });
            }

            if (typeof a.o?.c === "number" && !a.o.id) {
                a.o.id = uuid.v5({
                    namespace: "61cd1365-e5f3-4831-a436-f9bdff6c68bb",
                    name: controller + (a.d?.proc?.id ? `+${a.d.proc.id}` : ""),
                });
            }

            if (typeof a.t?.ut === "string") {
                const d = new Date(a.t.ut);
                const s = Math.floor(Number(d) / 1000);
                a.t.ut = {
                    s,
                    ns: 1e9 * (Number(d) / 1000 - s),
                };
            }

            if (a.ali?.record?.raw) {
                a.ali.record.base64 = ascii_plus_to_base64(a.ali.record.raw);
            }
        }

        for (const a of array) {
            running_hash.update(JSON.stringify(a));
            a.__entry_num = ++entry_num;
        }
        const digest = running_hash.digest();
        const old_digest = await t.oneOrNone<{ hash: Buffer }>(
            "SELECT hash FROM private.legacy_running_hash WHERE controller = $(controller) AND legacy_entry_num = $(entry_num)",
            { entry_num, controller },
        );
        running_hash = crypto.createHash("sha256");
        // include the old digest in the new digest
        running_hash.update(digest);
        digests.set(entry_num, digest);

        if (!has_run_delete && old_digest && old_digest.hash.equals(digest)) {
            console.log(`${controller} known good`, entry_num);
            entry_num_known_good = entry_num;
            array = [];
            console.timeEnd(controller + " push_array");
            return;
        } else if (!has_run_delete) {
            console.time(controller + " delete");
            console.log(controller + " deleting from", entry_num_known_good);
            await t.none(
                "DELETE FROM private.logs WHERE controller = $(controller) AND legacy AND (legacy_entry_num > $(entry_num_known_good) OR legacy_entry_num IS NULL)",
                { controller, entry_num_known_good },
            );
            await t.none(
                "DELETE FROM private.xfer_info WHERE controller = $(controller) AND legacy AND (legacy_max_entry_num > $(entry_num_known_good) OR legacy_max_entry_num IS NULL)",
                { controller, entry_num_known_good },
            );
            await t.none(
                "DELETE FROM private.ali_info WHERE controller = $(controller) AND legacy AND (legacy_entry_num > $(entry_num_known_good) OR legacy_entry_num IS NULL)",
                { controller, entry_num_known_good },
            );
            has_run_delete = true;
            console.timeEnd(controller + " delete");
        }

        const callids = new Set<string>();
        for (const a of array) {
            if (a.call?.id) {
                callids.add(a.call.id);
            }
        }

        console.log(`${controller} ${callids.size}`);

        const obj: string[] = [];
        for (const e of callids) {
            obj.push(e);
        }

        console.time(controller + " select");
        const select_start = process.hrtime();
        const result =
            obj.length > 0
                ? (
                      await t.any<{
                          entry: LogJSON & { call: { id: string } };
                          legacy_entry_num: bigint;
                      }>(
                          `SELECT entry, legacy_entry_num FROM private.logs
            WHERE callid = ANY($(obj)::uuid[])`,
                          { obj },
                      )
                  ).map(e => ({ ...e.entry, __entry_num: e.legacy_entry_num }))
                : [];
        console.log(`${controller} select result ${result.length}`);
        console.timeEnd(controller + " select");
        // this select normally takes a dozen or so milliseconds, if it takes 10 seconds or longer
        // running an ANALYZE seems to fix it; however we can't predict when we need it
        // and before the initial import we will have no data so there is no data to analyze
        // this also only happens on really big sites/imports
        const select_end = process.hrtime(select_start);
        const select_length = select_end[0] + select_end[1] * 1e-9;
        if (select_length > 10 && !has_run_analyze) {
            console.warn(
                "SELECT took over 10 seconds to run, running analyze...",
            );
            // we might as well analyze the entire database
            await t.none("ANALYZE");
            // we only want to do it once in a run. If the first time doesn't help doing it again won't help
            has_run_analyze = true;
        }

        // callid -> logid -> log
        const calls = new Map<
            string,
            Map<string, LogJSON & { __entry_num: bigint; call: { id: string } }>
        >();
        for (const r of result.concat(array.filter(x => x.call?.id))) {
            const m =
                calls.get(r.call.id) ??
                new Map<
                    string,
                    LogJSON & { __entry_num: bigint; call: { id: string } }
                >();
            m.set(r.id, r);
            calls.set(r.call.id, m);
        }

        console.time(controller + " insert");
        if (calls.size) {
            let to_insert: XferInfo[] = [];
            let to_insert_ali: AliInfo[] = [];
            for (const e of calls) {
                const value = process_call(new Set(e[1].values()));
                to_insert = to_insert.concat(value.xfers);
                to_insert_ali = to_insert_ali.concat(value.alis);
            }
            if (to_insert.length > 0) {
                await t.none(`INSERT INTO private.xfer_info(
                    controller, callid, xferid, incoming, legacy_callid, psap, other_psap, utc, answer,
                    call_length, abandoned, pos, calltaker, is_911, trunk, sip,
                    transfer_reaction, transfer_type, cos, ali_raw, ali_parsed, legacy, version,
                    ani, pani, destination, transfer_reaction_destination, transfer_reaction_xferid,
                    transfer_reaction_time, utc_connect, legacy_max_entry_num)
                VALUES ${db.$config.pgp.helpers.values(to_insert, [
                    "controller",
                    "callid",
                    "xferid",
                    "incoming",
                    "legacy_callid",
                    "psap",
                    "other_psap",
                    "utc",
                    "answer",
                    "call_length",
                    "abandoned",
                    "pos",
                    "calltaker",
                    "is_911",
                    "trunk",
                    "sip",
                    "transfer_reaction",
                    "transfer_type",
                    "cos",
                    "ali_raw",
                    "ali_parsed",
                    "legacy",
                    "version",
                    "ani",
                    "pani",
                    "destination",
                    "transfer_reaction_destination",
                    "transfer_reaction_xferid",
                    "transfer_reaction_time",
                    "utc_connect",
                    "max_entry_num",
                ])}
                ON CONFLICT(controller, callid, xferid, incoming) DO UPDATE SET
                    psap = EXCLUDED.psap, other_psap = EXCLUDED.other_psap, utc = EXCLUDED.utc,
                    answer = EXCLUDED.answer,
                    call_length = EXCLUDED.call_length, abandoned = EXCLUDED.abandoned, pos = EXCLUDED.pos,
                    calltaker = EXCLUDED.calltaker, is_911 = EXCLUDED.is_911, trunk = EXCLUDED.trunk,
                    sip = EXCLUDED.sip, transfer_reaction = EXCLUDED.transfer_reaction, cos = EXCLUDED.cos,
                    ali_raw = EXCLUDED.ali_raw, ali_parsed = EXCLUDED.ali_parsed, legacy = EXCLUDED.legacy,
                    version = EXCLUDED.version, ani = EXCLUDED.ani, pani = EXCLUDED.pani,
                    destination = EXCLUDED.destination, transfer_reaction_destination = EXCLUDED.transfer_reaction_destination,
                    transfer_reaction_xferid = EXCLUDED.transfer_reaction_xferid, transfer_reaction_time = EXCLUDED.transfer_reaction_time,
                    utc_connect = EXCLUDED.utc_connect, legacy_max_entry_num = EXCLUDED.legacy_max_entry_num`);
            }
            if (to_insert_ali.length > 0) {
                await t.none(`INSERT INTO private.ali_info(
                    controller, callid, subid, utc, ali_record, ali_parsed, config_version, legacy, legacy_entry_num)
                VALUES ${db.$config.pgp.helpers.values(to_insert_ali, [
                    "controller",
                    "callid",
                    "subid",
                    "utc",
                    "ali_record",
                    "ali_parsed",
                    "config_version",
                    "legacy",
                    "entry_num",
                ])}
                ON CONFLICT(controller, callid, subid) DO UPDATE SET
                    utc = EXCLUDED.utc, ali_record = EXCLUDED.ali_record, ali_parsed = EXCLUDED.ali_parsed,
                    config_version = EXCLUDED.config_version, legacy = EXCLUDED.legacy, legacy_entry_num = EXCLUDED.legacy_entry_num`);
            }
        }
        console.timeEnd(controller + " insert");

        console.time(controller + " bulk_insert");
        await t.none(`INSERT INTO private.logs(id, controller, entry, legacy, utc, callid, legacy_entry_num)
            VALUES ${db.$config.pgp.helpers.values(
                array.map(e => ({
                    id: e.id,
                    controller: e.controller,
                    entry: JSON.stringify({ ...e, __entry_num: undefined }),
                    legacy: true,
                    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
                    utc: new Date(1000 * (e.t.ut.s + 1e-9 * e.t.ut.ns)),
                    callid: e.call?.id ? e.call.id : null,
                    legacy_entry_num: e.__entry_num,
                })),
                cs,
            )}`);
        console.timeEnd(controller + " bulk_insert");
        array = [];
        console.timeEnd(controller + " push_array");
    }

    await db.tx(async t => {
        await t.none("SET LOCAL TIMEZONE TO 'UTC'");

        const config = (
            await t.one<{ config: unknown }>(
                "SELECT config FROM private.controller_config WHERE controller = $(controller)",
                { controller },
            )
        ).config;
        assert_is_controller_config(config);

        const dir_spec =
            config.legacy?.dir ?? path.join("/vagrant/tmp_logs", controller);

        let PATH: string;

        let cleanup_path = false;
        if (typeof dir_spec === "string") {
            PATH = dir_spec;
            console.log("local directory", dir_spec);
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        } else if (dir_spec.t === "sftp") {
            cleanup_path = true;
            console.log("sshfs", dir_spec);
            PATH = await fs.mkdtemp(path.join("/run/wt_rms", controller + "-"));
        } else {
            throw new Error("unknown legacy dir config");
        }

        let ssh_client: ssh2.Client | null = null;
        let sshfs: ChildProcessWithoutNullStreams | null = null;

        try {
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
            if (typeof dir_spec !== "string" && dir_spec.t === "sftp") {
                const hostHashes = await get_ssh_server(
                    metadb,
                    dir_spec.host,
                    dir_spec.port,
                );
                function hostVerifier(hash: string): boolean {
                    const buf = Buffer.from(hash, "hex");
                    for (const e of hostHashes) {
                        if (e.hash.equals(buf)) {
                            return true;
                        }
                    }
                    // if we don't recognize than fail
                    return false;
                }
                const cred = await get_ssh_credential(
                    metadb,
                    dir_spec.host,
                    dir_spec.port,
                    dir_spec.user,
                );
                if (cred === null) {
                    throw new Error("no SSH credentials provided!");
                }
                ssh_client = new ssh2.Client();
                await new Promise((resolve, reject) => {
                    const ready = () => {
                        ssh_client!.off("ready", ready);
                        ssh_client!.off("error", error);
                        resolve();
                    };
                    const error = (err: any) => {
                        ssh_client!.off("ready", ready);
                        ssh_client!.off("error", error);
                        reject(err);
                    };
                    ssh_client!.on("ready", ready);
                    ssh_client!.on("error", error);
                    ssh_client!.connect({
                        host: dir_spec.host,
                        port: dir_spec.port,
                        username: dir_spec.user,
                        password: cred.p,
                        hostHash: "sha256" as any,
                        hostVerifier,
                        algorithms: {
                            // only use key types we can recognize
                            serverHostKey: key_types.filter(e =>
                                hostHashes.some(h => h.kt === e),
                            ),
                        },
                    });
                });

                const channel = await new Promise<ssh2.ClientChannel>(
                    (resolve, reject) =>
                        ssh_client!.subsys("sftp", (err, ch) =>
                            err ? reject(err) : resolve(ch),
                        ),
                );

                sshfs = spawn("sshfs", [
                    "-o",
                    "slave,ro",
                    "-f",
                    ":" + dir_spec.dir,
                    PATH,
                ]);
                sshfs.stderr.pipe(process.stderr);
                sshfs.stdout.pipe(channel);
                channel.pipe(sshfs.stdin);

                // TODO: better method of waiting for mount
                await new Promise(resolve => setTimeout(resolve, 1000));
            }

            let dir = await fs.readdir(PATH);
            dir = dir
                .filter(x => x.startsWith(controller) && x.endsWith(".log"))
                .map(x => path.join(PATH, x));

            const stat = dir.map((x): [string, fs.Stats] => [
                path.basename(x),
                fs.statSync(x),
            ]);

            if (forced === undefined || forced === false) {
                // here we compare the previous import's file sizes with the current file sizes
                // if there is any difference, we reimport (taking advantage of ImmutableJS)
                const old_stat = await t.any<{
                    filename: string;
                    size: string;
                }>(
                    "SELECT filename, size FROM private.legacy_log_sizes WHERE controller = $(controller)",
                    { controller },
                );
                const left = Immutable.Map(stat.map(e => [e[0], e[1].size]));
                // postgresql BIGINT returns string, however files greater than ~2^53 bytes are unlikely
                const right = Immutable.Map(
                    old_stat.map(e => [e.filename, Number(e.size)]),
                );
                if (left.equals(right)) {
                    console.warn(
                        `skipping ${controller} because files have not changed`,
                    );
                    skipping = true;
                    await update_monitoring_legacy_run(t, controller);
                    return;
                }
            }

            const args = ["--json-config-on-stdin"].concat(dir);
            const child = spawn(LEGACY_CONVERTER_PATH, args);
            try {
                child.stderr.pipe(process.stderr);
                if (config.parse_config) {
                    child.stdin.write(JSON.stringify(config.parse_config));
                } else {
                    child.stdin.write("{}\n");
                }
                child.stdin.end(); // TODO: for some reason this is necessary

                await pipeline(
                    child.stdout,
                    newline_deliminated_json(),
                    new Writable({
                        async write(chunk, encoding, cb) {
                            // console.log("chunk", chunk);
                            try {
                                array.push(chunk);
                                if (array.length >= 10000) {
                                    await push_array(t);
                                }
                                cb();
                            } catch (e) {
                                cb(e);
                            }
                        },
                        async final(cb) {
                            try {
                                await push_array(t);
                                cb();
                            } catch (e) {
                                cb(e);
                            }
                        },
                        objectMode: true,
                    }),
                );

                console.log(controller + " start ali_vector update");
                console.time(controller + " ali_vector update");
                await t.none(
                    `UPDATE private.xfer_info
                    SET ali_vector = (SELECT private.tsvector_agg(to_tsvector('english', private.ali_info.ali_record->>'raw'))
                        FROM private.ali_info
                        WHERE private.ali_info.controller = private.xfer_info.controller
                            AND private.ali_info.callid = private.xfer_info.callid)
                    WHERE controller = $(controller) AND legacy AND legacy_max_entry_num > $(entry_num_known_good)`,
                    { controller, entry_num_known_good },
                );
                await t.none(
                    `UPDATE private.xfer_info
                    SET ali_parsed_text = (SELECT string_agg(DISTINCT t.value, ' ' ORDER BY t.value)
                        FROM private.ali_info, jsonb_each_text(private.ali_info.ali_parsed) t
                        WHERE private.ali_info.controller = private.xfer_info.controller
                            AND private.ali_info.callid = private.xfer_info.callid
                            AND key NOT IN ('format_id', 'line_split')
                    )
                    WHERE controller = $(controller) AND legacy AND legacy_max_entry_num > $(entry_num_known_good)`,
                    { controller, entry_num_known_good },
                );
                console.timeEnd(controller + " ali_vector update");

                console.timeEnd(controller + " import");
                console.log("done");

                if (!has_succeeded) {
                    throw new Error("did not receive success message");
                }

                await t.none(
                    "DELETE FROM private.legacy_log_sizes WHERE controller = $(controller)",
                    { controller },
                );
                for (const e of stat) {
                    await t.none(
                        `INSERT INTO private.legacy_log_sizes(controller, filename, size)
                            VALUES($(controller), $(filename), $(size))`,
                        { controller, filename: e[0], size: e[1].size },
                    );
                }

                await t.none(
                    "DELETE FROM private.legacy_running_hash WHERE controller = $(controller)",
                    { controller },
                );
                for (const e of digests) {
                    await t.none(
                        `INSERT INTO private.legacy_running_hash(controller, legacy_entry_num, hash)
                            VALUES($1, $2, $3)`,
                        [controller, e[0], e[1]],
                    );
                }

                const legacy_last_log = await t.one<{ t: Date | null }>(
                    "SELECT max(utc) AS t FROM private.logs WHERE controller = $(controller) AND legacy",
                    { controller },
                );
                const legacy_last_call = await t.one<{ t: Date | null }>(
                    "SELECT max(utc) AS t FROM private.xfer_info WHERE controller = $(controller) AND legacy",
                    { controller },
                );

                const end_time = process.hrtime(start_time);
                await update_monitoring(
                    t,
                    controller,
                    String(end_time[0] + end_time[1] * 1e-9) + " s",
                    legacy_last_call.t,
                    legacy_last_log.t,
                );
            } catch (e) {
                // if we have an error kill our child to prevent absurd resource usage
                child.kill();
                throw e;
            }
        } finally {
            if (sshfs) {
                sshfs.kill();
            }
            // TODO: better way of waiting for child to die
            await new Promise(resolve => setTimeout(resolve, 100));
            if (ssh_client) {
                ssh_client.end();
            }
            if (cleanup_path) {
                await fs.remove(PATH);
            }
        }
    });

    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (has_run_analyze) {
        console.warn("ANALZYE was ran during this import!");
    }

    // if we skipped importing a vacuum is unnecessary
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (!skipping) {
        console.time(controller + " vacuum");
        await db.none("VACUUM ANALYZE private.logs");
        console.timeEnd(controller + " vacuum");
    }
}
