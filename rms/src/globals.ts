import { Client } from "openid-client";
import pg_promise, { IDatabase } from "pg-promise";

import { TaskRunner } from "./task";

export const pgp = pg_promise();

export interface IKoaState {
    client: Client;
    db: IDatabase<unknown>;
    style_nonce: string;
    nonce: string;
    task_runner: TaskRunner;
    is_superadmin: boolean;
    access_token?: string;
}

export const OPENID_ISS =
    process.env.OPENID_ISS ??
    "http://10.0.0.1:7070/auth/realms/Test/.well-known/openid-configuration";
if (typeof OPENID_ISS !== "string") {
    throw new Error("OPENID_ISS is not defined");
}
export const OPENID_ISS_ORIGIN = new URL(OPENID_ISS).origin;

export const RMS_ROOT = process.env.RMS_ROOT ?? "http://10.0.0.2:8080";
if (typeof RMS_ROOT !== "string") {
    throw new Error("RMS_ROOT is not defined");
}

export const RMS_SECURE = new URL(RMS_ROOT).protocol === "https:";

export const RMS_OPENID_CB = new URL("/openid/cb", RMS_ROOT).href;

export const RMS_CLIENT_ID = process.env.RMS_CLIENT_ID ?? "rms";

/** a bearer token that allows superadmin access to potentially many endpoints, but at least /calls. Is probably very broken. Use only in testing. */
export const RMS_SUPERADMIN_TOKEN = process.env.RMS_SUPERADMIN_TOKEN;
