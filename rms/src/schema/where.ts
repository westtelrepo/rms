import ajv from "ajv";

import { WhereObject } from "./generated/where";
import where_schema from "./json/where.schema.json";

export { WhereObject };

const a = new ajv();
const verifier = a.compile(where_schema);

export function is_where_object(x: unknown): x is WhereObject {
    return verifier(x) as boolean;
}
