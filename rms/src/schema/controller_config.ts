import ajv from "ajv";
import node_assert from "assert";

const assert: (x: boolean) => asserts x = node_assert;

import { ControllerConfig, DirConfig } from "./generated/controller_config";
import controller_config_schema from "./json/controller_config.schema.json";

export { ControllerConfig, DirConfig };

const a = new ajv();
const verifier = a.compile(controller_config_schema);

export function is_controller_config(x: unknown): x is ControllerConfig {
    return verifier(x) as boolean;
}

export function assert_is_controller_config(
    x: unknown,
): asserts x is ControllerConfig {
    const result = verifier(x);
    assert(typeof result === "boolean");
    if (!result) {
        const e = new Error(
            "error verifying controller config: " +
                JSON.stringify(verifier.errors),
        );
        throw e;
    }
}

const dir_verifier = a.compile(controller_config_schema.definitions.dir);

export function assert_is_dir_config(x: unknown): asserts x is DirConfig {
    const result = dir_verifier(x);
    assert(typeof result === "boolean");
    if (!result) {
        const e = new Error(
            "error verifying dir config: " +
                JSON.stringify(dir_verifier.errors),
        );
        throw e;
    }
}
