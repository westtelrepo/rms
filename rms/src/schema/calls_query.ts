import ajv from "ajv";

import { CallsQuery } from "./generated/calls_query";
import calls_query_schema from "./json/calls_query.schema.json";

export { CallsQuery };

const a = new ajv();
const verifier = a.compile(calls_query_schema);

export function is_calls_query(x: unknown): x is CallsQuery {
    return verifier(x) as boolean;
}
