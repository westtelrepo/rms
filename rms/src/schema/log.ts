import ajv from "ajv";
import node_assert from "assert";

import { LogJSON, TimeJSON } from "./generated/log";
import log_schema from "./json/log.schema.json";

const assert: (x: boolean) => asserts x = node_assert;

export { LogJSON, TimeJSON };

const a = new ajv();
const verifier = a.compile(log_schema);

export function is_log(x: unknown): x is LogJSON {
    return verifier(x) as boolean;
}

export function assert_is_log(x: unknown): asserts x is LogJSON {
    const result = verifier(x);
    assert(typeof result === "boolean");
    if (!result) {
        const e = new Error(
            "error verifying log: " + JSON.stringify(verifier.errors),
        );
        throw e;
    }
}

export function cmp_log(a: LogJSON, b: LogJSON): -1 | 0 | 1 {
    if (a.o.id === b.o.id) {
        if (a.o.c < b.o.c) {
            return -1;
        } else if (a.o.c > b.o.c) {
            return 1;
        } else {
            return 0;
        }
    } else {
        const left = to_utc(a.t.ut);
        const right = to_utc(b.t.ut);
        if (left < right) {
            return -1;
        } else if (left > right) {
            return 1;
        } else {
            return 0;
        }
    }
}

export function to_utc(x: TimeJSON): number {
    return x.s + 1e-9 * x.ns;
}
