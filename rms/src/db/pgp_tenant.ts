import { IDatabase } from "pg-promise";

// NOTE: there is a leak here, but only if you destroy a bunch of tenants
const database_map = new WeakMap<
    IDatabase<unknown>,
    Map<string, IDatabase<unknown>>
>();

// cache IDatabase for each tenant
// db is the connection to the management db (rms)
export async function get_tenant_db(
    db: IDatabase<unknown>,
    name: string,
): Promise<IDatabase<unknown>> {
    // ensure that the DB exists and *is managed by us* before using
    await db.one(
        "SELECT name FROM public.managed_databases WHERE name = $(name)",
        { name },
    );

    if (!database_map.has(db)) {
        database_map.set(db, new Map());
    }

    const map = database_map.get(db)!;
    if (map.has(name)) {
        return map.get(name)!;
    } else {
        let cn = db.$cn;
        if (typeof cn === "string") {
            const uri = new URL(cn);
            uri.pathname = "/" + name;
            cn = uri.href;
        } else {
            cn = { database: name, ...cn };
        }
        const ret = db.$config.pgp(cn);
        map.set(name, ret);
        return ret;
    }
}
