import { IBaseProtocol, IDatabase, ITask } from "pg-promise";

import {
    assert_is_controller_config,
    assert_is_dir_config,
    ControllerConfig,
    DirConfig,
} from "../schema/controller_config";
import * as schema from "./schema";

export const mig: { [name: string]: schema.Migration } = {};

mig.initial = new schema.Migration({
    name: "Initial RMS Datastore Migration",
    description: "Initial RMS Data",
    parent: schema.InitialMigration.state_uuid,
    sql: `CREATE EXTENSION pg_trgm;

CREATE SCHEMA ro;
CREATE SCHEMA ro_support;
CREATE SCHEMA private;

CREATE TABLE private.controller_config (
    controller TEXT PRIMARY KEY,
    config JSONB NOT NULL
);

CREATE TABLE private.psap_config (
    psap TEXT PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE private.logs (
    id UUID PRIMARY KEY,
    controller TEXT NOT NULL,
    entry JSONB NOT NULL,
    legacy BOOLEAN NOT NULL,
    utc TIMESTAMPTZ NOT NULL,
    callid UUID,
    CONSTRAINT logs_id_check CHECK(id = (entry->>'id')::uuid),
    CONSTRAINT logs_callid_check CHECK(callid IS NOT DISTINCT FROM (entry->'call'->>'id')::uuid)
);

ALTER TABLE ONLY private.logs
    ADD CONSTRAINT logs_controller_fkey FOREIGN KEY (controller) REFERENCES private.controller_config(controller);

-- if any of these change, reimport all
CREATE TABLE private.legacy_log_sizes (
    controller TEXT NOT NULL,
    filename TEXT NOT NULL,
    size BIGINT NOT NULL,
    PRIMARY KEY(controller, filename)
);

ALTER TABLE ONLY private.legacy_log_sizes
    ADD CONSTRAINT legacy_log_sizes_controller_fkey FOREIGN KEY (controller)
    REFERENCES private.controller_config(controller);

-- reimport filename only if that file changes
CREATE TABLE private.log_sizes (
    controller TEXT NOT NULL,
    filename TEXT NOT NULL,
    size BIGINT NOT NULL,
    PRIMARY KEY(controller, filename)
);

ALTER TABLE ONLY private.log_sizes
    ADD CONSTRAINT log_sizes_controller_fkey FOREIGN KEY (controller) REFERENCES private.controller_config(controller);

CREATE INDEX logs_controller_utc_idx ON private.logs(controller, utc);
CREATE INDEX logs_callid_idx ON private.logs(callid);

CREATE TABLE private.xfer_info (
    controller TEXT NOT NULL,
    callid UUID NOT NULL,
    xferid UUID NOT NULL,
    incoming BOOLEAN NOT NULL,
    PRIMARY KEY(controller, callid, xferid, incoming),
    legacy_callid TEXT,
    psap TEXT NOT NULL,
    other_psap TEXT,
    utc TIMESTAMPTZ NOT NULL,
    utc_connect TIMESTAMPTZ,
    answer INTERVAL,
    call_length INTERVAL,
    abandoned BOOLEAN NOT NULL,
    pos INT,
    calltaker TEXT,
    is_911 BOOLEAN NOT NULL,
    trunk INT,
    sip TEXT,
    transfer_type TEXT NOT NULL,
    cos TEXT,
    ani TEXT,
    pani TEXT,
    destination TEXT,
    -- transfer reaction
    transfer_reaction TEXT NOT NULL,
    transfer_reaction_destination TEXT,
    transfer_reaction_xferid UUID,
    transfer_reaction_time INTERVAL,
    -- ALI options
    ali_raw JSONB,
    ali_parsed JSONB,
    ali_vector TSVECTOR, -- for searching
    ali_parsed_text TEXT, -- for searching
    -- maintenance options
    legacy BOOLEAN NOT NULL,
    version INT NOT NULL
);

ALTER TABLE ONLY private.xfer_info
    ADD CONSTRAINT xfer_info_controller_fkey FOREIGN KEY (controller) REFERENCES private.controller_config(controller);

CREATE INDEX xfer_info_ali_vector_gin ON private.xfer_info
    USING gin(ali_vector);

CREATE INDEX xfer_info_utc_brin ON private.xfer_info
    USING brin(utc);

CREATE INDEX xfer_info_ani_btree ON private.xfer_info(ani);
CREATE INDEX xfer_info_sip_btree ON private.xfer_info(sip);

-- pg_trgm indexes, very helpful for ILIKE '%stuff%'
CREATE INDEX xfer_info_ani_gin_trgm ON private.xfer_info USING gin(ani gin_trgm_ops);
CREATE INDEX xfer_info_pani_gin_trgm ON private.xfer_info USING gin(pani gin_trgm_ops);
CREATE INDEX xfer_info_sip_gin_trgm ON private.xfer_info USING gin(sip gin_trgm_ops);
CREATE INDEX xfer_info_cos_gin_trgm ON private.xfer_info USING gin(cos gin_trgm_ops);
CREATE INDEX xfer_info_calltaker_gin_trgm ON private.xfer_info USING gin(calltaker gin_trgm_ops);
CREATE INDEX xfer_info_ali_parsed_text_gim_trgm ON private.xfer_info USING gin(ali_parsed_text gin_trgm_ops);

CREATE TABLE private.ali_info (
    controller TEXT NOT NULL,
    callid UUID NOT NULL,
    subid INT NOT NULL, -- highest subid is latest ALI record for call
    PRIMARY KEY(controller, callid, subid),
    utc TIMESTAMPTZ NOT NULL,
    ali_record JSONB NOT NULL,
    ali_parsed JSONB, -- could fail to parse, aka null
    config_version UUID NOT NULL, -- hash of config used to parse ALI record - used for updates
    legacy BOOLEAN NOT NULL
);

CREATE INDEX ali_info_record_raw_gin ON private.ali_info
    USING gin((to_tsvector('english', ali_record->>'raw')));

CREATE TABLE private.ro_roles (
    name TEXT PRIMARY KEY
);

CREATE TABLE private.views (
    name TEXT PRIMARY KEY,
    config JSONB NOT NULL
);

CREATE TABLE ro_support.cos_groups_table (
    cos TEXT PRIMARY KEY,
    grp TEXT NOT NULL
);

-- PUBLIC can execute this function, which is fine
CREATE FUNCTION ro.to_cdr_uri(uuid) RETURNS text AS
'SELECT ''http://invalid/cdr?callid='' || $1::text;' LANGUAGE SQL STABLE;

-- PUBLIC can execute this function, which is fine
CREATE FUNCTION ro.cos_group(text) RETURNS text AS
'SELECT COALESCE((SELECT grp FROM ro_support.cos_groups_table WHERE cos = $1::text), $1::text);' LANGUAGE SQL STABLE;

-- we need this to populate private.xfer_info.ali_vector
CREATE AGGREGATE private.tsvector_agg(tsvector) (
    STYPE = pg_catalog.tsvector,
    SFUNC = pg_catalog.tsvector_concat,
    INITCOND = ''
);
`,
    async before_func(t) {
        // set permissions. This requires SUPERUSER
        const current_user = (
            await t.one<{ current_user: string }>("SELECT current_user")
        ).current_user;
        await t.none("ALTER SCHEMA public OWNER TO $(current_user~)", {
            current_user,
        });
        await t.none("REVOKE ALL ON SCHEMA public FROM PUBLIC");
        await t.none(
            "GRANT USAGE, CREATE ON SCHEMA public TO $(current_user~)",
            {
                current_user,
            },
        );

        const current_database = (
            await t.one<{ current_database: string }>(
                "SELECT current_database()",
            )
        ).current_database;
        await t.none(
            "REVOKE ALL ON DATABASE $(current_database~) FROM PUBLIC",
            {
                current_database,
            },
        );
    },
    async after_func(t) {
        // TODO: function to do this
        // TODO: better estimate for n_distinct
        // this is needed because PostgreSQL does really bad plans for
        // `SELECT * FROM private.logs WHERE callid IN set` otherwise when the database
        // size is large
        await t.none(
            "ALTER TABLE private.logs ALTER COLUMN callid SET (n_distinct = -0.11)",
        );
        // this puts the last line into effect
        await t.none("ANALYZE private.logs");
    },
});

mig.add_psap_tz = new schema.Migration({
    name: "Add PSAP TZ",
    description: "Add a TZ component to PSAPs",
    parent: mig.initial.state_uuid,
    sql: `ALTER TABLE private.psap_config ADD COLUMN timezone TEXT;`,
});

mig.monitoring = new schema.Migration({
    name: "Monitoring",
    description: "",
    parent: mig.add_psap_tz.state_uuid,
    sql: `CREATE TABLE private.monitoring (
    controller TEXT PRIMARY KEY,
    legacy_last_call TIMESTAMPTZ,
    legacy_last_log TIMESTAMPTZ,
    legacy_last_successful_import TIMESTAMPTZ
);

ALTER TABLE ONLY private.monitoring
    ADD CONSTRAINT monitoring_controller_fkey FOREIGN KEY (controller) REFERENCES private.controller_config(controller);
`,
});

mig.faster_reimport = new schema.Migration({
    name: "Faster Reimport",
    description: "",
    parent: mig.monitoring.state_uuid,
    sql: `CREATE TABLE private.legacy_running_hash (
    controller TEXT NOT NULL,
    legacy_entry_num BIGINT NOT NULL,
    PRIMARY KEY(controller, legacy_entry_num),
    hash BYTEA NOT NULL
);

ALTER TABLE private.logs ADD COLUMN legacy_entry_num BIGINT;
ALTER TABLE private.ali_info ADD COLUMN legacy_entry_num BIGINT;
ALTER TABLE private.xfer_info ADD COLUMN legacy_max_entry_num BIGINT;
`,
});

mig.track_last_import_time = new schema.Migration({
    name: "Monitoring timing",
    description: "",
    parent: mig.faster_reimport.state_uuid,
    sql: `ALTER TABLE private.monitoring ADD COLUMN legacy_interval INTERVAL;`,
});

export async function migrate({ db }: { db: IDatabase<unknown> }) {
    await schema.migrate_db({
        db,
        migrations: [
            mig.initial,
            mig.add_psap_tz,
            mig.monitoring,
            mig.faster_reimport,
            mig.track_last_import_time,
        ],
        to: mig.track_last_import_time.state_uuid,
    });
}

export async function create_ro_role_t(t: ITask<unknown>, role_name: string) {
    await t.none("CREATE ROLE $(role_name~)", { role_name });
    await t.none("INSERT INTO private.ro_roles(name) VALUES($(role_name))", {
        role_name,
    });
    await t.none("GRANT USAGE ON SCHEMA ro TO $(role_name~)", { role_name });
    await t.none("GRANT USAGE ON SCHEMA ro_support TO $(role_name~)", {
        role_name,
    });

    // ro.cos_groups_table

    // grant SELECT on all views currently in schema ro
    const views = await t.any<{ name: string }>(
        "SELECT name FROM private.views",
    );
    for (const v of views) {
        await t.none(
            "GRANT SELECT ON TABLE ro.$(view_name~) TO $(role_name~)",
            {
                role_name,
                view_name: v.name,
            },
        );
    }

    await t.none("ALTER ROLE $(role_name~) SET search_path = ro", {
        role_name,
    });

    const current_database = (
        await t.one<{ current_database: string }>("SELECT current_database()")
    ).current_database;
    await t.none(
        "GRANT CONNECT ON DATABASE $(current_database~) TO $(role_name~)",
        { role_name, current_database },
    );
}

export async function create_ro_role(
    db: IDatabase<unknown>,
    role_name: string,
) {
    return db.tx(async t => create_ro_role_t(t, role_name));
}

export async function drop_ro_role_t(t: ITask<unknown>, role_name: string) {
    await t.one(
        "DELETE FROM private.ro_roles WHERE name = $(role_name) RETURNING name",
        { role_name },
    );

    const current_database = (
        await t.one<{ current_database: string }>("SELECT current_database()")
    ).current_database;
    await t.none(
        "REVOKE CONNECT ON DATABASE $(current_database~) FROM $(role_name~)",
        { role_name, current_database },
    );

    await t.none("REVOKE USAGE ON SCHEMA ro FROM $(role_name~)", { role_name });
    await t.none("REVOKE USAGE ON SCHEMA ro_support FROM $(role_name~)", {
        role_name,
    });

    // revoke SELECT on all views currently in schema ro
    const views = await t.any<{ name: string }>(
        "SELECT name FROM private.views",
    );
    for (const v of views) {
        await t.none(
            "REVOKE SELECT ON TABLE ro.$(view_name~) FROM $(role_name~)",
            {
                role_name,
                view_name: v.name,
            },
        );
    }

    await t.none("DROP ROLE $(role_name~)", { role_name });
}

export async function drop_ro_role(db: IDatabase<unknown>, role_name: string) {
    return db.tx(async t => drop_ro_role_t(t, role_name));
}

export async function get_ro_roles_t(t: ITask<unknown>): Promise<string[]> {
    return (
        await t.any<{ name: string }>("SELECT name FROM private.ro_roles")
    ).map(e => e.name);
}

export async function get_ro_roles(db: IDatabase<unknown>): Promise<string[]> {
    return db.tx(async t => get_ro_roles_t(t));
}

export type ViewConfig =
    | {
          source: "xfer_info";
          is_911?: boolean;
          psap?: string;
          incoming?: boolean;
          controller?: string;
          intra_psap?: boolean;
          // transfer_type?: string; // to set to "initial" to exclude transfers
      }
    | {
          source: "logs";
      };

// TODO: add view config
export async function create_view_t(
    t: ITask<unknown>,
    view_name: string,
    view_config: ViewConfig,
) {
    switch (view_config.source) {
        case "logs":
            await t.none(
                "CREATE VIEW ro.$(view_name~) WITH (security_barrier) AS SELECT * FROM private.logs",
                { view_name },
            );
            break;
        case "xfer_info":
            await t.none(
                `CREATE VIEW ro.$(view_name~) WITH (security_barrier) AS
            SELECT
                controller, callid, xferid, incoming, psap, other_psap, utc, EXTRACT(EPOCH FROM answer) AS answer_sec,
                EXTRACT(EPOCH FROM call_length) AS call_length_sec, abandoned, pos, calltaker, is_911,
                trunk, sip, transfer_reaction, transfer_type, cos
            FROM private.xfer_info WHERE TRUE` +
                    (typeof view_config.is_911 === "boolean"
                        ? " AND is_911 = $(is_911)"
                        : "") +
                    (typeof view_config.psap === "string"
                        ? " AND psap = $(psap)"
                        : "") +
                    (typeof view_config.incoming === "boolean"
                        ? " AND incoming = $(incoming)"
                        : "") +
                    (typeof view_config.controller === "string"
                        ? " AND controller = $(controller)"
                        : "") +
                    (typeof view_config.intra_psap === "boolean"
                        ? view_config.intra_psap
                            ? " AND psap IS NOT DISTINCT FROM other_psap"
                            : " AND psap IS DISTINCT FROM other_psap"
                        : ""),
                { ...view_config, view_name },
            );
            break;
        default:
            throw new Error("Invalid ViewConfig in create_view_t");
    }

    await t.none(
        "INSERT INTO private.views(name, config) VALUES($(view_name), $(view_config))",
        { view_name, view_config },
    );

    const roles = await t.any<{ name: string }>(
        "SELECT name FROM private.ro_roles",
    );

    for (const r of roles) {
        await t.none("GRANT SELECT ON ro.$(view_name~) TO $(role_name~)", {
            role_name: r.name,
            view_name,
        });
    }
}

export async function create_view(
    db: IDatabase<unknown>,
    view_name: string,
    view_config: ViewConfig,
) {
    return db.tx(async t => create_view_t(t, view_name, view_config));
}

export async function drop_view_t(t: ITask<unknown>, view_name: string) {
    await t.one(
        "DELETE FROM private.views WHERE name = $(view_name) RETURNING name",
        { view_name },
    );
    await t.none("DROP VIEW ro.$(view_name~)", { view_name });
}

export async function drop_view(db: IDatabase<unknown>, view_name: string) {
    return db.tx(async t => drop_view_t(t, view_name));
}

export async function get_views_t(
    t: ITask<unknown>,
): Promise<{ name: string; config: any }[]> {
    return t.any<{ name: string; config: any }>(
        "SELECT name, config FROM private.views",
    );
}

export async function get_views(
    db: IDatabase<unknown>,
): Promise<{ name: string; config: any }[]> {
    return db.tx(async t => get_views_t(t));
}

export async function list_controllers(
    task: IBaseProtocol<unknown>,
): Promise<{ controller: string; config: ControllerConfig }[]> {
    return task.txIf(async t => {
        return (
            await t.any<{ controller: string; config: unknown }>(
                "SELECT controller, config FROM private.controller_config ORDER BY controller",
            )
        ).map(e => {
            const config = e.config;
            assert_is_controller_config(config);
            return { config, controller: e.controller };
        });
    });
}

export async function upsert_controller(
    db: IBaseProtocol<unknown>,
    controller: string,
    config: ControllerConfig,
) {
    assert_is_controller_config(config);
    return db.txIf(async t => {
        await t.none(
            `INSERT INTO private.controller_config(controller, config)
                VALUES($(controller), $(config))
            ON CONFLICT (controller) DO UPDATE SET
                config = EXCLUDED.config`,
            { controller, config: JSON.stringify(config) },
        );
    });
}

export async function drop_controller(
    db: IBaseProtocol<unknown>,
    controller: string,
) {
    return db.txIf(async t => {
        await t.none(
            "DELETE FROM private.logs WHERE controller = $(controller)",
            {
                controller,
            },
        );
        await t.none(
            "DELETE FROM private.xfer_info WHERE controller = $(controller)",
            { controller },
        );
        await t.none(
            "DELETE FROM private.legacy_log_sizes WHERE controller = $(controller)",
            { controller },
        );
        await t.none(
            "DELETE FROM private.controller_config WHERE controller = $(controller)",
            { controller },
        );
    });
}

export async function update_controller_legacy_dir(
    outer_t: IBaseProtocol<unknown>,
    controller: string,
    dir: DirConfig | undefined,
) {
    await outer_t.tx(async t => {
        if (dir !== undefined) {
            assert_is_dir_config(dir);
        }

        const old = (
            await t.one<{ config: unknown }>(
                "SELECT config FROM private.controller_config WHERE controller = $(controller)",
                { controller },
            )
        ).config;
        assert_is_controller_config(old);

        if (old.legacy === undefined) {
            old.legacy = {};
        }
        old.legacy.dir = dir;

        assert_is_controller_config(old);
        await t.none(
            `UPDATE private.controller_config
            SET config = $(old)
            WHERE controller = $(controller)`,
            { controller, old: JSON.stringify(old) },
        );
    });
}

export async function upsert_psap(
    t: IBaseProtocol<unknown>,
    psap: string,
    name: string,
    timezone?: string,
) {
    return t.none(
        `INSERT INTO private.psap_config(psap, name, timezone)
            VALUES($(psap), $(name), $(timezone))
        ON CONFLICT(psap) DO UPDATE SET
            name = EXCLUDED.name, timezone = EXCLUDED.timezone`,
        { psap, name, timezone },
    );
}

export async function list_psap(
    t: IBaseProtocol<unknown>,
): Promise<{ psap: string; name: string; timezone?: string }[]> {
    return t.any<{ psap: string; name: string; timezone?: string }>(
        "SELECT psap, name, timezone FROM private.psap_config ORDER BY psap",
    );
}

export async function update_monitoring(
    t: IBaseProtocol<unknown>,
    controller: string,
    legacy_interval: string,
    legacy_last_call: Date | null,
    legacy_last_log: Date | null,
    legacy_last_successful_import: Date = new Date(),
): Promise<void> {
    await t.none(
        `INSERT INTO private.monitoring(controller, legacy_last_call, legacy_last_log, legacy_last_successful_import, legacy_interval)
            VALUES($(controller), $(legacy_last_call), $(legacy_last_log), $(legacy_last_successful_import), $(legacy_interval))
        ON CONFLICT(controller) DO UPDATE SET
            legacy_last_call = EXCLUDED.legacy_last_call,
            legacy_last_log = EXCLUDED.legacy_last_log,
            legacy_last_successful_import = EXCLUDED.legacy_last_successful_import,
            legacy_interval = EXCLUDED.legacy_interval`,
        {
            controller,
            legacy_last_call: legacy_last_call?.toUTCString(),
            legacy_last_log: legacy_last_log?.toUTCString(),
            legacy_last_successful_import: legacy_last_successful_import.toUTCString(),
            legacy_interval,
        },
    );
}

export async function update_monitoring_legacy_run(
    t: IBaseProtocol<unknown>,
    controller: string,
    legacy_last_successful_import: Date = new Date(),
): Promise<void> {
    await t.none(
        `INSERT INTO private.monitoring(controller, legacy_last_successful_import)
            VALUES($(controller), $(legacy_last_successful_import))
        ON CONFLICT(controller) DO UPDATE SET
            legacy_last_successful_import = EXCLUDED.legacy_last_successful_import`,
        {
            controller,
            legacy_last_successful_import: legacy_last_successful_import.toUTCString(),
        },
    );
}

export async function list_monitoring(
    t: IBaseProtocol<unknown>,
): Promise<
    {
        controller: string;
        legacy_last_call: number | null;
        legacy_last_log: number | null;
        legacy_last_successful_import: number | null;
        legacy_interval: number | null;
    }[]
> {
    return t.any(
        `SELECT controller,
            EXTRACT(EPOCH FROM legacy_last_call) AS legacy_last_call,
            EXTRACT(EPOCH FROM legacy_last_log) AS legacy_last_log,
            EXTRACT(EPOCH FROM legacy_last_successful_import) AS legacy_last_successful_import,
            EXTRACT(EPOCH FROM legacy_interval) AS legacy_interval
        FROM private.monitoring ORDER BY controller`,
    );
}
