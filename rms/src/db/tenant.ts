import { IBaseProtocol, IDatabase } from "pg-promise";

import * as data_migrations from "./data_migrations";
import { get_tenant_db } from "./pgp_tenant";

export async function create_tenant(
    db: IDatabase<unknown>,
    database_name: string,
) {
    // assert that the database doesn't already exist
    await db.none(
        "SELECT datname FROM pg_database WHERE datname = $(database_name)",
        { database_name },
    );

    // TODO: enforce contents of database_name
    await db.none(
        "INSERT INTO managed_databases(name) VALUES($(database_name))",
        { database_name },
    );
    await db.none("CREATE DATABASE $(database_name~)", { database_name });

    await data_migrations.migrate({
        db: await get_tenant_db(db, database_name),
    });
}

export async function drop_tenant(
    db: IDatabase<unknown>,
    database_name: string,
) {
    // assert that we manage this database
    await db.one(
        "SELECT name FROM managed_databases WHERE name = $(database_name)",
        { database_name },
    );

    const exists = await db.oneOrNone<{ datname: string }>(
        "SELECT datname FROM pg_database WHERE datname = $(database_name)",
        { database_name },
    );

    // if the database exists, delete it
    if (exists !== null) {
        // allow ourselves to connect (in case of failure to drop)
        await db.none(
            "ALTER DATABASE $(database_name~) WITH ALLOW_CONNECTIONS TRUE",
            { database_name },
        );

        const inner_db = await get_tenant_db(db, database_name);
        await inner_db.tx(async t => {
            const ro_exists = await t.one<{ count: number }>(
                `SELECT COUNT(*) AS count
                FROM information_schema.tables
                WHERE table_schema = 'private' AND table_name = 'ro_roles'`,
            );
            // don't drop roles if the table does not exist (in case schema failed to initialize)
            if (ro_exists.count > 0) {
                const roles = await data_migrations.get_ro_roles_t(t);
                for (const role of roles) {
                    await data_migrations.drop_ro_role_t(t, role);
                }
            }
        });
        // not necessary because of pg_terminate_backend
        // await inner_db.$pool.end();

        await db.none(
            "ALTER DATABASE $(database_name~) WITH ALLOW_CONNECTIONS FALSE",
            { database_name },
        );
        // requires SUPERUSER
        await db.any(
            "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = $(database_name)",
            { database_name },
        );
        await db.none("DROP DATABASE $(database_name~)", { database_name });
    }

    await db.none(
        "DELETE FROM managed_databases WHERE name = $(database_name)",
        {
            database_name,
        },
    );
}

export async function list_tenants(
    db: IBaseProtocol<unknown>,
): Promise<string[]> {
    return db.txIf(async t =>
        (
            await t.any<{ name: string }>(
                "SELECT name FROM managed_databases ORDER BY name",
            )
        ).map(e => e.name),
    );
}

export async function list_tenants_info(
    db: IBaseProtocol<unknown>,
): Promise<
    { name: string; pretty_size: string; description: string | null }[]
> {
    return db.txIf(async t =>
        t.any(
            `SELECT name,
            pg_size_pretty(pg_database_size(name)) AS pretty_size,
            (SELECT shobj_description(oid, 'pg_database') AS description FROM pg_database WHERE datname = name) AS description
        FROM managed_databases ORDER BY name`,
        ),
    );
}

export async function rename_tenant(
    db: IDatabase<unknown>,
    old_name: string,
    new_name: string,
) {
    await db.tx(async t => {
        // assert we manage old database
        await t.one(
            "SELECT name FROM managed_databases WHERE name = $(old_name)",
            {
                old_name,
            },
        );

        // say we manage new database
        await t.none(
            "INSERT INTO managed_databases(name) VALUES($(new_name))",
            {
                new_name,
            },
        );

        // requires SUPERUSER
        await db.any(
            "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = $(old_name)",
            { old_name },
        );

        // rename database
        await t.none("ALTER DATABASE $(old_name~) RENAME TO $(new_name~)", {
            old_name,
            new_name,
        });

        // we no longer manage old name
        await t.none("DELETE FROM managed_databases WHERE name = $(old_name)", {
            old_name,
            new_name,
        });
    });
}
