import { IBaseProtocol, IDatabase } from "pg-promise";

import * as schema from "./schema";

export const mig: { [name: string]: schema.Migration } = {};

mig.initial = new schema.Migration({
    name: "Initial RMS Metadata Migration",
    description: "",
    parent: schema.InitialMigration.state_uuid,
    sql: `CREATE TABLE public.managed_databases (
    name TEXT PRIMARY KEY
);

CREATE TABLE public.ssh_servers (
    host TEXT,
    port INT,
    kt TEXT,
    PRIMARY KEY(host, port, kt),
    hash BYTEA NOT NULL
);

CREATE TABLE public.ssh_credentials (
    host TEXT,
    port INT,
    username TEXT,
    PRIMARY KEY(host, port, username),
    cred JSONB NOT NULL
);
`,
    async before_func(t) {
        // set permissions. This requires SUPERUSER
        const current_user = (
            await t.one<{ current_user: string }>("SELECT current_user")
        ).current_user;
        await t.none("ALTER SCHEMA public OWNER TO $(current_user~)", {
            current_user,
        });
        await t.none("REVOKE ALL ON SCHEMA public FROM PUBLIC");
        await t.none(
            "GRANT USAGE, CREATE ON SCHEMA public TO $(current_user~)",
            {
                current_user,
            },
        );

        const current_database = (
            await t.one<{ current_database: string }>(
                "SELECT current_database()",
            )
        ).current_database;
        await t.none(
            "REVOKE ALL ON DATABASE $(current_database~) FROM PUBLIC",
            {
                current_database,
            },
        );
    },
});

export async function migrate({ db }: { db: IDatabase<unknown> }) {
    await schema.migrate_db({
        db,
        migrations: [mig.initial],
        to: mig.initial.state_uuid,
    });
}

interface ICredential {
    t: "pass";
    p: string;
}

export async function get_ssh_credential(
    t: IBaseProtocol<unknown>,
    host: string,
    port: number,
    username: string,
): Promise<ICredential | null> {
    const ret = await t.oneOrNone<{ cred: ICredential }>(
        "SELECT cred FROM public.ssh_credentials WHERE host = $(host) AND port = $(port) AND username = $(username)",
        { host, port, username },
    );
    if (ret === null) {
        return null;
    } else {
        return ret.cred;
    }
}

export async function set_ssh_credential(
    t: IBaseProtocol<unknown>,
    host: string,
    port: number,
    username: string,
    cred: ICredential | null,
): Promise<void> {
    if (cred === null) {
        await t.none(
            "DELETE FROM public.ssh_credentials WHERE host = $(host) AND port = $(port) AND username = $(username)",
            { host, port, username },
        );
    } else {
        await t.none(
            `INSERT INTO public.ssh_credentials(host, port, username, cred)
            VALUES($(host), $(port), $(username), $(cred))
            ON CONFLICT (host, port, username)
            DO UPDATE
                SET cred = EXCLUDED.cred`,
            { host, port, username, cred },
        );
    }
}

export async function get_ssh_credentials(
    t: IBaseProtocol<unknown>,
): Promise<unknown> {
    return t.any(
        "SELECT host, port, username, cred FROM public.ssh_credentials ORDER BY host, port, username",
    );
}

export async function get_ssh_servers(
    t: IBaseProtocol<unknown>,
): Promise<{ host: string; port: number; kt: string; hash: Buffer }[]> {
    const results = await t.any<{
        host: string;
        port: number;
        kt: string;
        hash: Buffer;
    }>(
        "SELECT host, port, kt, hash FROM public.ssh_servers ORDER BY host, port, kt",
    );
    return results;
}

export async function set_ssh_server(
    trans: IBaseProtocol<unknown>,
    host: string,
    port: number,
    hashes: { kt: string; hash: Buffer }[],
): Promise<void> {
    await trans.txIf(async t => {
        await t.none(
            "DELETE FROM public.ssh_servers WHERE host = $(host) AND port = $(port)",
            { host, port },
        );
        for (const hash of hashes) {
            await t.none(
                "INSERT INTO public.ssh_servers(host, port, kt, hash) VALUES($(host), $(port), $(kt), $(hash))",
                { host, port, kt: hash.kt, hash: hash.hash },
            );
            console.log(hash);
        }
    });
}

export async function get_ssh_server(
    t: IBaseProtocol<unknown>,
    host: string,
    port: number,
): Promise<{ kt: string; hash: Buffer }[]> {
    return t.any<{ kt: string; hash: Buffer }>(
        "SELECT kt, hash FROM public.ssh_servers WHERE host = $(host) AND port = $(port)",
        { host, port },
    );
}
