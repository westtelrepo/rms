/* eslint-disable @typescript-eslint/no-unsafe-assignment */

import koa_router from "@koa/router";
import node_assert from "assert";
import Immutable from "immutable";
import koa from "koa";
import koa_bodyparser from "koa-bodyparser";
import ms from "ms";
import fetch from "node-fetch";
import uuid from "uuid-1345";

import { ali_formats, parse_ali, parse_ali_single_format } from "./ali";
import { get_cdr } from "./cdr";
import * as data from "./db/data_migrations";
import { get_tenant_db } from "./db/pgp_tenant";
import * as tenant from "./db/tenant";
import { IKoaState, OPENID_ISS_ORIGIN, pgp } from "./globals";
import { legacy_convert } from "./legacy_converter";
import { assert_auth, decrypt_rms } from "./openid";
import { is_calls_query } from "./schema/calls_query";
import { is_where_object, WhereObject } from "./schema/where";

const assert: (x: any) => asserts x = node_assert;

function user_assert(x: boolean, status: number, msg: string): asserts x {
    if (!x) {
        const e: any = new Error(msg);
        e.status = status;
        e.expose = true;
        throw e;
    }
}

const COS_GROUPS: [string, string][] = [
    ["WPH1", "Wireless"],
    ["MOBL", "Wireless"],
    ["WRLS", "Wireless"],
    ["BSNX", "Wireline"],
    ["BUS", "Wireline"],
    ["BUSN", "Wireline"],
    ["BUSX", "Wireline"],
    ["CN", "Wireline"],
    ["CNTX", "Wireline"],
    ["COIN", "Wireline"],
    ["CTX", "Wireline"],
    ["RES", "Wireline"],
    ["RES", "Wireline"],
    ["RESD", "Wireline"],
    ["RESX", "Wireline"],
    ["PAY$", "Wireline"],
    ["VOIP", "VoIP"],
    ["NRF", "Unknown"],
    ["RSDX", "Wireline"],
    ["RSDX", "Wireline"],
    ["PXB", "Wireline"],
    ["PBXB", "Wireline"],
    ["PBXR", "Wireline"],
    ["PBXb", "Wireline"],
    ["WPH2", "Wireless"],
];

export const tenant_router = new koa_router<IKoaState, koa.DefaultContext>();

async function assert_all_psaps_allowed(
    ctx: koa.ParameterizedContext<{
        access_token?: string;
        is_superadmin: boolean;
    }>,
    tenant_id: string,
    psaps: string[],
) {
    // superadmin is always allowed
    if (ctx.state.is_superadmin) {
        return;
    }

    const all_psaps = Immutable.Set(psaps.map(e => tenant_id + "/" + e));

    assert(typeof ctx.state.access_token === "string");

    const result = await fetch(
        new URL("/api/rms_psaps", OPENID_ISS_ORIGIN).href,
        {
            method: "POST",
            body: JSON.stringify(psaps.map(e => tenant_id + "/" + e)),
            headers: {
                Authorization: "Bearer " + ctx.state.access_token,
                "Content-Type": "application/json",
            },
        },
    );

    if (!result.ok) {
        throw new Error(
            `assert_all_psaps_allowed fetch not ok: ${result.status} ${result.statusText}`,
        );
    }

    const sub_psaps = await result.json();

    if (!Immutable.Set(sub_psaps).equals(all_psaps)) {
        const e: any = new Error("Not all psaps are allowed");
        e.expose = true;
        e.status = 403;
        throw e;
    }
}

async function assert_any_psap_allowed(
    ctx: koa.ParameterizedContext<{
        access_token?: string;
        is_superadmin: boolean;
    }>,
    tenant_id: string,
    psaps: string[],
) {
    // superadmin is always allowed
    if (ctx.state.is_superadmin) {
        return;
    }

    assert(typeof ctx.state.access_token === "string");

    const result = await fetch(
        new URL("/api/rms_psaps", OPENID_ISS_ORIGIN).href,
        {
            method: "POST",
            body: JSON.stringify(psaps.map(e => tenant_id + "/" + e)),
            headers: {
                Authorization: "Bearer " + ctx.state.access_token,
                "Content-Type": "application/json",
            },
        },
    );

    if (!result.ok) {
        throw new Error(
            `assert_any_psap_allowed fetch not ok: ${result.status} ${result.statusText}`,
        );
    }

    const sub_psaps: unknown = await result.json();

    if (Array.isArray(sub_psaps) && sub_psaps.length >= 1) {
        return;
    }

    const e: any = new Error("No psap allowed");
    e.expose = true;
    e.status = 403;
    throw e;
}

tenant_router.use(assert_auth(true));

tenant_router.get("/", async ctx => {
    const rms = await decrypt_rms(ctx);
    const db_list = await tenant.list_tenants_info(ctx.state.db);

    const result = await fetch(
        new URL("/api/rms_tenant", OPENID_ISS_ORIGIN).href,
        {
            method: "POST",
            body: JSON.stringify(db_list.map(e => e.name)),
            headers: {
                Authorization: "Bearer " + rms!.access_token,
                "Content-Type": "application/json",
            },
        },
    );
    const list: string[] = await result.json();

    ctx.body = db_list.filter(e => list.includes(e.name));
});

tenant_router.put("/:id", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to add tenant",
    );
    await tenant.create_tenant(ctx.state.db, ctx.params.id);
    ctx.body = null;
});

tenant_router.delete("/:id", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to delete tenant",
    );
    await tenant.drop_tenant(ctx.state.db, ctx.params.id);
    ctx.body = null;
});

tenant_router.get("/:id/comment", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    ctx.body = (
        await d.one<{ description: string }>(
            "SELECT shobj_description(oid, 'pg_database') AS description FROM pg_database WHERE datname = $(id)",
            { id: ctx.params.id },
        )
    ).description;
});

tenant_router.put("/:id/comment", async ctx => {
    assert(ctx.is("text/plain"));
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to set tenant comment",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await d.none("COMMENT ON DATABASE $(d~) IS $(c)", {
        d: ctx.params.id,
        c: ctx.request.body,
    });
    ctx.body = null;
});

tenant_router.post("/:id/rename_to", async ctx => {
    assert(ctx.is("text/plain"));
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to rename tenant",
    );
    await tenant.rename_tenant(ctx.state.db, ctx.params.id, ctx.request.body);
    ctx.body = null;
});

// TODO: this is a TEST route, don't actually use this in production (yet)
tenant_router.post("/:id/try_parse_ali", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to try_parse_ali",
    );
    console.log(`${ctx.params.id} start reparsing`);
    console.time(`${ctx.params.id} reparsing`);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await d.tx(async t => {
        const alis = await t.any<{
            controller: string;
            callid: string;
            subid: number;
            ali_record: any;
            ali_parsed: any;
        }>(
            `SELECT controller, callid, subid, ali_record
            FROM private.ali_info`,
        );
        for (const a of alis) {
            a.ali_parsed = parse_ali(a.ali_record);
        }

        if (alis.length > 0) {
            console.log(`${ctx.params.id} start updating`);
            console.time(`${ctx.params.id} reparse update`);
            await t.none(`UPDATE private.ali_info
                SET ali_parsed = data.ali_parsed::jsonb
                FROM (
                    VALUES ${d.$config.pgp.helpers.values(alis, [
                        "controller",
                        "callid",
                        "subid",
                        "ali_parsed",
                    ])}
                ) AS data(controller, callid, subid, ali_parsed)
                WHERE private.ali_info.controller = data.controller
                    AND private.ali_info.callid = data.callid::uuid
                    AND private.ali_info.subid = data.subid`);
            await t.none(
                `WITH val(controller, callid, subid, ali_parsed) AS (
                VALUES ${d.$config.pgp.helpers.values(alis, [
                    "controller",
                    "callid",
                    "subid",
                    "ali_parsed",
                ])}
            ), data AS (
                SELECT controller, callid, string_agg(DISTINCT value, ' ' ORDER BY value) AS ali_parsed_text
                FROM val, jsonb_each_text(val.ali_parsed::jsonb) t
                WHERE key NOT IN ('format_id', 'line_split')
                GROUP BY controller, callid
            )
            UPDATE private.xfer_info
                SET ali_parsed_text = data.ali_parsed_text
                FROM data
                WHERE private.xfer_info.controller = data.controller
                    AND private.xfer_info.callid = data.callid::uuid`,
            );
            console.timeEnd(`${ctx.params.id} reparse update`);
        }
    });

    console.timeEnd(`${ctx.params.id} reparsing`);
    ctx.body = null;
});

tenant_router.get("/:id/roles", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    ctx.body = await data.get_ro_roles(d);
});

tenant_router.put("/:id/roles/:rid", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to create role",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.create_ro_role(d, ctx.params.rid);
    ctx.body = null;
});

tenant_router.delete("/:id/roles/:rid", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to delete role",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.drop_ro_role(d, ctx.params.rid);
    ctx.body = null;
});

tenant_router.get("/:id/views", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    ctx.body = await data.get_views(d);
});

tenant_router.put("/:id/views/:vid", async ctx => {
    assert(ctx.is("application/json"));
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to create view",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.create_view(d, ctx.params.vid, ctx.request.body);
    ctx.body = null;
});

tenant_router.delete("/:id/views/:vid", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to delete view",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.drop_view(d, ctx.params.vid);
    ctx.body = null;
});

tenant_router.get("/:id/controllers", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    ctx.body = await data.list_controllers(d);
});

tenant_router.put("/:id/controllers/:cid", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to edit controller",
    );
    assert(ctx.is("application/json"));
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.upsert_controller(d, ctx.params.cid, ctx.request.body);
    ctx.body = null;
});

tenant_router.put("/:id/controllers/:cid/legacy/dir", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to edit controller",
    );
    const type = ctx.is("json", "text");
    switch (type) {
        case "json":
        case "text":
            const d = await get_tenant_db(ctx.state.db, ctx.params.id);
            await data.update_controller_legacy_dir(
                d,
                ctx.params.cid,
                ctx.request.body,
            );
            break;
        default:
            ctx.assert(400);
    }
    ctx.body = null;
});

tenant_router.get("/:id/psap", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    const rms = await decrypt_rms(ctx);
    const db_list = await data.list_psap(d);

    const result = await fetch(
        new URL("/api/rms_psaps", OPENID_ISS_ORIGIN).href,
        {
            method: "POST",
            body: JSON.stringify(
                db_list.map(e => `${ctx.params.id}/${e.psap}`),
            ),
            headers: {
                Authorization: "Bearer " + rms!.access_token,
                "Content-Type": "application/json",
            },
        },
    );
    const list: string[] = await result.json();
    const psap_list = list.map(e => e.split("/")[1]);
    ctx.body = db_list.filter(e => psap_list.includes(e.psap));
});

tenant_router.put("/:id/psap/:pid", async ctx => {
    assert(ctx.is("json"));
    ctx.assert(ctx.state.is_superadmin, 403, "must be superadmin to edit psap");
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.upsert_psap(
        d,
        ctx.params.pid,
        ctx.request.body.name,
        ctx.request.body.timezone,
    );
    ctx.body = null;
});

tenant_router.delete("/:id/controllers/:cid", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to delete controller",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    await data.drop_controller(d, ctx.params.cid);
    ctx.body = null;
});

tenant_router.post("/:id/controllers/:cid/refresh_legacy", async ctx => {
    ctx.assert(
        ctx.state.is_superadmin,
        403,
        "must be superadmin to refresh_legacy",
    );
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    ctx.state.task_runner.push({
        controller: ctx.params.cid,
        tenant: ctx.params.id,
        forced: true,
        run: async () =>
            legacy_convert({
                controller: ctx.params.cid,
                forced: true,
                db: d,
                metadb: ctx.state.db,
            }),
    });
    ctx.body = null;
});

tenant_router.get("/:id/calls/:callid/cdr", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);
    let callid: string = ctx.params.callid;
    // eslint-disable-next-line
    if (!uuid.check(callid)) {
        callid = uuid.v5({
            namespace: "063b8523-d954-4375-91eb-bfe0908b2610",
            name: callid,
        });
    }

    let redacted = false;
    if (ctx.query.redacted === "true") {
        redacted = true;
    }

    const cdr = await get_cdr(d, callid, redacted);

    // TODO: this route shows either 403 or 404 depending on if it exists
    // this is unideal
    await assert_any_psap_allowed(ctx, ctx.params.id, [
        cdr.controller,
        ...cdr.psaps_mentioned,
    ]);

    ctx.body = cdr;
});

/** escapes a string for usage in PostgreSQL's LIKE */
function like_escape(str: string): string {
    return str.replace("\\", "\\\\").replace("_", "\\_").replace("%", "\\%");
}

tenant_router.get("/:id/calls", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    const q: unknown = ctx.query;
    if (!is_calls_query(q)) {
        const e: any = new Error("parameters are invalid");
        e.expose = true;
        e.status = 400;
        throw e;
    }

    ctx.body = await d.tx(async t => {
        if (typeof q.tz === "string") {
            await t.none("SET LOCAL TIMEZONE TO $(tz)", { tz: q.tz });
        }

        const do_explain =
            typeof q.explain === "string"
                ? // eslint-disable-next-line
                  q.explain || "noanalyze"
                : null;

        let where = "WHERE TRUE";

        if (typeof q.where === "string") {
            const w: unknown = JSON.parse(q.where);
            user_assert(is_where_object(w), 400, "where is not WhereObject");
            where += ` AND ${format_where(w)}`;

            const cpsap = w.cpsap;
            if (cpsap === undefined) {
                await assert_all_psaps_allowed(ctx, ctx.params.id, ["*"]);
            } else {
                await assert_all_psaps_allowed(ctx, ctx.params.id, cpsap);
            }
        } else {
            await assert_all_psaps_allowed(ctx, ctx.params.id, ["*"]);
        }

        let weight = "";
        if (typeof q.omni === "string") {
            const omni: string = q.omni;
            const omni_array = omni.split(/\s+/);

            const pos_array: number[] = [];
            const trunk_array: number[] = [];
            const other_array: string[] = [];
            const pani_array: string[] = [];
            const loose_ani_array: string[] = [];
            const ani_array: string[] = [];
            const sip_array: string[] = [];
            const num_array: string[] = [];
            const cos_array: string[] = [];
            const calltaker_array: string[] = [];
            const ali_array: string[] = [];

            let abandoned: null | boolean = null;

            // [operator, seconds] tuple
            const duration_array: [string, number][] = [];
            const answer_array: [string, number][] = [];
            const transfer_reaction_time_array: [string, number][] = [];

            // [operator, time of day] tuple
            const time_array: [string, string][] = [];

            for (const str of omni_array) {
                let m: RegExpExecArray | null;
                if ((m = /^[pP](\d+)$/.exec(str))) {
                    pos_array.push(Number(m[1]));
                } else if ((m = /^[tT](\d+)$/.exec(str))) {
                    trunk_array.push(Number(m[1]));
                } else if ((m = /^[pP][aA][nN][iI]:(.+)$/.exec(str))) {
                    pani_array.push(m[1].replace(/[-().]/g, ""));
                } else if ((m = /^[aA][nN][iI]:(.+)$/.exec(str))) {
                    ani_array.push(m[1].replace(/[-().]/g, ""));
                } else if ((m = /^[sS][iI][pP]:(.+)$/.exec(str))) {
                    sip_array.push(m[1]);
                } else if ((m = /^[nN][uU][mM]:(.+)$/.exec(str))) {
                    num_array.push(m[1].replace(/[-().]/g, ""));
                } else if ((m = /^[cC][oO][sS]:(.+)$/.exec(str))) {
                    cos_array.push(m[1]);
                } else if (
                    (m = /^[cC][aA][lL][lL][tT][aA][kK][eE][rR]:(.+)$/.exec(
                        str,
                    ))
                ) {
                    calltaker_array.push(m[1]);
                } else if ((m = /^[aA][lL][iI]:(.+)$/.exec(str))) {
                    ali_array.push(m[1]);
                } else if ((m = /^([-().\d*]+)$/.exec(str))) {
                    loose_ani_array.push(m[1].replace(/[-().]/g, ""));
                    other_array.push(m[1]);
                } else if (
                    (m = /^[dD][uU][rR][aA][tT][iI][oO][nN]:?(>|<|=|>=|<=)([0-9a-zA-Z]+)$/.exec(
                        str,
                    ))
                ) {
                    const milli = ms(m[2]);
                    if (Number.isFinite(milli)) {
                        duration_array.push([m[1], milli / 1000]);
                    }
                } else if (
                    (m = /^[aA][nN][sS][wW][eE][rR]:?(>|<|>=|<=)([0-9a-zA-Z]+)$/.exec(
                        str,
                    ))
                ) {
                    const milli = ms(m[2]);
                    if (Number.isFinite(milli)) {
                        answer_array.push([m[1], milli / 1000]);
                    }
                } else if (
                    (m = /^[tT][rR][aA][nN][sS][fF][eE][rR][-_]?[tT][iI][mM][eE]:?(>|<|>=|<=)([0-9a-zA-Z]+)$/.exec(
                        str,
                    ))
                ) {
                    const milli = ms(m[2]);
                    if (Number.isFinite(milli)) {
                        transfer_reaction_time_array.push([m[1], milli / 1000]);
                    }
                } else if (
                    (m = /^[tT](?:[iI][mM][eE])?:?(>|<|>=|<=|=)((?:(?:0?[0-9]|1[0-9]|2[0-4]):[0-5][0-9](?::[0-5][0-9])?)|(?:0?[0-9]|1[0-2]):[0-5][0-9](?::[0-5][0-9])?[aApP][mM])$/.exec(
                        str,
                    ))
                ) {
                    time_array.push([m[1], m[2]]);
                } else if (
                    (m = /^[aA][bB][aA][nN][dD][oO][nN][eE][dD]$/.exec(str))
                ) {
                    abandoned = true;
                } else if (
                    (m = /^(!|[nN][oO][tT]-?)[-_]?[aA][bB][aA][nN][dD][oO][nN][eE][dD]$/.exec(
                        str,
                    ))
                ) {
                    abandoned = false;
                } else {
                    other_array.push(str);
                }
            }

            if (other_array.length > 0) {
                const omni_calltakers_loose = other_array.map(
                    e => "%" + like_escape(e) + "%",
                );
                const omni_cos_loose = other_array.map(e => like_escape(e));
                const omni_ani_loose = loose_ani_array.map(
                    e => "%" + like_escape(e) + "%",
                );

                where += pgp.as.format(
                    ` AND (calltaker ILIKE ANY($(omni_calltakers_loose)) OR cos ILIKE ANY($(omni_cos_loose)) OR ali_parsed_text ILIKE ANY ($(omni_calltakers_loose))
                    ${
                        loose_ani_array.length === 0
                            ? ""
                            : "OR ani ILIKE ANY($(omni_ani_loose)) OR sip ILIKE ANY($(omni_ani_loose))"
                    }
                )`,
                    { omni_calltakers_loose, omni_cos_loose, omni_ani_loose },
                );
                weight += pgp.as.format(
                    `+ (CASE WHEN calltaker ILIKE ANY($(omni_calltakers_loose)) THEN 1 ELSE 0 END)
                    + (CASE WHEN cos ILIKE ANY($(omni_cos_loose)) THEN 1 ELSE 0 END)
                    + (CASE WHEN ali_parsed_text ILIKE ANY($(omni_calltakers_loose)) THEN 1 ELSE 0 END)`,
                    { omni_calltakers_loose, omni_cos_loose },
                );
                if (loose_ani_array.length > 0) {
                    // we don't search pANI by default, it is rarely useful
                    weight += pgp.as.format(
                        `+ (CASE WHEN ani ILIKE ANY($(omni_ani_loose)) OR sip ILIKE ANY($(omni_ani_loose)) THEN 1 ELSE 0 END)`,
                        { omni_ani_loose },
                    );
                }
            }

            if (pos_array.length > 0) {
                where += pgp.as.format(` AND pos = ANY($(pos_array)::int[])`, {
                    pos_array,
                });
            }

            if (trunk_array.length > 0) {
                where += pgp.as.format(
                    ` AND trunk = ANY($(trunk_array)::int[])`,
                    { trunk_array },
                );
            }

            if (pani_array.length > 0) {
                where += pgp.as.format(` AND pani ILIKE ANY($(omni_pani))`, {
                    omni_pani: pani_array.map(e => "%" + like_escape(e) + "%"),
                });
            }

            if (ani_array.length > 0) {
                where += pgp.as.format(` AND ani ILIKE ANY($(omni_ani))`, {
                    omni_ani: ani_array.map(e => "%" + like_escape(e) + "%"),
                });
            }

            if (sip_array.length > 0) {
                where += pgp.as.format(` AND sip ILIKE ANY($(omni_sip))`, {
                    omni_sip: sip_array.map(e => "%" + like_escape(e) + "%"),
                });
            }

            if (num_array.length > 0) {
                where += pgp.as.format(
                    ` AND (sip ILIKE ANY($(omni_num)) OR ani ILIKE ANY($(omni_num)) OR pani ILIKE ANY($(omni_num)))`,
                    {
                        omni_num: num_array.map(
                            e => "%" + like_escape(e) + "%",
                        ),
                    },
                );
            }

            if (cos_array.length > 0) {
                where += pgp.as.format(` AND cos ILIKE ANY($(omni_cos))`, {
                    omni_cos: cos_array.map(e => like_escape(e)),
                });
            }

            if (calltaker_array.length > 0) {
                where += pgp.as.format(
                    ` AND calltaker ILIKE ANY($(omni_calltaker))`,
                    {
                        omni_calltaker: calltaker_array.map(
                            e => "%" + like_escape(e) + "%",
                        ),
                    },
                );
            }

            if (ali_array.length > 0) {
                where += pgp.as.format(
                    ` AND ali_vector @@ websearch_to_tsquery('english', $(omni_ali))`,
                    { omni_ali: ali_array.join(" ") },
                );
            }

            for (const e of duration_array) {
                const allowed_ops = new Set([">", "<", "=", ">=", "<="]);
                assert(allowed_ops.has(e[0]));
                // WARNING: YES THIS IS A BIT DANGEROUS, WE WHITELIST ALLOWED OPERATORS
                where += pgp.as.format(` AND call_length ${e[0]} $1`, [
                    `${e[1]} seconds`,
                ]);
            }

            for (const e of answer_array) {
                const allowed_ops = new Set([">", "<", "=", ">=", "<="]);
                assert(allowed_ops.has(e[0]));
                // WARNING: YES THIS IS A BIT DANGEROUS, WE WHITELIST ALLOWED OPERATORS
                where += pgp.as.format(` AND answer ${e[0]} $1`, [
                    `${e[1]} seconds`,
                ]);
            }

            for (const e of transfer_reaction_time_array) {
                const allowed_ops = new Set([">", "<", "=", ">=", "<="]);
                assert(allowed_ops.has(e[0]));
                // WARNING: YES THIS IS A BIT DANGEROUS, WE WHITELIST ALLOWED OPERATORS
                where += pgp.as.format(
                    ` AND transfer_reaction_time ${e[0]} $1`,
                    [`${e[1]} seconds`],
                );
            }

            for (const e of time_array) {
                const allowed_ops = new Set([">", "<", "=", ">=", "<="]);
                assert(allowed_ops.has(e[0]));
                // WARNING: YES THIS IS A BIT DANGEROUS, WE WHITELIST ALLOWED OPERATORS
                where += pgp.as.format(` AND utc::time ${e[0]} $1::time`, [
                    e[1],
                ]);
            }

            if (abandoned === true) {
                where += " AND abandoned";
            } else if (abandoned === false) {
                where += " AND NOT abandoned";
            }
        }

        let order: string | null = `${
            weight !== "" ? weight + " DESC, " : ""
        }utc DESC, xferid, incoming DESC`;
        if (typeof q.order === "string") {
            switch (q.order) {
                case "random":
                    order = "random()";
                    break;
                case "weighted_random":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }random()`;
                    break;
                case "utcd":
                    order = "utc DESC, xferid, incoming DESC";
                    break;
                case "weighted_utcd":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }utc DESC, xferid, incoming DESC`;
                    break;
                case "utc":
                    order = "utc, xferid, incoming DESC";
                    break;
                case "weighted_utc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }utc, xferid, incoming DESC`;
                    break;
                case "answer":
                    order = "answer";
                    break;
                case "answer_desc":
                    order = "answer DESC";
                    break;
                case "call_length":
                    order = "call_length";
                    break;
                case "call_length_desc":
                    order = "call_length DESC";
                    break;
                case "weighted_ani_sip":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }ani, sip`;
                    break;
                case "weighted_ani_sip_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }ani DESC, sip DESC`;
                    break;
                case "weighted_pos":
                    order = `${weight !== "" ? weight + " DESC, " : ""}pos`;
                    break;
                case "weighted_pos_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }pos DESC`;
                    break;
                // TODO: collation
                case "weighted_calltaker":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }calltaker`;
                    break;
                case "weighted_calltaker_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }calltaker DESC`;
                    break;
                case "weighted_trunk":
                    order = `${weight !== "" ? weight + " DESC, " : ""}trunk`;
                    break;
                case "weighted_trunk_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }trunk DESC`;
                    break;
                case "weighted_is_911":
                    order = `${weight !== "" ? weight + " DESC, " : ""}is_911`;
                    break;
                case "weighted_is_911_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }is_911 DESC`;
                    break;
                case "weighted_incoming":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }incoming`;
                    break;
                case "weighted_incoming_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }incoming DESC`;
                    break;
                case "weighted_cos":
                    order = `${weight !== "" ? weight + " DESC, " : ""}cos`;
                    break;
                case "weighted_cos_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }cos DESC`;
                    break;
                case "weighted_call_length":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }call_length`;
                    break;
                case "weighted_call_length_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }call_length DESC`;
                    break;
                case "weighted_transfer_reaction":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }transfer_reaction`;
                    break;
                case "weighted_transfer_reaction_desc":
                    order = `${
                        weight !== "" ? weight + " DESC, " : ""
                    }transfer_reaction DESC`;
                    break;
                case "none":
                    order = null;
                    break;
                default:
                    ctx.assert(false, 400, "order is not recognized");
            }
        }

        const limit =
            typeof q.limit === "string"
                ? Math.min(Number(q.limit), 10000)
                : 250;
        const offset =
            typeof q.offset === "string"
                ? Math.min(Number(q.offset), 100000)
                : 0;

        const query =
            // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
            (do_explain
                ? `EXPLAIN (FORMAT TEXT, VERBOSE, SUMMARY${
                      do_explain === "analyze" ? ", BUFFERS, ANALYZE" : ""
                  }) `
                : "") +
            pgp.as.format(
                `SELECT ${
                    weight !== "" ? weight : "0"
                } AS weight, controller, callid, xferid, incoming, legacy_callid, psap, EXTRACT(epoch FROM utc) AS utc,
                EXTRACT(EPOCH FROM answer) AS answer, EXTRACT(EPOCH FROM call_length) AS call_length, abandoned, pos, calltaker, is_911, trunk,
                transfer_reaction, transfer_type, cos, ani, pani, sip
            FROM private.xfer_info
            ${where}
            ${
                order !== null ? `ORDER BY ${order}` : ""
            } LIMIT $(limit) OFFSET $(offset)`,
                { limit, offset },
            );

        return {
            // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
            results: do_explain
                ? (await t.any<{ "QUERY PLAN": string }>(query))
                      .map(e => e["QUERY PLAN"])
                      .join("\n")
                : await t.any(query),
            limit,
            offset,
            query,
        };
    });
});

function format_where(where: WhereObject): string {
    let ret = "(TRUE";

    if (typeof where.from === "string") {
        ret += pgp.as.format(" AND utc >= date_trunc('day', $1::timestamptz)", [
            where.from,
        ]);
    }

    if (typeof where.to === "string") {
        ret += pgp.as.format(
            " AND utc < date_trunc('day', $1::timestamptz) + '1 day'::interval",
            [where.to],
        );
    }

    if (typeof where.from_exact === "string") {
        ret += pgp.as.format(" AND utc >= $1::timestamptz", [where.from_exact]);
    }

    if (typeof where.to_exact === "string") {
        ret += pgp.as.format(" AND utc < $1::timestamptz", [where.to_exact]);
    }

    if (typeof where.is_911 === "boolean") {
        if (where.is_911) {
            ret += " AND is_911";
        } else {
            ret += " AND NOT is_911";
        }
    }

    if (typeof where.abandoned === "boolean") {
        if (where.abandoned) {
            ret += " AND abandoned";
        } else {
            ret += " AND NOT abandoned";
        }
    }

    if (typeof where.incoming === "boolean") {
        if (where.incoming) {
            ret += " AND incoming";
        } else {
            ret += " AND NOT incoming";
        }
    }

    if (typeof where.intra_psap === "boolean") {
        if (where.intra_psap) {
            ret += " AND psap IS NOT DISTINCT FROM other_psap";
        } else {
            ret += " AND psap IS DISTINCT FROM other_psap";
        }
    }

    if (Array.isArray(where.transfer_type)) {
        ret += pgp.as.format(" AND transfer_type = ANY($1::text[])", [
            where.transfer_type,
        ]);
    }

    if (Array.isArray(where.transfer_type_not)) {
        ret += pgp.as.format(" AND transfer_type <> ALL($1::text[])", [
            where.transfer_type_not,
        ]);
    }

    if (Array.isArray(where.transfer_reaction)) {
        ret += pgp.as.format(" AND transfer_reaction = ANY($1::text[])", [
            where.transfer_reaction,
        ]);
    }

    if (Array.isArray(where.transfer_reaction_not)) {
        ret += pgp.as.format(" AND transfer_reaction <> ALL($1::text[])", [
            where.transfer_reaction_not,
        ]);
    }

    if (Array.isArray(where.cpsap)) {
        ret += pgp.as.format(
            " AND (psap = ANY($1::text[]) OR controller = ANY($1::text[]))",
            [where.cpsap],
        );
    }

    if (Array.isArray(where.psap)) {
        ret += pgp.as.format(" AND psap = ANY($1::text[])", [where.psap]);
    }

    if (Array.isArray(where.controller)) {
        ret += pgp.as.format(" AND controller = ANY($1::text[])", [
            where.controller,
        ]);
    }

    if (Array.isArray(where.calltaker)) {
        ret += pgp.as.format(" AND calltaker = ANY($1::text[])", [
            where.calltaker,
        ]);
    }

    if (Array.isArray(where.icalltaker)) {
        ret += pgp.as.format(
            ` AND (calltaker ILIKE ANY($1::text[])${
                where.icalltaker.some(x => x === null)
                    ? " OR calltaker IS NULL"
                    : ""
            })`,
            [
                where.icalltaker
                    .filter((x): x is string => typeof x === "string")
                    .map(like_escape),
            ],
        );
    }

    if (Array.isArray(where.cos)) {
        ret += pgp.as.format(
            ` AND (cos = ANY($1::text[])${
                where.cos.some(x => x === null) ? " OR cos IS NULL" : ""
            })`,
            [where.cos.filter(x => x !== null)],
        );
    }

    if (Array.isArray(where.coss)) {
        const cos = where.coss
            .flatMap(e =>
                COS_GROUPS.filter(([, grp]) => grp === e).map(([c]) => c),
            )
            .concat(where.coss.filter(x => x !== null) as string[]);
        ret += pgp.as.format(
            ` AND (cos = ANY($1::text[])${
                where.coss.some(x => x === null) ? " OR cos IS NULL" : ""
            })`,
            [cos],
        );
    }

    if (Array.isArray(where.trunk)) {
        ret += pgp.as.format(" AND trunk = ANY($1::int[])", [where.trunk]);
    }

    if (Array.isArray(where.pos)) {
        ret += pgp.as.format(" AND pos = ANY($1::int[])", [where.pos]);
    }

    if (Array.isArray(where.ani)) {
        ret += pgp.as.format(" AND ani = ANY($1::text[])", [where.ani]);
    }

    if (Array.isArray(where.pani)) {
        ret += pgp.as.format(" AND pani = ANY($1::text[])", [where.pani]);
    }

    if (Array.isArray(where.sip)) {
        ret += pgp.as.format(" AND sip = ANY($1::text[])", [where.sip]);
    }

    ret += ")";

    return ret;
}

tenant_router.get("/:id/report", async ctx => {
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    ctx.body = await d.tx(async t => {
        let tz: string;
        if (typeof ctx.query.tz === "string") {
            tz = ctx.query.tz;
            await t.none("SET LOCAL TIMEZONE TO $(tz)", { tz });
        } else {
            throw new Error("timezone is required");
        }

        type IField =
            | "answer"
            | "call_length"
            | "hourly_cnt"
            | "daily_cnt"
            | "monthly_cnt";
        type IAggregate =
            | {
                  t: "total";
              }
            | {
                  t: "total_perc";
              }
            | {
                  t: "count_transfers";
                  e: string;
              }
            | {
                  t: "count_transfers_not";
                  e: string;
              }
            | {
                  t: "max";
                  f: IField;
              }
            | {
                  t: "min";
                  f: IField;
              }
            | {
                  t: "avg";
                  f: IField;
              }
            | {
                  t: "percentile_cont";
                  f: IField;
                  p: number;
              }
            | {
                  t: "stddev_pop";
                  f: IField;
              }
            | {
                  t: "stddev_samp";
                  f: IField;
              }
            | {
                  t: "answer_less_than_seconds";
                  s: number;
                  /* not used here: r: number; */
              }
            | {
                  t: "count_abandoned";
              }
            | {
                  t: "count_failed_to_answer";
              }
            | {
                  t: "count_all_kinds_of_abandoned";
              }
            | {
                  t: "count_failed_xfer";
              };

        function format_agg(field_list_agg: IAggregate[]) {
            let top = "";
            let hourly = "";
            let daily = "";
            let monthly = "";
            let combine = "";

            function add(f: IField, name: string, format: string): void {
                combine += pgp.as.format(", $1:alias", [name]);
                switch (f) {
                    case "answer": {
                        const agg = pgp.as.format(format, { f });
                        top += pgp.as.format(
                            ", EXTRACT(EPOCH FROM $[agg:raw]) AS $[n:alias]",
                            { agg, n: name },
                        );
                        break;
                    }
                    case "call_length": {
                        const agg = pgp.as.format(format, { f });
                        top += pgp.as.format(
                            ", EXTRACT(EPOCH FROM $[agg:raw]) AS $[n:alias]",
                            { agg, n: name },
                        );
                        break;
                    }
                    case "hourly_cnt": {
                        const agg = pgp.as.format(format, { f: "cnt" });
                        hourly += pgp.as.format(
                            ", ($[agg:raw])::float AS $[n:alias]",
                            {
                                agg,
                                n: name,
                            },
                        );
                        break;
                    }
                    case "daily_cnt": {
                        const agg = pgp.as.format(format, { f: "cnt" });
                        daily += pgp.as.format(
                            ", ($[agg:raw])::float AS $[n:alias]",
                            {
                                agg,
                                n: name,
                            },
                        );
                        break;
                    }
                    case "monthly_cnt": {
                        const agg = pgp.as.format(format, { f: "cnt" });
                        monthly += pgp.as.format(
                            ", ($[agg:raw])::float AS $[n:alias]",
                            {
                                agg,
                                n: name,
                            },
                        );
                        break;
                    }
                }
            }

            let n = 0;
            for (const f of field_list_agg) {
                switch (f.t) {
                    case "total":
                        top += pgp.as.format(
                            ", COUNT(callid)::float AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "total_perc":
                        top += pgp.as.format(
                            ", COUNT(callid)::float AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "count_transfers":
                        top += pgp.as.format(
                            ", COALESCE(SUM((transfer_reaction = $[e])::int), 0)::float AS $[n:alias]",
                            { n: String(n), e: f.e },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "count_transfers_not":
                        top += pgp.as.format(
                            ", COALESCE(SUM((transfer_reaction <> $[e])::int), 0)::float AS $[n:alias]",
                            { n: String(n), e: f.e },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "count_abandoned":
                        top += pgp.as.format(
                            ", COALESCE(SUM((abandoned AND is_911 AND transfer_type = 'initial' AND incoming)::int), 0)::float AS $[n:alias]",
                            { n: String(n) },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "count_failed_to_answer":
                        top += pgp.as.format(
                            ", COALESCE(SUM((abandoned AND NOT is_911 AND transfer_type = 'initial' AND incoming)::int), 0)::float AS $[n:alias]",
                            { n: String(n) },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "count_all_kinds_of_abandoned":
                        top += pgp.as.format(
                            ", COALESCE(SUM(abandoned::int), 0)::float AS $[n:alias]",
                            { n: String(n) },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "count_failed_xfer":
                        top += pgp.as.format(
                            ", COALESCE(SUM((abandoned AND transfer_type <> 'initial')::int), 0)::float AS $[n:alias]",
                            { n: String(n) },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    case "max":
                        add(f.f, String(n), "MAX($[f:alias])");
                        break;
                    case "min":
                        add(f.f, String(n), "MIN($[f:alias])");
                        break;
                    case "avg":
                        add(f.f, String(n), "AVG($[f:alias])");
                        break;
                    case "percentile_cont":
                        add(
                            f.f,
                            String(n),
                            `percentile_cont(${pgp.as.format(
                                "$1",
                                f.p,
                            )}) WITHIN GROUP(ORDER BY $[f:alias])`,
                        );
                        break;
                    case "stddev_pop":
                        add(f.f, String(n), "stddev_pop($[f:alias])");
                        break;
                    case "stddev_samp":
                        add(f.f, String(n), "stddev_samp($[f:alias])");
                        break;
                    case "answer_less_than_seconds":
                        top += pgp.as.format(
                            ", SUM((answer < $[seconds])::int) / SUM((NOT abandoned)::int)::float AS $[n:alias]",
                            { seconds: `${f.s} seconds`, n: String(n) },
                        );
                        combine += pgp.as.format(
                            ", top.$[n:alias] AS $[n:alias]",
                            {
                                n: String(n),
                            },
                        );
                        break;
                    default:
                        ctx.throw(400, "unknown field type");
                        break;
                }
                n += 1;
            }

            return { top, hourly, daily, monthly, combine };
        }

        const fields = format_agg(JSON.parse(ctx.query.fields));
        const where = (() => {
            const ret: unknown = JSON.parse(ctx.query.where);
            user_assert(is_where_object(ret), 400, "where is not WhereObject");
            return ret;
        })();

        if (where.cpsap === undefined) {
            await assert_all_psaps_allowed(ctx, ctx.params.id, ["*"]);
        } else {
            await assert_all_psaps_allowed(ctx, ctx.params.id, where.cpsap);
        }

        const group_by = parse_group(ctx.query.group_by);

        const from = (
            await t.one("SELECT date_trunc('day', $1::timestamptz) AS from", [
                where.from,
            ])
        ).from;
        const to = (
            await t.one("SELECT date_trunc('day', $1::timestamptz) AS ret", [
                where.to,
            ])
        ).ret;

        type IGroup =
            | {
                  type: "simple";
                  select: string;
                  group_by: string;
              }
            | {
                  type: "complex";
                  join: string;
                  join_cond: string;
                  select: string;
              };

        function parse_group(name: string): IGroup {
            switch (name) {
                case "cos":
                    return {
                        type: "simple",
                        select: "cos",
                        group_by: "cos",
                    };
                case "coss": {
                    const coss = `(CASE WHEN cos IS NULL THEN 'No ALI Received' ${COS_GROUPS.map(
                        e =>
                            pgp.as.format(`WHEN cos = $1 THEN $2`, [
                                e[0],
                                e[1],
                            ]),
                    ).join(" ")} ELSE cos END)`;
                    return {
                        type: "simple",
                        select: `${coss} AS coss`,
                        group_by: coss,
                    };
                }
                case "pos":
                    // use really large position values to represent abandoned and failed to answer so they sort after
                    const p =
                        "CASE WHEN is_911 AND abandoned AND incoming AND transfer_type = 'initial' THEN 2147483646 WHEN abandoned AND incoming THEN 2147483647 ELSE pos END";
                    return {
                        type: "simple",
                        select: `(CASE WHEN (${p}) = 2147483646 THEN 'Abandoned' WHEN (${p}) = 2147483647 THEN 'Failed to Answer' ELSE (${p})::text END) AS pos`,
                        group_by: p,
                    };
                case "trunk":
                    return {
                        type: "simple",
                        select: "trunk",
                        group_by: "trunk",
                    };
                case "calltaker":
                    const c =
                        "CASE WHEN is_911 AND abandoned AND incoming AND transfer_type = 'initial' THEN 'Abandoned' WHEN abandoned AND incoming THEN 'Failed to Answer' ELSE calltaker END";
                    return {
                        type: "simple",
                        select: `${c} AS calltaker`,
                        group_by: c,
                    };
                case "icalltaker":
                    const ic =
                        "CASE WHEN is_911 AND abandoned AND incoming AND transfer_type = 'initial' THEN 'Abandoned' WHEN abandoned AND incoming THEN 'Failed to Answer' ELSE lower(calltaker) END";
                    return {
                        type: "simple",
                        select: `${ic} AS icalltaker`,
                        group_by: ic,
                    };
                case "day":
                    return {
                        type: "complex",
                        join: pgp.as.format(
                            `generate_series(date_trunc('day', $1::timestamptz), date_trunc('day', $2::timestamptz), '1 day')`,
                            [where.from, where.to],
                        ),
                        join_cond: "t.u = date_trunc('day', utc)",
                        select: "EXTRACT(EPOCH FROM top.u) AS day",
                    };
                case "hour":
                    return {
                        type: "complex",
                        join: pgp.as.format(
                            `generate_series(date_trunc('day', $1::timestamptz), date_trunc('day', $2::timestamptz) + '23 hours', '1 hour')`,
                            [where.from, where.to],
                        ),
                        join_cond: "t.u = date_trunc('hour', utc)",
                        select: "EXTRACT(EPOCH FROM top.u) AS hour",
                    };
                case "week":
                    return {
                        type: "complex",
                        join: pgp.as.format(
                            `generate_series(date_trunc('week', $1::timestamptz + '1 day'::interval) - '1 day'::interval,
                            date_trunc('week', $2::timestamptz + '1 day'::interval) - '1 day'::interval, '1 week')`,
                            [where.from, where.to],
                        ),
                        join_cond:
                            "t.u = date_trunc('week', utc + '1 day'::interval) - '1 day'::interval",
                        select: "EXTRACT(EPOCH FROM top.u) AS week",
                    };
                case "month":
                    return {
                        type: "complex",
                        join: pgp.as.format(
                            `generate_series(date_trunc('month', $1::timestamptz), date_trunc('month', $2::timestamptz), '1 month')`,
                            [where.from, where.to],
                        ),
                        join_cond: "t.u = date_trunc('month', utc)",
                        select: "EXTRACT(EPOCH FROM top.u) AS month",
                    };
                case "dow":
                    return {
                        type: "complex",
                        join: "generate_series(0,6)",
                        join_cond: "t.u = EXTRACT(DOW FROM utc)",
                        select: "top.u AS dow",
                    };
                case "hod":
                    return {
                        type: "complex",
                        join: "generate_series(0,23)",
                        join_cond: "t.u = EXTRACT(HOUR FROM utc)",
                        select: "top.u AS hod",
                    };
                case "moy":
                    return {
                        type: "complex",
                        join: "generate_series(1,12)",
                        join_cond: "t.u = EXTRACT(MONTH FROM utc)",
                        select: "top.u AS moy",
                    };
                default:
                    throw new Error("invalid group_by");
            }
        }

        let query: string;

        if (group_by.type === "simple") {
            query = `SELECT ${group_by.select}, GROUPING(${
                group_by.group_by
            }) AS grouping${fields.top}
            FROM private.xfer_info WHERE ${format_where(where)}
            GROUP BY ROLLUP(${group_by.group_by})
            ORDER BY ${group_by.group_by}`;
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        } else if (group_by.type === "complex") {
            const hour = pgp.as.format(
                `SELECT h.hour AS utc, COUNT(callid) AS cnt
                FROM generate_series(date_trunc('day', $1::timestamptz), date_trunc('day', $2::timestamptz) + '23 hours', '1 hour') h(hour)
                LEFT JOIN private.xfer_info
                ON h.hour = date_trunc('hour', utc) AND ${format_where(where)}
                GROUP BY 1`,
                [where.from, where.to],
            );
            const day = pgp.as.format(
                `SELECT d.day AS utc, COUNT(callid) AS cnt
                FROM generate_series(date_trunc('day', $1::timestamptz), date_trunc('day', $2::timestamptz), '1 day') d(day)
                LEFT JOIN private.xfer_info
                ON d.day = date_trunc('day', utc) AND ${format_where(where)}
                GROUP BY 1`,
                [where.from, where.to],
            );
            const month = pgp.as.format(
                `SELECT m.month AS utc, COUNT(callid) AS cnt
                FROM generate_series(date_trunc('month', $1::timestamptz), date_trunc('month', $2::timestamptz), '1 month') m(month)
                LEFT JOIN private.xfer_info
                ON m.month = date_trunc('month', utc) AND ${format_where(where)}
                GROUP BY 1`,
                [where.from, where.to],
            );

            const top = `SELECT t.u AS u, GROUPING(t.u) AS grouping${fields.top}
            FROM ${group_by.join} t(u) LEFT JOIN private.xfer_info
            ON ${group_by.join_cond} AND ${format_where(where)}
            GROUP BY ROLLUP(t.u)`;

            const daily = `SELECT t.u AS u, GROUPING(t.u) AS grouping${fields.daily}
            FROM ${group_by.join} t(u) LEFT JOIN (${day}) day
            ON ${group_by.join_cond}
            GROUP BY ROLLUP(t.u)`;

            const monthly = `SELECT t.u AS u, GROUPING(t.u) AS grouping${fields.monthly}
            FROM ${group_by.join} t(u) LEFT JOIN (${month}) month
            ON ${group_by.join_cond}
            GROUP BY ROLLUP(t.u)`;

            const hourly = `SELECT t.u AS u, GROUPING(t.u) AS grouping${fields.hourly}
            FROM ${group_by.join} t(u) LEFT JOIN (${hour}) hour
            ON ${group_by.join_cond}
            GROUP BY ROLLUP(t.u)`;

            query = `SELECT ${group_by.select}, top.grouping AS grouping${fields.combine}
            FROM (${top}) top
            `;
            // don't compute things we don't need to compute, aka if we don't have daily fields
            // don't have postgres doing crazy things needed to compute daily fields
            if (fields.daily !== "") {
                query += `JOIN (${daily}) daily
                    ON top.u IS NOT DISTINCT FROM daily.u AND top.grouping = daily.grouping
                    `;
            }
            if (fields.hourly !== "") {
                query += `JOIN (${hourly}) hourly
                    ON top.u IS NOT DISTINCT FROM hourly.u AND top.grouping = hourly.grouping
                    `;
            }
            if (fields.monthly !== "") {
                query += `JOIN (${monthly}) monthly
                    ON top.u IS NOT DISTINCT FROM monthly.u AND top.grouping = monthly.grouping
                    `;
            }
            query += `ORDER BY top.u, top.grouping`;
        } else {
            throw new Error("internal error");
        }

        console.log(query);

        const explain = (
            await t.any<{ "QUERY PLAN": string }>(
                "EXPLAIN (VERBOSE, SUMMARY) " + query,
            )
        )
            .map(e => e["QUERY PLAN"])
            .join("\n");

        const result = await t.any(query);
        const pre = {
            total: {
                ...result.find(e => e.grouping === 1),
                grouping: undefined,
            },
            table: result
                .filter(e => e.grouping === 0)
                // eslint-disable-next-line @typescript-eslint/no-unsafe-return
                .map(e => ({ ...e, grouping: undefined })),
            query,
            explain,
            tz,
            from,
            to,
        };

        const field_list: IAggregate[] = JSON.parse(ctx.query.fields);
        field_list.forEach((e, i) => {
            if (e.t === "total_perc") {
                for (const elem of pre.table) {
                    elem[i] /= pre.total[i];
                }
                pre.total[i] = 1;
            }
        });

        return pre;
    });
});

function format_fields(
    fields: string[],
): { top: string; bottom: string; combine: string } {
    let top = "";
    let bottom = "";
    let combine = "";
    for (const f of fields) {
        // top fields
        if (f === "total") {
            top += ", COUNT(callid)::float AS total";
            combine += ", top.total AS total";
        } else if (f === "max_answer") {
            top += ", EXTRACT(EPOCH FROM MAX(answer)) AS max_answer";
            combine += ", top.max_answer AS max_answer";
        } else if (f === "avg_answer") {
            top += ", EXTRACT (EPOCH FROM AVG(answer)) AS avg_answer";
            combine += ", top.avg_answer AS avg_answer";
        } else if (f === "answer_under_10") {
            top +=
                ", SUM((answer < '10 seconds'::interval)::int) / SUM((NOT abandoned)::int)::real AS answer_under_10";
            combine += ", top.answer_under_10 AS answer_under_10";
        } else if (f === "total_abandoned") {
            top +=
                ", COALESCE(SUM((abandoned)::int), 0)::float AS total_abandoned";
            combine += ", top.total_abandoned AS total_abandoned";
            // bottom fields
        } else if (f === "avg_cnt") {
            bottom += ", AVG(middle.count)::float AS avg_cnt";
            combine += ", bottom.avg_cnt AS avg_cnt";
        } else if (f === "max_cnt") {
            bottom += ", MAX(middle.count)::float AS max_cnt";
            combine += ", bottom.max_cnt AS max_cnt";
        } else if (f === "perc_95_cnt") {
            bottom +=
                ", percentile_cont(0.95) WITHIN GROUP(ORDER BY middle.count) AS perc_95_cnt";
            combine += ", bottom.perc_95_cnt AS perc_95_cnt";
            // else die
        } else {
            throw new Error("invalid field");
        }
    }

    return { top, bottom, combine };
}

tenant_router.get("/:id/pivot", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    ctx.body = await d.tx(async t => {
        if (typeof ctx.query.tz === "string") {
            await t.none("SET LOCAL TIMEZONE TO $(tz)", { tz: ctx.query.tz });
        }

        const where = (() => {
            const ret: unknown = JSON.parse(ctx.query.where);
            user_assert(is_where_object(ret), 400, "where is not WhereObject");
            return ret;
        })();

        function parse_group(
            name: string,
            which: string,
        ):
            | { select: string; order: string; join: undefined }
            | {
                  select: string;
                  order: string;
                  join: string;
                  join_cond: string;
              } {
            switch (name) {
                case "cos":
                    return { select: "cos", order: "cos", join: undefined };
                case "pos":
                    return { select: "pos", order: "pos", join: undefined };
                case "hour":
                    return {
                        select: "h.hour AS hour",
                        order: "h.hour",
                        join: pgp.as.format(
                            "generate_series(date_trunc('day', $1::timestamptz), date_trunc('day', $2::timestamptz + '23 hours'), '1 hour') h(hour)",
                            [where.from, where.to],
                        ),
                        join_cond: "(h.hour = date_trunc('hour', utc))",
                    };
                case "day":
                    return {
                        select: "d.day AS day",
                        order: "d.day",
                        join: pgp.as.format(
                            "generate_series(date_trunc('day', $1::timestamptz), date_trunc('day', $2::timestamptz), '1 day') d(day)",
                            [where.from, where.to],
                        ),
                        join_cond: "(d.day = date_trunc('day', utc))",
                    };
                default:
                    throw new Error("invalid group");
            }
        }

        const group_one = parse_group(ctx.query.group_one, "left");
        const group_two = parse_group(ctx.query.group_two, "right");
        const fields = format_fields(JSON.parse(ctx.query.fields));

        let from;

        if (group_one.join !== undefined && group_two.join !== undefined) {
            from = `(${group_one.join} CROSS JOIN ${
                group_two.join
            }) LEFT JOIN private.xfer_info ON ${group_one.join_cond} AND ${
                group_two.join_cond
            } AND ${format_where(where)}`;
        } else if (group_one.join !== undefined) {
            from = `${group_one.join} LEFT JOIN private.xfer_info ON ${
                group_one.join_cond
            } AND ${format_where(where)}`;
        } else if (group_two.join !== undefined) {
            from = `${group_two.join} LEFT JOIN private.xfer_info ON ${
                group_two.join_cond
            } AND ${format_where(where)}`;
        } else {
            from = `private.xfer_info WHERE ${format_where(where)}`;
        }

        const query = `SELECT ${group_one.select}, ${group_two.select}, GROUPING(${group_one.order}, ${group_two.order}) AS grouping${fields.top} FROM ${from} GROUP BY CUBE(${group_one.order}, ${group_two.order}) ORDER BY ${group_one.order}, ${group_two.order}`;

        console.log(query);
        console.log(
            (await t.any<{ "QUERY PLAN": string }>("EXPLAIN " + query))
                .map(e => e["QUERY PLAN"])
                .join("\n"),
        );

        const result = await t.any(query);
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return result;
    });
});

tenant_router.get("/:id/ali/info", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    ctx.body = {
        format_id: await d.any(
            "SELECT ali_parsed->>'format_id' AS format_id, char_length(ali_record->>'raw') AS length, count(*) AS count FROM private.ali_info GROUP BY 1, 2 ORDER BY 1 NULLS FIRST, 2",
        ),
    };
});

tenant_router.get("/:id/ali/random_unparsed", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    const ret = await d.one(
        "SELECT callid, ali_record FROM private.ali_info WHERE ali_parsed IS NULL ORDER BY random() LIMIT 1",
    );

    ctx.body = { ...ret.ali_record, callid: ret.callid };
});

tenant_router.get("/:id/ali/random_parsed", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    const ret = await d.one(
        "SELECT callid, ali_record FROM private.ali_info WHERE ali_parsed IS NOT NULL ORDER BY random() LIMIT 1",
    );

    ctx.body = { ...ret.ali_record, callid: ret.callid };
});

tenant_router.get("/:id/ali/random", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    const d = await get_tenant_db(ctx.state.db, ctx.params.id);

    const ret = await d.one(
        `SELECT callid, ali_record
        FROM private.ali_info
        WHERE ali_parsed->>'format_id' IS NOT DISTINCT FROM $(format_id)
            AND char_length(ali_record->>'raw') IS NOT DISTINCT FROM $(char_length)
        ORDER BY random() LIMIT 1`,
        { format_id: null, char_length: null, ...ctx.query },
    );

    ctx.body = { ...ret.ali_record, callid: ret.callid };
});

tenant_router.post("/:id/ali/parse", async ctx => {
    ctx.assert(ctx.state.is_superadmin, 403);
    ctx.assert(ctx.is("text/plain"), 400);

    const raw = ctx.request.body;
    ctx.assert(typeof raw === "string", 400);

    ctx.body = {
        results: ali_formats.map(e => {
            let result: any;
            try {
                result = parse_ali_single_format(e, raw, true);
            } catch (e) {
                result = e.message;
            }

            return { format_id: e.format_id, result };
        }),
    };
});

tenant_router.post(
    "/:id/ali/parse_with_format",
    koa_bodyparser({ enableTypes: ["json"] }),
    async ctx => {
        ctx.assert(ctx.state.is_superadmin, 403);
        ctx.assert(ctx.is("json"), 400);

        const body = ctx.request.body;
        ctx.assert(typeof body === "object", 400);

        let result: any;
        try {
            result = parse_ali_single_format(body.format, body.raw_ali, true);
        } catch (e) {
            result = e.message;
        }

        ctx.body = { result };
    },
);
