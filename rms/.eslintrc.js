module.exports = {
    env: {
        es6: true,
        node: true
    },
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: "tsconfig.json",
        sourceType: "module",
    },
    plugins: [
        "@typescript-eslint",
        "simple-import-sort",
    ],
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:prettier/recommended"
    ],
    reportUnusedDisableDirectives: true,
    rules: {
        "simple-import-sort/sort": "error",
        // some of these rules should be enabled because they are useful
        "@typescript-eslint/camelcase": "off", // don't do this

        // TODO: turn first to error, fix, turn second to error and remove first, fix
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/explicit-function-return-type": "off",

        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-inferrable-types": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-unused-vars": "off",
        "@typescript-eslint/no-use-before-define": "off",
        "@typescript-eslint/prefer-includes": "off",
        "@typescript-eslint/require-await": "off",
        "no-case-declarations": "off",
        "no-constant-condition": "off",
        "no-control-regex": "off", // ALI has this, a lot
        "no-inner-declarations": "off",

        "no-eval": "error",
        "no-dupe-else-if": "error",
        "no-setter-return": "error",
        // "require-atomic-updates": "error", // sounds good, too noisy
        "prefer-spread": "error",
        "prefer-rest-params": "error",
        "symbol-description": "error",
        "no-duplicate-imports": "error",
        "no-useless-computed-key": ["error", {enforceForClassMembers: true}],
        "no-useless-rename": "error",
        "no-var": "error",
        "object-shorthand": "error",
        "prefer-arrow-callback": "error",
        "prefer-const": "error",
        // "prefer-destructuring": "error",
        "prefer-numeric-literals": "error",
        // "prefer-template": "error",
        "no-useless-concat": "error",
        // "no-lonely-if": "error",
        // "no-nested-ternary": "error",
        "no-unneeded-ternary": "error",
        // "no-inline-comments": "error",
        "no-mixed-operators": ["error", {groups: [
            ["&&", "||"],
            ["&", "|", "^", "~", "<<", ">>", ">>>"],
            ["==", "!=", "===", "!==", ">", ">=", "<", "<="],
            ["in", "instanceof"],
        ]}],
        "no-new-object": "error",
        // "no-underscore-dangle": "error",
        "prefer-object-spread": "error",
        // "func-names": ["error", "as-needed"],
        "no-array-constructor": "error",
        "operator-assignment": "error",
        "prefer-exponentiation-operator": "error",
        "sort-vars": "error",

        "complexity": ["error", 110],
        "max-lines": ["error", 2000],
        "max-len": ["error", 835],
        "max-depth": ["error", 5],
        "max-lines-per-function": ["error", 610],
        "max-nested-callbacks": "error",
        "max-params": ["error", 7],
        "max-statements": ["error", 170],

        // nodejs stuff
        "no-buffer-constructor": "error",
        "no-path-concat": "error",

        "@typescript-eslint/default-param-last": "error",
        "@typescript-eslint/no-extra-parens": ["error", "all", {conditionalAssign: false, nestedBinaryExpressions: false}],
        "@typescript-eslint/no-unused-expressions": "error",
        "@typescript-eslint/no-useless-constructor": "error",
        // "@typescript-eslint/return-await": ["error", "in-try-catch"], // TODO: enable, and eventually set to "always"
        "@typescript-eslint/no-extra-semi": "error",

        "@typescript-eslint/array-type": "error",
        "@typescript-eslint/ban-ts-comment": "error",
        "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
        "@typescript-eslint/explicit-member-accessibility": ["error", {overrides: { constructors: "no-public" }}],
        "@typescript-eslint/member-ordering": "error",
        // "@typescript-eslint/naming-convention": "error", // TODO: configure and do?
        "@typescript-eslint/no-dynamic-delete": "error",
        "@typescript-eslint/no-extra-non-null-assertion": "error",
        "@typescript-eslint/no-extraneous-class": "error",
        "@typescript-eslint/no-floating-promises": "error",
        "@typescript-eslint/no-implied-eval": "error",
        "@typescript-eslint/no-non-null-asserted-optional-chain": "error",
        "@typescript-eslint/no-require-imports": "error",
        "@typescript-eslint/no-throw-literal": "error",
        "@typescript-eslint/no-unnecessary-condition": ["error", {allowConstantLoopConditions: true}],
        "@typescript-eslint/no-unnecessary-qualifier": "error",
        "@typescript-eslint/no-unnecessary-type-arguments": "error",
        "@typescript-eslint/prefer-as-const": "error",
        "@typescript-eslint/prefer-for-of": "error",
        "@typescript-eslint/prefer-function-type": "error",
        "@typescript-eslint/prefer-nullish-coalescing": "error",
        "@typescript-eslint/prefer-optional-chain": "error",
        "@typescript-eslint/prefer-readonly": "error",
        "@typescript-eslint/promise-function-async": "error",
        "@typescript-eslint/require-array-sort-compare": "error",
        "@typescript-eslint/restrict-template-expressions": ["error", {allowAny: true}], // TODO: remove allowAny
        "@typescript-eslint/strict-boolean-expressions": "error",
        "@typescript-eslint/unified-signatures": "error",
        "@typescript-eslint/no-unsafe-member-access": "off", // TODO: enable
        "@typescript-eslint/restrict-plus-operands": "error",
        "@typescript-eslint/no-unsafe-assignment": "error",
        "@typescript-eslint/no-unsafe-return": "error",
        "@typescript-eslint/ban-types": "error",
        "@typescript-eslint/no-unsafe-call": "error",
    }
};
