Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-buster64"

  # disable the vagrant-vbguest plugin, doesn't work for some reason
  config.vbguest.auto_update = false

  # install ansible
  config.vm.provision "shell", inline: <<-SHELL
    if [ ! -f /etc/first_run_done ]; then
      export DEBIAN_FRONTEND=noninteractive
      apt-get update
      apt-get -y install dirmngr
      echo deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main >> /etc/apt/sources.list
      apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
      apt-get update
      apt-get -y install ansible
      touch /etc/first_run_done
    fi
  SHELL

  config.vm.define "dev", primary: true do |dev|
    # forwarded ports aren't used, instead use `vagrant ssh -- -D1080` and point browser at proxy

    # metabase
    dev.vm.network "forwarded_port", guest: 3000, host: 3000, host_ip: "127.0.0.1"
    # stable SSH port
    dev.vm.network "forwarded_port", guest: 22, host: 2323, id: "ssh"

    dev.vm.network "private_network", ip: "10.7.0.3", virtualbox__intnet: 'wus_test'
    dev.vm.network "private_network", ip: "192.168.56.8"

    dev.vm.provider "virtualbox" do |vb|
      vb.cpus = 24
      vb.memory = 32768

      extra_storage = "./extra_storage.vdmk"
      unless File.exist?(extra_storage)
        vb.customize ["createhd", "--filename", extra_storage, "--size", 350 * 1024]
      end
      vb.customize ["storageattach", :id, "--medium", extra_storage, "--storagectl", "SATA Controller", "--port", 1, "--type", "hdd"]
    end

    dev.vm.provision "shell", run: "always", inline: <<-SHELL
      mkdir -p /vagrant/rms/node_modules
      mkdir -p /home/vagrant/.rms_node_modules
      mount --bind /home/vagrant/.rms_node_modules /vagrant/rms/node_modules
      chown vagrant:vagrant /home/vagrant/.rms_node_modules

      mkdir -p /vagrant/rms_web/node_modules
      mkdir -p /home/vagrant/.rms_web_node_modules
      mount --bind /home/vagrant/.rms_web_node_modules /vagrant/rms_web/node_modules
      chown vagrant:vagrant /home/vagrant/.rms_web_node_modules

      mkdir -p /vagrant/node_modules
      mkdir -p /home/vagrant/.vagrant_node_modules
      mount --bind /home/vagrant/.vagrant_node_modules /vagrant/node_modules
      chown vagrant:vagrant /home/vagrant/.vagrant_node_modules

      mkdir -p /run/wt_rms
      chown vagrant:vagrant /run/wt_rms
    SHELL

    dev.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "./playbooks/dev.yml"
    end

    dev.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "./playbooks/node_dev.yml"
    end

    dev.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "./playbooks/legacy_converter_dev.yml"
    end

    dev.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "./playbooks/metabase.yml"
    end

    dev.vm.hostname = "dev"
  end

  # JENKINS
  # requires manual steps:
  # go to localhost:8081 to initialize, follow instructions
  # install default/recommended plugins in installer
  # "continue as admin" (or make a default account)
  # "Configure Global Security" -> "Anyone can do anything" (auth is unnecessary, only localhost can access anyway)
  # install "Blue Ocean" plugin
  config.vm.define "jenkins", autostart: false do |jenkins|
    jenkins.vm.box = "debian/contrib-buster64"
    jenkins.vm.hostname = "jenkins"
    jenkins.vm.network "forwarded_port", guest: 8080, host: 8081, host_ip: "127.0.0.1"

    # don't allow jenkins to write to /vagrant
    jenkins.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=664"]

    jenkins.vm.provider "virtualbox" do |vb|
      vb.memory = 4096
    end

    jenkins.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "Jenkins/jenkins.yml"
    end
  end

  config.vm.define "deb9", autostart: false do |deb9|
    deb9.vm.box = "debian/contrib-stretch64"
    deb9.vm.hostname = "deb9"
    deb9.vm.network "private_network", ip: "10.7.0.4", virtualbox__intnet: 'wus_test'
    deb9.vm.network "private_network", ip: "192.168.56.9"

    # don't allow wt_rms to write to /vagrant
    deb9.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=664"]

    deb9.vm.provider "virtualbox" do |vb|
      vb.memory = 2048
    end

    deb9.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "playbooks/deploy.yml"
    end
  end
end
