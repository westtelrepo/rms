#!/bin/bash

set -e

export VERSION=2.0.6+$(git rev-parse --verify --short HEAD)
mkdir rms-$VERSION

# build the legacy_converter
CXX=clang++ CXXFLAGS="-Weverything -Wno-padded -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-missing-braces -Wno-return-std-move-in-c++11 --system-header-prefix=json/" \
    meson ./legacy_converter /tmp/build --buildtype release -Db_lto=true --warnlevel 3 --werror

pushd /tmp/build
ninja legacy_converter catch2-test
popd

/tmp/build/catch2-test --reporter junit --out junit-legacy_converter.xml
cp /tmp/build/legacy_converter rms-$VERSION/legacy_converter

yarn install --frozen-lockfile

# build the web frontend
pushd rms_web
# run the linter, allow no issues
yarn lint
NODE_ENV=production yarn run build
cp index.js ../rms-$VERSION/index.js
popd

# build the backend/server
pushd rms
mkdir src/schema/generated
yarn pre-build
# run the linter, allow no issues
yarn lint
echo "export const LEGACY_CONVERTER_PATH = '../legacy_converter'; export const INDEXJS_PATH = '../index.js';" > src/ci_const.ts
yarn run tsc
rm ./dist/tsconfig.tsbuildinfo
yarn run mocha --reporter=xunit --reporter-options output=../xunit-ci.xml
popd

rm -rf node_modules rms/node_modules rms_web/node_modules
echo '{"private":true,"workspaces":["rms"]}' > package.json
yarn install --production --frozen-lockfile

pushd rms
mkdir ../rms-$VERSION/server
cp -r ./dist ../rms-$VERSION/server/dist
cp ./WestTel_Logo.svg ../rms-$VERSION/server/
echo '{"main":"dist/index.js"}' > ../rms-$VERSION/server/package.json
echo "copy node_modules inside rms"
if [ -e ./node_modules ]; then
    cp -r ./node_modules ../rms-$VERSION/server/node_modules
fi
popd

echo "copy node_modules outside"
cp -r ./node_modules ./rms-$VERSION/

mkdir ./rms-$VERSION/node/
tar -xf /usr/local/src/node.tar.xz -C ./rms-$VERSION/node/ --strip-components=1

tar -cjf rms-$VERSION.tar.bz --sort=name --mtime="2020-10-27 16:48:33Z" --owner=0 --group=0 --numeric-owner rms-$VERSION
