# various possible improvements

Add some .vscode config into the repository to add some plugins be default.

Convert `rms/pre_build.js` to TypeScript and use `ts-node` to run it.

Have unit tests

Better document TLS stuff

Upgrade node (in dev VM and Jenkins)

Upgrade dependencies

Upgrade dev VM debian version

Fix lints and make them stricter

Make admin UX less awful

* document enterprise setups

Pagination of results (not trivial)

* allow full CSV download

Improve ALI editor

improve log import

* potentially remove ALI table, refactor logs table (RESEARCH NEEDED)
* allow legacy_converter to "suspend" somehow (MUCH RESEARCH NEEDED)

mass import/export of tenant configuration
