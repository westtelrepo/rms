/** @jsx jsx */
/** @jsxFrag React.Fragment */
import {
    DateRangeInput,
    FocusedInput,
    OnDatesChangeProps,
} from "@datepicker-react/styled";
import { css, jsx } from "@emotion/core";
import styled from "@emotion/styled";
import Immutable from "immutable";
import { DateTime } from "luxon";
import React, { useCallback, useMemo, useState } from "react";
import { Helmet } from "react-helmet";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { useDebouncedCallback } from "use-debounce";

import HelpTooltip from "./help_tooltip";
import { IAggregate } from "./iaggregate";
import Loading from "./loading";
import Search from "./search";
import { SearchHelp } from "./search_help";
import { useFetch } from "./useFetch";

const only911 = css`
    transition: height 0.5s;
    overflow: hidden;
`;

const Input = styled.input`
    font-size: inherit;
`;
const Table = styled.table`
    padding: 0px;
    border-spacing: 0px;
    width: 100%;
`;
const Label = styled.label`
    & > input {
        margin-right: 8px;
    }
`;
const Select = styled.select`
    font-size: inherit;
    color: inherit;
    option {
        color: black;
        font-size: 90%;
    }
`;
const Button = styled.button`
    font-size: inherit;
`;

const reportCss = css`
    width: 900px;
    margin: 0 auto;
    font-size: 22px;
`;

const unselected_style = css`
    color: #999;
`;

const subtitle_style = css`
    font-size: 14px;
`;

const MagicBar = styled.div`
    width: 100%;
    height: 25px;
    background-color: rgb(0, 55, 105);
    margin-top: 35px;
    margin-bottom: 10px;
`;

const ReportChoiceContainer = styled.div`
    margin-left: 15px;
    margin-right: 15px;
`;

const Where = styled.div`
    margin-left: 15px;
    margin-right: 15px;
`;

function VerticalDisappear({
    visible,
    children,
    className,
}: {
    visible: boolean;
    children: React.ReactNode;
    className?: string;
}) {
    const [height, set_height] = useState<undefined | number>(undefined);
    const set_height_ref = useCallback((e: HTMLDivElement | null) => {
        if (e !== null) {
            set_height(e.scrollHeight);
        }
    }, []);
    return (
        <div
            ref={set_height_ref}
            css={only911}
            className={className}
            style={{ height: visible ? height : 0 }}
        >
            {children}
        </div>
    );
}

function DateRange({
    from,
    to,
    onChange,
}: {
    from: string;
    to: string;
    onChange: (obj: { from: string; to: string }) => void;
}) {
    const [focus, set_focus] = useState<FocusedInput>(null);

    const from_date = useMemo(
        () => (from === "null" ? null : DateTime.fromISO(from).toJSDate()),
        [from],
    );
    const to_date = useMemo(
        () => (to === "null" ? null : DateTime.fromISO(to).toJSDate()),
        [to],
    );

    function onDatesChange(data: OnDatesChangeProps) {
        set_focus(data.focusedInput);
        onChange({
            from: data.startDate
                ? DateTime.fromJSDate(data.startDate).toFormat("yyyy-MM-dd")
                : "null",
            to: data.endDate
                ? DateTime.fromJSDate(data.endDate).toFormat("yyyy-MM-dd")
                : "null",
        });
    }

    return (
        <DateRangeInput
            onFocusChange={set_focus}
            focusedInput={focus}
            startDate={from_date}
            endDate={to_date}
            onDatesChange={onDatesChange}
            maxBookingDate={DateTime.local().startOf("day").toJSDate()}
            numberOfMonths={3}
        />
    );
}

export default function ReportUI() {
    const { tenantid } = useParams<{ tenantid: string }>();
    const location = useLocation();
    const history = useHistory();

    const psaps = useFetch<{ psap: string; name: string; timezone?: string }[]>(
        "/api/tenant/" + encodeURIComponent(tenantid) + "/psap",
    );

    const tenants = useFetch<
        { name: string; pretty_size: string; description: string | null }[]
    >("/api/tenant");
    const tenant_desc = tenants?.find(x => x.name === tenantid)?.description;

    function make_link(key: string, value: any) {
        const params = new URLSearchParams(location.search);
        if (typeof value === "string") {
            if (
                value === "" &&
                key !== "at_sec1" &&
                key !== "at_sec2" &&
                key !== "at_red1" &&
                key !== "at_red2"
            ) {
                params.delete(key);
            } else {
                params.set(key, value);
            }
        } else {
            params.set(key, JSON.stringify(value));
        }
        return (
            "/tenant/" +
            encodeURIComponent(tenantid) +
            "/reportui?" +
            String(params)
        );
    }

    function go(key: string, value: any) {
        history.replace(make_link(key, value));
    }

    const [omni_text, set_omni_text] = useState<string | null>(null);
    const { callback: set_omni } = useDebouncedCallback((str: string) => {
        go("omni", str);
    }, 1000);

    const search = new URLSearchParams(location.search);
    const type = search.get("type") ?? "911";
    const time_opt = search.get("time_opt") ?? "hod";
    const at_opt = search.get("at_opt") ?? "hod";
    const radio = search.get("radio") ?? "time";
    // TODO: not ideal... fix
    const psap =
        search.get("psap") ??
        (psaps && psaps.length > 0 ? psaps[0].psap : "null");
    const timezone = psaps?.find(x => x.psap === psap)?.timezone ?? "UTC";
    const direction = search.get("direction") ?? "inbound";
    // default is beginning of this year
    const from =
        search.get("from") ??
        DateTime.local().startOf("year").toFormat("yyyy-LL-dd");
    // default is yesterday
    const to =
        search.get("to") ??
        DateTime.local()
            .startOf("day")
            .minus({ days: 1 })
            .toFormat("yyyy-LL-dd");
    const omni = omni_text ?? search.get("omni") ?? "";

    const at_sec1 = search.get("at_sec1") ?? "10";
    const at_sec2 = search.get("at_sec2") ?? "20";

    const at_red1 = search.get("at_red1") ?? "90";
    const at_red2 = search.get("at_red2") ?? "95";

    const time_opt_select = (
        <Select
            value={time_opt}
            onChange={e => go("time_opt", e.target.value)}
            onClick={() => go("radio", "time")}
        >
            <option value="hod">Hour of Day</option>
            <option value="dow">Day of Week</option>
            <option value="moy">Month of Year</option>
            <option value="day">Day</option>
            <option value="week">Week</option>
            <option value="month">Month</option>
        </Select>
    );

    const at_opt_select = (
        <Select
            value={at_opt}
            onChange={e => go("at_opt", e.target.value)}
            onClick={() => go("radio", "atime")}
        >
            <option value="hod">Hour of Day</option>
            <option value="dow">Day of Week</option>
            <option value="moy">Month of Year</option>
            <option value="day">Day</option>
            <option value="week">Week</option>
            <option value="month">Month</option>
        </Select>
    );

    function changeRadio(event: React.ChangeEvent<HTMLInputElement>) {
        go("radio", event.target.value);
    }

    const where: {
        [index: string]: any;
        incoming?: boolean;
        is_911?: boolean;
        transfer_type?: string[];
        transfer_type_not?: string[];
    } = {};
    where.from = from === "null" ? undefined : from;
    where.to = to === "null" ? undefined : to;
    where.intra_psap = false;
    switch (type) {
        case "911":
            where.is_911 = true;
            break;
        case "admin":
            where.is_911 = false;
            break;
    }

    switch (direction) {
        case "inbound":
            where.incoming = true;
            where.transfer_type = ["initial"];
            break;
        case "ixfer":
            where.incoming = true;
            where.transfer_type_not = ["initial"];
            break;
        case "outbound":
            where.incoming = false;
            where.transfer_type = ["initial"];
            break;
        case "oixfer":
            where.incoming = false;
            where.transfer_type_not = ["initial"];
            break;
    }

    if (psap !== "null") {
        where.cpsap = [psap];
    }

    let group_by = "hod";
    let fields: IAggregate[] = [{ t: "total" }, { t: "total_perc" }];
    switch (radio) {
        case "time":
            group_by = time_opt;
            switch (group_by) {
                case "month":
                case "week":
                case "day":
                case "hour":
                    fields = [
                        { t: "total" },
                        { t: "total_perc" },
                        { t: "count_transfers", e: "tandem" },
                        { t: "count_transfers", e: "ng911" },
                        { t: "count_transfers", e: "blind" },
                        { t: "count_transfers", e: "attended" },
                        { t: "count_transfers", e: "conference" },
                        { t: "count_transfers_not", e: "none" },
                    ];
                    break;
                case "hod":
                    fields = [
                        { t: "avg", f: "hourly_cnt" },
                        { t: "max", f: "hourly_cnt" },
                        { t: "total" },
                        { t: "total_perc" },
                        { t: "count_transfers", e: "tandem" },
                        { t: "count_transfers", e: "ng911" },
                        { t: "count_transfers", e: "blind" },
                        { t: "count_transfers", e: "attended" },
                        { t: "count_transfers", e: "conference" },
                        { t: "count_transfers_not", e: "none" },
                    ];
                    break;
                case "dow":
                    fields = [
                        { t: "avg", f: "daily_cnt" },
                        { t: "max", f: "daily_cnt" },
                        { t: "total" },
                        { t: "total_perc" },
                        { t: "count_transfers", e: "tandem" },
                        { t: "count_transfers", e: "ng911" },
                        { t: "count_transfers", e: "blind" },
                        { t: "count_transfers", e: "attended" },
                        { t: "count_transfers", e: "conference" },
                        { t: "count_transfers_not", e: "none" },
                    ];
                    break;
                case "moy":
                    fields = [
                        { t: "avg", f: "monthly_cnt" },
                        { t: "max", f: "monthly_cnt" },
                        { t: "total" },
                        { t: "total_perc" },
                        { t: "count_transfers", e: "tandem" },
                        { t: "count_transfers", e: "ng911" },
                        { t: "count_transfers", e: "blind" },
                        { t: "count_transfers", e: "attended" },
                        { t: "count_transfers", e: "conference" },
                        { t: "count_transfers_not", e: "none" },
                    ];
                    break;
            }
            break;
        case "atime":
            group_by = at_opt;
            fields = [
                { t: "avg", f: "answer" },
                {
                    t: "answer_less_than_seconds",
                    s: Number(at_sec1),
                    r: Number(at_red1) / 100,
                },
                {
                    t: "answer_less_than_seconds",
                    s: Number(at_sec2),
                    r: Number(at_red2) / 100,
                },
                { t: "max", f: "answer" },
                { t: "avg", f: "call_length" },
                { t: "total" },
            ];
            if (where.incoming === true || where.incoming === undefined) {
                const transfer_type = Immutable.Set(
                    where.transfer_type === undefined
                        ? ["initial", "other"]
                        : where.transfer_type,
                ).subtract(
                    Immutable.Set(
                        where.transfer_type_not === undefined
                            ? []
                            : where.transfer_type_not,
                    ),
                );
                // these can only show up if "initial" transfers are allowed
                if (transfer_type.has("initial")) {
                    if (where.is_911 === true || where.is_911 === undefined) {
                        fields.push({ t: "count_abandoned" });
                    }
                    if (where.is_911 === false || where.is_911 === undefined) {
                        fields.push({ t: "count_failed_to_answer" });
                    }
                }

                // these can only show up if non "initial" transfers are allowed
                if (transfer_type.has("other")) {
                    fields.push({ t: "count_failed_xfer" });
                }
            }
            // TODO: outgoing
            break;
        case "pos":
            group_by = "pos";
            fields = [
                { t: "total" },
                { t: "total_perc" },
                { t: "count_transfers", e: "tandem" },
                { t: "count_transfers", e: "ng911" },
                { t: "count_transfers", e: "blind" },
                { t: "count_transfers", e: "attended" },
                { t: "count_transfers", e: "conference" },
                { t: "count_transfers_not", e: "none" },
            ];
            break;
        case "calltaker":
            group_by = "icalltaker";
            fields = [
                { t: "total" },
                { t: "total_perc" },
                { t: "count_transfers", e: "tandem" },
                { t: "count_transfers", e: "ng911" },
                { t: "count_transfers", e: "blind" },
                { t: "count_transfers", e: "attended" },
                { t: "count_transfers", e: "conference" },
                { t: "count_transfers_not", e: "none" },
            ];
            break;
        case "at_ct":
            group_by = "icalltaker";
            fields = [
                { t: "avg", f: "answer" },
                {
                    t: "answer_less_than_seconds",
                    s: Number(at_sec1),
                    r: Number(at_red1) / 100,
                },
                {
                    t: "answer_less_than_seconds",
                    s: Number(at_sec2),
                    r: Number(at_red2) / 100,
                },
                { t: "max", f: "answer" },
                { t: "avg", f: "call_length" },
                { t: "total" },
            ];

            break;
        case "cos":
            group_by = "cos";
            break;
        case "coss":
            group_by = "coss";
            break;
        case "trunk":
            group_by = "trunk";
            break;
    }

    const subtitle = (() => {
        let ret = "";
        if (type === "911") {
            ret += "9-1-1 ";
        } else if (type === "admin") {
            ret += "Admin ";
        } else if (type === "total") {
            ret += "Total ";
        }

        if (direction === "inbound") {
            ret += "Inbound Calls ";
        } else if (direction === "ixfer") {
            ret += "Inbound PSAP Transfers ";
        } else if (direction === "outbound") {
            ret += "Outbound Calls ";
        } else if (direction === "oixfer") {
            ret += "Outbound Transfers ";
        }

        ret += " – ";

        if (radio === "atime" || radio === "at_ct") {
            ret += "Answer Time";
        } else {
            ret += "Calls";
        }

        return ret;
    })();

    const report_url =
        "/tenant/" +
        encodeURIComponent(tenantid) +
        "/report" +
        "?tz=" +
        encodeURIComponent(timezone) +
        "&group_by=" +
        encodeURIComponent(group_by) +
        "&where=" +
        encodeURIComponent(JSON.stringify(where)) +
        "&fields=" +
        encodeURIComponent(JSON.stringify(fields)) +
        "&subtitle=" +
        encodeURIComponent(subtitle);

    let body: JSX.Element;
    const search_omni = search.get("omni");
    if (search_omni === null) {
        const at_sec_params = (visible: boolean) => {
            const input_style = css`
                width: 8ch;
            `;
            return (
                <VerticalDisappear
                    visible={visible}
                    css={css`
                        margin-left: 40px;
                        font-size: 75%;
                    `}
                >
                    <label>
                        Percentage of calls answered within{" "}
                        <input
                            css={input_style}
                            type="number"
                            value={at_sec1}
                            onChange={e => go("at_sec1", e.target.value)}
                            min="0"
                            step="any"
                        />{" "}
                        seconds,{" "}
                    </label>
                    <label>
                        show red under{" "}
                        <input
                            css={input_style}
                            type="number"
                            value={at_red1}
                            onChange={e => go("at_red1", e.target.value)}
                            min="0"
                            max="100"
                            step="any"
                        />
                        %
                    </label>
                    <br />
                    <label>
                        Percentage of calls answered within{" "}
                        <input
                            css={input_style}
                            type="number"
                            value={at_sec2}
                            onChange={e => go("at_sec2", e.target.value)}
                            min="0"
                            step="any"
                        />{" "}
                        seconds,{" "}
                    </label>
                    <label>
                        show red under{" "}
                        <input
                            css={input_style}
                            type="number"
                            value={at_red2}
                            onChange={e => go("at_red2", e.target.value)}
                            min="0"
                            max="100"
                            step="any"
                        />
                        %
                    </label>
                </VerticalDisappear>
            );
        };
        body = (
            <>
                <ReportChoiceContainer>
                    <div style={{ marginTop: 15 }}>CALL COUNT</div>
                    <hr />
                    <Label
                        css={radio !== "time" ? unselected_style : undefined}
                    >
                        <input
                            type="radio"
                            value="time"
                            onChange={changeRadio}
                            checked={radio === "time"}
                        />
                        Calls by {time_opt_select}
                    </Label>{" "}
                    <HelpTooltip>
                        Links to “drill down” into specific data are only
                        available for the Calls by Day, Week, and Month reports.
                    </HelpTooltip>
                    <br />
                    <Label css={radio !== "pos" ? unselected_style : undefined}>
                        <input
                            type="radio"
                            value="pos"
                            onChange={changeRadio}
                            checked={radio === "pos"}
                        />
                        Calls by Position
                    </Label>
                    <br />
                    <Label
                        css={
                            radio !== "calltaker" ? unselected_style : undefined
                        }
                    >
                        <input
                            type="radio"
                            value="calltaker"
                            onChange={changeRadio}
                            checked={radio === "calltaker"}
                        />
                        Calls by Call-Taker
                    </Label>
                    <br />
                    <VerticalDisappear visible={type === "911"}>
                        <Label
                            css={radio !== "cos" ? unselected_style : undefined}
                        >
                            <input
                                type="radio"
                                value="cos"
                                onChange={changeRadio}
                                checked={radio === "cos"}
                            />
                            Calls by Class of Service
                        </Label>
                        <br />
                        <Label
                            css={
                                radio !== "coss" ? unselected_style : undefined
                            }
                        >
                            <input
                                type="radio"
                                value="coss"
                                onChange={changeRadio}
                                checked={radio === "coss"}
                            />
                            Calls by Class of Service Summary
                        </Label>
                        <br />
                    </VerticalDisappear>
                    <Label
                        css={radio !== "trunk" ? unselected_style : undefined}
                    >
                        <input
                            type="radio"
                            value="trunk"
                            onChange={changeRadio}
                            checked={radio === "trunk"}
                        />
                        Calls by Trunk/Line
                    </Label>
                    <br />
                    <div style={{ marginTop: 15 }}>ANSWER TIME</div>
                    <hr />
                    <Label
                        css={radio !== "atime" ? unselected_style : undefined}
                    >
                        <input
                            type="radio"
                            value="atime"
                            onChange={changeRadio}
                            checked={radio === "atime"}
                        />
                        Answer Time by {at_opt_select}
                    </Label>{" "}
                    <HelpTooltip>
                        Links to “drill down” into specific data are only
                        available for the Answer Time by Day, Week and Month
                        reports.
                    </HelpTooltip>
                    {at_sec_params(radio === "atime")}
                    <Label
                        css={radio !== "at_ct" ? unselected_style : undefined}
                    >
                        <input
                            type="radio"
                            value="at_ct"
                            onChange={changeRadio}
                            checked={radio === "at_ct"}
                        />
                        Answer Time by Call-Taker
                    </Label>
                    {at_sec_params(radio === "at_ct")}
                </ReportChoiceContainer>
                <MagicBar style={{ height: 15 }} />
                <div
                    css={css`
                        margin-left: 15px;
                        margin-right: 15px;
                    `}
                >
                    <Button
                        onClick={() => {
                            history.push(report_url);
                        }}
                        disabled={from === "null" || to === "null"}
                        css={css`
                            margin-left: auto;
                            margin-right: 0;
                            display: block;
                        `}
                    >
                        Create Report
                    </Button>
                </div>
            </>
        );
    } else {
        const new_search = new URLSearchParams();
        new_search.set("tz", timezone);
        new_search.set("omni", search_omni);
        new_search.set("where", JSON.stringify(where));
        new_search.set("order", "weighted_utcd");
        body = <Search search={"?" + String(new_search)} />;
    }

    if (!psaps) {
        return <Loading msg="Loading" />;
    }

    return (
        <div css={reportCss}>
            <Helmet>
                <title>MIS {tenant_desc ?? tenantid} Create Report</title>
            </Helmet>
            <MagicBar />
            <Where>
                <div style={{ marginBottom: 10 }}>
                    <div css={subtitle_style}>PSAP</div>
                    <div>
                        <Select
                            style={{ width: "100%" }}
                            value={psap}
                            onChange={e => go("psap", e.target.value)}
                        >
                            {psaps.length > 0 ? (
                                psaps.map(e => (
                                    <option key={e.name} value={e.psap}>
                                        {e.name}
                                    </option>
                                ))
                            ) : (
                                <option value="null">EVERYTHING</option>
                            )}
                        </Select>
                    </div>
                </div>
                <Table style={{ marginBottom: 10 }}>
                    <tbody>
                        <tr css={subtitle_style}>
                            <td>TYPE</td>
                            <td>DIRECTION</td>
                            <td style={{ width: "100%" }} />
                            <td>DATE RANGE</td>
                        </tr>
                        <tr>
                            <td style={{ paddingRight: 10 }}>
                                <Select
                                    value={type}
                                    onChange={e => go("type", e.target.value)}
                                    style={{ width: 100 }}
                                >
                                    <option value="911">9-1-1</option>
                                    <option value="admin">Admin</option>
                                    <option value="total">Total</option>
                                </Select>
                            </td>
                            <td>
                                <Select
                                    value={direction}
                                    onChange={e =>
                                        go("direction", e.target.value)
                                    }
                                    style={{ width: 300 }}
                                >
                                    <option value="inbound">
                                        Inbound Calls
                                    </option>
                                    <option value="ixfer">
                                        Inbound PSAP Transfers
                                    </option>
                                    <option value="outbound">
                                        Outbound Calls
                                    </option>
                                    <option value="oixfer">
                                        Outbound Transfers
                                    </option>
                                </Select>
                            </td>
                            <td />
                            <td>
                                <DateRange
                                    from={from}
                                    to={to}
                                    onChange={e => {
                                        const p = new URLSearchParams(
                                            location.search,
                                        );
                                        p.set("from", e.from);
                                        p.set("to", e.to);
                                        history.replace(
                                            location.pathname + "?" + String(p),
                                        );
                                    }}
                                />
                            </td>
                        </tr>
                    </tbody>
                </Table>
                <div css={subtitle_style}>
                    SEARCH <SearchHelp />
                </div>
                <Input
                    style={{ width: "100%" }}
                    type="search"
                    value={omni}
                    onChange={e => {
                        set_omni_text(e.target.value);
                        set_omni(e.target.value);
                    }}
                />
                <div
                    css={css`
                        margin-top: 10px;
                        display: flex;
                        flex-direction: row;
                        justify-content: flex-end;
                    `}
                >
                    <Button
                        onClick={() => {
                            set_omni_text(null);
                            history.replace(location.pathname);
                        }}
                    >
                        Reset Form
                    </Button>
                    {omni === "" ? (
                        <Button
                            css={css`
                                margin-left: 0.5em;
                            `}
                            onClick={() => {
                                set_omni_text(" ");
                                set_omni(" ");
                            }}
                        >
                            Search CDR
                        </Button>
                    ) : (
                        <Button
                            css={css`
                                margin-left: 0.5em;
                            `}
                            onClick={() => {
                                set_omni_text(null);
                                set_omni("");
                            }}
                        >
                            Clear CDR Search
                        </Button>
                    )}
                </div>
            </Where>
            <MagicBar />
            {body}
        </div>
    );
}
