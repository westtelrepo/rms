import React, { createContext, useCallback, useContext, useState } from "react";
import useSWR from "swr";

import openid from "./openid";

interface IState {
    state: string;
    sub: string;
    preferred_username: string;
}

const OIDCContext = createContext<IState>(null as any);

const FetchContext = createContext<
    (input: RequestInfo, init?: RequestInit) => Promise<Response>
>(() => {
    throw Error("not usable");
});

export function useOIDCState(): IState {
    return useContext(OIDCContext);
}

export function useFetchFunc(): (
    input: RequestInfo,
    init?: RequestInit,
) => Promise<Response> {
    return useContext(FetchContext);
}

interface IFetchConfig {
    refreshInterval?: number;
    revalidateOnFocus?: boolean;
}

export function useFetch<T>(
    url: string | null,
    config?: IFetchConfig,
): T | null {
    const gfetch = useFetchFunc();
    const with_json = useCallback(
        async (u: string) => {
            const d = await gfetch(u);
            if (!d.ok) {
                throw Error("data not ok");
            }
            return d.json();
        },
        [gfetch],
    );
    const { data } = useSWR<T>(url, with_json, config);
    return data === undefined ? null : data;
}

export function useFetchText(url: string): string | null {
    const gfetch = useFetchFunc();
    const with_text = useCallback(
        async (u: string) => {
            const d = await gfetch(u);
            if (!d.ok) {
                throw Error("data not ok");
            }
            return d.text();
        },
        [gfetch],
    );
    const { data } = useSWR<string>(url, with_text);
    return data === undefined ? null : data;
}

class GlobalFetch {
    public readonly sub: string;
    public readonly preferred_username: string;

    private readonly check_session_iframe?: string;

    private refresh_promise: Promise<void> | null = null;
    private session_state: string;
    private _state: string;
    private csrf: string | Promise<string>;

    constructor(private readonly set_state: (arg: IState) => void) {
        this.sub = openid.sub;
        this.check_session_iframe = openid.check_session_iframe;
        this.session_state = openid.session_state;
        this.preferred_username = openid.preferred_username;
        this.setup_iframe().catch(e =>
            console.error("could not run setup_iframe!", e),
        );
        this._state = "login";
        this.set_state({
            state: "login",
            preferred_username: this.preferred_username,
            sub: this.sub,
        });
        this.csrf = (async () => {
            const get = await fetch("/api/csrf");
            const ret: string = (await get.json()).csrf;
            this.csrf = ret;
            return ret;
        })();
    }

    public get state() {
        return this._state;
    }

    public async fetch(
        input: RequestInfo,
        init_arg?: RequestInit,
    ): Promise<Response> {
        // if refreshing, wait until done to do
        if (this.refresh_promise) {
            await this.refresh_promise;
        }

        // WARNING: on Microsoft Edge, Headers constructor does not like `undefined`
        const init = {
            ...init_arg,
            headers: new Headers(init_arg ? init_arg.headers ?? {} : {}),
        };
        init.headers.set("X-OpenID-Sub", this.sub);
        const csrf1 =
            typeof this.csrf === "string" ? this.csrf : await this.csrf;
        init.headers.set("X-CSRF", csrf1);

        const res = await fetch(input, init);
        if (
            res.status === 403 &&
            res.headers.get("X-403-Reason") === "refresh"
        ) {
            await this.refresh();

            const csrf2 =
                typeof this.csrf === "string" ? this.csrf : await this.csrf;
            init.headers.set("X-CSRF", csrf2);

            const try_again = await fetch(input, init);
            return try_again;
        } else {
            return res;
        }
    }

    private async refresh(): Promise<void> {
        const do_iframe = async () => {
            const iframe = document.createElement("iframe");
            iframe.sandbox.value = "allow-scripts allow-same-origin";
            iframe.style.display = "none";
            document.body.appendChild(iframe);
            try {
                iframe.src =
                    "/openid/refresh?sub=" + encodeURIComponent(this.sub);

                let resolve: () => void;
                const promise = new Promise<void>(res => {
                    resolve = res;
                });

                const rec = (ev: MessageEvent) => {
                    if (
                        ev.source === iframe.contentWindow &&
                        ev.origin === location.origin
                    ) {
                        const obj = JSON.parse(ev.data);
                        if (obj.state === "error" || obj.sub !== this.sub) {
                            this._state = "error";
                            this.set_state({
                                state: "error",
                                preferred_username: this.preferred_username,
                                sub: this.sub,
                            });
                        }
                        if (obj.session_state) {
                            this.session_state = obj.session_state;
                        }
                        resolve();
                    }
                };
                window.addEventListener("message", rec, false);

                // race 10 second timeout
                await Promise.race([
                    promise,
                    new Promise(res => setTimeout(res, 10000)),
                ]);
                window.removeEventListener("message", rec, false);
                // refresh csrf token (may not always be necessary)
                this.csrf = (async () => {
                    const get = await fetch("/api/csrf");
                    const csrf: string = (await get.json()).csrf;
                    this.csrf = csrf;
                    return csrf;
                })();
            } finally {
                document.body.removeChild(iframe);
                this.refresh_promise = null;
            }
        };

        if (this.refresh_promise === null) {
            this.refresh_promise = do_iframe();
        }
        return this.refresh_promise;
    }

    private async setup_iframe() {
        if (!this.check_session_iframe) {
            return;
        }

        const check_session_origin = new URL(this.check_session_iframe).origin;

        // TODO: we don't have any mechanism for cleanup here
        const i = window.document.createElement("iframe");
        i.style.display = "none";
        i.sandbox.value = "allow-scripts allow-same-origin";
        window.document.body.appendChild(i);
        i.src = this.check_session_iframe;

        while (true) {
            const intervalID = setInterval(() => {
                if (i.contentWindow && this.state === "login") {
                    i.contentWindow!.postMessage(
                        openid.client_id + " " + this.session_state,
                        check_session_origin,
                    );
                }
            }, 1000);

            let got_message = false;

            await new Promise(resolve => {
                const receiveMessage = async (ev: MessageEvent) => {
                    if (
                        ev.origin === check_session_origin &&
                        ev.source === i.contentWindow &&
                        !got_message
                    ) {
                        if (ev.data === "changed") {
                            got_message = true;
                            window.removeEventListener(
                                "message",
                                receiveMessage,
                                false,
                            );
                            clearInterval(intervalID);
                            await this.refresh();
                            resolve();
                        } else if (ev.data === "error") {
                            console.log("error from session management");
                        }
                    }
                };
                window.addEventListener("message", receiveMessage, false);
            });
        }
    }
}

export function OIDCSession({ children }: { children: React.ReactNode }) {
    const [state, set_state] = useState<IState>({
        sub: "",
        preferred_username: "",
        state: "login",
    });

    const [{ do_fetch }] = useState(() => {
        const ret = new GlobalFetch(set_state);
        return { do_fetch: ret.fetch.bind(ret) };
    });

    return (
        <OIDCContext.Provider value={state}>
            <FetchContext.Provider value={do_fetch}>
                {children}
            </FetchContext.Provider>
        </OIDCContext.Provider>
    );
}
