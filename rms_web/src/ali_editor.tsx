import React, { useState } from "react";
import { Link, useParams } from "react-router-dom";
import useSWR from "swr";

import { format_ali } from "./cdr";
import Loading from "./loading";
import { useFetch, useFetchFunc } from "./useFetch";

export default function AliEditor() {
    const { tenantid } = useParams<{ tenantid?: string }>();
    if (!tenantid) {
        throw new Error("");
    }

    const ali_info = useFetch<{
        format_id: { format_id: string; length: number; count: string }[];
    }>("/api/tenant/" + encodeURIComponent(tenantid) + "/ali/info");

    const [ali_record_all, set_ali_record] = useState<null | {
        raw: string;
        callid: string;
    }>(null);
    const ali_record = ali_record_all?.raw ?? "";
    const [ali_format, set_ali_format] = useState("");
    const [parsed, set_parsed] = useState<any>(undefined);
    const fetch_func = useFetchFunc();

    async function post_get(uri: string, raw: string): Promise<any> {
        const data = await fetch_func(uri, {
            method: "POST",
            headers: {
                "Content-Type": "text/plain",
            },
            body: raw,
        });
        return data.json();
    }

    const { data: parse_results } = useSWR<{
        results: { format_id: string; result: any }[];
    }>(
        [
            "/api/tenant/" + encodeURIComponent(tenantid) + "/ali/parse",
            ali_record,
        ],
        post_get,
    );

    async function load_random() {
        const data = await fetch_func(
            "/api/tenant/" +
                encodeURIComponent(tenantid!) +
                "/ali/random_unparsed",
        );
        if (data.ok) {
            const record = await data.json();
            set_ali_record(record);
        }
    }

    async function load_parsed() {
        const data = await fetch_func(
            "/api/tenant/" +
                encodeURIComponent(tenantid!) +
                "/ali/random_parsed",
        );
        if (data.ok) {
            const record = await data.json();
            set_ali_record(record);
        }
    }

    async function parse() {
        const data = await fetch_func(
            "/api/tenant/" +
                encodeURIComponent(tenantid!) +
                "/ali/parse_with_format",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    raw_ali: ali_record,
                    format: JSON.parse(ali_format),
                }),
            },
        );
        if (data.ok) {
            const j = await data.json();
            set_parsed(j.result);
        }
    }

    async function load(format_id: string, length: number) {
        const q = new URLSearchParams();
        if (format_id) {
            q.set("format_id", format_id);
        }
        if (length) {
            q.set("char_length", String(length));
        }

        const data = await fetch_func(
            "/api/tenant/" + encodeURIComponent(tenantid!) + "/ali/random?" + q,
        );
        if (data.ok) {
            const record = await data.json();
            set_ali_record(record);
        }
    }

    return (
        <>
            <h1>{tenantid}</h1>
            <table>
                <thead>
                    <tr>
                        <th>Format ID</th>
                        <th>Length</th>
                        <th>Count</th>
                        <th />
                    </tr>
                </thead>
                <tbody>
                    {ali_info ? (
                        ali_info.format_id.map(e => (
                            <tr key={e.format_id + "_" + e.length}>
                                <td>{e.format_id}</td>
                                <td>{e.length}</td>
                                <td>{e.count}</td>
                                <td>
                                    <button
                                        onClick={load.bind(
                                            null,
                                            e.format_id,
                                            e.length,
                                        )}
                                    >
                                        Load Random
                                    </button>
                                </td>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan={3}>
                                <Loading msg="Loading" />
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
            <div>
                <button onClick={load_random}>Load Random Unparsed ALI</button>
                <button onClick={load_parsed}>Load Random Parsed ALI</button>
                <div>Total length: {ali_record.length}</div>
                {ali_record_all ? (
                    <Link
                        to={`/tenant/${encodeURIComponent(
                            tenantid,
                        )}/calls/${encodeURIComponent(
                            ali_record_all.callid,
                        )}/cdr`}
                    >
                        CDR
                    </Link>
                ) : null}
                <pre>{format_ali(ali_record, "CR")}</pre>
                <textarea
                    value={ali_format}
                    onChange={e => set_ali_format(e.target.value)}
                />
                <button onClick={parse}>Parse</button>
                <pre>{JSON.stringify(parsed)}</pre>
                <table>
                    <thead>
                        <tr>
                            <td>Format ID</td>
                            <td>Result</td>
                        </tr>
                    </thead>
                    <tbody>
                        {parse_results
                            ? parse_results.results.map(e => (
                                  <tr key={e.format_id}>
                                      <td>{e.format_id}</td>
                                      <td>
                                          <span
                                              style={{
                                                  fontFamily:
                                                      "monospace, monospace",
                                              }}
                                          >
                                              {typeof e.result === "string"
                                                  ? e.result
                                                  : JSON.stringify(e.result)}
                                          </span>
                                      </td>
                                  </tr>
                              ))
                            : null}
                    </tbody>
                </table>
            </div>
        </>
    );
}
