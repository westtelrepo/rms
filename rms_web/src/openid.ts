const elem = document.getElementById("openid")!;

interface OpenIDJson {
    sub: string;
    check_session_iframe?: string;
    session_state: string;
    preferred_username: string;
    client_id: string;
    openid_iss_origin: string;
}

const openid: OpenIDJson = JSON.parse(elem.textContent!);

// remove the child, removing the nonce from the document
// no idea if this is necessary or useful
elem.parentNode!.removeChild(elem);

export default openid;
