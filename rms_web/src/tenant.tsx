import React, { useState } from "react";
import { trigger } from "swr";

import { useFetch, useFetchFunc, useFetchText } from "./useFetch";

function ControllerEditor({
    tenantid,
    id,
    config,
    refresh,
}: {
    tenantid: string;
    id: string;
    config: any;
    refresh: () => Promise<void>;
}) {
    const dir = config.legacy?.dir ? config.legacy.dir : null;

    const [type, set_type] = useState("sftp");
    const [input_dir, set_input_dir] = useState("");
    const [cred, set_cred] = useState("");

    const server_creds = useFetch<any[]>("/api/ssh/creds");

    const gfetch = useFetchFunc();

    async function set() {
        let content_type: string;
        let body: string;
        switch (type) {
            case "sftp":
                content_type = "application/json";
                const match = /^(.*)@(.*):(\d*)$/.exec(cred);
                if (match === null) {
                    throw new Error("invalid credential name");
                }
                body = JSON.stringify({
                    t: "sftp",
                    host: match[2],
                    port: Number(match[3]),
                    user: match[1],
                    dir: input_dir,
                });
                break;
            case "dir":
                content_type = "text/plain";
                body = input_dir;
                break;
            default:
                throw new Error("unknown type of directory");
        }
        await gfetch(
            "/api/tenant/" +
                encodeURIComponent(tenantid) +
                "/controllers/" +
                encodeURIComponent(id) +
                "/legacy/dir",
            {
                method: "PUT",
                headers: {
                    "Content-Type": content_type,
                },
                body,
            },
        );
        await refresh();
    }

    return (
        <>
            <tr>
                <td />
                <td colSpan={3}>
                    Current dir config:{" "}
                    <pre>{JSON.stringify(dir, null, 2)}</pre>
                    <br />
                    <select
                        value={type}
                        onChange={e => set_type(e.target.value)}
                    >
                        <option value="sftp">SFTP (SSH)</option>
                        <option value="dir">Local Directory</option>
                    </select>
                    {server_creds && type === "sftp" ? (
                        <select
                            value={cred}
                            onChange={e => set_cred(e.target.value)}
                        >
                            <option value="">Select Credential</option>
                            {server_creds.map((e: any) => (
                                <option
                                    key={
                                        e.username + "@" + e.host + ":" + e.port
                                    }
                                    value={
                                        e.username + "@" + e.host + ":" + e.port
                                    }
                                >
                                    {e.username + "@" + e.host}
                                    {e.port === 22 ? undefined : ":" + e.port}
                                </option>
                            ))}
                        </select>
                    ) : null}
                    Directory:{" "}
                    <input
                        type="text"
                        value={input_dir}
                        onChange={e => set_input_dir(e.target.value)}
                    />
                    <button
                        onClick={set}
                        disabled={
                            (cred === "" && type === "sftp") || input_dir === ""
                        }
                    >
                        Set Config
                    </button>
                </td>
            </tr>
        </>
    );
}

export default function Tenant(props: { name: string }) {
    const { name } = props;

    const roles = useFetch<string[]>(
        "/api/tenant/" + encodeURIComponent(name) + "/roles",
    );
    const [new_role, set_new_role] = useState("");

    const views = useFetch<{ name: string; config: any }[]>(
        "/api/tenant/" + encodeURIComponent(name) + "/views",
    );
    const [new_view, set_new_view] = useState("");
    const [view_config, set_view_config] = useState("");

    const controllers = useFetch<{ controller: string; config: unknown }[]>(
        "/api/tenant/" + encodeURIComponent(name) + "/controllers",
    );
    const [new_controller, set_new_controller] = useState("");
    const [new_controller_config, set_new_controller_config] = useState("");

    const psaps = useFetch<{ psap: string; name: string; timezone?: string }[]>(
        "/api/tenant/" + encodeURIComponent(name) + "/psap",
    );
    const [new_psap, set_new_psap] = useState("");
    const [new_psap_name, set_new_psap_name] = useState("");
    const [new_psap_tz, set_new_psap_tz] = useState("");

    const comment_field = useFetchText(
        "/api/tenant/" + encodeURIComponent(name) + "/comment",
    );
    const [comment, set_comment] = useState<null | string>(null);

    const gfetch = useFetchFunc();

    async function refresh() {
        void trigger("/api/tenant/" + encodeURIComponent(name) + "/roles");
        void trigger("/api/tenant/" + encodeURIComponent(name) + "/views");
        void trigger(
            "/api/tenant/" + encodeURIComponent(name) + "/controllers",
        );
        void trigger("/api/tenant/" + encodeURIComponent(name) + "/psap");
        void trigger("/api/tenant/" + encodeURIComponent(name) + "/comment");
    }

    async function create_role() {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/roles/" +
                encodeURIComponent(new_role),
            { method: "PUT" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function drop_role(role_name: string) {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/roles/" +
                encodeURIComponent(role_name),
            { method: "DELETE" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function create_view() {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/views/" +
                encodeURIComponent(new_view),
            {
                method: "PUT",
                body: view_config,
                headers: {
                    "Content-Type": "application/json",
                },
            },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function drop_view(view_name: string) {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/views/" +
                encodeURIComponent(view_name),
            { method: "DELETE" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function upsert_controller() {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/controllers/" +
                encodeURIComponent(new_controller),
            {
                method: "PUT",
                body: new_controller_config,
                headers: {
                    "Content-Type": "application/json",
                },
            },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function upsert_psap() {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/psap/" +
                encodeURIComponent(new_psap),
            {
                method: "PUT",
                body: JSON.stringify({
                    name: new_psap_name,
                    timezone: new_psap_tz,
                }),
                headers: {
                    "Content-Type": "application/json",
                },
            },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function drop_controller(controller: string) {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/controllers/" +
                encodeURIComponent(controller),
            { method: "DELETE" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function refresh_controller(controller: string) {
        const data = await gfetch(
            "/api/tenant/" +
                encodeURIComponent(name) +
                "/controllers/" +
                encodeURIComponent(controller) +
                "/refresh_legacy",
            { method: "POST" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
    }

    async function refresh_ali() {
        const data = await gfetch(
            "/api/tenant/" + encodeURIComponent(name) + "/try_parse_ali",
            { method: "POST" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
    }

    async function put_comment() {
        const data = await gfetch(
            "/api/tenant/" + encodeURIComponent(name) + "/comment",
            {
                method: "PUT",
                body: comment,
                headers: {
                    "Content-Type": "text/plain",
                },
            },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        set_comment(null);
        await refresh();
    }

    async function rename() {
        const new_name = window.prompt(
            "Set the new name of the database (BIG FAT WARNING)",
            name,
        );
        if (typeof new_name === "string") {
            const data = await gfetch(
                "/api/tenant/" + encodeURIComponent(name) + "/rename_to",
                {
                    method: "POST",
                    body: new_name,
                    headers: {
                        "Content-Type": "text/plain",
                    },
                },
            );

            if (!data.ok) {
                throw new Error("data not ok");
            }

            window.alert(`Renamed to ${new_name}!`);
        } else {
            window.alert("Did not rename");
        }
    }

    return (
        <div>
            <input
                type="text"
                value={comment ?? comment_field ?? ""}
                onChange={e => set_comment(e.target.value)}
            />{" "}
            <button onClick={put_comment}>Set Comment</button>
            <br />
            <button onClick={rename}>Rename</button>
            <br />
            <button onClick={refresh_ali}>Refresh ALI</button>
            <br />
            <input
                type="text"
                value={new_role}
                onChange={e => set_new_role(e.target.value)}
            />{" "}
            <button onClick={create_role}>Create Role</button>
            <table>
                <tbody>
                    {roles
                        ? roles.map(e => (
                              <tr key={e}>
                                  <td>{e}</td>
                                  <td>
                                      <button onClick={drop_role.bind(null, e)}>
                                          Drop
                                      </button>
                                  </td>
                              </tr>
                          ))
                        : null}
                </tbody>
            </table>
            <input
                type="text"
                value={new_view}
                onChange={e => set_new_view(e.target.value)}
            />
            <input
                type="text"
                value={view_config}
                onChange={e => set_view_config(e.target.value)}
            />
            <button onClick={create_view}>Create View</button>
            <table>
                <tbody>
                    {views
                        ? views.map(e => (
                              <tr key={e.name}>
                                  <td>{e.name}</td>
                                  <td>{JSON.stringify(e.config)}</td>
                                  <td>
                                      <button
                                          onClick={drop_view.bind(null, e.name)}
                                      >
                                          Drop
                                      </button>
                                  </td>
                              </tr>
                          ))
                        : null}
                </tbody>
            </table>
            ID:{" "}
            <input
                type="text"
                value={new_controller}
                onChange={e => set_new_controller(e.target.value)}
            />
            Config (JSON):{" "}
            <input
                type="text"
                value={new_controller_config}
                onChange={e => set_new_controller_config(e.target.value)}
            />
            <button onClick={upsert_controller}>
                Create/Replace Controller Config
            </button>
            <table>
                <tbody>
                    {controllers
                        ? controllers.map(e => (
                              <React.Fragment key={e.controller}>
                                  <tr>
                                      <td>{e.controller}</td>
                                      <td>{JSON.stringify(e.config)}</td>
                                      <td>
                                          <button
                                              onClick={drop_controller.bind(
                                                  null,
                                                  e.controller,
                                              )}
                                          >
                                              Drop
                                          </button>
                                      </td>
                                      <td>
                                          <button
                                              onClick={refresh_controller.bind(
                                                  null,
                                                  e.controller,
                                              )}
                                          >
                                              Refresh
                                          </button>
                                      </td>
                                  </tr>
                                  <ControllerEditor
                                      tenantid={name}
                                      id={e.controller}
                                      config={e.config}
                                      refresh={refresh}
                                  />
                              </React.Fragment>
                          ))
                        : null}
                </tbody>
            </table>
            <h1>PSAPS</h1>
            ID:{" "}
            <input
                type="text"
                value={new_psap}
                onChange={e => set_new_psap(e.target.value)}
            />
            Name:{" "}
            <input
                type="text"
                value={new_psap_name}
                onChange={e => set_new_psap_name(e.target.value)}
            />
            TZ:{" "}
            <input
                type="text"
                value={new_psap_tz}
                onChange={e => set_new_psap_tz(e.target.value)}
            />
            <button onClick={upsert_psap}>Update PSAP</button>
            <table>
                <tbody>
                    {psaps
                        ? psaps.map(e => (
                              <tr key={e.psap}>
                                  <td>{e.psap}</td>
                                  <td>{e.name}</td>
                                  <td>{e.timezone}</td>
                                  {}
                              </tr>
                          ))
                        : null}
                </tbody>
            </table>
        </div>
    );
}
