/** @jsx jsx */
import { css, jsx } from "@emotion/core";

export default function Loading({ msg }: { msg: string }) {
    // creating a loading ellipsis with CSS
    const style = css`
        font-size: 50px;
        text-align: center;
        &:after {
            display: inline-block;
            animation: ellipsis steps(1, end) 1s infinite;
            content: "";
            width: 0;
        }
        @keyframes ellipsis {
            0% {
                content: "";
            }
            25% {
                content: ".";
            }
            50% {
                content: "..";
            }
            75% {
                content: "...";
            }
            100% {
                content: "";
            }
        }
    `;
    return <div css={style}>{msg}</div>;
}
