/** @jsx jsx */
/** @jsxFrag React.Fragment */
import { css, jsx } from "@emotion/core";
import humanizeDuration from "humanize-duration";
import React from "react";
import { Link } from "react-router-dom";

import Loading from "./loading";
import { useFetch } from "./useFetch";

interface IMonitor {
    tenant: string;
    controller: string;
    legacy_last_call: null | number;
    legacy_last_log: null | number;
    legacy_last_successful_import: null | number;
    legacy_interval: null | number;
}

export default function Monitor() {
    const stuff = useFetch<IMonitor[]>("/api/monitor", {
        refreshInterval: 120000,
    });
    const tasks = useFetch<any>("/api/tasks", { refreshInterval: 1000 });

    const pre_style = css`
        max-width: 100%;
        overflow: auto;
        max-height: 500px;
    `;

    const tableCss = css`
        border-collapse: collapse;
        th,
        td {
            border: 1px solid black;
            padding: 2px;
        }
    `;
    return (
        <>
            <h1>Monitoring</h1>
            <table css={tableCss}>
                <thead>
                    <tr>
                        <th>Tenant</th>
                        <th>Controller</th>
                        <th>Last Call</th>
                        <th>Last Log</th>
                        <th>Last Successful Import (legacy)</th>
                        <th>Last Full Legacy Import</th>
                    </tr>
                </thead>
                <tbody>
                    {stuff ? (
                        stuff.map(e => (
                            <tr key={e.tenant + "+" + e.controller}>
                                <td>{e.tenant}</td>
                                <td>{e.controller}</td>
                                <td>
                                    {e.legacy_last_call
                                        ? new Date(
                                              e.legacy_last_call * 1000,
                                          ).toLocaleString()
                                        : "unknown"}
                                </td>
                                <td>
                                    {e.legacy_last_log
                                        ? new Date(
                                              e.legacy_last_log * 1000,
                                          ).toLocaleString()
                                        : "unknown"}
                                </td>
                                <td>
                                    {e.legacy_last_successful_import
                                        ? new Date(
                                              e.legacy_last_successful_import *
                                                  1000,
                                          ).toLocaleString()
                                        : "unknown"}
                                </td>
                                <td>
                                    {e.legacy_interval
                                        ? humanizeDuration(
                                              e.legacy_interval * 1000,
                                              { round: true },
                                          )
                                        : "unknown"}
                                </td>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan={6}>
                                <Loading msg="Loading" />
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
            <h1>Task List</h1>
            <pre css={pre_style}>{JSON.stringify(tasks, null, 2)}</pre>
        </>
    );
}

interface IMonitorALI {
    tenant: string;
    ali_unparsed_count: string;
}

export function MonitorALI() {
    const stuff = useFetch<IMonitorALI[]>("/api/monitor_ali");
    const tableCss = css`
        border-collapse: collapse;
        th,
        td {
            border: 1px solid black;
            padding: 2px;
        }
    `;
    return (
        <table css={tableCss}>
            <thead>
                <tr>
                    <th>Tenant</th>
                    <th>Unparsed ALI Count</th>
                </tr>
            </thead>
            <tbody>
                {stuff ? (
                    stuff.map(e => (
                        <tr key={e.tenant}>
                            <td>
                                <Link
                                    to={`/tenant/${encodeURIComponent(
                                        e.tenant,
                                    )}/ali_editor`}
                                >
                                    {e.tenant}
                                </Link>
                            </td>
                            <td>{e.ali_unparsed_count}</td>
                        </tr>
                    ))
                ) : (
                    <tr>
                        <td colSpan={2}>
                            <Loading msg="Loading" />
                        </td>
                    </tr>
                )}
            </tbody>
        </table>
    );
}
