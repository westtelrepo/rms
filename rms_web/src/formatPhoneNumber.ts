export function formatPhoneNumber(num: string): string {
    let regex: RegExpExecArray | null;
    if ((regex = /^(1|9|91)(\d{3})(\d{3})(\d{4})$/.exec(num))) {
        return regex[1] + " (" + regex[2] + ") " + regex[3] + "-" + regex[4];
    } else if ((regex = /^(\d{3})(\d{3})(\d{4})$/.exec(num))) {
        return "(" + regex[1] + ") " + regex[2] + "-" + regex[3];
    } else if ((regex = /^9(\d{3})(\d{4})$/.exec(num))) {
        return "9." + regex[1] + "-" + regex[2];
    } else if ((regex = /^(\d{3})(\d{4})$/.exec(num))) {
        return regex[1] + "-" + regex[2];
    } else {
        return num;
    }
}
