import styled from "@emotion/styled";
import React, { useState } from "react";

const TooltipContainer = styled.span`
    position: relative;
`;

const Blue = styled.span`
    color: blue;
    cursor: pointer;
    font-variant-position: super;
`;

const Behind = styled.div`
    opacity: 0;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    z-index: 1000;
`;

const TooltipContents = styled.div`
    position: absolute;
    background-color: rgb(217, 226, 243);
    top: 10px;
    left: 10px;
    padding: 10px;
    font-size: 16px;
    white-space: nowrap;
    transition: all 0.5s;
    z-index: 1001;
`;

export default function HelpTooltip({
    children,
}: {
    children: React.ReactNode;
}) {
    const [visible, set_visible] = useState(false);

    return (
        <TooltipContainer>
            <Blue onClick={() => set_visible(e => !e)}>[?]</Blue>
            <Behind
                style={visible ? {} : { display: "none" }}
                onClick={() => set_visible(false)}
            />
            <TooltipContents
                style={
                    visible
                        ? { transform: "scale(1)", opacity: 1 }
                        : { transform: "scale(0)", opacity: 0 }
                }
            >
                {children}
            </TooltipContents>
        </TooltipContainer>
    );
}
