/** @jsx jsx */
/** @jsxFrag React.Fragment */
/* eslint-disable @typescript-eslint/no-unsafe-call */

import { css, jsx } from "@emotion/core";
import { Buffer } from "buffer";
import { DateTime } from "luxon";
import React from "react";
import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";

import { useDebugMode } from "./debug";
import { formatPhoneNumber } from "./formatPhoneNumber";
import { PrintIcon } from "./open_iconic";
import { useFetch } from "./useFetch";

export function base64_to_ascii_plus(str: string): string {
    const buf = Buffer.from(str, "base64");
    let ret = "";
    for (const c of buf.values()) {
        if (c === 0 || c > 127) {
            ret += String.fromCharCode(0xe000 + c);
        } else {
            ret += String.fromCharCode(c);
        }
    }
    return ret;
}

export function format_ali(ali: string, line_split: any): React.ReactNode {
    const ret: React.ReactNode[] = [];
    let insert_newline = 0;

    if (typeof line_split !== "number") {
        line_split = 300;
    }

    const control_style = css`
        color: green;
    `;
    for (let i = 0; i < ali.length; i++) {
        const c = ali[i];
        if (c === "\u0002") {
            ret.push(<span css={control_style}>{"<STX>"}</span>);
        } else if (c === "\r") {
            if (ali[i + 1] === "\n") {
                i++;
                ret.push(
                    <>
                        <span css={control_style}>{"<CR><LF>"}</span>
                        {"\n"}
                    </>,
                );
            } else {
                ret.push(
                    <>
                        <span css={control_style}>{"<CR>"}</span>
                        {"\n"}
                    </>,
                );
            }
            insert_newline = -1;
        } else if (c === "\u0003") {
            ret.push(<span css={control_style}>{"<ETX>"}</span>);
        } else if (c === "\n") {
            ret.push(
                <>
                    <span css={control_style}>{"<LF>"}</span>
                    {"\n"}
                </>,
            );
            insert_newline = -1;
        } else {
            ret.push(c);
        }

        insert_newline++;
        if (insert_newline === line_split) {
            insert_newline = 0;
            ret.push("\n");
        }
    }
    return ret;
}

export default function Cdr({
    callid,
    tenant,
}: {
    callid: string;
    tenant: string;
}) {
    const debug_mode = useDebugMode();
    const location = useLocation();
    const cdr = useFetch<any>(
        "/api/tenant/" +
            encodeURIComponent(tenant) +
            "/calls/" +
            encodeURIComponent(callid) +
            "/cdr" +
            location.search,
    );
    if (!cdr) {
        return null;
    }

    const tz = new URLSearchParams(location.search).get("tz") ?? "local";

    const table_style = css`
        width: 100%;
        border-collapse: collapse;
        table,
        th,
        td {
            border: 1px solid black;
        }
        td {
            padding: 3px;
        }
        tbody > tr:nth-child(even) {
            background-color: rgb(217, 226, 243);
        }
        tbody > tr:nth-child(odd) {
            background-color: rgb(255, 255, 255);
        }
    `;

    const pre_style = css`
        font-family: monospace, monospace;
        white-space: pre-wrap;
    `;

    const table = (
        <table css={table_style}>
            <tbody>
                {cdr.events.map((e: any, i: number) => {
                    let extended_desc: React.ReactNode;
                    if (e.desc.desc_extended) {
                        if (typeof e.desc.desc_extended === "string") {
                            extended_desc = (
                                <div css={pre_style}>
                                    {e.desc.desc_extended}
                                </div>
                            );
                        } else if (e.desc.desc_extended.base64) {
                            extended_desc = (
                                <div css={pre_style}>
                                    {format_ali(
                                        base64_to_ascii_plus(
                                            e.desc.desc_extended.base64,
                                        ),
                                        e.desc.desc_extended.line_split,
                                    )}
                                </div>
                            );
                        }
                    }

                    const date = DateTime.fromSeconds(e.utc, { zone: tz });
                    const date_formatted = date.toFormat(
                        // U+00A0 NON BREAKING SPACE
                        "h:mm:ss.u\u00A0a\u00A0ZZZZ\nM/dd/yyyy",
                    );

                    return (
                        <tr key={i}>
                            <td
                                title={debug_mode ? date_formatted : undefined}
                                css={css`
                                    white-space: pre-wrap;
                                    text-align: right;
                                `}
                            >
                                <time dateTime={date.toISO()}>
                                    {debug_mode ? date.toISO() : date_formatted}
                                </time>
                                {"\n+"}
                                {e.t.toFixed(3)}
                            </td>
                            <td>
                                <span
                                    css={css`
                                        font-family: monospace;
                                        white-space: pre-wrap;
                                    `}
                                >
                                    {e.code}
                                </span>
                            </td>
                            <td
                                css={css`
                                    white-space: pre-wrap;
                                    /* safari and edge don't support "overflow-wrap: anywhere",
                                     * so we have to use "word-break: break-all" */
                                    @supports (overflow-wrap: anywhere) {
                                        overflow-wrap: anywhere;
                                    }
                                    @supports not (overflow-wrap: anywhere) {
                                        word-break: break-all;
                                    }
                                `}
                            >
                                <div>
                                    {e.desc.desc}
                                    {extended_desc ? (
                                        <>
                                            <br />
                                            {extended_desc}
                                        </>
                                    ) : null}
                                </div>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );

    const style = css`
        width: 90vw;
        font-size: 18px;
        @media print {
            font-size: 11px;
        }
        h1 {
            text-align: center;
        }
    `;

    const oldlog_table_style = css`
        white-space: pre;
        font-family: monospace;
        border-collapse: collapse;
        table,
        th,
        td {
            border: 1px solid black;
        }
        td {
            padding: 3px;
        }
        tbody > tr:nth-child(even) {
            background-color: rgb(217, 226, 243);
        }
        tbody > tr:nth-child(odd) {
            background-color: rgb(255, 255, 255);
        }
    `;

    const oldlog_table = debug_mode ? (
        <>
            <h1>Oldlog (DEBUG)</h1>
            <table css={oldlog_table_style}>
                <tbody>
                    {cdr.events.map((e: any, i: number) => {
                        if (e.log?.oldlog) {
                            const o = e.log.oldlog;
                            return (
                                <tr key={i}>
                                    <td>
                                        <time
                                            dateTime={DateTime.fromISO(o.utc, {
                                                zone: tz,
                                            }).toISO()}
                                        >
                                            {DateTime.fromISO(o.utc, {
                                                zone: tz,
                                            }).toLocaleString(
                                                DateTime.DATETIME_SHORT_WITH_SECONDS,
                                            )}
                                        </time>
                                    </td>
                                    <td>{o.code}</td>
                                    <td>
                                        {typeof o.trunk === "number"
                                            ? `T${o.trunk}`
                                            : o.trunk}
                                    </td>
                                    <td>
                                        {typeof o.position === "number"
                                            ? `P${o.position}`
                                            : o.position}
                                    </td>
                                    <td>{o.thread}</td>
                                    <td>{o.ani}</td>
                                    <td>{o.pani}</td>
                                    <td>{o.thread}</td>
                                    <td>{o.direction}</td>
                                    <td>{o.message}</td>
                                </tr>
                            );
                        } else {
                            return (
                                <tr key={i}>
                                    <td colSpan={11}>no oldlog</td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </>
    ) : null;

    const cdr_time = DateTime.fromSeconds(cdr.events[0].utc, {
        zone: tz,
    });
    const cdr_time_formatted = cdr_time.toFormat("M/dd/yyyy, h:mm:ss a ZZZZ");

    return (
        <div
            css={css`
                display: flex;
                justify-content: center;
            `}
        >
            <Helmet>
                <title>CDR {cdr_time_formatted}</title>
            </Helmet>
            <div css={style}>
                <h1>
                    CDR{" "}
                    <time dateTime={cdr_time.toISO()}>
                        {cdr_time_formatted}
                    </time>
                </h1>
                <div
                    css={css`
                        margin-top: 10px;
                        margin-bottom: 10px;
                        display: flex;
                        justify-content: space-between;
                        align-items: flex-end;
                    `}
                >
                    <div>
                        <div>
                            {cdr.is_911 ? (
                                <span
                                    css={css`
                                        font-weight: bold;
                                    `}
                                >
                                    9-1-1 Call
                                </span>
                            ) : (
                                "Admin Call"
                            )}
                        </div>
                        {cdr.initial_participant?.calltaker ? (
                            <div>
                                Initial Calltaker:{" "}
                                {cdr.initial_participant.calltaker}
                            </div>
                        ) : null}
                        {cdr.initial_participant?.num ? (
                            <div>
                                Initial Position: P{cdr.initial_participant.num}
                            </div>
                        ) : null}
                        {typeof cdr.ani === "string" ? (
                            <div>ANI: {formatPhoneNumber(cdr.ani)}</div>
                        ) : null}
                        {typeof cdr.sip === "string" ? (
                            <div>SIP: {cdr.sip}</div>
                        ) : null}
                        {cdr.pani ? (
                            <div>pANI: {formatPhoneNumber(cdr.pani)}</div>
                        ) : null}
                        {cdr.trunk ? <div>Trunk: T{cdr.trunk}</div> : null}
                        <div>
                            Callid: {cdr.callid}
                            {cdr.controller_callid
                                ? ` (${cdr.controller_callid})`
                                : ""}
                        </div>
                    </div>
                    <button
                        onClick={() => window.print()}
                        css={css`
                            @media print {
                                display: none;
                            }
                        `}
                    >
                        <PrintIcon
                            css={css`
                                width: 1em;
                                height: 1em;
                                fill: black;
                                vertical-align: middle;
                            `}
                        />{" "}
                        Print CDR
                    </button>
                </div>
                {table}
                {oldlog_table}
                {debug_mode ? (
                    <pre
                        css={css`
                            max-width: 1100px;
                            max-height: 600px;
                            overflow: auto;
                        `}
                    >
                        {JSON.stringify(cdr, null, 2)}
                    </pre>
                ) : null}
            </div>
        </div>
    );
}
