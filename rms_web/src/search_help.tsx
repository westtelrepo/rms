/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import styled from "@emotion/styled";

import HelpTooltip from "./help_tooltip";

const HelpLine = styled.div`
    margin-top: 0.3em;
`;

export function SearchHelp() {
    return (
        <HelpTooltip>
            <h1
                css={css`
                    margin-top: 0px;
                    margin-bottom: 10px;
                `}
            >
                Search Tips
            </h1>
            <HelpLine>
                Type “<code>P3</code>” or “<code>p3</code>” to search for
                position 3
            </HelpLine>
            <HelpLine>
                Type “<code>T3</code>” or “<code>t3</code>” to search for trunk
                3
            </HelpLine>
            <HelpLine>
                Type “<code>1234</code>” to search for calls with an ANI or SIP
                containing 1234
            </HelpLine>
            <HelpLine>
                Type “<code>num:1234</code>” to search for calls with an ANI,
                pANI, or SIP containing 1234
            </HelpLine>
            <HelpLine>
                Type “<code>wrls</code>” or “<code>cos:wrls</code>” to search
                for calls with a COS of <code>WRLS</code>
            </HelpLine>
            <HelpLine>
                Type “<code>bob</code>” or “<code>calltaker:bob</code>” to
                search for call-taker bob
            </HelpLine>
            <HelpLine>
                Type “<code>pani:1234</code>” to search for calls with a pANI
                containing 1234
            </HelpLine>
            <HelpLine>
                Type “<code>sip:58.11</code>” to search for calls with a SIP
                containing 58.11
            </HelpLine>
            <HelpLine>
                Type “<code>ani:1234</code>” to search for calls with an ANI
                containing 1234
            </HelpLine>
            <HelpLine>
                Type “<code>abandoned</code>” to search for abandoned and/or
                unanswered calls
            </HelpLine>
            <HelpLine>
                Type “<code>!abandoned</code>” or “<code>not-abandoned</code>”
                to search for answered calls (calls that are neither abandoned
                nor unanswered)
            </HelpLine>
            <HelpLine>
                Type “<code>duration:&gt;5m</code>” to search for calls longer
                than 5 minutes (<code>&gt;</code>, <code>&lt;</code>,{" "}
                <code>&gt;=</code>, and <code>&lt;=</code> may also be used)
            </HelpLine>
            <HelpLine>
                Type “<code>answer:&gt;10s</code>” to search for calls with an
                answer time longer than 10 seconds (<code>&gt;</code>,{" "}
                <code>&lt;</code>, <code>&gt;=</code>, and <code>&lt;=</code>{" "}
                may also be used)
            </HelpLine>
            <HelpLine>
                Type “<code>transfer-time:&gt;2m</code>” to search for calls
                transfered in a time longer than 2 minutes (<code>&gt;</code>,{" "}
                <code>&lt;</code>, <code>&gt;=</code>, and <code>&lt;=</code>{" "}
                may also be used)
            </HelpLine>
            <HelpLine>
                Type “<code>time:&gt;3:00pm</code>” to search for calls after
                3:00 PM. (<code>&gt;</code>, <code>&lt;</code>,{" "}
                <code>&gt;=</code>, and <code>&lt;=</code> may also be used).
                <br />
                It may be shortened to “<code>t&gt;3:00pm</code>”. Military time
                may also be used, as in “<code>t&gt;14:00</code>” (after 2 PM).
            </HelpLine>
        </HelpTooltip>
    );
}
