/** @jsx jsx */
/** @jsxFrag React.Fragment */
import createCache from "@emotion/cache";
import { CacheProvider, css, Global, jsx } from "@emotion/core";
import normalize_css from "normalize.css/normalize.css";
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { Helmet } from "react-helmet";
import { BrowserRouter, Link, Redirect, Route, Switch } from "react-router-dom";
import { trigger } from "swr";

import AliEditor from "./ali_editor";
import Cdr from "./cdr";
import DB from "./db";
import { useDebugMode } from "./debug";
import Loading from "./loading";
import Monitor, { MonitorALI } from "./monitor";
import openid from "./openid";
import Report from "./report";
import ReportUI from "./reportui";
import Search from "./search";
import SSHSettings from "./ssh_settings";
import Tenant from "./tenant";
import { OIDCSession, useFetch, useFetchFunc, useOIDCState } from "./useFetch";

function App() {
    const tenants = useFetch<{ name: string; pretty_size: string }[]>(
        "/api/tenant",
    );
    const [new_tenant, set_new_tenant] = useState("");

    const gfetch = useFetchFunc();

    async function create_tenant() {
        const data = await gfetch(
            "/api/tenant/" + encodeURIComponent(new_tenant),
            { method: "PUT" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
    }

    async function delete_tenant(name: string) {
        const prompt = window.prompt(
            `Delete tenant ${name}?\ntype tenant name`,
            "",
        );
        if (prompt !== name) {
            return;
        }

        const data = await gfetch("/api/tenant/" + encodeURIComponent(name), {
            method: "DELETE",
        });
        if (!data.ok) {
            throw new Error("data not ok");
        }
    }

    async function all_tenant_refresh() {
        const data = await gfetch("/api/all_tenant_refresh", {
            method: "POST",
        });
        if (!data.ok) {
            throw new Error("data not ok");
        }
    }

    async function all_tenant_recreate() {
        const prompt = window.prompt(
            `INSERT THE MAGIC WORD TO DESTROY EVERYTHING`,
            "",
        );
        if (prompt !== "please") {
            return;
        }

        const data = await gfetch("/api/recreate_all_tenants", {
            method: "POST",
        });
        if (!data.ok) {
            throw new Error("data not ok");
        }

        window.alert("DONE!");
    }

    return (
        <div>
            <button onClick={all_tenant_refresh}>All Tenant Refresh</button>
            <button onClick={all_tenant_recreate}>All Tenant Recreate</button>
            <input
                type="text"
                value={new_tenant}
                onChange={e => set_new_tenant(e.target.value)}
            />{" "}
            <button onClick={create_tenant}>Create Tenant</button>
            <br />
            {tenants
                ? tenants.map(e => (
                      <div key={e.name}>
                          <div style={{ fontSize: "200%" }}>
                              {e.name} {e.pretty_size}{" "}
                              <button
                                  onClick={delete_tenant.bind(null, e.name)}
                              >
                                  Drop
                              </button>
                              <br />
                          </div>
                          <Tenant name={e.name} />
                      </div>
                  ))
                : null}
        </div>
    );
}

function Tenants() {
    // const [tenants, set_tenants] = useState<{name: string, pretty_size: string}[]>([]);
    const tenants = useFetch<{ name: string; pretty_size: string }[]>(
        "/api/tenant",
    );
    const [new_tenant, set_new_tenant] = useState("");
    const gfetch = useFetchFunc();

    async function refresh() {
        void trigger("/api/tenant");
    }

    async function create() {
        const data = await gfetch(
            "/api/tenant/" + encodeURIComponent(new_tenant),
            { method: "PUT" },
        );
        if (!data.ok) {
            throw new Error("data not ok");
        }
        await refresh();
    }

    async function delete_tenant(name: string) {
        const prompt = window.prompt(
            `Delete tenant ${name}?\ntype tenant name`,
            "",
        );
        if (prompt !== name) {
            return;
        }

        const data = await gfetch("/api/tenant/" + encodeURIComponent(name), {
            method: "DELETE",
        });
        if (!data.ok) {
            throw new Error("data not ok");
        }

        await refresh();
    }

    async function all_tenant_refresh() {
        const data = await gfetch("/api/all_tenant_refresh", {
            method: "POST",
        });
        if (!data.ok) {
            throw new Error("data not ok");
        }
    }

    async function all_tenant_recreate() {
        const prompt = window.prompt(
            `INSERT THE MAGIC WORD TO DESTROY EVERYTHING`,
            "",
        );
        if (prompt !== "please") {
            return;
        }

        const data = await gfetch("/api/recreate_all_tenants", {
            method: "POST",
        });
        if (!data.ok) {
            throw new Error("data not ok");
        }

        window.alert("DONE!");
    }

    return (
        <div>
            <div>
                <Link to="/ssh">SSH Settings</Link>{" "}
                <Link to="/monitor">Monitoring</Link>{" "}
                <Link to="/db">DB Settings</Link>{" "}
                <Link to="/init">Initialize DB</Link>
            </div>
            <table style={{ fontSize: "14px" }}>
                <tbody>
                    {tenants
                        ? tenants.map(e => (
                              <tr key={e.name}>
                                  <td>
                                      <Link
                                          to={
                                              "/tenant/" +
                                              encodeURIComponent(e.name)
                                          }
                                      >
                                          {e.name}
                                      </Link>
                                  </td>
                                  <td>{e.pretty_size}</td>
                                  <td>
                                      <Link
                                          to={
                                              "/tenant/" +
                                              encodeURIComponent(e.name) +
                                              "/calls"
                                          }
                                      >
                                          Search
                                      </Link>
                                  </td>
                                  <td>
                                      <Link
                                          to={
                                              "/tenant/" +
                                              encodeURIComponent(e.name) +
                                              "/reportui"
                                          }
                                      >
                                          Reports
                                      </Link>
                                  </td>
                                  <td>
                                      <button
                                          onClick={delete_tenant.bind(
                                              null,
                                              e.name,
                                          )}
                                      >
                                          Delete
                                      </button>
                                  </td>
                              </tr>
                          ))
                        : null}
                </tbody>
            </table>
            <hr />
            <input
                type="text"
                value={new_tenant}
                onChange={e => set_new_tenant(e.target.value)}
            />
            <button onClick={create}>Create Tenant</button>
            <br />
            <button onClick={all_tenant_refresh}>All Tenant Refresh</button>
            <button onClick={all_tenant_recreate}>All Tenant Recreate</button>
        </div>
    );
}

function TenantList() {
    const tenants = useFetch<
        { name: string; pretty_size: string; description: string | null }[]
    >("/api/tenant");

    if (!tenants) {
        return null;
    } else if (tenants.length === 0) {
        return (
            <div
                css={css`
                    text-align: center;
                    font-size: 35px;
                `}
            >
                You don&apos;t have access to MIS
            </div>
        );
    } else if (tenants.length === 1) {
        return (
            <Redirect
                to={
                    "/tenant/" +
                    encodeURIComponent(tenants[0].name) +
                    "/reportui"
                }
            />
        );
    } else {
        const flex_style = css`
            columns: 300px;
            max-width: 1310px;
            margin-left: auto;
            margin-right: auto;
            & > div {
                padding: 10px;
            }
            a {
                color: #0000ee;
                text-decoration: none;
            }
            a:hover {
                text-decoration: underline;
            }
        `;
        return (
            <div
                css={css`
                    margin-left: auto;
                    margin-right: auto;
                    font-size: 25px;
                `}
            >
                <h1
                    css={css`
                        text-align: center;
                    `}
                >
                    MIS Tenant List
                </h1>
                <div css={flex_style}>
                    {tenants.map(e => (
                        <div key={e.name}>
                            <Link
                                to={
                                    "/tenant/" +
                                    encodeURIComponent(e.name) +
                                    "/reportui"
                                }
                            >
                                {e.description ?? e.name}
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

function Init() {
    const gfetch = useFetchFunc();
    async function init() {
        const data = await gfetch("/api/init_meta_db", { method: "POST" });
        if (!data.ok) {
            window.alert("Could not init DB!");
        } else {
            window.alert("DB inited!");
        }
    }
    return <button onClick={init}>Initialize DB</button>;
}

function WestTelHeader() {
    const state = useOIDCState();
    const debug_mode = useDebugMode();

    const style = css`
        color: rgb(0, 55, 105);
        font-size: 18px;
        a,
        a:visited {
            color: rgb(0, 55, 105);
        }
        a,
        span {
            margin-left: 10px;
            margin-right: 10px;
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
        padding: 10px;
        @media print {
            display: none;
        }
        display: flex;
        justify-content: space-between;
        * {
            box-sizing: border-box;
        }
    `;

    return (
        <>
            <header css={style}>
                <div
                    css={css`
                        display: flex;
                        align-items: center;
                    `}
                >
                    {debug_mode ? <span>DEBUG MODE</span> : null}
                    <a
                        href="https://westtel.com"
                        css={css`
                            height: 30px;
                        `}
                    >
                        <img src="/svg/WestTel.svg" width={90} height={30} />
                    </a>
                    <Link to="/">MIS – Metrics &amp; Reporting</Link>
                </div>
                <div
                    css={css`
                        display: flex;
                        align-items: center;
                    `}
                >
                    <a href={openid.openid_iss_origin}>Account</a>
                    <a href={"/logout?sub=" + encodeURIComponent(state.sub)}>
                        Logout
                    </a>
                </div>
            </header>
            <hr
                css={css`
                    background-color: rgb(0, 55, 105);
                    margin-bottom: 50px;
                    height: 10px;
                    @media print {
                        display: none;
                    }
                `}
            />
        </>
    );
}

function Corona() {
    const result = useFetch<{ ret: string[][]; periods: [string, string][] }>(
        "/api/corona_report",
    );

    const [over, set_over] = useState("30");

    const over_per = Number(over);

    if (result) {
        const tableCss = css`
            border-collapse: collapse;
            th,
            td {
                border: 1px solid black;
                padding: 2px;
            }
        `;

        return (
            <div>
                Over percent:{" "}
                <input
                    type="text"
                    onChange={e => set_over(e.target.value)}
                    value={over}
                />
                <br />
                <table css={tableCss}>
                    <tr>
                        <td>Tenant</td>
                        <td>Controller</td>
                        <td>
                            {result.periods[0][0]} to {result.periods[0][1]}
                        </td>
                        <td>Growth</td>
                        <td>
                            {result.periods[1][0]} to {result.periods[1][1]}
                        </td>
                        <td>Growth</td>
                        <td>
                            {result.periods[2][0]} to {result.periods[2][1]}
                        </td>
                    </tr>
                    {result.ret.map((e, index) => {
                        const g1 =
                            (100 * (Number(e[3]) - Number(e[2]))) /
                            Number(e[2]);
                        const g2 =
                            (100 * (Number(e[4]) - Number(e[3]))) /
                            Number(e[3]);

                        return (
                            <tr key={index}>
                                <td>{e[0]}</td>
                                <td>{e[1]}</td>
                                <td>{e[2]}</td>
                                <td
                                    style={{
                                        color:
                                            g1 > over_per ? "red" : undefined,
                                    }}
                                >
                                    {g1.toFixed(2)}%
                                </td>
                                <td>{e[3]}</td>
                                <td
                                    style={{
                                        color:
                                            g2 > over_per ? "red" : undefined,
                                    }}
                                >
                                    {g2.toFixed(2)}%
                                </td>
                                <td>{e[4]}</td>
                            </tr>
                        );
                    })}
                </table>
            </div>
        );
    } else {
        return <Loading msg="Loading" />;
    }
}

const footer_style = css`
    * {
        box-sizing: border-box;
    }
    @media print {
        display: none;
    }
    height: 42px;
    background: #444444;
    font-size: 11px;
    color: white;
    font-family: "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, Verdana,
        sans-serif;
    display: flex;
    align-items: center;
    justify-content: left;
    padding-left: 20px;
`;

function RealApp() {
    const state = useOIDCState();

    if (state.state === "login") {
        return (
            <>
                <BrowserRouter>
                    <Helmet>
                        <title>WestTel – Metrics &amp; Reporting</title>
                    </Helmet>
                    <div
                        css={css`
                            min-height: calc(100vh - 42px);
                            box-sizing: border-box;
                            padding-bottom: 50px;
                        `}
                    >
                        <WestTelHeader />
                        <Switch>
                            <Route exact={true} path="/">
                                <TenantList />
                            </Route>
                            <Route exact={true} path="/old_view">
                                <App />
                            </Route>
                            <Route
                                exact={true}
                                path="/tenant/:tenantid"
                                render={props => (
                                    <Tenant
                                        name={props.match.params.tenantid}
                                    />
                                )}
                            />
                            <Route path="/admin" exact={true}>
                                <Tenants />
                            </Route>
                            <Route path="/admin/corona" exact={true}>
                                <Corona />
                            </Route>
                            <Route path="/init" exact={true}>
                                <Init />
                            </Route>
                            <Route
                                exact={true}
                                path="/tenant/:tenantid/calls/:callid/cdr"
                                render={props => (
                                    <Cdr
                                        callid={props.match.params.callid}
                                        tenant={props.match.params.tenantid}
                                    />
                                )}
                            />
                            <Route exact={true} path="/tenant/:tenantid/calls">
                                <Search />
                            </Route>
                            <Route
                                exact={true}
                                path="/tenant/:tenantid/reportui"
                            >
                                <ReportUI />
                            </Route>
                            <Route exact={true} path="/tenant/:tenantid/report">
                                <Report />
                            </Route>
                            <Route
                                exact={true}
                                path="/tenant/:tenantid/ali_editor"
                            >
                                <AliEditor />
                            </Route>
                            <Route exact={true} path="/ssh">
                                <SSHSettings />
                            </Route>
                            <Route exact={true} path="/db">
                                <DB />
                            </Route>
                            <Route exact={true} path="/monitor">
                                <Monitor />
                            </Route>
                            <Route exact={true} path="/monitor/ali">
                                <MonitorALI />
                            </Route>
                            <Route>
                                <h1
                                    css={css`
                                        text-align: center;
                                    `}
                                >
                                    Not Found
                                </h1>
                            </Route>
                        </Switch>
                    </div>
                    <footer css={footer_style}>
                        <span>
                            © Copyright 2020 - WestTel International | All
                            Rights Reserved | 303-695-5000
                        </span>
                    </footer>
                </BrowserRouter>
            </>
        );
    } else {
        return <a href="/">This session has ended, click this link to login</a>;
    }
}

const main_emotion_cache = createCache({
    nonce: (window as any).__webpack_nonce__,
});

function Wrapper() {
    return (
        <CacheProvider value={main_emotion_cache}>
            <Global styles={css(normalize_css)} />
            <OIDCSession>
                <RealApp />
            </OIDCSession>
        </CacheProvider>
    );
}

ReactDOM.render(<Wrapper />, document.getElementById("root"));
