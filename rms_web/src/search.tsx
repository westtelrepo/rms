/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import * as d3 from "d3-dsv";
import { DateTime } from "luxon";
import pretty_ms from "pretty-ms";
import React, { useMemo } from "react";
import { Helmet } from "react-helmet";
import { Link, useLocation, useParams } from "react-router-dom";

import { useDebugMode } from "./debug";
import { formatPhoneNumber } from "./formatPhoneNumber";
import Loading from "./loading";
import { AscIcon, DescIcon } from "./open_iconic";
import { useFetch } from "./useFetch";

export default function Search(props: { search?: string }) {
    const debug_mode = useDebugMode();
    const { tenantid } = useParams<{ tenantid: string }>();
    const location = useLocation();

    const location_search = props.search ?? location.search;

    const results = useFetch<{
        results: any[] | string;
        query?: string;
        limit: number;
        offset: number;
    }>(
        "/api/tenant/" +
            encodeURIComponent(tenantid) +
            "/calls" +
            location_search,
        { revalidateOnFocus: false },
    );

    const tableClass = css`
        border-collapse: collapse;
        thead {
            font-weight: bold;
        }
        tbody a {
            color: rgb(0, 0, 238);
            text-decoration: none;
        }
        tbody a:visited {
            color: rgb(0, 0, 238);
        }
        tbody a:hover {
            text-decoration: underline;
        }
        th,
        td {
            border: 1px solid black;
            padding: 5px;
            vertical-align: top;
            font-weight: normal;
            white-space: pre-wrap;
        }
        thead {
            background-color: rgb(0, 55, 105);
            color: white;
        }

        thead a {
            color: white;
            text-decoration: none;
        }
        thead a:visited {
            color: white;
        }
        thead a:hover {
            text-decoration: underline;
        }

        tbody > tr:nth-child(even) {
            background-color: rgb(217, 226, 243);
        }
        tbody > tr:nth-child(odd) {
            background-color: rgb(255, 255, 255);
        }
    `;

    function link_with_new_where(key: string, value: any) {
        const params = new URLSearchParams(location_search);
        const where = JSON.parse(params.get("where") ?? "{}");
        where[key] = value;
        params.set("where", JSON.stringify(where));
        return (
            "/tenant/" +
            encodeURIComponent(tenantid) +
            "/calls?" +
            String(params)
        );
    }

    function new_order_link(first: string, second: string) {
        const params = new URLSearchParams(location_search);
        const order = params.get("order");
        if (order === first) {
            params.set("order", second);
        } else {
            params.set("order", first);
        }
        return (
            "/tenant/" +
            encodeURIComponent(tenantid) +
            "/calls?" +
            String(params)
        );
    }

    function asc_desc_icon(asc: string, desc: string) {
        const order = new URLSearchParams(location_search).get("order");
        const style = css`
            width: 1em;
            height: 1em;
            fill: white;
            vertical-align: middle;
        `;
        if (order === asc) {
            return <AscIcon css={style} />;
        } else if (order === desc) {
            return <DescIcon css={style} />;
        } else {
            return null;
        }
    }

    const search = new URLSearchParams(location_search);
    const tz = search.get("tz");

    const csv = useMemo(() => {
        if (results && Array.isArray(results.results)) {
            return d3.csvFormat(
                results.results.map((e: any) => ({
                    Time: DateTime.fromSeconds(e.utc, {
                        zone: tz ?? "local",
                    }).toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS),
                    "Phone Number": `${e.ani ? formatPhoneNumber(e.ani) : ""} ${
                        e.sip ?? ""
                    }`.trim(),
                    "Call-taker": e.calltaker,
                    Trunk: typeof e.trunk === "number" ? `T${e.trunk}` : "",
                    Type: e.is_911 ? "911" : "Admin",
                    Direction: e.incoming ? "Incoming" : "Outgoing",
                    COS: e.cos,
                    "Duration (seconds)": e.call_length,
                    Transfer: e.transfer_reaction,
                })),
                [
                    "Time",
                    "Phone Number",
                    "Call-taker",
                    "Trunk",
                    "Type",
                    "Direction",
                    "COS",
                    "Duration (seconds)",
                    "Transfer",
                ],
            );
        } else {
            return "";
        }
    }, [results, tz]);

    const results_display = (() => {
        if (results && Array.isArray(results.results)) {
            if (results.results.length === results.limit) {
                return `first ${results.results.length} results`;
            } else {
                return `${results.results.length}`;
            }
        } else {
            return null;
        }
    })();

    return (
        <div
            css={css`
                display: flex;
                justify-content: center;
            `}
        >
            {props.search ? null : (
                <Helmet>
                    <title>Call List {tenantid}</title>
                </Helmet>
            )}
            <div
                css={css`
                    font-size: 18px;
                `}
            >
                {debug_mode ? (
                    <div
                        css={css`
                            max-width: 1310px;
                            overflow: auto;
                        `}
                    >
                        <div>
                            <pre>{location_search}</pre>
                        </div>
                        <div>
                            <pre>
                                {results && typeof results.query === "string"
                                    ? results.query
                                    : null}
                            </pre>
                        </div>
                    </div>
                ) : null}
                <div
                    css={css`
                        display: flex;
                        justify-content: space-between;
                    `}
                >
                    <div># Results: {results_display}</div>
                    <div>
                        <a
                            download="search.csv"
                            href={"data:text/csv," + encodeURIComponent(csv)}
                            css={css`
                                @media print {
                                    display: none;
                                }
                            `}
                        >
                            Download CSV
                            {results &&
                            Array.isArray(results.results) &&
                            results.results.length === results.limit
                                ? " (this page only)"
                                : ""}
                        </a>
                    </div>
                </div>
                {results && typeof results.results === "string" ? (
                    <pre>{results.results}</pre>
                ) : (
                    <table css={tableClass}>
                        <thead>
                            <tr>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_utcd",
                                            "weighted_utc",
                                        )}
                                    >
                                        Time (CDR){" "}
                                        {asc_desc_icon(
                                            "weighted_utc",
                                            "weighted_utcd",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_ani_sip",
                                            "weighted_ani_sip_desc",
                                        )}
                                    >
                                        Phone Number{" "}
                                        {asc_desc_icon(
                                            "weighted_ani_sip",
                                            "weighted_ani_sip_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_pos",
                                            "weighted_pos_desc",
                                        )}
                                    >
                                        Position{" "}
                                        {asc_desc_icon(
                                            "weighted_pos",
                                            "weighted_pos_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_calltaker",
                                            "weighted_calltaker_desc",
                                        )}
                                    >
                                        Call-taker{" "}
                                        {asc_desc_icon(
                                            "weighted_calltaker",
                                            "weighted_calltaker_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_trunk",
                                            "weighted_trunk_desc",
                                        )}
                                    >
                                        Trunk{" "}
                                        {asc_desc_icon(
                                            "weighted_trunk",
                                            "weighted_trunk_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_is_911",
                                            "weighted_is_911_desc",
                                        )}
                                    >
                                        Type{" "}
                                        {asc_desc_icon(
                                            "weighted_is_911",
                                            "weighted_is_911_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_incoming",
                                            "weighted_incoming_desc",
                                        )}
                                    >
                                        Direction{" "}
                                        {asc_desc_icon(
                                            "weighted_incoming",
                                            "weighted_incoming_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_cos",
                                            "weighted_cos_desc",
                                        )}
                                    >
                                        <abbr title="Class of Service">
                                            COS
                                        </abbr>{" "}
                                        {asc_desc_icon(
                                            "weighted_cos",
                                            "weighted_cos_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_call_length",
                                            "weighted_call_length_desc",
                                        )}
                                    >
                                        Duration{" "}
                                        {asc_desc_icon(
                                            "weighted_call_length",
                                            "weighted_call_length_desc",
                                        )}
                                    </Link>
                                </td>
                                <td>
                                    <Link
                                        to={new_order_link(
                                            "weighted_transfer_reaction",
                                            "weighted_transfer_reaction_desc",
                                        )}
                                    >
                                        Transfer{" "}
                                        {asc_desc_icon(
                                            "weighted_transfer_reaction",
                                            "weighted_transfer_reaction_desc",
                                        )}
                                    </Link>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            {results && Array.isArray(results.results) ? (
                                results.results.map((e: any) => (
                                    <tr
                                        key={
                                            e.xferid + (e.incoming ? "t" : "f")
                                        }
                                    >
                                        <td>
                                            <Link
                                                to={
                                                    "/tenant/" +
                                                    encodeURIComponent(
                                                        tenantid,
                                                    ) +
                                                    "/calls/" +
                                                    encodeURIComponent(
                                                        e.callid,
                                                    ) +
                                                    "/cdr" +
                                                    (search.get("tz")
                                                        ? "?tz=" +
                                                          encodeURIComponent(
                                                              search.get("tz")!,
                                                          )
                                                        : "")
                                                }
                                            >
                                                <time
                                                    dateTime={DateTime.fromSeconds(
                                                        e.utc,
                                                        {
                                                            zone:
                                                                search.get(
                                                                    "tz",
                                                                ) ?? "local",
                                                        },
                                                    ).toISO()}
                                                >
                                                    {debug_mode
                                                        ? DateTime.fromSeconds(
                                                              e.utc,
                                                              {
                                                                  zone:
                                                                      search.get(
                                                                          "tz",
                                                                      ) ??
                                                                      "local",
                                                              },
                                                          ).toISO()
                                                        : DateTime.fromSeconds(
                                                              e.utc,
                                                              {
                                                                  zone:
                                                                      search.get(
                                                                          "tz",
                                                                      ) ??
                                                                      "local",
                                                              },
                                                          ).toLocaleString(
                                                              DateTime.DATETIME_SHORT_WITH_SECONDS,
                                                          )}
                                                </time>
                                            </Link>
                                        </td>
                                        <td>
                                            <Link
                                                to={link_with_new_where("ani", [
                                                    e.ani,
                                                ])}
                                            >
                                                {formatPhoneNumber(e.ani)}
                                            </Link>{" "}
                                            <Link
                                                to={link_with_new_where("sip", [
                                                    e.sip,
                                                ])}
                                            >
                                                {e.sip}
                                            </Link>
                                        </td>
                                        <td>
                                            {typeof e.pos === "number" ? (
                                                <Link
                                                    to={link_with_new_where(
                                                        "pos",
                                                        [e.pos],
                                                    )}
                                                >
                                                    P{e.pos}
                                                </Link>
                                            ) : null}
                                        </td>
                                        <td>
                                            {e.calltaker ? (
                                                <Link
                                                    to={link_with_new_where(
                                                        "icalltaker",
                                                        [e.calltaker],
                                                    )}
                                                >
                                                    {e.calltaker}
                                                </Link>
                                            ) : null}
                                        </td>
                                        <td>
                                            {typeof e.trunk === "number" ? (
                                                <Link
                                                    to={link_with_new_where(
                                                        "trunk",
                                                        [e.trunk],
                                                    )}
                                                >
                                                    T{e.trunk}
                                                </Link>
                                            ) : null}
                                        </td>
                                        <td>
                                            {e.is_911 ? (
                                                <Link
                                                    to={link_with_new_where(
                                                        "is_911",
                                                        true,
                                                    )}
                                                >
                                                    9-1-1
                                                </Link>
                                            ) : (
                                                <Link
                                                    to={link_with_new_where(
                                                        "is_911",
                                                        false,
                                                    )}
                                                >
                                                    Admin
                                                </Link>
                                            )}
                                        </td>
                                        <td>
                                            {e.incoming ? (
                                                <Link
                                                    to={link_with_new_where(
                                                        "incoming",
                                                        true,
                                                    )}
                                                >
                                                    Incoming
                                                </Link>
                                            ) : (
                                                <Link
                                                    to={link_with_new_where(
                                                        "incoming",
                                                        false,
                                                    )}
                                                >
                                                    Outgoing
                                                </Link>
                                            )}
                                        </td>
                                        <td>
                                            {e.cos ? (
                                                <Link
                                                    to={link_with_new_where(
                                                        "cos",
                                                        [e.cos],
                                                    )}
                                                >
                                                    {e.cos}
                                                </Link>
                                            ) : null}
                                        </td>
                                        <td
                                            css={css`
                                                text-align: right;
                                            `}
                                        >
                                            {typeof e.call_length === "number"
                                                ? pretty_ms(
                                                      1000 * e.call_length,
                                                  )
                                                : null}
                                        </td>
                                        <td>
                                            <Link
                                                to={link_with_new_where(
                                                    "transfer_reaction",
                                                    [e.transfer_reaction],
                                                )}
                                            >
                                                {e.transfer_reaction}
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={10}>
                                        <Loading msg="Searching" />
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                )}
            </div>
        </div>
    );
}
