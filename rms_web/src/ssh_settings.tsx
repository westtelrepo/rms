import React, { useState } from "react";
import { trigger } from "swr";

import { useFetch, useFetchFunc } from "./useFetch";

export default function SSHSettings() {
    const [newServer, set_newServer] = useState("");
    const [user, set_user] = useState("");
    const [password, set_password] = useState("");
    const [hostport, set_hostport] = useState("none");

    const server_list = useFetch<any[]>("/api/ssh/servers");
    const server_creds = useFetch<any[]>("/api/ssh/creds");

    const gfetch = useFetchFunc();

    async function updateState() {
        void trigger("/api/ssh/servers");
        void trigger("/api/ssh/creds");
    }

    async function addServer() {
        const keys_request = await gfetch("/api/ssh/keyscan", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "text/plain",
            },
            body: newServer,
        });
        const keys = await keys_request.json();
        const res = window.confirm(
            `check the result below:\nssh-keyscan ${newServer} | ssh-keygen -lf -\n` +
                JSON.stringify(keys, null, 2),
        );
        if (res) {
            await gfetch("/api/ssh/servers", {
                headers: {
                    "Content-Type": "application/json",
                },
                method: "POST",
                body: JSON.stringify({
                    host: newServer,
                    port: 22,
                    hashes: keys,
                }),
            });
            await updateState();
        }
    }

    async function setPassword() {
        const match = /^(.*):(\d*)$/.exec(hostport);
        if (match === null) {
            return;
        }

        await gfetch("/api/ssh/cred", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                host: match[1],
                port: Number(match[2]),
                username: user,
                cred: {
                    t: "pass",
                    p: password,
                },
            }),
        });
        await updateState();
    }

    return (
        <>
            <div>
                <h1>SSH Servers</h1>
                <input
                    type="text"
                    value={newServer}
                    onChange={e => set_newServer(e.target.value)}
                />
                <button onClick={addServer}>Add Server</button>
                {server_list ? (
                    <table>
                        <tbody>
                            {server_list.map((e: any) => (
                                <tr key={e.host + ":" + e.port}>
                                    <td>
                                        {e.host}
                                        {e.port === 22
                                            ? undefined
                                            : ":" + e.port}
                                    </td>
                                    <td>
                                        <pre>
                                            {
                                                // eslint-disable-next-line @typescript-eslint/no-unsafe-call
                                                e.hashes.map(
                                                    (hash: any) =>
                                                        hash.kt +
                                                        " " +
                                                        hash.hash +
                                                        "\n",
                                                )
                                            }
                                        </pre>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                ) : null}
            </div>
            <div>
                <h1>SSH Credentials</h1>
                <input
                    disabled={hostport === "none"}
                    type="text"
                    value={user}
                    onChange={e => set_user(e.target.value)}
                />
                @
                <select
                    value={hostport}
                    onChange={e => set_hostport(e.target.value)}
                >
                    <option value="none">None</option>
                    {server_list
                        ? server_list.map((e: any) => (
                              <option
                                  key={e.host + ":" + e.port}
                                  value={e.host + ":" + e.port}
                              >
                                  {e.host}
                                  {e.port === 22 ? undefined : ":" + e.port}
                              </option>
                          ))
                        : null}
                </select>
                <input
                    disabled={hostport === "none"}
                    type="text"
                    value={password}
                    onChange={e => set_password(e.target.value)}
                    autoComplete="off"
                />
                <button
                    disabled={
                        hostport === "none" || user === "" || password === ""
                    }
                    onClick={setPassword}
                >
                    Set Password
                </button>
                {server_creds ? (
                    <table>
                        <tbody>
                            {server_creds.map((e: any) => (
                                <tr key={e.host + ":" + e.port}>
                                    <td>
                                        {e.username}@{e.host}
                                        {e.port === 22
                                            ? undefined
                                            : ":" + e.port}
                                    </td>
                                    <td>{JSON.stringify(e.cred)}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                ) : null}
            </div>
        </>
    );
}
