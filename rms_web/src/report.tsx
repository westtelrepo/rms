/** @jsx jsx */
/** @jsxFrag React.Fragment */
import { css, jsx } from "@emotion/core";
import * as d3 from "d3-dsv";
import Immutable from "immutable";
import { DateTime, Info } from "luxon";
import React, { ReactNode, useState } from "react";
import { Helmet } from "react-helmet";
import { Link, useHistory, useLocation, useParams } from "react-router-dom";
import {
    Area,
    AreaChart,
    Bar,
    BarChart,
    CartesianGrid,
    Cell,
    Legend,
    Pie,
    PieChart,
    RechartsFunction,
    Tooltip,
    XAxis,
    YAxis,
} from "recharts";

import { useDebugMode } from "./debug";
import { format_field, str_agg } from "./iaggregate";
import Loading from "./loading";
import { useFetch } from "./useFetch";

interface IReportResult {
    total: any;
    table: any[];
    query: string;
    explain: string;
    from: string;
    tz: string;
    to: string;
}

const group_by_name = Immutable.Map<string, string>([
    ["cos", "COS"],
    ["coss", "COS Group"],
    ["pos", "Position"],
    ["trunk", "Trunk"],
    ["calltaker", "Call-Taker (Windows Login)"],
    ["icalltaker", "Call-Taker (Windows Login)"],
    ["day", "Day"],
    ["hour", "Hour"],
    ["week", "Week Of"],
    ["month", "Month"],
    ["dow", "Day"],
    ["hod", "Hour"],
    ["moy", "Month"],
]);

const divCss = css`
    margin-left: auto;
    margin-right: auto;
    h1,
    h2,
    h3 {
        text-align: center;
        margin: 10px 0;
        font-weight: normal;
    }
    h1 {
        font-size: 41px;
        font-family: "Droid Sans", HelveticaNeue, "Helvetica Neue", Helvetica,
            Arial, sans-serif;
        text-transform: uppercase;
    }
    h2 {
        font-size: 26px;
        color: #666666;
    }
    h3 {
        font-size: 23px;
        padding-top: 5px;
        color: #666666;
    }
`;

const tableCss = css`
    border-collapse: collapse;
    text-align: center;
    vertical-alight: middle;
    margin-left: auto;
    margin-right: auto;
    font-size: 20px;
    margin-top: 10px;
    border-spacing: 0px;
    padding: 0px;
    td {
        border: 2px solid black;
        padding: 5px;
        vertical-align: top;
    }
    .header {
        background-color: rgb(0, 55, 105);
        color: white;
        white-space: pre-wrap;
    }
    .summary {
        background-color: rgb(68, 114, 196);
        color: white;
    }
    .greenbar_odd {
        background-color: rgb(255, 255, 255);
    }
    .greenbar_even {
        background-color: rgb(217, 226, 243);
    }

    a {
        color: rgb(0, 0, 238);
        text-decoration: none;
    }
    a:visited {
        color: rgb(0, 0, 238);
    }
    a:hover {
        text-decoration: underline;
    }

    .clickable_header {
        cursor: pointer;
    }
    .clickable_header:hover {
        text-decoration: underline;
    }

    .tooltip-container {
        position: relative;
    }
    .tooltip-container:hover > .tooltip {
        opacity: 1;
        transform: scale(1);
    }
    .tooltip {
        position: absolute;
        top: -4px;
        left: 105%;
        transform: scale(0);
        transition: all 0.5s;
        opacity: 0;
        padding: 4px;
        z-index: 1000;
        white-space: nowrap;
        border-radius: 6px;
        color: white;
        background-color: #424242;
    }

    .tooltip a {
        color: white;
    }
`;

export default function Report() {
    const debug_mode = useDebugMode();

    const { tenantid } = useParams<{ tenantid: string }>();
    const location = useLocation();
    const history = useHistory();
    const [graph_col, set_graph_col] = useState(0);

    const result = useFetch<IReportResult>(
        "/api/tenant/" +
            encodeURIComponent(tenantid) +
            "/report" +
            location.search,
        { revalidateOnFocus: false },
    );

    // probably shouldn't just load every psap, though it'll be in cache already
    const psaps = useFetch<{ psap: string; name: string }[]>(
        "/api/tenant/" + encodeURIComponent(tenantid) + "/psap",
    );

    if (result && psaps) {
        const search = new URLSearchParams(location.search);
        const fields: any[] = JSON.parse(search.get("fields")!);
        const where: { [index: string]: any; cpsap?: string[] } = JSON.parse(
            search.get("where")!,
        );
        const group_by = search.get("group_by")!;

        const group_by_subtitle: string = (() => {
            switch (group_by) {
                case "hour":
                    return "Hour";
                case "day":
                    return "Day";
                case "month":
                    return "Month";
                case "hod":
                    return "Hour of Day";
                case "dow":
                    return "Day of Week";
                case "week":
                    return "Week";
                case "moy":
                    return "Month of Year";
                case "icalltaker":
                    return "Call-Taker";
                case "cos":
                    return "Class of Service";
                case "coss":
                    return "Class of Service Summary";
                case "trunk":
                    return "Trunk/Line";
                case "pos":
                    return "Position";
                default:
                    return `(unknown group_by ${group_by})`;
            }
        })();

        function format_groupby(
            elem: any,
        ): { str: string; r: ReactNode; url: null | string } {
            const tz = result!.tz;
            if (
                elem === null &&
                group_by !== "cos" &&
                group_by !== "icalltaker"
            ) {
                return { str: "Unknown", r: "Unknown", url: null };
            } else {
                function new_url(
                    new_groupby: string,
                    from: DateTime,
                    to: DateTime,
                ): string {
                    const new_where = JSON.parse(search.get("where")!);
                    new_where.from = from.toISODate();
                    new_where.to = to.toISODate();

                    const new_search = new URLSearchParams(location.search);
                    new_search.set("where", JSON.stringify(new_where));
                    new_search.set("group_by", new_groupby);

                    return "/tenant/" + tenantid + "/report?" + new_search;
                }
                switch (group_by) {
                    case "month": {
                        const date = DateTime.fromSeconds(elem, { zone: tz });
                        const str = date.toLocaleString({
                            year: "numeric",
                            month: "long",
                        });

                        const url = new_url("day", date, date.endOf("month"));

                        return {
                            str,
                            r: <Link to={url}>{str}</Link>,
                            url,
                        };
                    }
                    case "week": {
                        const date = DateTime.fromSeconds(elem, { zone: tz });
                        const str = date.toLocaleString(DateTime.DATE_MED);

                        const url = new_url(
                            "day",
                            date,
                            date.plus({ days: 6 }),
                        );

                        return {
                            str,
                            r: <Link to={url}>{str}</Link>,
                            url,
                        };
                    }
                    case "day": {
                        const date = DateTime.fromSeconds(elem, { zone: tz });
                        const str = date.toLocaleString(DateTime.DATE_MED);

                        const new_where = JSON.parse(search.get("where")!);
                        new_where.from = undefined;
                        new_where.to = undefined;
                        new_where.from_exact = date.toISO();
                        new_where.to_exact = date.plus({ days: 1 }).toISO();
                        const new_link =
                            "/tenant/" +
                            encodeURIComponent(tenantid) +
                            "/calls?order=utc&tz=" +
                            encodeURIComponent(tz) +
                            "&where=" +
                            encodeURIComponent(JSON.stringify(new_where));
                        const url = new_url("hour", date, date);

                        const r = (
                            <div className="tooltip-container">
                                <Link to={url}>{str}</Link>
                                <div className="tooltip">
                                    <Link to={url}>Calls stats by hour</Link>
                                    <br />
                                    <Link to={new_link}>Call List</Link>
                                </div>
                            </div>
                        );
                        return { str, r, url };
                    }
                    case "hour": {
                        const date = DateTime.fromSeconds(elem, { zone: tz });
                        const str = date.toLocaleString(
                            DateTime.DATETIME_SHORT,
                        );

                        const new_where = JSON.parse(search.get("where")!);
                        new_where.from = undefined;
                        new_where.to = undefined;
                        new_where.from_exact = date.toISO();
                        new_where.to_exact = date.plus({ hours: 1 }).toISO();
                        const new_link =
                            "/tenant/" +
                            encodeURIComponent(tenantid) +
                            "/calls?order=utc&tz=" +
                            encodeURIComponent(tz) +
                            "&where=" +
                            encodeURIComponent(JSON.stringify(new_where));

                        return {
                            str,
                            r: <Link to={new_link}>{str}</Link>,
                            url: new_link,
                        };
                    }
                    case "trunk": {
                        const new_where = JSON.parse(search.get("where")!);
                        new_where.trunk = [Number(elem)];
                        const new_link =
                            "/tenant/" +
                            encodeURIComponent(tenantid) +
                            "/calls?order=utc&tz=" +
                            encodeURIComponent(tz) +
                            "&where=" +
                            encodeURIComponent(JSON.stringify(new_where));
                        return {
                            str: String(elem),
                            r: <Link to={new_link}>{String(elem)}</Link>,
                            url: new_link,
                        };
                    }
                    case "pos": {
                        const s = String(elem);
                        if (/^\d+$/.test(s)) {
                            const new_where = JSON.parse(search.get("where")!);
                            new_where.pos = [Number(elem)];
                            const new_link =
                                "/tenant/" +
                                encodeURIComponent(tenantid) +
                                "/calls?order=utc&tz=" +
                                encodeURIComponent(tz) +
                                "&where=" +
                                encodeURIComponent(JSON.stringify(new_where));
                            return {
                                str: String(elem),
                                r: <Link to={new_link}>{String(elem)}</Link>,
                                url: new_link,
                            };
                        } else if (s === "Abandoned") {
                            const new_where = JSON.parse(search.get("where")!);
                            new_where.abandoned = true;
                            new_where.is_911 = true;
                            const new_link =
                                "/tenant/" +
                                encodeURIComponent(tenantid) +
                                "/calls?order=utc&tz=" +
                                encodeURIComponent(tz) +
                                "&where=" +
                                encodeURIComponent(JSON.stringify(new_where));
                            return {
                                str: String(elem),
                                r: <Link to={new_link}>{String(elem)}</Link>,
                                url: new_link,
                            };
                        } else if (s === "Failed to Answer") {
                            const new_where = JSON.parse(search.get("where")!);
                            new_where.abandoned = true;
                            new_where.is_911 = false;
                            const new_link =
                                "/tenant/" +
                                encodeURIComponent(tenantid) +
                                "/calls?order=utc&tz=" +
                                encodeURIComponent(tz) +
                                "&where=" +
                                encodeURIComponent(JSON.stringify(new_where));
                            return {
                                str: String(elem),
                                r: <Link to={new_link}>{String(elem)}</Link>,
                                url: new_link,
                            };
                        } else {
                            return {
                                str: String(elem),
                                r: String(elem),
                                url: null,
                            };
                        }
                    }
                    case "cos": {
                        const str =
                            elem === null ? "No ALI Received" : String(elem);
                        const new_where = JSON.parse(search.get("where")!);
                        new_where.cos = [elem];
                        const new_link =
                            "/tenant/" +
                            encodeURIComponent(tenantid) +
                            "/calls?order=utc&tz=" +
                            encodeURIComponent(tz) +
                            "&where=" +
                            encodeURIComponent(JSON.stringify(new_where));
                        return {
                            str,
                            r: <Link to={new_link}>{str}</Link>,
                            url: new_link,
                        };
                    }
                    case "coss": {
                        const str = String(elem);
                        const new_where = JSON.parse(search.get("where")!);
                        new_where.coss = [
                            elem === "No ALI Received" ? null : elem,
                        ];
                        const new_link =
                            "/tenant/" +
                            encodeURIComponent(tenantid) +
                            "/calls?order=utc&tz=" +
                            encodeURIComponent(tz) +
                            "&where=" +
                            encodeURIComponent(JSON.stringify(new_where));
                        return {
                            str,
                            r: <Link to={new_link}>{str}</Link>,
                            url: new_link,
                        };
                    }
                    case "calltaker":
                        return {
                            str: String(elem),
                            r: String(elem),
                            url: null,
                        };
                    case "icalltaker": {
                        const str = elem === null ? "Unknown" : String(elem);
                        const new_where = JSON.parse(search.get("where")!);

                        new_where.icalltaker = [elem];
                        if (elem === null) {
                            new_where.abandoned = false;
                        } else if (elem === "Abandoned") {
                            // TODO: better way of signaling this
                            new_where.abandoned = true;
                            new_where.icalltaker = undefined;
                            new_where.is_911 = true;
                        } else if (elem === "Failed to Answer") {
                            new_where.abandoned = true;
                            new_where.icalltaker = undefined;
                            new_where.is_911 = false;
                        }

                        const new_link =
                            "/tenant/" +
                            encodeURIComponent(tenantid) +
                            "/calls?order=utc&tz=" +
                            encodeURIComponent(tz) +
                            "&where=" +
                            encodeURIComponent(JSON.stringify(new_where));
                        return {
                            str,
                            r: <Link to={new_link}>{str}</Link>,
                            url: new_link,
                        };
                    }
                    case "dow": {
                        const str = Info.weekdays()[(elem + 6) % 7];
                        return { str, r: str, url: null };
                    }
                    case "hod":
                        return {
                            str: elem + ":00",
                            r: elem + ":00",
                            url: null,
                        };
                    case "moy": {
                        const str = Info.months()[elem - 1];
                        return { str, r: str, url: null };
                    }
                    default:
                        return {
                            str: "TODO" + group_by + String(elem) + tz,
                            r: "TODO" + group_by + String(elem) + tz,
                            url: null,
                        };
                }
            }
        }

        const data = result.table.map(e => {
            const formatted = format_groupby(e[group_by]);
            return {
                name: formatted.str,
                [str_agg(fields[graph_col])]: e[graph_col],
                url: formatted.url,
            };
        });

        const header = (
            <tr className="header">
                <td>{group_by_name.get(group_by, group_by)}</td>
                {fields.map((e, i) => (
                    <td key={i}>
                        <span
                            className="clickable_header"
                            onClick={() => set_graph_col(i)}
                        >
                            {str_agg(e)}
                        </span>
                    </td>
                ))}
            </tr>
        );
        const summary = (
            <tr className="summary">
                <td>—</td>
                {fields.map((f, i) => (
                    <td key={i}>{format_field(f, result.total[i]).jsx}</td>
                ))}
            </tr>
        );

        const handleGraphClick: RechartsFunction = (arg0: any) => {
            if (arg0 && typeof arg0.activeTooltipIndex === "number") {
                const activeTooltipIndex: number = arg0.activeTooltipIndex;
                const e = result.table[activeTooltipIndex];
                if (e) {
                    const url = format_groupby(e[group_by]).url;
                    if (typeof url === "string") {
                        history.push(url);
                    }
                }
            }
        };

        const graph_style = css`
            .recharts-legend-item-text {
                font-size: 20px;
                margin-left: 5px;
            }
        `;
        let graph: React.ReactNode;
        switch (group_by) {
            case "day":
            case "hour":
            case "week":
            case "month":
            case "dow":
            case "hod":
            case "moy":
                graph = (
                    <AreaChart
                        width={900}
                        height={594}
                        data={data}
                        style={{ marginLeft: "auto", marginRight: "auto" }}
                        margin={{ top: 10, right: 30 }}
                        onClick={handleGraphClick}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Area
                            dataKey={str_agg(fields[graph_col])}
                            fill="#8884d8"
                        />
                    </AreaChart>
                );
                break;
            case "pos":
            case "trunk":
                graph = (
                    <BarChart
                        width={900}
                        height={594}
                        data={data}
                        style={{ marginLeft: "auto", marginRight: "auto" }}
                        margin={{ top: 10, right: 30 }}
                        onClick={handleGraphClick}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" interval={0} />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar
                            dataKey={str_agg(fields[graph_col])}
                            fill="#8884d8"
                        />
                    </BarChart>
                );
                break;
            case "calltaker":
            case "icalltaker":
            case "cos":
                graph = (
                    <BarChart
                        layout="vertical"
                        width={900}
                        height={Math.max(594, data.length * 50)}
                        data={data}
                        style={{ marginLeft: "auto", marginRight: "auto" }}
                        margin={{ top: 10, right: 30 }}
                        onClick={handleGraphClick}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis type="number" />
                        <YAxis
                            type="category"
                            dataKey="name"
                            width={100}
                            interval={0}
                        />
                        <Tooltip />
                        <Legend />
                        <Bar
                            dataKey={str_agg(fields[graph_col])}
                            fill="#8884d8"
                        />
                    </BarChart>
                );
                break;
            case "coss":
                const handlePieClick: RechartsFunction = (arg0: any) => {
                    if (arg0 && typeof arg0.name === "string") {
                        const name: string = arg0.name;
                        const datum = data.find(x => x.name === name);
                        if (datum && typeof datum.url === "string") {
                            history.push(datum.url);
                        }
                    }
                };
                const colors = [
                    "#0057A5",
                    "#DC3912",
                    "#FF9900",
                    "#109618",
                    "#990099",
                    "#3B3EAC",
                    "#0099C6",
                    "#DD4477",
                    "#66AA00",
                    "#B82E2E",
                    "#316395",
                    "#994499",
                    "#22AA99",
                    "#AAAA11",
                    "#6633CC",
                    "#E67300",
                    "#8B0707",
                    "#329262",
                    "#5574A6",
                    "#3B3EAC",
                ];
                graph = (
                    <PieChart
                        width={900}
                        height={594}
                        style={{ marginLeft: "auto", marginRight: "auto" }}
                    >
                        <Pie
                            dataKey={str_agg(fields[graph_col])}
                            data={data}
                            nameKey="name"
                            label={({ index, percent }) =>
                                `${data[index!].name}: ${(
                                    100 * percent!
                                ).toFixed(1)}%`
                            }
                            labelLine={true}
                            onClick={handlePieClick}
                        >
                            {data.map((_, index) => (
                                <Cell
                                    key={index}
                                    fill={colors[index % colors.length]}
                                />
                            ))}
                        </Pie>
                        <Tooltip />
                        <Legend verticalAlign="top" />
                    </PieChart>
                );
                break;
            default:
                graph = "Unknown group_by";
                break;
        }

        const csv = d3.csvFormat(
            [...result.table, { ...result.total, [group_by]: undefined }].map(
                e => {
                    const ret: { [index: string]: string } = {};
                    if (e[group_by] !== undefined) {
                        ret[
                            group_by_name.get(group_by, group_by)
                        ] = format_groupby(e[group_by]).str;
                    }
                    fields.forEach((f, i) => {
                        ret[str_agg(f).replace("\n", " ")] = format_field(
                            f,
                            e[i],
                        ).str;
                    });
                    return ret;
                },
            ),
            [
                group_by_name.get(group_by, group_by),
                ...fields.map(f => str_agg(f).replace("\n", " ")),
            ],
        );

        const subtitle = search.get("subtitle") + " by " + group_by_subtitle;

        let title = "all PSAPs known to tenant";
        if (where.cpsap) {
            title = where.cpsap
                .map(e => {
                    const p = psaps.find(x => x.psap === e);
                    if (p) {
                        return p.name;
                    } else {
                        return e;
                    }
                })
                .join(", ");
        }

        const date_subtitle: string = (function () {
            const from = DateTime.fromISO(result.from, { zone: result.tz });
            const to = DateTime.fromISO(result.to, { zone: result.tz });
            // DATE_SHORT is MM/DD/YYYY in en-US locale
            if (from.equals(to)) {
                // for a single day just return that day
                return from.toLocaleString(DateTime.DATE_SHORT);
            } else {
                // this is inclusive (includes the last day)
                return `${from.toLocaleString(
                    DateTime.DATE_SHORT,
                )} to ${to.toLocaleString(DateTime.DATE_SHORT)}`;
            }
        })();

        const search_params_debug = () => {
            const search = new URLSearchParams(location.search);
            const ret: React.ReactNode[] = [];
            for (const [k, v] of search) {
                ret.push(
                    <div key={k}>
                        {k}={v}
                    </div>,
                );
            }

            return <pre>{ret}</pre>;
        };

        return (
            <div css={divCss}>
                <Helmet>
                    <title>
                        {title}: {subtitle}
                    </title>
                </Helmet>
                {debug_mode ? search_params_debug() : null}
                <h1>{title}</h1>
                <h2>{subtitle}</h2>
                <h3>{date_subtitle}</h3>
                <div css={graph_style}>{graph}</div>
                <table css={tableCss}>
                    <tbody>
                        <tr>
                            <td
                                style={{ border: "none" }}
                                colSpan={fields.length + 1}
                            >
                                <div style={{ marginBottom: 15 }}>
                                    <div style={{ float: "left" }}>SUMMARY</div>
                                </div>
                            </td>
                        </tr>
                        {header}
                        {summary}
                        <tr>
                            <td
                                style={{ border: "none" }}
                                colSpan={fields.length + 1}
                            >
                                <div
                                    style={{ marginBottom: 15, marginTop: 30 }}
                                >
                                    <div style={{ float: "left" }}>DETAIL</div>
                                    <div style={{ float: "right" }}>
                                        <a
                                            download={`${title}-${subtitle}-${date_subtitle}.csv`}
                                            href={
                                                "data:text/csv," +
                                                encodeURIComponent(csv)
                                            }
                                            css={css`
                                                @media print {
                                                    display: none;
                                                }
                                            `}
                                        >
                                            Download CSV
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        {header}
                        {result.table.map((e, i) => (
                            <tr
                                key={i}
                                className={
                                    i % 2 === 0
                                        ? "greenbar_odd"
                                        : "greenbar_even"
                                }
                            >
                                <td>{format_groupby(e[group_by]).r}</td>
                                {fields.map((f, j) => (
                                    <td key={j}>
                                        {
                                            format_field(f, result.table[i][j])
                                                .jsx
                                        }
                                    </td>
                                ))}
                            </tr>
                        ))}
                        {summary}
                    </tbody>
                </table>
                {debug_mode ? (
                    <>
                        <pre>{result.query}</pre>
                        <pre>{result.explain}</pre>
                    </>
                ) : null}
            </div>
        );
    } else {
        return <Loading msg="Creating report" />;
    }
}
