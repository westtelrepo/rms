/** @jsx jsx */
import { css, jsx } from "@emotion/core";

export type IField =
    | "answer"
    | "call_length"
    | "hourly_cnt"
    | "daily_cnt"
    | "monthly_cnt";

export type IAggregate =
    | {
          t: "total";
      }
    | {
          t: "total_perc";
      }
    | {
          t: "count_transfers";
          e: string;
      }
    | {
          t: "count_transfers_not";
          e: string;
      }
    | {
          t: "max";
          f: IField;
      }
    | {
          t: "min";
          f: IField;
      }
    | {
          t: "avg";
          f: IField;
      }
    | {
          t: "percentile_cont";
          f: IField;
          p: number;
      }
    | {
          t: "stddev_pop";
          f: IField;
      }
    | {
          t: "stddev_samp";
          f: IField;
      }
    | {
          t: "answer_less_than_seconds";
          s: number;
          /// show red when under this value
          r?: number;
      }
    | {
          t: "count_abandoned";
      }
    | {
          t: "count_failed_to_answer";
      }
    | {
          t: "count_all_kinds_of_abandoned";
      }
    | {
          t: "count_failed_xfer";
      };

export function str_agg(agg: IAggregate): string {
    function str(f: IField) {
        switch (f) {
            case "answer":
                return "Answer Time\n(Seconds)";
            case "call_length":
                return "Call Length\n(Seconds)";
            case "daily_cnt":
                return "Daily Call Count";
            case "hourly_cnt":
                return "Hourly Call Count";
            case "monthly_cnt":
                return "Monthly Call Count";
            default:
                return f;
        }
    }

    if (agg.t === "total") {
        return "Total Call\nCount";
    } else if (agg.t === "total_perc") {
        return "Percentage"; // TODO:?
    } else if (agg.t === "percentile_cont") {
        return `${(100 * agg.p).toFixed(1)}% Percentile of\n${str(agg.f)}`;
    } else if (agg.t === "avg") {
        return `Average\n${str(agg.f)}`;
    } else if (agg.t === "max") {
        return `Highest\n${str(agg.f)}`;
    } else if (agg.t === "min") {
        return `Lowest\n${str(agg.f)}`;
    } else if (agg.t === "stddev_pop" || agg.t === "stddev_samp") {
        return `Standard Deviation of\n${str(agg.f)}`;
    } else if (agg.t === "answer_less_than_seconds") {
        return `Percentage of Calls\nAnswered Within ${agg.s}\nSeconds`;
    } else if (agg.t === "count_transfers_not" && agg.e === "none") {
        return "Total\nTransfers";
    } else if (agg.t === "count_transfers" && agg.e === "tandem") {
        return "Tandem\nTransfers";
    } else if (agg.t === "count_transfers" && agg.e === "ng911") {
        return "NG911\nTransfers";
    } else if (agg.t === "count_transfers" && agg.e === "blind") {
        return "Blind\nTransfers";
    } else if (agg.t === "count_transfers" && agg.e === "attended") {
        return "Attended\nTransfers";
    } else if (agg.t === "count_transfers" && agg.e === "conference") {
        return "Conference\nTransfers";
    } else if (agg.t === "count_abandoned") {
        return "Abandoned";
    } else if (agg.t === "count_failed_to_answer") {
        return "Failed to\nAnswer";
    } else if (agg.t === "count_all_kinds_of_abandoned") {
        return "Failed/Abandoned Transfer/Call"; // TODO: better name
    } else if (agg.t === "count_failed_xfer") {
        return "Failed\nTransfer";
    } else {
        // we have no idea
        return JSON.stringify(agg);
    }
}

export function format_field(
    agg: IAggregate,
    data: number | null | undefined,
): { str: string; jsx: React.ReactNode } {
    if (data === null) {
        return { str: "Unknown", jsx: "Unknown" };
    }
    if (data === undefined) {
        return { str: "N/A", jsx: "N/A" };
    }

    if (agg.t === "total") {
        return { str: String(data), jsx: String(data) };
    } else if (agg.t === "answer_less_than_seconds") {
        if (
            typeof data === "number" &&
            typeof agg.r === "number" &&
            data < agg.r
        ) {
            return {
                str: (100 * data).toFixed(1) + "%",
                jsx: (
                    <span
                        css={css`
                            color: red;
                        `}
                    >
                        {(100 * data).toFixed(1) + "%"}
                    </span>
                ),
            };
        } else {
            return {
                str: (100 * data).toFixed(1) + "%",
                jsx: (100 * data).toFixed(1) + "%",
            };
        }
    } else if (agg.t === "total_perc") {
        return {
            str: (100 * data).toFixed(1) + "%",
            jsx: (100 * data).toFixed(1) + "%",
        };
    } else if (
        agg.t === "min" ||
        agg.t === "max" ||
        agg.t === "percentile_cont" ||
        agg.t === "avg"
    ) {
        switch (agg.f) {
            case "answer":
            case "call_length":
            case "daily_cnt":
            case "hourly_cnt":
            case "monthly_cnt":
                return { str: data.toFixed(1), jsx: data.toFixed(1) };
            default:
                return { str: String(data), jsx: String(data) };
        }
    } else {
        return { str: String(data), jsx: String(data) };
    }
}
