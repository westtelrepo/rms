import { useLayoutEffect, useState } from "react";

let debug_mode = false;
const listeners = new Set<(arg: boolean) => void>();

(window as any).enableDebugMode = function () {
    console.log("Welcome to debug mode!");
    debug_mode = true;

    for (const f of listeners) {
        f(debug_mode);
    }
};

(window as any).disableDebugMode = function () {
    console.log("leaving debug mode");
    debug_mode = false;

    for (const f of listeners) {
        f(debug_mode);
    }
};

export function useDebugMode(): boolean {
    const [, set_state] = useState(debug_mode);
    useLayoutEffect(() => {
        listeners.add(set_state);
        return () => {
            listeners.delete(set_state);
        };
    }, []);
    return debug_mode;
}
