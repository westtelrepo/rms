import React from "react";
import { trigger } from "swr";

import Loading from "./loading";
import { useFetch, useFetchFunc } from "./useFetch";

export default function DB() {
    const data = useFetch("/api/db");
    const gfetch = useFetchFunc();

    async function migrate_all_tenants() {
        if (window.confirm("migrate all tenants?")) {
            await gfetch("/api/db/migrate_all_tenants", { method: "POST" });
            void trigger("/api/db");
        }
    }

    return (
        <>
            <button onClick={migrate_all_tenants}>Migrate All Tenants</button>
            {data ? (
                <pre>{JSON.stringify(data, null, 2)}</pre>
            ) : (
                <Loading msg="Loading" />
            )}
        </>
    );
}
