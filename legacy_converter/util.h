#ifndef WESTTEL_UTIL_H
#define WESTTEL_UTIL_H

#include <boost/optional.hpp>
#include <iostream>
#include <libgen.h>
#include <memory>
#include <string>
#include <vector>

namespace westtel { inline namespace util {

    inline std::string str_basename(const std::string& s) {
        // basename can write to argument, so that requires a copy
        // however, writing to .c_str() is undefined behavior:
        // http://en.cppreference.com/w/cpp/string/basic_string/c_str
        std::unique_ptr<char[]> string{new char[s.length() + 1]};
        std::copy(s.begin(), s.end(), string.get());
        string[s.length()] = '\0';
        return basename(string.get());
    }

    inline std::string str_dirname(const std::string& s) {
        // same as str_basename
        std::unique_ptr<char[]> string{new char[s.length() + 1]};
        std::copy(s.begin(), s.end(), string.get());
        string[s.length()] = '\0';
        return dirname(string.get());
    }

    inline boost::optional<std::string> emptystr_null(const std::string& s) {
        if (s == "") return boost::none;
        else return s;
    }

    template<class T>
    inline std::ostream& operator<<(std::ostream& out, boost::optional<T> o) {
        if (o) out << *o;
        else out << "null";
        return out;
    }

    template<class T>
    inline std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
        out << "[";
        if (v.size() >= 1) out << v[0];
        for (size_t i = 1; i < v.size(); ++i)
            out << ", " << v[i];
        out << "]";
        return out;
    }

    inline std::string trim(const std::string& s) {
        std::string ret = "";
        ret.reserve(s.size());
        size_t i = 0;
        for (; i < s.size() && isspace(s[i]); ++i)
            ;

        std::string tmp = "";
        for (; i < s.size(); ++i) {
            if (isspace(s[i])) tmp.push_back(s[i]);
            else {
                ret.append(tmp);
                ret.push_back(s[i]);
                tmp = "";
            }
        }
        return ret;
    }

    template<class T> T sto_int(const std::string& s, size_t* pos = nullptr,
                                int base = 10);

    template<> inline int sto_int<int>(const std::string& s, size_t* pos, int base)
    { return std::stoi(s, pos, base); }

    template<>
    inline long sto_int<long>(const std::string& s, size_t* pos, int base)
    { return std::stol(s, pos, base); }

    template<>
    inline unsigned long sto_int<unsigned long>(const std::string& s, size_t* pos, int base)
    { return std::stoul(s, pos, base); }

    template<>
    inline unsigned long long sto_int<unsigned long long>(const std::string& s, size_t* pos, int base)
    { return std::stoull(s, pos, base); }


    template<>
    inline unsigned short sto_int<unsigned short>(const std::string& s, size_t* pos, int base)
    {
        unsigned long ret = sto_int<unsigned long>(s, pos, base);
        assert(ret <= std::numeric_limits<unsigned short>::max());
        return static_cast<unsigned short>(ret);
    }

    template<class T> T string_to_int(const std::string& s, int base = 10) {
        std::size_t pos = 0;
        T ret = sto_int<T>(s, &pos, base);
        if (pos != s.size()) throw std::runtime_error{"pos != s.size"};
        assert(pos == s.size());
        return ret;
    }

    inline std::vector<std::string> split(const std::string& str,
        const char delim, size_t limit = SIZE_MAX)
    {
        std::vector<std::string> ret;
        std::string temp = "";
        for (size_t i = 0; i < str.size(); ++i) {
            if (str[i] == delim) {
                if (--limit <= 0) {
                    for (; i < str.size(); ++i)
                        temp.push_back(str[i]);
                } else {
                    ret.push_back(temp);
                    temp = "";
                }
            } else
                temp.push_back(str[i]);
        }
        ret.push_back(temp);
        return ret;
    }

}};

#endif
