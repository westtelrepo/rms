#include "parse_config.h"

#include <iostream>

parse_config parse_config::parse(const Json::Value& config) {
    parse_config ret;

    const std::string controller = config["controller"].asString();

    // psap -> position set
    std::map<std::string, std::set<int>> psap_positions;
    // position -> psap
    std::map<int, std::string> position_psap;

    const auto& psaps = config["psaps"];
    for (auto iter = psaps.begin(); iter != psaps.end(); ++iter) {
        const std::string psap_id = iter.name();

        std::set<int> positions;
        for (const auto& position: (*iter)["positions"]) {
            const int pos = position.asInt();
            positions.insert(pos);
            ret.controller_positions[controller][pos] = psap_id;
            position_psap[pos] = psap_id;
        }

        psap_positions[psap_id] = positions;

        for (const auto& trunk: (*iter)["trunks"]) {
            ret.controller_trunks[controller][trunk.asInt()].psap = psap_id;
        }

        for (const auto& trunk: (*iter)["911_trunks"]) {
            trunk_config c;
            c.psap = psap_id;
            c.call_type = call_type::is_911;
            ret.controller_trunks[controller][trunk.asInt()] = c;
        }

        for (const auto& trunk: (*iter)["admin_trunks"]) {
            trunk_config c;
            c.psap = psap_id;
            c.call_type = call_type::admin;
            ret.controller_trunks[controller][trunk.asInt()] = c;
        }
    }

    const auto& dialplan = config["dialplan"];
    for (auto iter = dialplan.begin(); iter != dialplan.end(); ++iter) {
        const std::string num = iter.name();
        dialplan_config c;

        for (const auto& position: (*iter)["positions"]) {
            const int pos = position.asInt();
            c.positions.insert(pos);
            c.psap = position_psap[pos];
        }

        if (iter->isMember("psap")) {
            c.psap = (*iter)["psap"].asString();
            if (c.positions.empty()) {
                c.positions = psap_positions[c.psap];
            }
        }

        ret.controller_dialplan[controller][num] = c;
    }

    return ret;
}
