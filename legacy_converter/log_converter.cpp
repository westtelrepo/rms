#include "log_converter.h"

#include <regex>
#include <stddef.h>
#include <utf8.h>

struct log_converter::impl {
    std::regex r101_starting{R"(\[INFO\] Starting 911 Controller Version \((\S+)\) in Master Mode(?: UUID=\S+)?)"};
    std::regex r143_warning_blocked{R"(\[WARNING\] Transfer Blocked, Position (\d+) not part of Conference Call)"};
    std::regex r146_warning{R"(\[WARNING\] (.*))"};
    std::regex r147_warning{R"(\[WARNING\] (.*))"};
    std::regex r204_lis_deref{R"(HTTP De-reference sent to : (\S+))"};
    // this comment is load bearing
    std::regex r204_lost{R"(HTTP LoST Service by Location Request (.+) sent to : (\S+))"};
    std::regex r205_http_location_request{R"(HTTP LocationRequest sent to : (\S+))"};
    std::regex r208_reduced_heartbeat{R"(\[WARNING\] Begin Reduced Heartbeat Interval; Interval=(\S+) Seconds \(Ping (Successful|Failed)\))"};
    std::regex r209{R"(ACK Received Index=(.*))"};
    std::regex r216_invalid_ali_warning{R"(\[WARNING\] Invalid ALI Data Received, /START OF SAMPLE/(.*)/END OF SAMPLE/)"};
    std::regex r220_warning{R"(\[WARNING\] (.*))"};
    std::regex r222_1{R"(Request Sent with RequestKey=(\d+); Index=(\d+); Trunk=(\d+) \(to DB=(\d+) PP=(\d+)\))"};
    std::regex r222_2{R"(Request Sent Index=(\d+) \(to DB=(\d+) PP=(\d+); with TransactionID=(\d+)\))"};
    std::regex r222_3{R"(Request Sent Index=(\d+) Trunk=(\d+) \(to ALI Service=(\d+) \))"};
    std::regex r225{R"(\[WARNING\] No ALI Database can be Bid for RequestKey=(\S+))"};
    std::regex r226{R"((?:Initial )?Record received \(Data Follows\)(.*))"};
    std::regex r231_resent{R"(Request Resent with RequestKey=(\d+); Index=(\d+); Trunk=(\d+) \(to DB=(\d+) PP=(\d+)\))"};
    std::regex r232_unmatched{R"(\[WARNING\] UnMatched Record received(?:, TransactionId=(\d+))? \(Data Follows\)(.*))"};
    std::regex r236_incoming_data_ignored{R"(\[ALARM\] Incoming Data Ignored for (\d+) Minutes; Reason: (.*))"};
    std::regex r236_port_data_reception_inhibited{R"(\[WARNING\] Port Data Reception Inhibited for (\d+) Minutes; Consecutive Invalid ALI Records =  (\d+))"};
    std::regex r236_port_data_reception_inhibited_alarm{R"(\[ALARM\] Port Data Reception Inhibited for (\d+) Minutes; Consecutive Invalid ALI Records =  (\d+))"};
    std::regex r241_warning{R"(\[WARNING\] Buffer Cleared Length = (\S+))"};
    std::regex r246_warning{R"(\[WARNING\] (.*))"};
    std::regex r261_warning{R"(\[WARNING\] (.*))"};
    std::regex r266{R"(Rebid Record received \(Data Follows\)(.*))"};
    std::regex r267{R"(Manual Bid Record received \(Data Follows\)(.*))"};
    std::regex r268{R"(Auto Rebid Record received \(Data Follows\)(.*))"};
    std::regex r269{R"(Manual Request Sent with RequestKey=(\d+); Index=(\d+); Trunk=(\d+) \(to DB=(\d+) PP=(\d+)\))"};
    std::regex r269_ali{R"(Manual Request Sent Index=(\d+) Trunk=(\d+) \(to ALI Service=(\d+) ?\))"};
    std::regex r270{R"(Rebid Request Sent with RequestKey=(\d+); Index=(\d+); Trunk=(\d+) \(to DB=(\d+) PP=(\d+)\))"};
    std::regex r270_tid{R"(Rebid Request Sent Index=(\d+) \(to DB=(\d+) PP=(\d+); with TransactionID=(\d+)\))"};
    std::regex r270_ali{R"(Rebid Request Sent Index=(\d+) Trunk=(\d+) \(to ALI Service=(\d+) ?\))"};
    std::regex r271{R"(Auto Request Sent with RequestKey=(\d+); Index=(\d+); Trunk=(\d+) \(to DB=(\d+) PP=(\d+)\))"};
    std::regex r271_tid{R"(Auto Request Sent Index=(\d+) \(to DB=(\d+) PP=(\d+); with TransactionID=(\d+)\))"};
    std::regex r271_ali{R"(Auto Request Sent Index=(\d+) Trunk=(\d+) \(to ALI Service=(\d+) ?\))"};
    std::regex r278_lis{R"(HTTP Data Received : (.*))"};
    std::regex r278_unmatched{R"(\[WARNING\] Unmatched HTTP Data Received : (.*))"};
    std::regex r279_lis{R"(NG911 Location Data received \(Data Follows\)(.*))"};

    std::regex r306{R"(Connect \(From=(.*)\))"};
    std::regex r308_1{R"(Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r316{R"(Ringing \(From=(.*)\))"};
    std::regex r341_i3{R"(NENA I3 CallId: (\S+) )"};
    std::regex r341_i3_incidentid{R"(NENA I3 IncidentId: (\S+) )"};
    std::regex r342_1{R"(Conference Leave ?)"};
    std::regex r342_2{R"(Conference Leave exten: (.*))"};

    std::regex r351{R"regex(\[TDD\] TDD Character "(.*)" Received)regex"};
    std::regex r352{R"regex(\[TDD\] TDD Caller Says: "(.*)")regex"};
    std::regex r352_sms{R"(SMS Text From (\d+): (.*))"};

    std::regex r361{R"(TDD Conversation Record \(Data Follows\)\r?\n([\s\S]*))"};
    std::regex r361_sms{R"(Text Conversation Record \(Data Follows\)\r?\n([\s\S]*))"};

    std::regex r365_1{R"(Conference Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_2{R"(Attended Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_3{R"(Blind Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_4{R"(Tandem Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_5{R"(NG911 SIP Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_6{R"(Flash Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_7{R"(Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_indigital{R"(INdigital Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_conference_id{R"(Conference Transfer from GUI; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r365_ng911_id{R"(NG911 SIP Transfer from GUI; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r365_tandem_id{R"(Tandem Transfer from GUI; \(Destination=(\S+)\) ID=(\S+))"};
    std::regex r365_intrado_id{R"(Intrado Transfer from GUI; \(Destination=(\S+)\) ID=(\S+))"};
    std::regex r365_indigital_id{R"(INdigital Transfer from GUI; \(Destination=(\S+)\) ID=(\S+))"};

    std::regex r366_1{R"(Transfer from Phone Cancelled; \(Destination=(.*)\))"};
    std::regex r366_2{R"(Attended Transfer from Phone Cancelled; \(Destination=(.*)\))"};
    std::regex r366_3{R"(Blind Transfer from Phone Cancelled; \(Destination=(.*)\))"};
    // std::regex r366_4{R"(\{INFO\] Transfer Cancelled; (.*))"}; // NOTE: doesn't exist
    std::regex r366_attended_id{R"(Attended Transfer from Phone Cancelled; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r366_attended_useless_id{R"(Attended Transfer from Phone Cancelled; \(Destination=(.*)\) ID=)"};

    std::regex r367_1{R"(Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_2{R"(Transfer from GUI Cancelled; \(Destination=(.*?)\) Reason: (.*))"};
    std::regex r367_3{R"(Attended Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_4{R"(Blind Transfer from GUI (?:Cancelled|Failed); \(Destination=(.*)\))"};
    std::regex r367_6{R"(Tandem Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_7{R"(Conference Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_8{R"(Blind Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_conference_fail_id{R"(Conference Transfer from GUI Cancelled; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r367_intrado_fail_id{R"(Intrado Transfer from GUI Cancelled; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r367_tandem_fail_id{R"(Tandem Transfer from GUI Cancelled; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r368_warning{R"(\[WARNING\] (.*))"};

    std::regex r369_1{R"(Attended Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*))"};
    std::regex r369_2{R"(Blind Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*))"};
    std::regex r369_blind_id{R"(Blind Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*) ID=(\S+))"};
    std::regex r369_attended_id{R"(Attended Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*) ID=(\S+))"};

    std::regex r370_1{R"(Outbound Call from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_2{R"(Attended Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_3{R"(Unattended Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_4{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_5{R"(Outbound Blind Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_valet{R"(Valet Park Pickup from lot: (.*))"};
    std::regex r370_attended_id{R"(Attended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r370_unattended_id{R"(Unattended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r370_blind_id{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r370_blind_useless_id{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\) ID=)"};
    std::regex r370_valet_to_lot{R"(Valet Park to lot: (\d+) ID=(\S+))"};
    std::regex r370_attended_useless_id{R"(Attended Transfer from Phone Connected; \(Destination=(\S*)\) ID=)"};

    std::regex r372{R"(Outbound Call from Phone Dialed; \(Destination=(.*)\))"};

    std::regex r373_1{R"(Transfer from Phone Dialed; \(Destination=(.*)\))"};
    std::regex r373_2{R"(\[INFO\] Transfer\/Conf Dialed; (.*))"};
    std::regex r373_attended{R"(Attended Transfer from Phone Dialed; \(Destination=(.*)\))"};
    std::regex r373_attended_id{R"(Attended Transfer from Phone Dialed; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r378_1{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_2{R"(Blind Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_3{R"(Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_4{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_conference_id{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r378_conference_useless_id{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\) ID=)"};
    std::regex r378_ng911_id{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S*))"};

    std::regex r381_info{R"(\[INFO\] (.*))"};
    std::regex r315_info{R"(\[INFO\] (.*))"};
    std::regex r382{R"(\[TDD\] Dispatcher (.*) says: (.*))"};
    std::regex r382_sms{R"(SMS Text From Dispatcher (\d+) To (\d+) : (.*))"};

    std::regex r383_blind{R"(Blind Transfer From GUI Failed; \(Destination=(.*)\); Reason=(.*))"};
    std::regex r383_conference{R"(Conference Transfer From GUI Failed; \(Destination=(.*)\); Reason=(.*))"};
    std::regex r383_ng911{R"(NG911 SIP Transfer From GUI Failed; \(Destination=(\S*)\); Reason=(\S+) ID=(\S+))"};

    std::regex r391{R"(TDD MODE ON; Channel: (.*))"};

    std::regex r392{R"(Blind Transfer from Phone Dialed; \(Destination=(.*)\))"};
    std::regex r392_id{R"(Blind Transfer from Phone Dialed; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r395_1{R"(Outbound Call to Position Connected; \(Destination=(.*)\))"};
    std::regex r395_ng911_sip_no_id{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\) ID=)"};
    std::regex r395_ng911_sip_id{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_blind_id{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_unattended_id{R"(Unattended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_attended_id{R"(Attended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_conference_id{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r396_merge{R"(Call Merged with UUid (\d+))"};

    std::regex r408_warning{R"(\[WARNING\] (.*))"};
    std::regex r413_warning{R"(\[WARNING\] (.*))"};
    std::regex r428_warning{R"(\[WARNING\] (.*))"};
    std::regex r430_warning{R"(\[WARNING\] (.*))"};

    std::regex r709{R"(\[INFO\] (?:User (.*) - )?Workstation (.*) At IP: (.*) Now On Line)"};
    std::regex r717{R"(\[INFO\] (?:User (.*) - )?Workstation (.*) At IP: (.*) Now Off Line)"};

    std::map<int, std::string> calltaker;

    std::string current_version = "";
    int proc_id = 0;

    bool throw_exceptions_less_ = false;

    parse_config config;
};

void log_converter::set_parse_config(parse_config config) {
    impl_->config = config;
}

log_converter::log_converter(): impl_{new impl{}, [](impl* i){ delete i; }} {
}

Json::Value log_converter::convert(const OldLog& e) {
    try {
        Json::Value log;
        log["oldlog"] = e.to_json();
        log["controller"] = e.psap;
        log["t"]["ut"] = e.utc();
        log["code"] = "code/" + std::to_string(e.code) + "/";
        log["d"]["s"] = 6; // default is NOTICE

        log["d"]["msg"] = e.message;
        log["d"]["proc"]["name"] = "ng911";

        if (impl_->current_version != "") {
            log["d"]["proc"]["version"] = impl_->current_version;
        }

        log["d"]["proc"]["id"] = std::to_string(impl_->proc_id);

        if (e.thread) {
            log["d"]["thread"]["name"] = *e.thread;
        }

        if (e.position.value_or(0) != 0) {
            log["user"]["position"] = e.position.value();
            if (impl_->calltaker.count(e.position.value())) {
                log["user"]["name"] = impl_->calltaker[e.position.value()];
            }

            // if position has PSAP, set it, otherwise PSAP is controller
            if (auto psap = impl_->config.position(e.psap, e.position.value())) {
                log["user"]["psap"] = psap.value();
            } else {
                log["user"]["psap"] = e.psap;
            }
        }

        if (e.callid) {
            log["call"]["id"] = e.callid.value();
        }

        if (e.trunk) {
            log["call"]["trunk"] = e.trunk.value();
        }

        if (e.callid && e.trunk) {
            if (auto trunk = impl_->config.trunk(e.psap, e.trunk.value())) {
                if (trunk->call_type == call_type::is_911) {
                    log["call"]["is_911"] = true;
                }
            }
        }

        if (e.ani) {
            if (e.ani.value() == "9999999999") {
                log["call"]["is_911"] = true;
            } else if (e.ani.value() == "1111111111") {
                log["call"]["is_911"] = false;
            }

            log["call"]["ani"] = e.ani.value();
        }

        if (e.pani) {
            if (e.pani.value() == "p9999999999" || e.pani.value() == "p0000000000") {
                log["call"]["is_911"] = true;
            }

            log["call"]["pani"] = e.pani.value();
            log["call"]["is_911"] = true;
        }

        auto no_newlines = [] (const std::string& s) {
            std::string ret;
            for (auto c: s) {
                if (c != '\r' && c != '\n')
                    ret += c;
            }
            return ret;
        };

        std::smatch m;
        const std::string message_no_new = no_newlines(e.message);

        if (e.code == 101) {
            // TODO: parse and pass UUID
            if (std::regex_match(message_no_new, m, impl_->r101_starting)) {
                log["code"] = "krn/starting/";
                log["d"]["proc"]["version"] = m[1].str();
                impl_->current_version = m[1].str();
                log["d"]["s"] = 5; // NOTICE
                impl_->proc_id++;
                log["d"]["proc"]["id"] = std::to_string(impl_->proc_id);
            } else {
                throw std::runtime_error{"Unknown 101 message: " + message_no_new};
            }
        } else if (e.code == 143) {
            if (std::regex_match(message_no_new, m, impl_->r143_warning_blocked)) {
                log["code"] = "warning/transfer_blocked/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 143 message: " + message_no_new};
            }
        } else if (e.code == 146) {
            if (std::regex_match(message_no_new, m, impl_->r146_warning)) {
                log["d"]["s"] = 4;
                if (m[1] == "Forced Disconnect Sent") {
                    log["code"] = "call/warning/forced_disconnect/";
                } else {
                    log["code"] = "warning/";
                }
            } else {
                throw std::runtime_error{"Unknown 146 message: " + message_no_new};
            }
        } else if (e.code == 147) {
            if (std::regex_match(message_no_new, m, impl_->r147_warning)) {
                log["code"] = "warning/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 147 message: " + message_no_new};
            }
        } else if (e.code == 166) {
            if (message_no_new == "[WARNING] ALI Rebid Received with ALI Button Off") {
                log["code"] = "warning/ali/rebid_received_ali_button_off/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 166 message: " + message_no_new};
            }
        } else if (e.code == 181) {
            if (message_no_new == "[INFO] 911 Controller Shutting Down") {
                log["code"] = "krn/shutting_down/";
                log["d"]["s"] = 5; // NOTICE
            } else {
                throw std::runtime_error{"Unknown 181 message: " + message_no_new};
            }
        } else if (e.code == 204) {
            if (std::regex_match(message_no_new, m, impl_->r204_lis_deref)) {
                log["code"] = "lis/deref/";
                log["_sent_to"] = m[1].str();
                log["d"]["msg"] = "HTTP Dereference sent to " + m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r204_lost)) {
                log["code"] = "lis/lost/";
                log["_urn"] = m[1].str();
                log["_sent_to"] = m[2].str();
                log["d"]["msg"] = "HTTP LoST Service by Location Request " + m[1].str() + " sent to: " + m[2].str();
            } else {
                throw std::runtime_error{"Unknown 204 message: " + message_no_new};
            }
        } else if (e.code == 205) {
            if (std::regex_match(message_no_new, m, impl_->r205_http_location_request)) {
                log["call"]["is_911"] = true;
                log["code"] = "lis/locationrequest/http/";
                log["_sent_to"] = m[1].str();
                log["d"]["msg"] = "LIS HTTP location request sent to " + m[1].str();
            } else {
                throw std::runtime_error{"Unknown 205 HTTP LocationRequest message: " + message_no_new};
            }
        } else if (e.code == 208) {
            if (std::regex_match(message_no_new, m, impl_->r208_reduced_heartbeat)) {
                log["code"] = "warning/ali/reduced_heartbeat_interval/";
                log["_reduced_heartbeat_interval"] = m[1].str();
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 208 message: " + message_no_new};
            }
        } else if (e.code == 209) {
            if (std::regex_match(message_no_new, m, impl_->r209)) {
                log["code"] = "ali/ack_received/";
                log["ali"]["request"]["index"] = m[1].str();
            } else {
                // only throw unknown if it has a callid
                if (e.callid) {
                    throw std::runtime_error{"Unknown 209 message: " + message_no_new};
                }
            }
        } else if (e.code == 211) {
            if (message_no_new == "[WARNING] End Reduced Heartbeat Interval") {
                log["code"] = "warning/ali/end_reduced_heartbeat_interval/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 211 message: " + message_no_new};
            }
        } else if (e.code == 216) {
            if (std::regex_match(message_no_new, m, impl_->r216_invalid_ali_warning)) {
                log["code"] = "warning/ali/invalid_ali/";
                log["d"]["s"] = 4;
                log["ali"]["record"]["raw"] = parse_angle_form(m[1].str());
                log["d"]["msg"] = "[WARNING] Invalid ALI data received";
            } else {
                throw std::runtime_error{"Unknown 216 message: " + message_no_new};
            }
        } else if (e.code == 220) {
            if (std::regex_match(message_no_new, m, impl_->r220_warning)) {
                log["code"] = "warning/";
            } else {
                throw std::runtime_error{"Unknown 220 message: " + message_no_new};
            }
        } else if (e.code == 222) {
            log["code"] = "ali/request/initial/";
            log["call"]["is_911"] = true;
            log["ali"]["type"] = "initial";
            log["ali"]["state"] = "request";

            // user is meaningless
            log.removeMember("user");

            if (std::regex_match(message_no_new, m, impl_->r222_1)) {
                log["ali"]["request"]["request_key"] = m[1].str();
                log["ali"]["request"]["index"] = m[2].str();
                log["ali"]["request"]["trunk"] = m[3].str();
                log["ali"]["request"]["db"] = m[4].str();
                log["ali"]["request"]["pp"] = m[5].str();
                log["d"]["msg"] = "Initial ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " with RequestKey=" + m[1].str() + "; Index=" + m[2].str() + "; Trunk=" + m[3].str()
                    + " (to DB=" + m[4].str() + " PP=" + m[5].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r222_2)) {
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["db"] = m[2].str();
                log["ali"]["request"]["pp"] = m[3].str();
                log["ali"]["request"]["transaction_id"] = m[4].str();
                log["d"]["msg"] = "Initial ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " Index=" + m[1].str() + " (to DB=" + m[2].str() + " PP=" + m[3].str() + "; with TransactionID=" + m[4].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r222_3)) {
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["trunk"] = m[2].str();
                log["ali"]["request"]["ali_service"] = m[3].str();
                // remove extra space
                log["d"]["msg"] = "Initial ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " Index=" + m[1].str() + " Trunk=" + m[2].str() + " (to ALI Service=" + m[3].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 222 message: " + message_no_new};
            }
        } else if (e.code == 225) {
            log["call"]["is_911"] = true;
            log["code"] = "ali/nobid/";
            log["ali"]["type"] = "nobid";
            log["ali"]["state"] = "nobid";
            if (std::regex_match(message_no_new, m, impl_->r225)) {
                log["ali"]["request"]["request_key"] = m[1].str();
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 225 message: " + message_no_new};
            }
        } else if (e.code == 226) {
            log["call"]["is_911"] = true;
            log["code"] = "ali/response/initial/";
            log["ali"]["type"] = "initial";
            log["ali"]["state"] = "response";

            // user is meaningless
            log.removeMember("user");

            if (std::regex_match(message_no_new, m, impl_->r226)) {
                log["ali"]["record"]["raw"] = parse_angle_form(m[1].str());
                log["d"]["msg"] = "Initial ALI record received" + (e.thread ? " (from " + *e.thread + ")" : "");
            } else {
                throw std::runtime_error{"Unknown 226 message: " + message_no_new};
            }
        } else if (e.code == 228) {
            if (message_no_new == "[WARNING] Heartbeat ACK Timeout") {
                log["d"]["s"] = 4;
                log["code"] = "warning/ali/heartbeat_ack_timeout/";
            } else {
                throw std::runtime_error{"Unknown 228 message: " + message_no_new};
            }
        } else if (e.code == 231) {
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r231_resent)) {
                log["code"] = "ali/resent/";
            } else {
                throw std::runtime_error{"Unknown 231 ALI resent message: " + message_no_new};
            }
        } else if (e.code == 232) {
            log["call"]["is_911"] = true;
            log["code"] = "ali/response/unmatched/";
            log["ali"]["state"] = "response";
            log["ali"]["type"] = "unmatched";
            log["d"]["s"] = 4;
            if (std::regex_match(message_no_new, m, impl_->r232_unmatched)) {
                if (m[1].matched) {
                    log["ali"]["request"]["transaction_id"] = m[1].str();
                    log["d"]["msg"] = "Unmatched ALI record received, TransactionId=" + m[1].str();
                } else {
                    log["d"]["msg"] = "Unmatched ALI record received";
                }
                log["ali"]["record"]["raw"] = parse_angle_form(m[2].str());
            } else {
                throw std::runtime_error{"Unknown 232 ALI unmatched error: " + message_no_new};
            }
        } else if (e.code == 235) {
            if (message_no_new == "[ALARM] Port Data Reception Restored") {
                log["d"]["s"] = 3;
                log["code"] = "error/ali/port_data_reception_restored/";
            } else if (message_no_new == "[WARNING] Port Data Reception Restored") {
                log["d"]["s"] = 4;
                log["code"] = "warning/ali/post_data_reception_restored/";
            } else {
                throw std::runtime_error{"Unknown 235 Port Data Reception Restored message: " + message_no_new};
            }
        } else if (e.code == 236) {
            if (std::regex_match(message_no_new, m, impl_->r236_incoming_data_ignored)) {
                log["code"] = "error/ali/incoming_data_ignored/";
                log["d"]["s"] = 3;
                log["_incoming_data_ignored_minutes"] = m[1].str();
                log["_reason"] = m[2].str();
            } else if (std::regex_match(message_no_new, m, impl_->r236_port_data_reception_inhibited)) {
                log["code"] = "error/ali/port_data_reception_inhibited/";
                log["d"]["s"] = 4;
                log["_port_data_reception_inhibited_minutes"] = m[1].str();
                log["_consecutive_invalid_ali_records"] = m[2].str();
            } else if (std::regex_match(message_no_new, m, impl_->r236_port_data_reception_inhibited_alarm)) {
                log["code"] = "error/ali/port_data_reception_inhibited/";
                log["d"]["s"] = 3;
                log["_port_data_reception_inhibited_minutes"] = m[1].str();
                log["_consecutive_invalid_ali_records"] = m[2].str();
            } else {
                throw std::runtime_error{"Unknown 236 message: " + message_no_new};
            }
        } else if (e.code == 241) {
            if (std::regex_match(message_no_new, m, impl_->r241_warning)) {
                log["code"] = "warning/buffer_cleared/";
                log["buffer_cleared_length"] = westtel::string_to_int<Json::UInt64>(m[1].str());
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 241 buffer cleared warning: " + message_no_new};
            }
        } else if (e.code == 246) {
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r246_warning)) {
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 246 message: " + message_no_new};
            }
        } else if (e.code == 261) {
            if (std::regex_match(message_no_new, m, impl_->r261_warning)) {
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown 261 message: " + message_no_new};
            }
        } else if (e.code == 266) {
            log["call"]["is_911"] = true;
            log["code"] = "ali/response/rebid/";
            log["ali"]["type"] = "rebid";
            log["ali"]["state"] = "response";
            if (std::regex_match(message_no_new, m, impl_->r266)) {
                log["ali"]["record"]["raw"] = parse_angle_form(m[1].str());
                log["d"]["msg"] = "Rebid ALI request received" + (e.thread ? " (from " + *e.thread + ")" : "");
            } else {
                throw std::runtime_error{"Unknown 266 ALI Rebid message: " + message_no_new};
            }
        } else if (e.code == 267) {
            log["call"]["is_911"] = true;
            log["code"] = "ali/response/manual/";
            log["ali"]["type"] = "manual";
            log["ali"]["state"] = "response";
            if (std::regex_match(message_no_new, m, impl_->r267)) {
                log["ali"]["record"]["raw"] = parse_angle_form(m[1].str());
                log["d"]["msg"] = "Manual ALI bid record received" + (e.thread ? " (from " + *e.thread + ")" : "");
            } else {
                throw std::runtime_error{"Unknown 267 ALI manual message: " + message_no_new};
            }
        } else if (e.code == 268) {
            log["call"]["is_911"] = true;
            log["code"] = "ali/response/autorebid/";
            log["ali"]["type"] = "autorebid";
            log["ali"]["state"] = "response";

            // user is meaningless
            log.removeMember("user");
            if (std::regex_match(message_no_new, m, impl_->r268)) {
                log["ali"]["record"]["raw"] = parse_angle_form(m[1].str());
                log["d"]["msg"] = "ALI auto rebid record received" + (e.thread ? " (from " + *e.thread + ")" : "");
            } else {
                throw std::runtime_error{"Unknown 268 autorebid message: " + message_no_new};
            }
        } else if (e.code == 269) {
            log["code"] = "ali/request/manual/";
            log["ali"]["state"] = "request";
            log["ali"]["type"] = "manual";
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r269)) {
                log["ali"]["request"]["request_key"] = m[1].str();
                log["ali"]["request"]["index"] = m[2].str();
                log["ali"]["request"]["trunk"] = m[3].str();
                log["ali"]["request"]["db"] = m[4].str();
                log["ali"]["request"]["pp"] = m[5].str();
                log["d"]["msg"] = "Manual ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " with RequestKey=" + m[1].str() + "; Index=" + m[2].str() + "; Trunk=" + m[3].str()
                    + " (to DB=" + m[4].str() + " PP=" + m[5].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r269_ali)) {
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["trunk"] = m[2].str();
                log["ali"]["request"]["ali_service"] = m[3].str();
                log["d"]["msg"] = "Manual ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "") + " Index=" + m[1].str()
                    + " Trunk=" + m[2].str() + " (to ALI Service=" + m[3].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 269 manual ali bid message: " + message_no_new};
            }
        } else if (e.code == 270) {
            log["code"] = "ali/request/rebid/";
            log["ali"]["state"] = "request";
            log["ali"]["type"] = "rebid";
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r270)) {
                log["ali"]["request"]["request_key"] = m[1].str();
                log["ali"]["request"]["index"] = m[2].str();
                log["ali"]["request"]["trunk"] = m[3].str();
                log["ali"]["request"]["db"] = m[4].str();
                log["ali"]["request"]["pp"] = m[5].str();
                log["d"]["msg"] = "Rebid ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " with RequestKey=" + m[1].str() + "; Index=" + m[2].str() + "; Trunk=" + m[3].str()
                    + "(to DB=" + m[4].str() + " PP=" + m[5].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r270_tid)) {
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["db"] = m[2].str();
                log["ali"]["request"]["pp"] = m[3].str();
                log["ali"]["request"]["transaction_id"] = m[4].str();
                log["d"]["msg"] = "Rebid ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " Index=" + m[1].str() + " (to DB=" + m[2].str() + " PP=" + m[3].str() + "; with TransactionID=" + m[4].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r270_ali)) {\
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["trunk"] = m[2].str();
                log["ali"]["request"]["ali_service"] = m[3].str();
                // remove space
                log["d"]["msg"] = "Rebid ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "") + " Index=" + m[1].str()
                    + " Trunk=" + m[2].str() + " (to ALI Service=" + m[3].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 270 Rebid Request message: " + message_no_new};
            }
        } else if (e.code == 271) {
            log["code"] = "ali/request/autorebid/";
            log["call"]["is_911"] = true;
            log["ali"]["state"] = "request";
            log["ali"]["type"] = "autorebid";

            // user is meaningless
            log.removeMember("user");

            if (std::regex_match(message_no_new, m, impl_->r271)) {
                log["ali"]["request"]["request_key"] = m[1].str();
                log["ali"]["request"]["index"] = m[2].str();
                log["ali"]["request"]["trunk"] = m[3].str();
                log["ali"]["request"]["db"] = m[4].str();
                log["ali"]["request"]["pp"] = m[5].str();
                log["d"]["msg"] = "Auto ALI Request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " with RequestKey=" + m[1].str() + "; Index=" + m[2].str() + "; Trunk=" + m[3].str() + " (to DB=" + m[4].str() + " PP=" + m[5].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r271_tid)) {
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["db"] = m[2].str();
                log["ali"]["request"]["pp"] = m[3].str();
                log["ali"]["request"]["transaction_id"] = m[4].str();
                log["d"]["msg"] = "Auto ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "")
                    + " Index=" + m[1].str() + "(to DB=" + m[2].str() + " PP=" + m[3].str() + "; with TransactionID=" + m[4].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r271_ali)) {
                log["ali"]["request"]["index"] = m[1].str();
                log["ali"]["request"]["trunk"] = m[2].str();
                log["ali"]["request"]["ali_service"] = m[3].str();
                // remove extra space
                log["d"]["msg"] = "Auto ALI request sent"
                    + (e.thread ? " (by " + *e.thread + ")" : "") + " Index=" + m[1].str()
                    + " Trunk=" + m[2].str() + " (to ALI Service=" + m[3].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 271 Auto Request message: " + message_no_new};
            }
        } else if (e.code == 278) {
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r278_lis)) {
                log["code"] = "lis/received/http/";
                log["_lis_received"] = m[1].str();
                log["d"]["msg"] = "LIS HTTP Data Received: " + m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r278_unmatched)) {
                log["code"] = "lis/received/http/unmatched/";
                log["d"]["s"] = 4;
                log["d"]["msg"] = "[WARNING] Unmatched LIS HTTP Data Received: " + m[1].str();
                log["_lis_received"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 278 LIS Received message: " + message_no_new};
            }
        } else if (e.code == 279) {
            log["code"] = "lis/ng911/received/";
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r279_lis)) {
                log["ali"]["record"]["raw"] = parse_angle_form(m[1].str());
                log["d"]["msg"] = "NG911 Location Data received";
            } else {
                throw std::runtime_error{"Unknown 279 LIS message: " + message_no_new};
            }
        } else if (e.code == 306) {
            log["transfer"]["state"] = "join";
            log["transfer"]["type"] = "initial";

            log["code"] = "call/connect/";
            log["user"]["exten"] = Json::nullValue;
            log["user"]["_may_be_wrong_outbound"] = true;

            if (e.message == "Connect") {
            } else if (std::regex_match(message_no_new, m, impl_->r306)) {
                log["call"]["sip"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 306 Connect message: " + message_no_new};
            }
        } else if (e.code == 307) {
            if (!log.isMember("user")) {
                log["user"]["exten"] = Json::nullValue;
            }

            log["code"] = "call/disconnect/";
            if (e.message == "Disconnect") {
            } else {
                throw std::runtime_error{"Unknown 307 Disconnect message: " + message_no_new};
            }
        } else if (e.code == 308) {
            log["transfer"]["state"] = "join";
            if (std::regex_match(message_no_new, m, impl_->r308_1)) {
                log["transfer"]["type"] = "unknown";
                log["transfer"]["destination"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 308 message: " + message_no_new};
            }
        } else if (e.code == 309) {
            log["code"] = "call/join/";
            if (e.message == "Conference Join") {
            } else {
                throw std::runtime_error{"Unknown 309 Conference Join message: " + message_no_new};
            }
        } else if (e.code == 310) {
            log["code"] = "call/hold/on/";
            if (e.message == "On Hold") {
            } else {
                throw std::runtime_error{"Unknown 310 On Hold message: " + message_no_new};
            }
        } else if (e.code == 311) {
            log["code"] = "call/hold/off/";
            if (e.message == "Off Hold") {
            } else {
                throw std::runtime_error{"Unknown 311 Off Hold message: " + message_no_new};
            }
        } else if (e.code == 312) {
            log["transfer"]["state"] = "abandon";
            log["transfer"]["type"] = "initial";

            // destroy user information because position is incorrect and useless
            log["user"] = Json::objectValue;
            if (e.trunk) {
                if (auto trunk = impl_->config.trunk(e.psap, e.trunk.value())) {
                    log["user"]["psap"] = trunk->psap;
                } else {
                    log["user"]["psap"] = e.psap;
                }
            } else {
                log["user"]["psap"] = e.psap;
            }

            log["user"]["_may_be_wrong_outbound"] = true;

            log["code"] = "call/abandoned/";
            if (e.message == "Abandoned") {
            } else {
                throw std::runtime_error{"Unknown 312 Abandoned message: " + message_no_new};
            }
        } else if (e.code == 316) {
            if (log.isMember("user"))
                throw std::runtime_error{"316 Ringing has position user"};

            if (e.ani)
                log["user"]["exten"] = e.ani.value();
            else if (e.pani)
                log["user"]["exten"] = e.pani.value();
            else
                log["user"]["exten"] = Json::nullValue;

            log["code"] = "call/ringing/";

            log["transfer"]["state"] = "dial";
            log["transfer"]["type"] = "initial";
            if (e.message == "Ringing") {
            } else if (e.message == "Ring Back") {
                log.removeMember("transfer");
                log["code"] = "call/316_ring_back/";
            } else if (std::regex_match(message_no_new, m, impl_->r316)) {
                log["call"]["sip"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 316 message: " + message_no_new};
            }
        } else if (e.code == 341) {
            log["call"]["is_911"] = true;
            if (std::regex_match(message_no_new, m, impl_->r341_i3)) {
                log["code"] = "nena/i3/id/";
                log["d"]["msg"] = "NENA i3 callid: " + m[1].str();
                log["nena_i3"]["callid"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r341_i3_incidentid)) {
                log["code"] = "nena/i3/incidentid/";
                log["d"]["msg"] = "NENA i3 incident id: " + m[1].str();
                log["nena_i3"]["incidentid"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 341 NENA i3 message: " + message_no_new};
            }
        } else if (e.code == 342) {
            if (std::regex_match(message_no_new, m, impl_->r342_1)) {
                log["code"] = "call/leave/";
                if (e.position.value_or(0) == 0)
                    throw std::runtime_error{"Conference Leave w/o exten with zero or null position"};
                log["d"]["msg"] = "Conference Leave"; // strip off space at the end
            } else if (std::regex_match(message_no_new, m, impl_->r342_2)) {
                log["code"] = "call/leave/";
                if (!log.isMember("user")) {
                    log["user"]["exten"] = m[1].str();
                } else {
                    throw std::runtime_error{"342 Conference Leave has both exten and position"};
                }
            } else {
                throw std::runtime_error{"Unknown 342 Conference Leave message: " + message_no_new};
            }
        } else if (e.code == 351) {
            // [TDD] TDD Character "*" Received
            if (std::regex_match(message_no_new, m, impl_->r351)) {
                log["code"] = "tdd/character_received/";
                log["tdd_character_received"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 351 message: " + message_no_new};
            }
        } else if (e.code == 352) {
            // [TDD] Caller Says: "*"
            if (std::regex_match(message_no_new, m, impl_->r352)) {
                log["code"] = "tdd/caller_says/";
                log["tdd_caller_says"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r352_sms)) {
                log["code"] = "sms/text/";
                log["_sms"]["from"] = m[1].str();
                log["_sms"]["message"] = m[2].str();
            } else {
                throw std::runtime_error{"Unknown 352 message: " + message_no_new};
            }
        } else if (e.code == 361) {
            if (std::regex_match(e.message, m, impl_->r361)) {
                log["code"] = "tdd/conversation_record/";
                log["tdd"]["conversation_record"] = m[1].str();
                log["d"]["msg"] = "TDD Conversation Record";
            } else if (std::regex_match(e.message, m, impl_->r361_sms)) {
                log["code"] = "sms/conversation_record/";
                log["_sms"]["conversation_record"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 361 message: " + message_no_new};
            }
        } else if (e.code == 365) {
            log["transfer"]["state"] = "dial";
            if (std::regex_match(message_no_new, m, impl_->r365_1)) {
                log["code"] = "transfer/dial/conference/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "conference";
            } else if (std::regex_match(message_no_new, m, impl_->r365_2)) {
                throw std::runtime_error{"Got a 365 Attended Transfer from GUI, which does not exist"};
                //log["code"] = "call/transfer/attended/";
                //log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r365_3)) {
                log["code"] = "transfer/dial/blind/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "blind";
            } else if (std::regex_match(message_no_new, m, impl_->r365_4)) {
                log["code"] = "transfer/dial/tandem/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "tandem";
            } else if (std::regex_match(message_no_new, m, impl_->r365_5)) {
                log["code"] = "transfer/dial/ng911/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "ng911";
                log["transfer"]["no_callee_is_abandoned"] = true;
            } else if (std::regex_match(message_no_new, m, impl_->r365_6)) {
                throw std::runtime_error{"Got 365 Flash Transfer from GUI, which does not exist"};
                //log["code"] = "call/transfer/flash/";
                //log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r365_7)) {
                // NOTE: does not exist in 2018
                log["code"] = "transfer/dial/unknown/gui/";
                log["transfer"]["type"] = "unknown";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r365_indigital)) {
                // TODO: only one site has these
                log["code"] = "transfer/dial/indigital/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "indigital";
            } else if (std::regex_match(message_no_new, m, impl_->r365_conference_id)) {
                log["code"] = "transfer/dial/conference/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "conference";
                log["d"]["msg"] = "Conference Transfer from GUI (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r365_ng911_id)) {
                log["code"] = "transfer/dial/ng911/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "ng911";
                log["d"]["msg"] = "NG911 SIP Transfer from GUI (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r365_tandem_id)) {
                log["code"] = "transfer/dial/tandem/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "tandem";
                log["d"]["msg"] = "Tandem Transfer from GUI; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r365_intrado_id)) {
                log["code"] = "transfer/dial/intrado/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "intrado";
                log["d"]["msg"] = "Intrado Transfer from GUI; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r365_indigital_id)) {
                log["code"] = "transfer/dial/indigital/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "indigital";
                log["d"]["msg"] = "INdigital Transfer from GUI; (Destination=" + m[1].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 365 Transfer from GUI message: " + message_no_new};
            }
        } else if (e.code == 366) {
            log["transfer"]["state"] = "abandon";
            if (std::regex_match(message_no_new, m, impl_->r366_1)) {
                // NOTE: this only exists in 2011
                log["code"] = "transfer/abandon/unknown/phone/";
                log["transfer"]["type"] = "*"; // TODO: is it really?
                log["transfer"]["destination"] = m[1].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r366_2)) {
                log["code"] = "transfer/abandon/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r366_3)) {
                log["code"] = "transfer/abandon/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r366_attended_id)) {
                log["code"] = "transfer/abandon/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Attended Transfer from Phone Cancelled; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r366_attended_useless_id)) {
                log["code"] = "transfer/abandon/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["_match_with_id"] = true;
                log["d"]["msg"] = "Attended Transfer from Phone Cancelled; (Destination=" + m[1].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 366 Transfer Cancelled message: " + message_no_new};
            }
        } else if (e.code == 367) {
            log["transfer"]["state"] = "abandon";
            if (std::regex_match(message_no_new, m, impl_->r367_1)) {
                log["code"] = "transfer/abandon/unknown/gui/";
                log["transfer"]["type"] = "unknown";
                log["transfer"]["destination"] = m[1].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r367_2)) {
                log["code"] = "transfer/abandon/unknown/gui/";
                log["transfer"]["type"] = "*";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r367_3)) {
                log["code"] = "transfer/abandon/attended/gui/";
                log["transfer"]["destination"] = m[1].str();
                throw std::runtime_error{"370 Attended Transfer from GUI Cancelled doesn't exist"};
            } else if (std::regex_match(message_no_new, m, impl_->r367_4)) {
                log["code"] = "transfer/abandon/blind/gui/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r367_6)) {
                log["code"] = "transfer/abandon/tandem/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "tandem";
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r367_7)) {
                log["code"] = "transfer/abandon/conference/gui/";
                log["transfer"]["type"] = "conference";
                log["transfer"]["destination"] = m[1].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r367_8)) {
                log["transfer"]["destination"] = m[1].str();
                throw std::runtime_error{"Programming error, a 367 message should not appear here"};
            } else if (std::regex_match(message_no_new, m, impl_->r367_conference_fail_id)) {
                log["code"] = "transfer/abandon/conference/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "conference";
                log["d"]["msg"] = "Conference Transfer from GUI Cancelled; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r367_intrado_fail_id)) {
                log["code"] = "transfer/abandon/intrado/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "intrado";
                log["d"]["msg"] = "Intrado Transfer from GUI Cancelled; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r367_tandem_fail_id)) {
                log["code"] = "transfer/abandon/tandem/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "tandem";
                log["d"]["msg"] = "Tandem Transfer from GUI Cancelled; (Destination=" + m[1].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 367 Transfer GUI Cancelled message: " + message_no_new};
            }
        } else if (e.code == 368) {
            if (std::regex_match(message_no_new, m, impl_->r368_warning)) {
                log["d"]["s"] = 4; // warning
            } else {
                throw std::runtime_error{"Unknown 368 message"};
            }
        } else if (e.code == 369) {
            log["transfer"]["state"] = "abandon";
            if (std::regex_match(message_no_new, m, impl_->r369_1)) {
                log["code"] = "transfer/abandon/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r369_2)) {
                log["code"] = "transfer/abandon/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r369_blind_id)) {
                log["code"] = "transfer/abandon/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[3].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log["d"]["msg"] = "Blind Transfer from Phone Failed; (Destination=" + m[1].str() + "); Reason=" + m[2].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r369_attended_id)) {
                log["code"] = "transfer/abandon/blind/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log["transfer"]["id"] = m[3].str();
                log["d"]["msg"] = "Attended Transfer from Phone Failed; (Destination=" + m[1].str() + "); Reason=" + m[2].str();
                log.removeMember("user");
            } else {
                throw std::runtime_error{"Unknown 369 message: " + message_no_new};
            }

        } else if (e.code == 370) {
            log["transfer"]["state"] = "join";

            if (std::regex_match(message_no_new, m, impl_->r370_1)) {
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "initial";
                log["code"] = "call/connect/outbound/";
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r370_2)) {
                log["code"] = "transfer/join/attended/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "attended";
            } else if (std::regex_match(message_no_new, m, impl_->r370_3)) {
                log["code"] = "transfer/join/attended/unattended/";
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "*"; // TODO: really?
                log["transfer"]["unattended"] = true;
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r370_4)) {
                log["code"] = "transfer/join/blind/phone/";
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "blind";
            } else if (std::regex_match(message_no_new, m, impl_->r370_5)) {
                log["code"] = "transfer/join/outbound_blind/";
                log["transfer"]["type"] = "unknown";
                log["transfer"]["destination"] = m[1].str();
                // NOTE: doesn't appear in 2017-2018
            } else if (std::regex_match(message_no_new, m, impl_->r370_valet)) {
                // TODO: we are ignoring this
                log.removeMember("transfer");
            } else if (std::regex_match(message_no_new, m, impl_->r370_valet_to_lot)) {
                // TODO: we are ignoring this
                log.removeMember("transfer");
            } else if (std::regex_match(message_no_new, m, impl_->r370_attended_id)) {
                log["code"] = "transfer/join/attended/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Attended Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r370_unattended_id)) {
                log["code"] = "transfer/join/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["unattended"] = true;
                log["d"]["msg"] = "Unattended Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r370_blind_id)) {
                log["code"] = "transfer/join/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Blind Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r370_blind_useless_id)) {
                log["code"] = "transfer/join/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log["d"]["msg"] = "Blind Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r370_attended_useless_id)) {
                log["code"] = "transfer/join/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["d"]["msg"] = "Attended Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (e.message == "Reconnect Ringback") {
                log["code"] = "call/error/reconnect_ringback/";
                log.removeMember("transfer");
            } else {
                throw std::runtime_error{"Unknown 370 message: " + message_no_new};
            }
        } else if (e.code == 372) {
            log["transfer"]["state"] = "dial";
            log["transfer"]["type"] = "initial";

            log["code"] = "call/outbound/";
            if (std::regex_match(message_no_new, m, impl_->r372)) {
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 372 message: " + message_no_new};
            }
        } else if (e.code == 373) {
            log["transfer"]["state"] = "dial";

            if (std::regex_match(message_no_new, m, impl_->r373_1)) {
                log["code"] = "transfer/dial/unknown/phone/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "unknown";
            } else if (std::regex_match(message_no_new, m, impl_->r373_2)) {
                // TODO: what do we do here
            } else if (std::regex_match(message_no_new, m, impl_->r373_attended)) {
                log["code"] = "transfer/dial/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r373_attended_id)) {
                log["code"] = "transfer/dial/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Attended Transfer from Phone Dialed; (Destination=" + m[1].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 373 message: " + message_no_new};
            }
        } else if (e.code == 378) {
            log["transfer"]["state"] = "join";

            if (std::regex_match(message_no_new, m, impl_->r378_1)) {
                log["code"] = "transfer/join/conference/gui/";
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "conference";
                log["user"]["_wrong_if_same_position"] = true;
            } else if (std::regex_match(message_no_new, m, impl_->r378_2)) {
                log["code"] = "transfer/join/blind/gui/";
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "blind";
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r378_3)) {
                log["code"] = "transfer/join/unknown/gui/";
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "*";
            } else if (std::regex_match(message_no_new, m, impl_->r378_4)) {
                log["code"] = "transfer/join/ng911/gui/";
                log["call"]["destination"] = m[1].str();
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["type"] = "ng911";
                log["user"]["_wrong_if_same_position"] = true;
            } else if (std::regex_match(message_no_new, m, impl_->r378_conference_id)) {
                log["code"] = "transfer/join/conference/gui/";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["transfer"]["type"] = "conference";
                log["user"]["_wrong_if_same_position"] = true;
                log["d"]["msg"] = "Conference Transfer from GUI Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r378_conference_useless_id)) {
                log["code"] = "transfer/join/conference/gui/";
                if (m[1].str() != "") {
                    log["transfer"]["destination"] = m[1].str();
                }
                log["transfer"]["type"] = "conference";
                log["transfer"]["_match_with_id"] = true;
                log["user"]["_wrong_if_same_position"] = true;
                log["d"]["msg"] = "Conference Transfer from GUI Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r378_ng911_id)) {
                log["code"] = "transfer/join/ng911/gui/";
                auto dest = m[1].str();
                if (dest != "") {
                    log["transfer"]["destination"] = m[1].str();
                }
                auto id = m[2].str();
                if (id != "") {
                    log["transfer"]["id"] = m[2].str();
                }
                log["transfer"]["type"] = "ng911";
                log["user"]["_wrong_if_same_position"] = true;
                log["d"]["msg"] = "NG911 SIP Transfer from GUI Connected; (Destination=" + dest + ")";
            } else {
                throw std::runtime_error{"Unknown 378 message: " + message_no_new};
            }
        } else if (e.code == 381) {
            if (std::regex_match(message_no_new, m, impl_->r381_info)) {

            } else {
                throw std::runtime_error{"Unknown 381 message: " + message_no_new};
            }
        }
        else if (e.code == 315)
        {
            if (std::regex_match(message_no_new, m, impl_->r315_info))
            {
            }
            else
            {
                throw std::runtime_error{"Unknown 315 message: " + message_no_new};
            }
        }
        else if (e.code == 382)
        {
            // 382 DISPATCHER SAYS
            if (std::regex_match(message_no_new, m, impl_->r382)) {
                log["code"] = "tdd/dispatcher_says/";
                log["tdd"]["says"] = m[2].str();
                log["tdd"]["_dispatcher"] = m[1].str(); // TODO: what is this
            } else if (std::regex_match(message_no_new, m, impl_->r382_sms)) {
                log["code"] = "sms/dispatcher/";
                log["_sms"]["dispatcher"] = m[1].str();
                log["_sms"]["message"] = m[2].str();
            } else {
                throw std::runtime_error{"Unknown 382 message: " + message_no_new};
            }
        } else if (e.code == 383) {
            if (std::regex_match(message_no_new, m, impl_->r383_blind)) {
                log["transfer"]["type"] = "blind";
                log["transfer"]["state"] = "abandon";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r383_conference)) {
                log["transfer"]["type"] = "conference";
                log["transfer"]["state"] = "abandon";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log.removeMember("user");
            } else if (std::regex_match(message_no_new, m, impl_->r383_ng911)) {
                log["code"] = "transfer/abandon/ng911/gui/";
                log["transfer"]["type"] = "ng911";
                log["transfer"]["state"] = "abandon";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["abandon_reason"] = m[2].str();
                log["transfer"]["id"] = m[3].str();
                log["d"]["msg"] = "NG911 SIP Transfer From GUI Failed; (Destination=" + m[1].str() + "); Reason=" + m[2].str();
                log.removeMember("user");
            } else {
                throw std::runtime_error{"Unknown 383 message: " + message_no_new};
            }
        } else if (e.code == 391) {
            // 391 TDD MODE ON
            if (std::regex_match(message_no_new, m, impl_->r391)) {
                log["code"] = "tdd/on/";
            } else if (message_no_new == "TDD MODE ON") {
                log["code"] = "tdd/on/";
            } else {
                throw std::runtime_error{"Unknown 391 message: " + message_no_new};
            }
        } else if (e.code == 392) {
            log["transfer"]["state"] = "dial";
            log["code"] = "transfer/dial/blind/phone/";
            if (std::regex_match(message_no_new, m, impl_->r392)) {
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r392_id)) {
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Blind Transfer from Phone Dialed; (Destination=" + m[1].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 392 message: " + message_no_new};
            }
        } else if (e.code == 395) {
            log["transfer"]["state"] = "join";

            if (std::regex_match(message_no_new, m, impl_->r370_3)) {
                log["code"] = "transfer/join/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["unattended"] = true;
            } else if (std::regex_match(message_no_new, m, impl_->r378_1)) {
                log["code"] = "transfer/join/conference/gui/";
                log["transfer"]["type"] = "conference";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r378_4)) {
                log["code"] = "transfer/join/ng911/gui/";
                log["transfer"]["type"] = "ng911";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r370_4)) {
                log["code"] = "transfer/join/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r395_1)) {
                log["code"] = "call/connect/outbound_call_to_position/"; // TODO
                log["transfer"]["type"] = "initial";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r370_2)) {
                log["code"] = "transfer/join/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r378_2)) {
                log["code"] = "transfer/join/blind/gui/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
            } else if (std::regex_match(message_no_new, m, impl_->r395_ng911_sip_no_id)) {
                log["code"] = "transfer/join/ng911/gui/";
                log["transfer"]["type"] = "ng911";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["_match_with_id"] = true;
                log["d"]["msg"] = "NG911 SIP Transfer from GUI Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r395_blind_id)) {
                log["code"] = "transfer/join/blind/phone/";
                log["transfer"]["type"] = "blind";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Blind Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r395_unattended_id)) {
                log["code"] = "transfer/join/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["unattended"] = true;
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Unattended Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r395_ng911_sip_id)) {
                log["code"] = "transfer/join/ng911/gui/";
                log["transfer"]["type"] = "ng911";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "NG911 SIP Transfer from GUI Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r395_attended_id)) {
                log["code"] = "transfer/join/attended/phone/";
                log["transfer"]["type"] = "attended";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Attended Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else if (std::regex_match(message_no_new, m, impl_->r395_conference_id)) {
                log["code"] = "transfer/join/conference/gui/";
                log["transfer"]["type"] = "conference";
                log["transfer"]["destination"] = m[1].str();
                log["transfer"]["id"] = m[2].str();
                log["d"]["msg"] = "Conference Transfer from Phone Connected; (Destination=" + m[1].str() + ")";
            } else {
                throw std::runtime_error{"Unknown 395 message: " + message_no_new};
            }
        } else if (e.code == 396) {
            if (std::regex_match(message_no_new, m, impl_->r396_merge)) {
                // TODO: figure out call merging, which is... interesting
            } else {
                throw std::runtime_error{"Unknown 396 message: "};
            }
        } else if (e.code == 402) {
            if (message_no_new == "ALI Record Sent") {
                log["call"]["is_911"] = true;
                log["code"] = "cad/ali_sent/";
                log["d"]["msg"] = "ALI record sent to CAD" + (e.thread ? " (" + *e.thread + ")" : std::string{});
            } else {
                throw std::runtime_error{"Unknown 402 ALI Record Sent message: " + message_no_new};
            }
        } else if (e.code == 404) {
            if (message_no_new == "ACK Received") {
                log["code"] = "cad/ack_received/";
            } else {
                throw std::runtime_error{"Unknown 404 ACK Received message: " + message_no_new};
            }
        } else if (e.code == 408) {
            if (std::regex_match(e.message, m, impl_->r408_warning)) {
                log["code"] = "warning/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown CAD 408 message: " + message_no_new};
            }
        } else if (e.code == 413) {
            if (std::regex_match(e.message, m, impl_->r413_warning)) {
                log["code"] = "warning/";
                log["d"]["s"];
            } else {
                throw std::runtime_error{"Unknown CAD 413 message: " + message_no_new};
            }
        } else if (e.code == 428) {
            if (std::regex_match(e.message, m, impl_->r428_warning)) {
                log["code"] = "warning/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown CAD 428 message: " + message_no_new};
            }
        } else if (e.code == 430) {
            if (std::regex_match(e.message, m, impl_->r430_warning)) {
                log["code"] = "warning/";
                log["d"]["s"] = 4;
            } else {
                throw std::runtime_error{"Unknown CAD 430 message: " + message_no_new};
            }
        } else if (e.code == 709) {
            log["code"] = "login/login/";
            if (std::regex_match(e.message, m, impl_->r709)) {
                log["user"]["name"] = m[1].str();

                if (std::stoi(m[2].str()) != e.position.value())
                    throw std::runtime_error{"709 Login Workstation does not match position"};

                impl_->calltaker[e.position.value()] = m[1].str();
            } else {
                throw std::runtime_error{"Unknown 709 message: " + message_no_new};
            }
        } else if (e.code == 717) {
            log["code"] = "login/logout/";
            if (std::regex_match(e.message, m, impl_->r717)) {
                log["user"]["name"] = m[1].str();

                if (std::stoi(m[2].str()) != e.position.value())
                    throw std::runtime_error{"717 Logout Workstation does not match position"};

                impl_->calltaker.erase(e.position.value());
            } else {
                throw std::runtime_error{"Unknown 717 message: " + message_no_new};
            }
        } else if (e.code == 733) {
            if (e.message == "Abandoned Call Taken Control by Workstation" || e.message == "Abandoned Call Taken Control by Software") {
                // TODO: temporary, no idea
                log["code"] = "abandoned/takeover/";
            } else {
                throw std::runtime_error{"Unknown 733 message: " + message_no_new};
            }
        } else {
            if (!impl_->throw_exceptions_less_)
                throw std::runtime_error{"unknown log line"};

            if (e.callid && !std::regex_match(message_no_new, m, impl_->r146_warning))
                throw std::runtime_error{"unknown log line with callid"};
        }

        if (log.isMember("user") && !log["user"].isMember("position")) {
            log["user"].removeMember("name");
        }

        for (auto x: impl_->calltaker) {
            log["user_positions"][std::to_string(x.first)] = x.second;
        }

        return log;
    } catch(const std::exception&) {
        std::cerr << "exception with " << e << std::endl;
        throw;
    }
}

void log_converter::throw_exceptions_less() {
    impl_->throw_exceptions_less_ = true;
}

std::string parse_angle_form(const std::string& str) {
    std::string ret;
    std::size_t i = 0;

    for (; i < str.size(); i++) {
        if (str[i] == '<') {
            std::string code;
            i++;
            while (true) {
                if (i >= str.size()) {
                    ret += '<' + code;
                    goto break_outer_loop;
                } else if (str[i] == '<') {
                    // retry code processing on new '<'
                    i--;
                    ret += '<' + code;
                    goto continue_outer_loop;
                } else if (str[i] == '>') {
                    break;
                }
                code += str[i];
                i++;
            }

            if (code == "STX")
                ret += '\x02';
            else if (code == "ETX")
                ret += '\x03';
            else if (code == "CR")
                ret += '\r';
            else if (code == "LF")
                ret += '\n';
            else {
                int ch;
                try {
                    ch = std::stoi(code);
                } catch(const std::invalid_argument&) {
                    ch = -1;
                } catch(const std::out_of_range&) {
                    ch = -1;
                }
                if (ch == 0 || (ch > 127 && ch <= 255)) {
                    //ret += "<unknown>"; // TODO: fromCharCode(0xE000 + ch);
                    utf8::append(0xE000 + static_cast<uint32_t>(ch), std::back_inserter(ret));
                } else if (ch > 0 && ch <= 127) {
                    ret += static_cast<char>(ch);
                } else {
                    ret += '<' + code + '>';
                }
            }
        } else
            ret += str[i];
        continue_outer_loop: ;
    }
    break_outer_loop: ;
    return ret;
}
