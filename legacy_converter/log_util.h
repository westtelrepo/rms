#ifndef WESTTEL_CONVERT_LOG_UTIL
#define WESTTEL_CONVERT_LOG_UTIL

#include "util.h"
#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>
#include <json/json.h>
#include <regex>
#include <string>

inline std::string normalizeUtcDateString(const std::string& utc) {
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wexit-time-destructors"
	static const std::regex r{R"((\d{4})[-/](\d{2})[-/](\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{3})(?: UTC|Z)?)"};
	#pragma clang diagnostic pop

	std::smatch m;
	if (std::regex_match(utc, m, r)) {
		assert(m.size() == 8);
		return m[1].str() + "-" + m[2].str() + "-" + m[3].str()
		     + "T" + m[4].str() + ":" + m[5].str() + ":"
		     + m[6].str() + "." + m[7].str() + "Z";
	} else
		throw std::runtime_error{"Unexpected date format: " + utc};
}

// add properties for original trunk (T00) and position (P04) as text
// convert callid to text
struct OldLog {
private:
	std::string utc_;
public:
	std::string psap; // TODO: rename to controller
	int32_t code;
	boost::optional<std::string> callid;
	boost::optional<int32_t> trunk;
	boost::optional<int32_t> position;
	boost::optional<std::string> ani;
	boost::optional<std::string> pani;
	boost::optional<std::string> thread;
	boost::optional<std::string> direction;
	std::string message;

	std::string filename;
	int64_t entry;
	int64_t linenum;

	OldLog() = default;
	OldLog(const OldLog&) = default;
	OldLog(OldLog&&) = default;
	OldLog& operator=(const OldLog&) = default;
	OldLog& operator=(OldLog&&) = default;

	bool issamelog(const OldLog& o) const {
		return psap == o.psap && utc_ == o.utc_ && code == o.code
		       && callid == o.callid && trunk == o.trunk &&
		       position == o.position && ani == o.ani && pani == o.pani
		       && thread == o.thread && direction == o.direction &&
		       message == o.message;
	}

	void set_utc(const std::string& n) {
		utc_ = normalizeUtcDateString(n);
	}

	std::string utc() const { return utc_; }

	// FOR DEBUGGING PURPOSES ONLY
	Json::Value to_json() const {
		Json::Value ret{Json::objectValue};
		ret["utc"] = utc_;
		ret["psap"] = psap;
		ret["code"] = code;
		if (callid) ret["callid"] = *callid;
		if (trunk) ret["trunk"] = *trunk;
		if (position) ret["position"] = *position;
		if (ani) ret["ani"] = *ani;
		if (pani) ret["pani"] = *pani;
		if (thread) ret["thread"] = *thread;
		if (direction) ret["direction"] = *direction;
		ret["message"] = message;
		return ret;
	}
};

inline std::ostream& operator<<(std::ostream& out, const OldLog& log) {
	out << "{" << log.psap << ", " << log.utc() << ", " << log.code
	    << ", " << log.callid << ", " << log.trunk << ", " << log.position
	    << ", " << log.ani << ", " << log.pani << ", " << log.thread
	    << ", " << log.direction << ", " << log.message << "}";
	return out;
}

#endif
