#ifndef WESTTEL_RMS_PARSE_CONFIG
#define WESTTEL_RMS_PARSE_CONFIG

#include <boost/optional.hpp>
#include <json/json.h>
#include <map>
#include <ostream>
#include <set>
#include <string>

enum class call_type {
    is_911,
    admin,
};

inline std::ostream& operator<<(std::ostream& stream, call_type type) {
    switch(type) {
        case call_type::is_911:
            stream << "call_type::is_911";
            break;
        case call_type::admin:
            stream << "call_type::admin";
            break;
    }
    return stream;
}

struct trunk_config {
    std::string psap;
    boost::optional<call_type> call_type;
};

struct dialplan_config {
    std::string psap;
    std::set<int> positions;
};

struct parse_config {
    // controller -> position -> psap
    std::map<std::string,std::map<int,std::string>> controller_positions;

    boost::optional<std::string> position(std::string controller, int position) const {
        auto c = controller_positions.find(controller);

        if (c != controller_positions.end()) {
            auto p = c->second.find(position);

            if (p != c->second.end()) {
                return p->second;
            }
        }

        return boost::optional<std::string>{};
    }

    // controller -> trunk -> (psap, call_type)
    std::map<std::string,std::map<int,trunk_config>> controller_trunks;

    boost::optional<trunk_config> trunk(std::string controller, int trunk) const {
        auto c = controller_trunks.find(controller);

        if (c != controller_trunks.end()) {
            auto p = c->second.find(trunk);

            if (p != c->second.end()) {
                return p->second;
            }
        }

        return boost::optional<trunk_config>{};
    }

    // controller -> phone number -> dialplan_config
    std::map<std::string,std::map<std::string,dialplan_config>> controller_dialplan;

    boost::optional<dialplan_config> dialplan(std::string controller, std::string number) const {
        auto c = controller_dialplan.find(controller);

        if (c != controller_dialplan.end()) {
            auto p = c->second.find(number);

            if (p != c->second.end()) {
                return p->second;
            }
        }

        return boost::optional<dialplan_config>{};
    }

    static parse_config parse(const Json::Value& config);
};

#endif
