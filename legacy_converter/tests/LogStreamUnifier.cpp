#include <catch.hpp>

#include "../LogStreamUnifier.h"

static std::vector<OldLog> helper(const std::vector<const char*> contents) {
    std::map<std::string, LogStream> files;
    for (size_t i = 0; i < contents.size(); i++) {
        LogStream reader{std::unique_ptr<std::istringstream>{new std::istringstream{contents[i]}}, "test" + std::to_string(i)};
        reader.set_preserve_newlines();
        files.emplace("test" + std::to_string(i), std::move(reader));
    }
    LogStreamUnifier unif{std::move(files)};

    std::vector<OldLog> oldlog;
    while (true) {
        auto logs = unif.process();

        if (!logs.size())
            break;

        for(auto& log: logs)
            oldlog.push_back(log);
    }

    return oldlog;
}

TEST_CASE("unifier basic") {
    const char* file1 =
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
)";

    auto oldlog = helper({file1});

    REQUIRE(oldlog.size() == 3);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
    REQUIRE(oldlog.at(2).utc() == "2018-01-01T01:00:02.000Z");
}

TEST_CASE("unifier multiple files") {
    const char* file1 = "WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing\n";
    const char* file2 = "WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)\n";
    const char* file3 = "WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)\n";

    auto oldlog = helper({file1, file2, file3});

    REQUIRE(oldlog.size() == 3);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
    REQUIRE(oldlog.at(2).utc() == "2018-01-01T01:00:02.000Z");
}

TEST_CASE("unifer overlapping") {
    const char* file1 =
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
)";

    const char* file2 = "WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)\n";

    auto oldlog = helper({file1, file2});

    REQUIRE(oldlog.size() == 3);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
    REQUIRE(oldlog.at(2).utc() == "2018-01-01T01:00:02.000Z");
}

TEST_CASE("unifer overlapping beginning") {
    const char* file1 =
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
)";

    const char* file2 = "WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing\n";

    auto oldlog = helper({file1, file2});

    REQUIRE(oldlog.size() == 3);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
    REQUIRE(oldlog.at(2).utc() == "2018-01-01T01:00:02.000Z");
}

TEST_CASE("unifer overlapping ending") {
    const char* file1 =
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
)";

    const char* file2 = "WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)\n";

    auto oldlog = helper({file1, file2});

    REQUIRE(oldlog.size() == 3);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
    REQUIRE(oldlog.at(2).utc() == "2018-01-01T01:00:02.000Z");
}

TEST_CASE("unifier empty file") {
    const char* file1 = "WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing\n";
    const char* file2 = "WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)\n";
    const char* file3 = "";

    auto oldlog = helper({file1, file2, file3});

    REQUIRE(oldlog.size() == 2);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
}
