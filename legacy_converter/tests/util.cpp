#include "../util.h"

#include <catch.hpp>

TEST_CASE("str_basename works") {
    const std::string orig = "/root/.ssh/config";
    const std::string base = westtel::str_basename(orig);

    REQUIRE(orig == "/root/.ssh/config");
    REQUIRE(base == "config");
}

TEST_CASE("str_dirname works") {
    const std::string orig = "/root/.ssh/config";
    const std::string dir = westtel::str_dirname(orig);

    REQUIRE(orig == "/root/.ssh/config");
    REQUIRE(dir == "/root/.ssh");
}
