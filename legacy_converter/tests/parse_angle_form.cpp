#include <catch.hpp>

#include "../log_converter.h"

TEST_CASE("basic parse_angle_form", "[parse_angle_form]") {
    REQUIRE(parse_angle_form("<CR>") == "\r");
    REQUIRE(parse_angle_form("<LF>") == "\n");
    REQUIRE(parse_angle_form("<CR") == "<CR");
    REQUIRE(parse_angle_form("<STX>") == "\u0002");
    REQUIRE(parse_angle_form("<ETX>") == "\u0003");
    REQUIRE(parse_angle_form("<83>") == "\u0053");
    REQUIRE(parse_angle_form("<UNKNOWN>") == "<UNKNOWN>");
    REQUIRE(parse_angle_form("<22222222222222222222>") == "<22222222222222222222>");
    REQUIRE(parse_angle_form("<CR<CR>") == "<CR\r");
    REQUIRE(parse_angle_form("<CR<CR") == "<CR<CR");
    REQUIRE(parse_angle_form("<128>") == "\uE080");
    REQUIRE(parse_angle_form("<0>") == "\uE000");
    REQUIRE(parse_angle_form("<000>") == "\uE000");
    REQUIRE(parse_angle_form("<255>") == "\uE0FF");
    REQUIRE(parse_angle_form("hello<0>string") == "hello\uE000string");
    REQUIRE(parse_angle_form("") == "");
    REQUIRE(parse_angle_form("<255><255><255>") == "\uE0FF\uE0FF\uE0FF");
}
