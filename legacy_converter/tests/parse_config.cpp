#include "../parse_config.h"

#include <boost/optional/optional_io.hpp>
#include <sstream>

#include <catch.hpp>

using namespace std::string_literals;

static Json::Value parse(const std::string& in) {
    std::stringstream ss{in};
    Json::Value ret;
    ss >> ret;

    return ret;
}

TEST_CASE("parse_config empty") {
    auto config = parse_config::parse(Json::objectValue);

    REQUIRE(config.controller_dialplan.empty());
    REQUIRE(config.controller_positions.empty());
    REQUIRE(config.controller_trunks.empty());
}

TEST_CASE("parse_config positions only") {
    auto config = parse_config::parse(parse(R"({
        "controller": "WT-001-1",
        "psaps": {
            "WT-001-A": {
                "positions": [1,2,3]
            },
            "WT-001-B": {
                "positions": [4,5]
            }
        }
})"));

    REQUIRE(config.controller_dialplan.empty());
    REQUIRE(config.controller_trunks.empty());
    REQUIRE(config.controller_positions.size() == 1);
    REQUIRE(config.controller_positions.count("WT-001-1"));

    auto& c = config.controller_positions["WT-001-1"];
    REQUIRE(c.size() == 5);
    REQUIRE(c[1] == "WT-001-A");
    REQUIRE(c[2] == "WT-001-A");
    REQUIRE(c[3] == "WT-001-A");
    REQUIRE(c[4] == "WT-001-B");
    REQUIRE(c[5] == "WT-001-B");

    REQUIRE(config.position("WT-001-1", 1) == "WT-001-A"s);
    REQUIRE(config.position("WT-001-1", 2) == "WT-001-A"s);
    REQUIRE(config.position("WT-001-1", 3) == "WT-001-A"s);
    REQUIRE(config.position("WT-001-1", 4) == "WT-001-B"s);
    REQUIRE(config.position("WT-001-1", 5) == "WT-001-B"s);
}

TEST_CASE("parse_config basic trunks") {
    auto config = parse_config::parse(parse(R"({
        "controller": "WT-001-1",
        "psaps": {
            "WT-001-A": {
                "positions": [1, 2],
                "trunks": [0],
                "911_trunks": [1],
                "admin_trunks": [2, 3]
            }
        }
    })"));

    REQUIRE(config.controller_dialplan.empty());
    REQUIRE(config.controller_positions.size() == 1);
    REQUIRE(config.controller_trunks.size() == 1);

    REQUIRE(config.controller_positions.count("WT-001-1"));
    REQUIRE(config.controller_trunks.count("WT-001-1"));

    auto& t = config.controller_trunks["WT-001-1"];
    auto& p = config.controller_positions["WT-001-1"];

    REQUIRE(p.size() == 2);
    REQUIRE(t.size() == 4);

    REQUIRE(p[1] == "WT-001-A");
    REQUIRE(p[2] == "WT-001-A");

    REQUIRE(t[0].psap == "WT-001-A");
    REQUIRE(!t[0].call_type);

    REQUIRE(t[1].psap == "WT-001-A");
    REQUIRE(t[1].call_type == call_type::is_911);

    REQUIRE(t[2].psap == "WT-001-A");
    REQUIRE(t[2].call_type == call_type::admin);

    REQUIRE(t[3].psap == "WT-001-A");
    REQUIRE(t[3].call_type == call_type::admin);

    REQUIRE(config.position("WT-001-1", 1) == "WT-001-A"s);
    REQUIRE(config.position("WT-001-1", 2) == "WT-001-A"s);

    REQUIRE(config.trunk("WT-001-1", 0)->psap == "WT-001-A");
    REQUIRE(!config.trunk("WT-001-1", 0)->call_type);

    REQUIRE(config.trunk("WT-001-1", 1)->psap == "WT-001-A");
    REQUIRE(config.trunk("WT-001-1", 1)->call_type == call_type::is_911);

    REQUIRE(config.trunk("WT-001-1", 2)->psap == "WT-001-A");
    REQUIRE(config.trunk("WT-001-1", 2)->call_type == call_type::admin);

    REQUIRE(config.trunk("WT-001-1", 3)->psap == "WT-001-A");
    REQUIRE(config.trunk("WT-001-1", 3)->call_type == call_type::admin);
}

TEST_CASE("parse_config dialplan") {
    auto config = parse_config::parse(parse(R"({
        "controller": "WT-001-1",
        "psaps": {
            "WT-001-A": {
                "positions": [1, 2, 3]
            },
            "WT-001-B": {
                "positions": [4, 5]
            }
        },
        "dialplan": {
            "1001": {
                "positions": [1]
            },
            "1005": {
                "positions": [5]
            },
            "2001": {
                "psap": "WT-001-A"
            },
            "2002": {
                "psap": "WT-001-B"
            }
        }
    })"));

    REQUIRE(config.controller_dialplan.size() == 1);
    REQUIRE(config.controller_positions.size() == 1);
    REQUIRE(config.controller_trunks.empty());

    REQUIRE(config.controller_dialplan.count("WT-001-1"));
    auto& d = config.controller_dialplan["WT-001-1"];

    REQUIRE(d.size() == 4);
    REQUIRE(d.count("1001"));
    REQUIRE(d.count("1005"));
    REQUIRE(d.count("2001"));
    REQUIRE(d.count("2002"));

    REQUIRE(d["1001"].psap == "WT-001-A");
    REQUIRE(d["1001"].positions.size() == 1);
    REQUIRE(d["1001"].positions.count(1));

    REQUIRE(d["1005"].psap == "WT-001-B");
    REQUIRE(d["1005"].positions.size() == 1);
    REQUIRE(d["1005"].positions.count(5));

    REQUIRE(d["2001"].psap == "WT-001-A");
    REQUIRE(d["2001"].positions.size() == 3);
    REQUIRE(d["2001"].positions.count(1));
    REQUIRE(d["2001"].positions.count(2));
    REQUIRE(d["2001"].positions.count(3));

    REQUIRE(d["2002"].psap == "WT-001-B");
    REQUIRE(d["2002"].positions.size() == 2);
    REQUIRE(d["2002"].positions.count(4));
    REQUIRE(d["2002"].positions.count(5));
}
