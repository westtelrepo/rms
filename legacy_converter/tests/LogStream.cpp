#include <catch.hpp>

#include "../LogStream.h"

TEST_CASE("LogStream basic") {
    const char* file1 =
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:00:01.000 UTC | Tue 2018/01/01 01:00:01 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:00:02.000 UTC | Tue 2018/01/01 01:00:02 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
)";

    LogStream reader{std::unique_ptr<std::istream>{new std::istringstream{file1}}, "filename"};

    std::vector<OldLog> oldlog;

    while(auto log = reader.next())
        oldlog.push_back(*log);

    REQUIRE(oldlog.size() == 3);
    REQUIRE(oldlog.at(0).utc() == "2018-01-01T01:00:00.000Z");
    REQUIRE(oldlog.at(1).utc() == "2018-01-01T01:00:01.000Z");
    REQUIRE(oldlog.at(2).utc() == "2018-01-01T01:00:02.000Z");
}
