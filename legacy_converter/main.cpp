#include "LogStream.h"
#include "LogStreamUnifier.h"
#include "log_converter.h"
#include "parse_config.h"
#include <boost/optional.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <json/json.h>
#include <list>
#include <map>

static void process_call(std::vector<Json::Value>& logs, const parse_config& config) {
    if (logs.size() == 0) {
        return;
    }

    std::string callid = logs[0]["call"]["id"].asString();

    boost::optional<std::string> last_tid;
    int num = 0;

    // destination -> transfer id
    std::map<std::string, std::string> last_tid_by_dest;

    // assign Transfer IDs
    for (auto& log: logs) {
        if (log.isMember("transfer") && log["transfer"].isMember("type")) {
            std::string type = log["transfer"]["type"].asString();

            assert(log["transfer"].isMember("state"));
            std::string state = log["transfer"]["state"].asString();

            std::string dest = log["transfer"].isMember("destination") ? log["transfer"]["destination"].asString() : "";

            if (log["transfer"].isMember("id")) {
                // do nothing, the ID has already been assigned
            } else if (type == "initial") {
                // assign a transfer ID for the initial transfer
                log["transfer"]["id"] = callid + "+initial";
            } else if (state == "dial") {
                // every "dial" event is associated with a transfer
                std::string id = callid + "+" + type + "+" + std::to_string(num++);
                log["transfer"]["id"] = id;
                last_tid_by_dest[dest] = id;
            } else if (last_tid) {
                if (type == "*") {
                    log["transfer"]["id"] = *last_tid;
                } else if (last_tid_by_dest.count(dest)) {
                    log["transfer"]["id"] = last_tid_by_dest[dest];
                } else {
                    // TODO: really dumb, not the proper logic
                    log["transfer"]["id"] = *last_tid;
                }
            }

            if (log["transfer"].isMember("id")) {
                last_tid = log["transfer"]["id"].asString();
            }
        }
    }

    // _wrong_if_same_position handling
    // xferid -> pos (of dialer)
    std::map<std::string, Json::Int> dial_pos_by_id;
    for (auto& log: logs) {
        if (log.isMember("transfer") && log["transfer"].isMember("id") && log["transfer"].isMember("state")
            && log.isMember("user") && log["user"].isMember("position")) {
            std::string id = log["transfer"]["id"].asString();
            Json::Int pos = log["user"]["position"].asInt();
            std::string state = log["transfer"]["state"].asString();

            if (state == "dial") {
                dial_pos_by_id[id] = pos;
            } else if (state == "join" && log["user"].isMember("_wrong_if_same_position")) {
                if (dial_pos_by_id[id] == pos) {
                    log.removeMember("user");
                } else {
                    log["user"].removeMember("_wrong_if_same_position");
                }
            }
        }
    }

    // _may_be_wrong_outbound handling
    for (auto& log: logs) {
        if (log.isMember("user") && log["user"].isMember("_may_be_wrong_outbound")
            && log.isMember("transfer") && log["transfer"].isMember("id")) {
            boost::optional<Json::Value&> dial;
            for (auto& d: logs) {
                if (d.isMember("transfer") && d["transfer"].isMember("id")
                    && d["transfer"].isMember("state") && d["transfer"]["state"] == "dial"
                    && d["transfer"]["id"] == log["transfer"]["id"]) {
                    dial = d;
                }
            }

            if (dial && dial->isMember("user") && (*dial)["user"].isMember("psap")) {
                log.removeMember("user");
            } else {
                log["user"].removeMember("_may_be_wrong_outbound");
            }
        }
    }

    // enterprise fixup
    for (auto& log: logs) {
        if (log.isMember("transfer")
            && log["transfer"].isMember("destination")
            && log["transfer"].isMember("state")
            && (log["transfer"]["state"] == "join" || log["transfer"]["state"] == "abandon")
            && !log.isMember("user")) {
            auto dialplan = config.dialplan(log["controller"].asString(), log["transfer"]["destination"].asString());
            if (dialplan) {
                log["user"]["psap"] = dialplan->psap;
            }
        }
    }
}

int main(int argc, char** argv) {
    try {
        boost::program_options::options_description desc{"Options"};
        desc.add_options()
            ("json-config-on-stdin", "Follow the JSON config on stdin")
            ("file", boost::program_options::value<std::vector<std::string>>());

        boost::program_options::positional_options_description pos;
        pos.add("file", -1);

        boost::program_options::variables_map vm;
        boost::program_options::store(
            boost::program_options::command_line_parser(argc, argv).options(desc).positional(pos).run(),
            vm
        );
        boost::program_options::notify(vm);

        if (vm.count("json-config-on-stdin")) {
            Json::Value j;
            std::cin >> j;

            std::cerr << j << std::endl;

            parse_config config = parse_config::parse(j);

            auto files = vm["file"].as<std::vector<std::string>>();

            std::map<std::string, LogStream> streams;

            for (auto& file: files) {
                LogStream reader{std::unique_ptr<std::ifstream>{
                    new std::ifstream{file}},
                    file
                };
                reader.set_preserve_newlines();
                streams.emplace(file, std::move(reader));
            }

            LogStreamUnifier unif{std::move(streams)};

            log_converter convert;
            convert.throw_exceptions_less();
            convert.set_parse_config(config);

            // generate order ID
            Json::UInt64 order = 0;

            Json::FastWriter writer;

            std::list<std::map<std::string, std::vector<Json::Value>>> log_list;
            log_list.push_back(std::map<std::string, std::vector<Json::Value>>{});

            auto process_list = [&writer, &config](std::map<std::string, std::vector<Json::Value>>& calls) {
                for (auto& pair: calls) {
                    process_call(pair.second, config);
                    for (auto& l: pair.second) {
                        l.removeMember("user_positions");
                        std::cout << writer.write(l);
                    }
                }
            };

            while(true) {
                auto logs = unif.process();
                if (logs.size() == 0)
                    break;

                for (auto& log: logs) {
                    auto out = convert.convert(log);
                    out["o"]["c"] = order++;
                    out["id"] = std::to_string(order);

                    // remove p from pani
                    if (out.isMember("call") && out["call"].isMember("pani")) {
                        auto pani = out["call"]["pani"].asString();
                        if (pani[0] == 'p') {
                            pani = pani.substr(1);
                        }
                        out["call"]["pani"] = pani;
                    }

                    if (out.isMember("call") && out["call"].isMember("id")) {
                        std::string callid = out["call"]["id"].asString();
                        for (auto list = log_list.begin(); list != log_list.end(); ++list) {
                            if (list->count(callid) || list == --log_list.end()) {
                                (*list)[callid].push_back(out);
                                break;
                            }
                        }
                    } else {
                        out.removeMember("user_positions");
                        std::cout << writer.write(out);
                    }
                }

                if (log_list.back().size() > 1000) {
                    log_list.push_back(std::map<std::string, std::vector<Json::Value>>{});
                }

                if (log_list.size() > 3) {
                    auto list = log_list.front();
                    log_list.pop_front();
                    process_list(list);
                }
            }

            for (auto& list: log_list) {
                process_list(list);
            }

            std::cout << "{\"__success\":true}" << std::endl;

            return EXIT_SUCCESS;
        } else {
            std::cerr << desc << std::endl;
            return EXIT_FAILURE;
        }
    } catch(const std::exception& e) {
        std::cerr << e.what() << std::endl;
        throw e;
    }
}
