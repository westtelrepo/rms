#include "LogStream.h"

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <regex>
#include <set>


struct LogStream::impl {
	std::unique_ptr<std::istream> in_;
	const std::string filename_;

	bool peeking_{false};
	boost::optional<OldLog> the_peek_{};

	int64_t line_counter_{0};
	int64_t entry_counter_{0};

	const std::set<int32_t> newline_codes{361};

	boost::optional<OldLog> next_{};

	// internal line-stream for peeking
	// TODO: refactor out
	bool line_peeking_{false};
	boost::optional<std::string> line_peek_{};

	bool preserve_newlines_ = false;

	impl(std::unique_ptr<std::istream> in, std::string filename)
	: in_{std::move(in)}, filename_{std::move(filename)} {
	}
};

LogStream::LogStream(std::unique_ptr<std::istream> in, std::string filename)
: impl_{new impl{std::move(in), std::move(filename)}, [] (impl* i) { delete i; }} {
}

boost::optional<OldLog> LogStream::peek() {
	if (impl_->peeking_) return impl_->the_peek_;
	else {
		impl_->the_peek_ = next();
		impl_->peeking_ = true;
		return impl_->the_peek_;
	}
}

boost::optional<OldLog> LogStream::next() {
	try {
		if (impl_->peeking_) {
			impl_->peeking_ = false;
			return impl_->the_peek_;
		}
		if ( !(*impl_->in_) )
			return boost::none;

		boost::optional<std::string> line_raw = read_line();

		if (!line_raw)
			return boost::none;
		assert(line_raw);

		OldLog ret;
		const Line first{*line_raw};

		auto follows = [](const Line& f, const Line& s) {
			return f.psap == s.psap && f.utc == s.utc && f.code == s.code
				&& s.callid == "" && s.trunk == "" && s.position == ""
				&& s.ani == "" && s.pani == "" && s.thread == ""
				&& s.direction == "";
		};

		ret.psap = first.psap;
		ret.set_utc(first.utc);
		ret.code = first.code;
		ret.callid = first.callid == ""
			? boost::optional<std::string>{}
			: first.callid;

		if (first.trunk == "")
			ret.trunk = boost::none;
		else if (first.trunk[0] == 'T')
			ret.trunk = westtel::string_to_int<int32_t>(first.trunk.substr(1));
		else
			throw std::runtime_error{"invalid trunk format: " + first.trunk};

		if (first.position == "")
			ret.position = boost::none;
		else if (first.position[0] == 'P')
			ret.position = westtel::string_to_int<int32_t>(first.position.substr(1));
		else
			throw std::runtime_error{"invalid position format: " + first.position};

		ret.ani = westtel::emptystr_null(first.ani);
		ret.pani = westtel::emptystr_null(first.pani);
		ret.thread = westtel::emptystr_null(first.thread);
		ret.direction = westtel::emptystr_null(first.direction);
		ret.message = first.message;

		ret.filename = impl_->filename_;
		ret.entry = impl_->entry_counter_++;
		ret.linenum = impl_->line_counter_;

		while(true) {
			auto line = peek_line();
			if (!line) break;
			if (std::count(line->begin(), line->end(), '|') == 0) {
				if (impl_->preserve_newlines_)
					ret.message += "\n";
				ret.message += *line;
				read_line();
				continue;
			}
			Line n{*line};
			if (!follows(first, n)) break;
			if (impl_->preserve_newlines_ || impl_->newline_codes.count(n.code))
				ret.message += "\n";
			ret.message += n.message;
			read_line();
		}

		return ret;
	} catch(const std::exception&) {
		// TODO: print warnings with flag
		//std::cout << "Invalid line: " << e.what() << std::endl;
		return next();
	}
}

boost::optional<std::string> LogStream::peek_line() {
	if (impl_->line_peeking_) {
		return impl_->line_peek_;
	}
	else {
		impl_->line_peek_ = read_line();
		impl_->line_peeking_ = true;
		return impl_->line_peek_;
	}
}

boost::optional<std::string> LogStream::read_line() {
	if (impl_->line_peeking_) {
		impl_->line_peeking_ = false;
		return impl_->line_peek_;
	} else if ( !(*impl_->in_) ) {
		return boost::optional<std::string>{};
	}

	std::string ret;
	std::getline(*impl_->in_, ret);
	impl_->line_counter_++;
	if (ret[ret.size() - 1] == '\r')
		ret.resize(ret.size() - 1);

	if (ret == "" && !(*impl_->in_) ) {
		return boost::none;
	}

	for (auto ch: ret)
		if (static_cast<unsigned char>(ch) > 127 || ch == 0)
			throw std::runtime_error{
			      "Invalid non-ascii character in file "
			      + impl_->filename_};
	return ret;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"

static const std::regex whole_line{
	R"((\w{2}-\d{3}-1) \| )" // PSAP name
	R"((\d{4}[-/]\d{2}[-/]\d{2} \d{2}:\d{2}:\d{2}.\d{3})(?: UTC)? \| )" // UTC
	R"(\w* \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2} \w* \| )" // local time (unused)
	R"((\d+) \| )" // code
	R"(( *| *\d+ *) \| )" // callid
	R"((T\d*| *) \| )" // trunk number
	R"((P\d*| *) \| )" // position number
	R"(( *| *[^|]+ *) \| )" // ani
	R"(( *| *p\d+ *) \| )" // pani
	R"(([^|]*) \| )" // thread
	R"(([^|]*) \| ?)" // direction
	R"((.*))" // comment
};

#pragma clang diagnostic pop

LogStream::Line::Line(const std::string& line) {
	try {
		// by avoiding the regex in the common case, we make
		// this whole importer about 50% faster

		// a log line must be at least this long to be valid
		if (line.size() < 150) {
			throw std::runtime_error{"line too short"};
		}

		// these characters are always the same
		if (line[8] != ' ' || line[9] != '|' || line[10] != ' ' // after controller
			|| line[34] != ' ' || line[35] != 'U' || line[36] != 'T' || line[37] != 'C' || line[38] != ' ' || line[39] != '|' || line[40] != ' ' // after utc
			|| line[68] != ' ' || line[69] != '|' || line[70] != ' ' // after localtime
			|| line[74] != ' ' || line[75] != '|' || line[76] != ' ' // after code
			|| line[95] != ' ' || line[96] != '|' || line[97] != ' ' // after callid
			|| line[101] != ' ' || line[102] != '|' || line[103] != ' ' // after trunk
			|| line[107] != ' ' || line[108] != '|' || line[109] != ' ' // after position
			|| line[120] != ' ' || line[121] != '|' || line[122] != ' ' // after ani
			|| line[134] != ' ' || line[135] != '|' || line[136] != ' ' // after pani
			|| line[144] != ' ' || line[145] != '|' || line[146] != ' ' // after thread
			|| line[148] != ' ' || line[149] != '|' // after direction
			) {
			throw std::runtime_error{"wrong characters"};
		}

		psap = line.substr(0, 8);
		if (psap[2] != '-' || psap[6] != '-' || psap[7] != '1') {
			throw std::runtime_error{"bad psap name"};
		}

		// Expected format: 2016/01/01 03:42:24.123
		utc = line.substr(11, 23);
		if (!isdigit(utc[0]) || !isdigit(utc[1]) || !isdigit(utc[2]) || !isdigit(utc[3])
			|| utc[4] != '/' || !isdigit(utc[5]) || !isdigit(utc[6])
			|| utc[7] != '/' || !isdigit(utc[8]) || !isdigit(utc[9])
			|| utc[10] != ' ' || !isdigit(utc[11]) || !isdigit(utc[12])
			|| utc[13] != ':' || !isdigit(utc[14]) || !isdigit(utc[15])
			|| utc[16] != ':' || !isdigit(utc[17]) || !isdigit(utc[18])
			|| utc[19] != '.' || !isdigit(utc[20]) || !isdigit(utc[21]) || !isdigit(utc[22])
			) {
			throw std::runtime_error{"bad utc"};
		}

		code = westtel::string_to_int<int32_t>(line.substr(71, 3));

		callid = line.substr(77, 18);
		if (callid == "                  ")
			callid = "";

		trunk = line.substr(98, 3);
		if (trunk == "   ") {
			trunk = "";
		}

		position = line.substr(104, 3);
		if (position == "   ") {
			position = "";
		}

		ani = boost::trim_copy(line.substr(110, 10));

		pani = line.substr(123, 11);
		if (pani == "           ") {
			pani = "";
		}
		if (pani.size() && pani[0] != 'p') {
			throw std::runtime_error{"bad pani"};
		}

		thread = boost::trim_copy(line.substr(137, 7));

		direction = line.substr(147, 1);
		if (direction == " ") {
			direction = "";
		}

		if (line.size() >= 151) {
			if (line[150] != ' ')
				throw std::runtime_error{"bad message"};
			message = line.substr(151);
		}
	} catch(const std::exception&) {
		// if the above doesn't work, use the regex fallback (which is more tolerant)
		std::smatch m;
		if (std::regex_match(line, m, whole_line)) {
			assert(m.size() == 12);
		} else {
			throw std::runtime_error{"Invalid line: " + line};
		}
		psap = boost::trim_copy(m[1].str());
		utc = boost::trim_copy(m[2].str());

		code = westtel::string_to_int<int32_t>(boost::trim_copy(m[3].str()));
		callid = boost::trim_copy(m[4].str());
		trunk = boost::trim_copy(m[5].str());
		position = boost::trim_copy(m[6].str());
		ani = boost::trim_copy(m[7].str());
		pani = boost::trim_copy(m[8].str());
		thread = boost::trim_copy(m[9].str());
		direction = boost::trim_copy(m[10].str());
		message = m[11].str();
	}
}

void LogStream::set_preserve_newlines() { impl_->preserve_newlines_ = true; }
