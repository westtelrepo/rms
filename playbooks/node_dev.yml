- hosts: all
  become: yes
  tasks:
    - name: install packages
      apt:
        name:
          - apt-transport-https
          - curl
          - moreutils
    - name: add NodeSource apt key
      apt_key:
        url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
        id: 9FD3B784BC1C6FC31A8A0A1C1655A0AB68576280
        state: present
    - name: add NodeSource repository
      apt_repository:
        state: present
        repo: 'deb https://deb.nodesource.com/node_14.x stretch main'
    - name: add Yarn apt key
      apt_key:
        url: https://dl.yarnpkg.com/debian/pubkey.gpg
        id: 72ECF46A56B4AD39C907BBB71646B01B86E50310
        state: present
    - name: add Yarn repository
      apt_repository:
        state: present
        repo: 'deb https://dl.yarnpkg.com/debian/ stable main'
    - name: install yarn
      apt: name=yarn state=present
    - name: add PostgreSQL apt key
      apt_key:
        url: https://www.postgresql.org/media/keys/ACCC4CF8.asc
        id: ACCC4CF8
        state: present
    - name: add PostgreSQL repository
      apt_repository:
        repo: deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main
        state: present
    - name: install PostgreSQL 12
      apt: state=present name=postgresql-12
    - name: install python-psycopg2 to allow database setup
      apt: state=present name=python-psycopg2
    - name: install acl to allow become as unprivileged user
      apt: state=present name=acl
    - name: allow connections to db
      copy:
        dest: /etc/postgresql/12/main/pg_hba.conf
        content: |
          local all postgres peer
          local all all peer
          host all all 127.0.0.1/32 md5
          host all all ::1/128 md5
          host all all 0.0.0.0/0 md5
      notify: restart postgres
    - name: allow postgres to listen to all
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?listen_addresses\s*='
        line: "listen_addresses = '*'"
      notify: restart postgres
    - name: increase shared_buffers
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?shared_buffers\s*='
        line: "shared_buffers = 2048MB"
      notify: restart postgres
    - name: increase work_mem
      lineinfile:
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?work_mem\s*='
        line: "work_mem = 32MB"
      notify: reload postgres
    - name: shared_preload_libraries
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?shared_preload_libraries\s*='
        line: "shared_preload_libraries = 'auto_explain'"
      notify: restart postgres
    - name: auto_explain minimum duration
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?auto_explain.log_min_duration\s*='
        line: "auto_explain.log_min_duration = '-1'"
      notify: reload postgres
    - name: auto_explain analyze
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?auto_explain.log_analyze\s*='
        line: "auto_explain.log_analyze = on"
      notify: reload postgres
    - name: auto_explain buffers
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?auto_explain.log_buffers\s*='
        line: "auto_explain.log_buffers = on"
      notify: reload postgres
    - name: auto_explain verbose
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?auto_explain.log_verbose\s*='
        line: "auto_explain.log_verbose = on"
      notify: reload postgres
    - name: auto_explain format
      lineinfile:
        state: present
        dest: /etc/postgresql/12/main/postgresql.conf
        regexp: '^\s*#?auto_explain.log_format\s*='
        line: "auto_explain.log_format = text"
      notify: reload postgres
    - name: create RMS user
      become_user: postgres
      postgresql_user:
        state: present
        name: rms
        password: rms_test
        role_attr_flags: SUPERUSER,CREATEROLE,CREATEDB
    - name: create MIS database (no schema)
      become_user: postgres
      postgresql_db:
        state: present
        name: rms
        owner: rms
    - name: set PG variables for vagrant user
      lineinfile:
        dest: /home/vagrant/.bashrc
        line: 'export PGUSER=rms PGDATABASE=rms PGPASSWORD=rms_test PGHOST=localhost'
        insertafter: 'EOF'
        state: present
  handlers:
    - name: restart postgres
      systemd:
        name: postgresql@12-main
        state: restarted
    - name: reload postgres
      systemd:
        name: postgresql@12-main
        state: reloaded
